/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */
import React from 'react';
import { AppRegistry } from 'react-native';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import App from './App';
import { store, persistor } from './src/redux/configureStore';
import { name as appName } from './app.json';

const AppCont = () => (
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <App />
    </PersistGate>
  </Provider>
);

// AppRegistry.registerComponent(appName, () => AppCont)
AppRegistry.registerComponent(appName, () => AppCont);
