import HelperFetch from './HelperFetch';

/**
 * Resend the otp to requested Phone Number
 * @return {success}
 */

const resendOtp = async mobileNumber => {
  const extension = '/otpVerification/resendOtp';
  await HelperFetch(
    {
      extension,
      post: true,
      body: {
        phoneNumber: mobileNumber,
      },
    },
    {
      handleError: () => {},
      handleSuccess: () => {},
    }
  );
};

export default resendOtp;
