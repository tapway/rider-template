import { store } from '../redux/configureStore';
import HelperFetch from './HelperFetch';

/**
 * Verifies the Delivery User using otp
 * @return {success,data}
 */

const onVerificationCode = async (mobileNumber, verificationCode, handleLogin) => {
  const { deviceInfo } = store.getState();
  const extension = '/auth/otpVerify';
  await HelperFetch(
    {
      extension,
      post: true,
      body: { phoneNumber: mobileNumber, otp: verificationCode, deviceInfo },
    },
    {
      handleError: () => {},
      handleSuccess: res => handleLogin(res),
    }
  );
};

export default onVerificationCode;
