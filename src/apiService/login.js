import HelperFetch from './HelperFetch';

/**
 * Request DeliveryUserAuth For Verification
 * @return {success}
 */

const login = async (mob, handleLogin) => {
  const extension = '/auth/login';
  await HelperFetch(
    {
      extension,
      post: true,
      body: { phoneNumber: mob },
    },
    {
      handleError: () => {},
      handleSuccess: result => {
        handleLogin(result);
      },
    },
  );
};

export default login;
