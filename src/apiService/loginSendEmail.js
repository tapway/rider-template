import HelperFetch from './HelperFetch';

/**
 * Request DeliveryUserAuth For Verification
 * @return {success}
 */

const loginSendEmail = async (mob, email, handleLogin) => {
    const extension = '/auth/loginSendEmail';
    await HelperFetch(
        {
            extension,
            post: true,
            body: { phoneNumber: mob, email: email },
        },
        {
            handleError: () => {console.log('loginsendemail error occured') },
            handleSuccess: result => {
                handleLogin(result);
            },
        },
    );
};

export default loginSendEmail;
