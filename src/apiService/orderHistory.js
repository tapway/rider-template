import HelperFetch from './HelperFetch';

/**
 * Fetch Order History
 * @return {success,data}
 */

const fetchOrderHistory = async (
  jwtAccessToken,
  id,
  limit = 10,
  skip,
  handleResult,
  handleLoading
) => {
  const extension = '/orders/fetchLimitedOrders';
  await HelperFetch(
    {
      extension,
      post: true,
      Authorization: jwtAccessToken,
      body: {
        deliveryId: id,
        limit,
        skip,
      },
    },
    {
      handleError: () => {},
      handleSuccess: res => {
        handleResult(res.data);
        handleLoading();
      },
    }
  );
};

export default fetchOrderHistory;
