import HelperFetch from './HelperFetch';

/**
 * Set Availablity Of Delivery User
 * @returns {Response:true}
 */

const setTripStatus = async (value, jwtAccessToken) => {
  const extension = '/deliveryUser/setTripStatus';
  await HelperFetch(
    {
      extension,
      put: true,
      Authorization: jwtAccessToken,
      body: { set: value },
    },
    {
      handleError: () => {},
      handleSuccess: () => {},
    }
  );
};

export default setTripStatus;
