import Toast from 'react-native-easy-toast';
import HelperFetch from './HelperFetch';

/**
 * Send data for Image Upload
 * @return {success,URL}
 */

const imageUpload = async (imageData, type, handleFunc) => {
  const extension = '/deliveryUser/updateImage';
  await HelperFetch(
    {
      extension,
      put: true,
      body: imageData,
      updateType: type,
    },
    {
      handleError: err => {
        Toast.show({
          text: `Error while updating profile ${err}`,
          position: 'bottom',
          duration: 1500,
        });
      },
      handleSuccess: result => {
        handleFunc(result);
      },
    }
  );
};

export default imageUpload;
