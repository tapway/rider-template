import HelperFetch from './HelperFetch';

/**
 * Request DeliveryUserAuth For Logout
 * @return {success,data}
 */

const logout = async (jwtAccessToken, handleLogout) => {
  const extension = '/auth/logout';
  await HelperFetch(
    {
      extension,
      get: true,
      Authorization: jwtAccessToken,
    },
    {
      handleError: () => {},
      handleSuccess: res => handleLogout(res),
    }
  );
};

export default logout;
