import HelperFetch from './HelperFetch';

/**
 * Send data for registration
 * @return {success,savedUser}
 */

const registerUser = async (payload, handleSuc, handleErr) => {
  const extension = '/deliveryUser/create';
  await HelperFetch(
    {
      extension,
      post: true,
      body: {
        userobj: payload,
      },
    },
    {
      handleError: res => {
        console.log('error occured')
        handleErr(res);
      },
      handleSuccess: res => {
        console.log('success create')

        handleSuc(res);
      },
    },
  );
};
export default registerUser;
