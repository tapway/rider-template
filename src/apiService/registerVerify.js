import HelperFetch from './HelperFetch';

/**
 * Send data for registration
 * @return {success,savedUser}
 */

const registerVerify = async (mobileNumber, obj, handleReg) => {
  const extension = '/otpVerification/register';
  await HelperFetch(
    {
      extension,
      post: true,
      body: {
        phoneNumber: mobileNumber,
        obj
      },
    },
    {
      handleError: () => { },
      handleSuccess: res => {
        handleReg(res);
      },
    }
  );
};
export default registerVerify;
