import { Alert } from 'react-native';
import HelperFetch from './HelperFetch';

/**
 * Get App Config
 * @returns {Response:true}
 */

const appConfig = async handleFunction => {
  const extension = '/appConfig/';
  await HelperFetch(
    {
      extension,
      get: true,
    },
    {
      handleError: () => {
        Alert.alert(
          'Server Error',
          'Either the server provided is wrong or server is not running',
          [
            {
              text: 'OK',
              onPress: () => {},
            },
          ]
        );
      },
      handleSuccess: result => {
        if (result.success) {
          handleFunction(result);
        } else {
          Alert.alert('Server Error', 'Unable to fetch app config', [
            {
              text: 'OK',
              onPress: () => {},
            },
          ]);
        }
      },
    }
  );
};

export default appConfig;
