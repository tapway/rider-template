import HelperFetch from './HelperFetch';

/**
 * Updates the requested field of Delivery User
 * @return {success,savedUser}
 */

const update = async (jwtAccessToken, user, handleUpdate, handleNavigate) => {
  const extension = '/deliveryUser/update';
  await HelperFetch(
    {
      extension,
      put: true,
      Authorization: jwtAccessToken,
      body: {
        userobj: user,
      },
    },
    {
      handleError: () => {},
      handleSuccess: res => {
        handleUpdate(res.data);
        handleNavigate();
      },
    }
  );
};
export default update;
