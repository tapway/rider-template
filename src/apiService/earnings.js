import HelperFetch from './HelperFetch';

/**
 * Fetch Earnings
 * @return {success,data}
 */

const fetchEarnings = async (jwtAccessToken, handleSet) => {
  const extension = '/earnings/fetchEarnings';
  await HelperFetch(
    {
      extension,
      get: true,
      Authorization: jwtAccessToken,
    },
    {
      handleError: () => {},
      handleSuccess: result => {
        if (result.success) {
          handleSet(result.data);
        } else {
          handleSet({
            todayEarn: '-',
            yesterdayEarn: '-',
            weekEarn: '-',
            monthEarn: '-',
            walletBalance: '-',
          });
        }
      },
    }
  );
};

export default fetchEarnings;
