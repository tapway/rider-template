import HelperFetch from './HelperFetch';

/**
 * Check User Existance In Database
 * @returns {Response:true/false}
 */

const checkUserExistance = async (phoneNumber, handleCheckUserExistance, incomingThis) => {
  const extension = '/auth/checkUserExistance';
  await HelperFetch(
    {
      extension,
      post: true,
      body: { phoneNumber },
    },
    {
      handleError: () => {},
      handleSuccess: result => {
        handleCheckUserExistance(incomingThis, result);
      },
    }
  );
};

export default checkUserExistance;
