import HelperFetch from './HelperFetch';

/**
 * Send data for registration
 * @return {success,message}
 */

const verifyRide = async (otp, orderId, handleVerifyRide) => {
  const extension = '/orders/verifyRide';
  await HelperFetch(
    {
      extension,
      post: true,
      body: { otp, orderId },
    },
    {
      handleError: () => {},
      handleSuccess: res => {
        handleVerifyRide(res);
      },
    }
  );
};
export default verifyRide;
