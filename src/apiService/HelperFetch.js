import SERVER from '../config/config';

const HelperFetch = async (properties, { handleSuccess, handleError }) => {
  const method = properties.get ? 'GET' : properties.post ? 'POST' : properties.put ? 'PUT' : 'GET';
  let headers = properties.Authorization
    ? {
        Accept: 'application/json',
        Authorization: properties.Authorization,
      }
    : { Accept: 'application/json' };
  headers = properties.updateType
    ? {
        ...headers,
        updateType: properties.updateType,
      }
    : {
        ...headers,
        'Content-Type': 'application/json',
      };
  const parameters = properties.body
    ? {
        method,
        headers,
        body:
          // eslint-disable-next-line no-undef
          properties.body instanceof FormData ? properties.body : JSON.stringify(properties.body),
      }
    : { method, headers };

  try {
    // eslint-disable-next-line no-undef
    const res = await fetch(`${SERVER}/api${properties.extension}`, parameters);
    const myJson = await res.json();
    handleSuccess(myJson);
  } catch (err) {
    handleError(err);
  }
};
export default HelperFetch;
