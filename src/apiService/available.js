import HelperFetch from './HelperFetch';

/**
 * Set Availablity Of Delivery User
 * @returns {Response:true}
 */

const setAvailable = async (value, jwtAccessToken) => {
  const extension = '/deliveryUser/setAvailable';
  await HelperFetch(
    {
      extension,
      put: true,
      Authorization: jwtAccessToken,
      body: { set: value },
    },
    {
      handleError: () => {},
      handleSuccess: () => {},
    },
  );
};

export default setAvailable;
