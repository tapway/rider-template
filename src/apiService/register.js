import { store } from '../redux/configureStore';
import HelperFetch from './HelperFetch';

/**
 * Send data for registration
 * @return {success,savedUser}
 */

const register = async (user, verificationCode, mobileNumber, handleReg) => {
  const { deviceInfo } = store.getState();
  const extension = '/otpVerification/otpVerifyDeliveryUser';
  await HelperFetch(
    {
      extension,
      post: true,
      body: {
        userobj: user,
        phoneNumber: mobileNumber,
        otp: verificationCode,
        deviceInfo,
      },
    },
    {
      handleError: () => {},
      handleSuccess: res => {
        handleReg(res);
      },
    },
  );
};
export default register;
