/* eslint-disable no-use-before-define */
import React from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  SafeAreaView,
  StyleSheet,
  Platform,
} from 'react-native';
import IconI from 'react-native-vector-icons/Ionicons';
import PropTypes from 'prop-types';
import getImage from '../utils/getImage';
import { heightRatio, widthRatio } from '../utils/stylesheet';
import colors from '../utils/colors';

const Header = props => {
  const { onlyBack, back, title, twoButtons, next,customstyle } = props;
  return (
    <View style={[styles.container,customstyle?customstyle:null ]}>
      {onlyBack ? (
        <SafeAreaView style={styles.safeAreaView}>
          <View style={styles.safeAreaView}>
            <TouchableOpacity onPress={back}>
              <View style={styles.imageView}>
                <IconI name="md-arrow-back" size={28} color='#030E1A' />
              </View>
            </TouchableOpacity>
          </View>
        </SafeAreaView>
      ) : twoButtons ? (
        <SafeAreaView style={styles.safeAreaView}>
          <View style={styles.twoButtonsView}>
            <TouchableOpacity onPress={back}>
              <View style={styles.imageView}>
                <IconI name="md-arrow-back" size={28} />
              </View>
            </TouchableOpacity>
            <View style={styles.textView}>
              <Text style={styles.headingText}>{title}</Text>
            </View>
            <View style={styles.secondImage}>
              <TouchableOpacity onPress={next}>
                <Image style={styles.image} source={getImage.edit} />
              </TouchableOpacity>
            </View>
          </View>
        </SafeAreaView>
      ) : (
        <SafeAreaView style={styles.safeAreaView}>
          {title !== 'home' && (
            <React.Fragment>
              <TouchableOpacity onPress={back}>
                <View style={styles.imageView}>
                  <IconI name="md-arrow-back" size={28} />
                </View>
              </TouchableOpacity>
              <View style={styles.textView}>
                <Text style={styles.headingText}>{title}</Text>
              </View>
            </React.Fragment>
          )}
        </SafeAreaView>
      )}
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  container: {
    height: Platform.OS === 'ios' ? heightRatio * 80 : heightRatio * 50,
    backgroundColor: colors.themeBackgroundColor,
    borderColor: colors.secondaryBorderColor,
    borderBottomWidth: 0.5,
  },
  safeAreaView: {
    flex: 1,
    backgroundColor: colors.themeBackgroundColor,
    flexDirection: 'row',
  },
  imageView: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'center',
    paddingLeft: widthRatio * 18,
  },
  secondImage: {
    alignItems: 'flex-start',
    justifyContent: 'center',
    paddingRight: widthRatio * 18,
  },
  textView: {
    justifyContent: 'center',
    width: widthRatio * 311,
  },
  headingText: {
    fontSize: heightRatio * 18,
    textAlign: 'center',
  },
  twoButtonsView: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  image: { height: heightRatio * 12, width: widthRatio * 14, overflow: 'visible' },
});

Header.propTypes = {
  back: PropTypes.func,
  next: PropTypes.func,
  onlyBack: PropTypes.bool,
  twoButtons: PropTypes.bool,
  title: PropTypes.string,
};
Header.defaultProps = {
  title: null,
  twoButtons: false,
  onlyBack: false,
  next: null,
  back: null,
};
