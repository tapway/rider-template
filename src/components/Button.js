/* eslint-disable no-use-before-define */
import React from 'react';
import { Text, TouchableOpacity, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { heightRatio } from '../utils/stylesheet';
import colors from '../utils/colors';

const Button = props => {
  const { submit, title } = props;
  return (
    <TouchableOpacity style={styles.loginButton} onPress={submit}>
      <Text style={styles.loginText}>{title}</Text>
    </TouchableOpacity>
  );
};

export default Button;

const styles = StyleSheet.create({
  loginButton: {
    width: '100%',
    height: 44 * heightRatio,
    alignItems: 'center',
    padding: 12,
    borderRadius: 50,
    backgroundColor: colors.primaryBtnBgColor,
    justifyContent: 'center',
    borderColor: colors.transparent,
  },
  loginText: {
    color: colors.textForPrimaryBgColor,
    fontSize: heightRatio * 16,
  },
});

Button.propTypes = {
  submit: PropTypes.func,
  title: PropTypes.string,
};
Button.defaultProps = {
  title: null,
  submit: null,
};
