/* eslint-disable no-use-before-define */
import React, { PureComponent } from 'react';
import { TextInput, Text, StyleSheet, Dimensions } from 'react-native';
import PropTypes from 'prop-types';
import colors from '../utils/colors';
import { heightRatio } from '../utils/stylesheet';

const { height } = Dimensions.get('window');

export default class MyTextInput extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { props } = this;
    return (
      <React.Fragment>
        <Text style={[styles.headingText2, styles.margin]}>{props.heading}</Text>
        <TextInput
          secureTextEntry={props.isSecure}
          selectionColor={colors.primaryColor}
          placeholder={props.placeholder}
          value={props.value}
          style={[
            styles.textInputBox,
            props.focusInput === 0
              ? styles.nofocus
              : props.focusInput === 1
              ? styles.focus
              : styles.redFocus,
          ]}
          onFocus={() => props.handlerFocus(props.focus, 1)}
          onBlur={() => props.handlerFocus(props.focus, 0)}
          onChangeText={text => props.handlerInput(props.field, text)}
        />
      </React.Fragment>
    );
  }
}

const styles = StyleSheet.create({
  headingText2: {
    fontSize: 14 * heightRatio,
    paddingTop: height / 80,
    fontWeight: '200',
  },
  margin: {
    marginTop: 10 * heightRatio,
  },
  textInputBox: {
    borderStyle: 'solid',
    borderBottomWidth: 1,
    padding: 4 * heightRatio,
    marginTop: 6 * heightRatio,
    paddingLeft: 0,
    fontSize: 12 * heightRatio,
    color: colors.inputTextColor,
  },
  focus: {
    borderColor: colors.primaryColor,
  },
  nofocus: {
    borderColor: colors.noFoucsBorderColor,
  },
  redFocus: {
    borderColor: 'red',
  },
});

MyTextInput.propTypes = {
  heading: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  isSecure: PropTypes.bool,
  handlerFocus: PropTypes.func.isRequired,
  handlerInput: PropTypes.func.isRequired,
  focus: PropTypes.string.isRequired,
  field: PropTypes.string.isRequired,
  focusInput: PropTypes.number.isRequired,
};
MyTextInput.defaultProps = {
  isSecure: false,
};
