/* eslint-disable no-useless-escape */
export default class formValidation {
  static emailVaildate(email) {
    const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(email) === true) {
      return true;
    }
    return false;
  }

  static passwordValidate(password) {
    if (password.length < 8) {
      return false;
    }
    return true;
  }
}
