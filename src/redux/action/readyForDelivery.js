import { SET_STATUS } from '../actionType';

/**
 * Set Status To Redux
 */

const setStatus = curr => {
  return {
    type: SET_STATUS,
    payload: { status: curr },
  };
};
export default setStatus;
