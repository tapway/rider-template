import {
  UPDATE_PROFILE,
  UPDATE_BANK_DETAILS,
  UPDATE_KYC,
  UPDATE_VEHICLE_DETAILS,
  SET_USER,
} from '../actionType';

/**
 * Update Delivery User Profile To Redux
 */

/* eslint-disable object-shorthand */
export const updateProfile = (mob, pass, fname, mail, birth, gende, curr, lang) => {
  return {
    type: UPDATE_PROFILE,
    payload: {
      fullName: fname,
      password: pass,
      phoneNumber: mob,
      email: mail,
      dob: birth,
      gender: gende,
      currency: curr,
      language: lang,
    },
  };
};

/**
 * Update Delivery User Bank Details To Redux
 */

export const updateBankDetails = (accname, accnum, bankname, ifsccode) => {
  return {
    type: UPDATE_BANK_DETAILS,
    payload: {
      bankDetails: {
        accountName: accname,
        accountNumber: accnum,
        bankName: bankname,
        IFSC_Code: ifsccode,
      },
    },
  };
};

/**
 * Update Delivery User KYC Details To Redux
 */

export const updateKYC = (address, licenceNumber, cardHolder, dateOfIssue, validity) => {
  return {
    type: UPDATE_KYC,
    payload: {
      verificationDetails: {
        address,
        licenceNumber,
        cardHolder,
        dateOfIssue,
        validity,
      },
      address: address,
    },
  };
};

/**
 * Update Delivery User Vehicle Details To Redux
 */

export const updateVehicleDetail = (modal, registrationNumber, date, insurancePolicy, number) => {
  return {
    type: UPDATE_VEHICLE_DETAILS,
    payload: {
      vehicleDetails: {
        vehicleVerificationModal: modal,
        vehicleRegistrationNumber: registrationNumber,
        vehicleInsurancePolicy: insurancePolicy,
        vehicleDate: date,
        vehicleNumber: number,
      },
    },
  };
};

/**
 * Update Delivery User To Redux
 */

export const updateDeliveryUser = res => {
  return {
    type: SET_USER,
    payload: {
      // eslint-disable-next-line no-underscore-dangle
      id: res._id,
      fullName: res.fullName,
      dob: res.dob,
      gender: res.gender,
      language: res.language,
      currency: res.currency,
      password: res.password,
      address: res.address,
      GpsLocation: res.GpsLocation,
      savedAddresses: res.savedAddresses,
      phoneNumber: res.phoneNumber,
      email: res.email,
      profileImage: res.profileImage,
      isVerified: res.isVerified,
      verificationDetails: res.verificationDetails,
      verificationImage: res.verificationImage,
      vehicleVerificationImage: res.vehicleVerificationImage,
      isAvailable: res.isAvailable,
      bankDetails: res.bankDetails,
      vehicleDetails: res.vehicleDetails,
    },
  };
};
