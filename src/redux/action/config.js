import { SET_CONFIG } from '../actionType';

/**
 * Set Config To Redux
 */

const setConfig = config => {
  return {
    type: SET_CONFIG,
    payload: {
      config,
    },
  };
};
export default setConfig;
