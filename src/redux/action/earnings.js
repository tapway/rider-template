import { SET_EARNINGS } from '../actionType';

/**
 * Set Earnings To Redux
 */

const setEarnings = (today, yesterday, week, month, wallet) => {
  return {
    type: SET_EARNINGS,
    payload: {
      todayEarn: today,
      yesterdayEarn: yesterday,
      weekEarn: week,
      monthEarn: month,
      walletBalance: wallet,
    },
  };
};
export default setEarnings;
