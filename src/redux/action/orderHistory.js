import { FETCH_ORDER_HISTORY } from '../actionType';

/**
 * Set Order History To Redux
 */

const setOrderHistory = data => {
  return {
    type: FETCH_ORDER_HISTORY,
    payload: data,
  };
};
export default setOrderHistory;
