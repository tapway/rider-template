import { SET_DEVICE_INFO } from '../actionType';

/**
 * Set Device Info To Redux
 */

const setDeviceInfo = (userId, pushToken) => {
  return {
    type: SET_DEVICE_INFO,
    payload: {
      userId,
      pushToken,
    },
  };
};
export default setDeviceInfo;
