import { SET_USER } from '../actionType';

/**
 * Set Delivery User Fields To Redux
 */
const setUser = (user, jwt) => {
  return {
    type: SET_USER,
    payload: {
      jwtAccessToken: jwt,
      // eslint-disable-next-line no-underscore-dangle
      id: user._id,
      fullName: user.fullName,
      dob: user.dob,
      gender: user.gender,
      address: user.address,
      currency: user.currency,
      language: user.language,
      GpsLocation: user.GpsLocation,
      savedAddresses: user.savedAddresses,
      phoneNumber: user.phoneNumber,
      email: user.email,
      isLoggedIn: true,
      profileImage: user.profileImage,
      isVerified: user.isVerified,
      verificationDetails: user.verificationDetails,
      verificationImage: user.verificationImage,
      vehicleVerificationImage: user.vehicleVerificationImage,
      isAvailable: false,
      bankDetails: user.bankDetails,
      vehicleDetails: user.vehicleDetails,
    },
  };
};
export default setUser;
