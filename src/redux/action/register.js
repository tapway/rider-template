/* eslint-disable object-shorthand */
import { SET_USER } from '../actionType';

/**
 * Set Delivery User Profile Details To Redux
 */
export const setProfile = (mob, fname, mail, birth, gende, curr, lang) => {
  return {
    type: SET_USER,
    payload: {
      fullName: fname,
      phoneNumber: mob,
      email: mail,
      dob: birth,
      gender: gende,
      currency: curr,
      language: lang,
    },
  };
};

/**
 * Set Delivery User KYC Details To Redux
 */
export const setKYC = (address, licenceNumber, cardHolder, dateOfIssue, validity) => {
  return {
    type: SET_USER,
    payload: {
      verificationDetails: {
        address,
        licenceNumber,
        cardHolder,
        dateOfIssue,
        validity,
      },
      address: address,
    },
  };
};

/**
 * Set Delivery User Bank Details To Redux
 */
export const setBankDetails = (accname, accnum, bankname, ifsccode) => {
  return {
    type: SET_USER,
    payload: {
      bankDetails: {
        accountName: accname,
        accountNumber: accnum,
        bankName: bankname,
        IFSC_Code: ifsccode,
      },
    },
  };
};

/**
 * Set Delivery User Vehicle Details To Redux
 */
export const setVehicleDetail = (modal, registrationNumber, date, insurancePolicy, number) => {
  return {
    type: SET_USER,
    payload: {
      vehicleDetails: {
        vehicleVerificationModal: modal,
        vehicleRegistrationNumber: registrationNumber,
        vehicleInsurancePolicy: insurancePolicy,
        vehicleDate: date,
        vehicleNumber: number,
      },
    },
  };
};

/**
 * Dump Delivery User Details To Redux
 */
export const backFromRegistration = () => {
  return {
    type: 'USER_SIGNOUT',
  };
};
