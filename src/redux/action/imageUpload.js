import { SET_USER, UPDATE_PROFILE } from '../actionType';

/* eslint-disable no-undef */
import { store } from '../configureStore';
import imageUpload from '../../apiService/imageUpload';

/**
 * Set and upload images to the server
 */

function updateUserProfilePicAsync(userDetails, type) {
  const imageData = new FormData();
  const name = Math.random()
    .toString(36)
    .replace(/[^a-z]+/g, '');
  imageData.append('image', {
    uri: userDetails.localUrl,
    name: `${name}.jpg`,
    type: 'image/jpg',
  });
  imageData.append(userDetails);
  imageData.append('updateType', type);
  const handleFunc = res => {
    store.dispatch({
      type: SET_USER,
      payload: {
        profileImage: res.data.url,
      },
    });
  };
  imageUpload(imageData, type, handleFunc);
}

function changeUserProfilePicAsync(userDetails, type) {
  const imageData = new FormData();
  const name = Math.random()
    .toString(36)
    .replace(/[^a-z]+/g, '');
  imageData.append('image', {
    uri: userDetails.localUrl,
    name: `${name}.jpg`,
    type: 'image/jpg',
  });
  imageData.append(userDetails);
  imageData.append('updateType', type);
  const handleFunc = res => {
    store.dispatch({
      type: UPDATE_PROFILE,
      payload: {
        profileImage: res.data.url,
      },
    });
  };
  imageUpload(imageData, type, handleFunc);
}

function updateVerificationPicAsync(userDetails, type) {
  const imageData = new FormData();
  const name = Math.random()
    .toString(36)
    .replace(/[^a-z]+/g, '');
  imageData.append('image', {
    uri: userDetails.localUrl,
    name: `${name}.jpg`,
    type: 'image/jpg',
  });
  imageData.append(userDetails);
  imageData.append('updateType', type);
  const handleFunc = res => {
    store.dispatch({
      type: SET_USER,
      payload: {
        verificationImage: res.data.url,
      },
    });
  };
  imageUpload(imageData, type, handleFunc);
}

function changeVerificationPicAsync(userDetails, type) {
  const imageData = new FormData();
  const name = Math.random()
    .toString(36)
    .replace(/[^a-z]+/g, '');
  imageData.append('image', {
    uri: userDetails.localUrl,
    name: `${name}.jpg`,
    type: 'image/jpg',
  });
  imageData.append(userDetails);
  imageData.append('updateType', type);
  const handleFunc = res => {
    store.dispatch({
      type: UPDATE_PROFILE,
      payload: {
        verificationImage: res.data.url,
      },
    });
  };
  imageUpload(imageData, type, handleFunc);
}

function updateVehicleVerificationPicAsync(userDetails, type) {
  const imageData = new FormData();
  const name = Math.random()
    .toString(36)
    .replace(/[^a-z]+/g, '');
  imageData.append('image', {
    uri: userDetails.localUrl,
    name: `${name}.jpg`,
    type: 'image/jpg',
  });
  imageData.append(userDetails);
  imageData.append('updateType', type);
  const handleFunc = res => {
    store.dispatch({
      type: SET_USER,
      payload: {
        vehicleVerificationImage: res.data.url,
      },
    });
  };
  imageUpload(imageData, type, handleFunc);
}

function changeVehicleVerificationPicAsync(userDetails, type) {
  const imageData = new FormData();
  const name = Math.random()
    .toString(36)
    .replace(/[^a-z]+/g, '');
  imageData.append('image', {
    uri: userDetails.localUrl,
    name: `${name}.jpg`,
    type: 'image/jpg',
  });
  imageData.append(userDetails);
  imageData.append('updateType', type);
  const handleFunc = res => {
    store.dispatch({
      type: UPDATE_PROFILE,
      payload: {
        vehicleVerificationImage: res.data.url,
      },
    });
  };
  imageUpload(imageData, type, handleFunc);
}

export {
  changeUserProfilePicAsync,
  changeVerificationPicAsync,
  changeVehicleVerificationPicAsync,
  updateUserProfilePicAsync,
  updateVerificationPicAsync,
  updateVehicleVerificationPicAsync,
};
