import { SET_CURRENT_LOCATION, SET_AVAILABLE } from '../actionType';

/**
 * Update Delivery User Location To Redux
 */
export const setLocation = coor => {
  return {
    type: SET_CURRENT_LOCATION,
    payload: { coordinate: coor },
  };
};

/**
 * Set Delivery User Availabilty To Redux
 */
export const setAvailable = val => {
  return {
    type: SET_AVAILABLE,
    payload: { value: val },
  };
};
