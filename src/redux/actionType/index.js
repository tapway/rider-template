// Delivery User Action Types
export const FETCH_CURRENT_USER = 'FETCH_CURRENT_USER';
export const SET_USER = 'SET_USER';
export const SET_CURRENT_LOCATION = 'SET_CURRENT_LOCATION';
export const USER_SIGNOUT = 'USER_SIGNOUT';
export const ADD_SECOND_ADDRESS = 'ADD_SECOND_ADDRESS';
export const SET_AVAILABLE = 'SET_AVAILABLE';
export const SET_TRIP_STATUS = 'SET_TRIP_STATUS';

// Order Action Types
export const SET_ORDER = 'SET_ORDER';
export const DUMP_ORDER = 'DUMP_ORDER';
export const FETCH_DATA_ORDER_TYPE = 'FETCH_DATA_ORDER_TYPE';
export const UPDATE_DATA_ORDER_TYPE = 'UPDATE_DATA_ORDER_TYPE';
export const FETCH_ORDER_HISTORY = 'FETCH_ORDER_HISTORY';
export const CLEAR_ORDER_HISTORY = 'CLEAR_ORDER_HISTORY';

// Request Action Types
export const SET_REQUEST = 'SET_REQUEST';
export const DUMP_REQUEST = 'DUMP_REQUEST';

// Status Action Types
export const SET_STATUS = 'SET_STATUS';
export const DUMP_STATUS = 'DUMP_STATUS';

// Edit Profile Action Types
export const UPDATE_PROFILE = 'UPDATE_PROFILE';
export const UPDATE_KYC = 'UPDATE_KYC';
export const UPDATE_VEHICLE_DETAILS = 'UPDATE_VEHICLE_DETAILS';
export const UPDATE_BANK_DETAILS = 'UPDATE_BANK_DETAILS';
export const SET_EDIT_PROFILE = 'SET_EDIT_PROFILE';

// Earnings Action Types
export const SET_EARNINGS = 'SET_EARNINGS';
export const CLEAR_EARNINGS = 'CLEAR_EARNINGS';

// Device Info Action Types
export const SET_DEVICE_INFO = 'SET_DEVICE_INFO';
export const DUMP_DEVICE_INFO = 'DUMP_DEVICE_INFO';

// Config Action Types
export const FETCH_CONFIG = 'FETCH_CONFIG';
export const SET_CONFIG = 'SET_CONFIG';
export const CLEAR_CONFIG = 'CLEAR_CONFIG';
