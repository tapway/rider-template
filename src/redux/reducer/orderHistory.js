import { FETCH_ORDER_HISTORY, CLEAR_ORDER_HISTORY } from '../actionType/index';

const { uniqBy } = require('lodash');

export default function(state = [], action) {
  switch (action.type) {
    case FETCH_ORDER_HISTORY: {
      if (action.payload && state) return uniqBy([...state, ...action.payload], '_id');
      if (!action.payload && state) return { state };
      if (!state && action.payload) return [...action.payload];
      return state;
    }
    case CLEAR_ORDER_HISTORY: {
      return [];
    }
    default: {
      return state;
    }
  }
}
