import { FETCH_CONFIG, SET_CONFIG, CLEAR_CONFIG } from '../actionType/index';

const initialState = {};

export default function(state = initialState, action) {
  switch (action.type) {
    case FETCH_CONFIG: {
      return { ...state };
    }
    case SET_CONFIG: {
      return action.payload.config;
    }
    case CLEAR_CONFIG: {
      return initialState;
    }
    default: {
      return state;
    }
  }
}
