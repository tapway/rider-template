import { FETCH_DATA_ORDER_TYPE, UPDATE_DATA_ORDER_TYPE } from '../actionType';

const initialState = {
  catTypeData: {},
  loading: false,
  test: '',
};

export default function(state = initialState, action) {
  switch (action.type) {
    case FETCH_DATA_ORDER_TYPE: {
      return { ...state, catTypeData: action.res, loading: true };
    }
    case UPDATE_DATA_ORDER_TYPE: {
      return { ...state };
    }
    default: {
      return state;
    }
  }
}
