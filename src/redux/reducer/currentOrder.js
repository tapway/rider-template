import { SET_ORDER, DUMP_ORDER } from '../actionType';
import returnProperty from '../helper';

const initialState = {
  _id: '',
  userId: '',
  deliveryId: '',
  deliveryLocation: '',
  pickUpLocation: '',
  driverGpsLocation: '',
  processingStatus: '',
  deliveryAddress: '',
  deliveryInstructions: {},
  paymentType: '',
  paymentAmount: '',
  userRating: '',
  deliveryRating: '',
  userReview: '',
  deliveryReview: '',
  reviewCatagory: '',
  ratingCatagory: '',
  anyIssue: '',
  pickUpAddress: '',
  catagory: '',
  deliveredDateTime: '',
  createdAt: '',
  updatedAt: '',
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SET_ORDER: {
      return {
        _id: returnProperty('_id', action.payload, state),
        userId: returnProperty('userId', action.payload, state),
        deliveryId: returnProperty('deliveryId', action.payload, state),
        deliveryInstructions: returnProperty('deliveryInstructions', action.payload, state),
        deliveryLocation: returnProperty('deliveryLocation', action.payload, state),
        pickUpLocation: returnProperty('pickUpLocation', action.payload, state),
        driverGpsLocation: returnProperty('driverGpsLocation', action.payload, state),
        processingStatus: returnProperty('processingStatus', action.payload, state),
        deliveryAddress: returnProperty('deliveryAddress', action.payload, state),
        paymentType: returnProperty('paymentType', action.payload, state),
        paymentAmount: returnProperty('paymentAmount', action.payload, state),
        userRating: returnProperty('userRating', action.payload, state),
        deliveryReview: returnProperty('deliveryReview', action.payload, state),
        reviewCatagory: returnProperty('reviewCatagory', action.payload, state),
        anyIssue: returnProperty('anyIssue', action.payload, state),
        pickUpAddress: returnProperty('pickUpAddress', action.payload, state),
        catagory: returnProperty('catagory', action.payload, state),
        deliveredDateTime: returnProperty('deliveredDateTime', action.payload, state),
        createdAt: returnProperty('createdAt', action.payload, state),
        updatedAt: returnProperty('updatedAt', action.payload, state),
      };
    }
    case DUMP_ORDER: {
      return initialState;
    }
    default: {
      return state;
    }
  }
}
