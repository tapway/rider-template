import {
  FETCH_CURRENT_USER,
  SET_USER,
  SET_CURRENT_LOCATION,
  USER_SIGNOUT,
  ADD_SECOND_ADDRESS,
  SET_AVAILABLE,
  SET_TRIP_STATUS,
} from '../actionType';
import returnProperty from '../helper';

const initialState = {
  jwtAccessToken: '',
  id: '',
  fullName: '',
  dob: '',
  gender: '',
  address: '',
  GpsLocation: [],
  savedAddresses: [],
  phoneNumber: 0,
  email: '',
  profileImage: '',
  isLoggedIn: false,
  currency: '',
  language: '',
  isVerified: false,
  verificationDetails: {
    cardHolder: '',
    licenceNumber: '',
    address: '',
    dateOfIssue: '',
    validity: '',
  },
  verificationImage: '',
  vehicleDetails: {
    vehicleVerificationModal: '',
    vehicleRegistrationNumber: '',
    vehicleInsurancePolicy: '',
    vehicleDate: '',
    vehicleNumber: '',
  },
  vehicleVerificationImage: '',
  isAvailable: false,
  onTrip: false,
  bankDetails: {
    accountName: '',
    accountNumber: '',
    bankName: '',
    IFSC_Code: '',
  },
};

export default function(state = initialState, action) {
  // debugger;
  switch (action.type) {
    case FETCH_CURRENT_USER: {
      return { ...state };
    }
    case SET_USER: {
      const temp = {
        jwtAccessToken: returnProperty('jwtAccessToken', action.payload, state),
        id: returnProperty('id', action.payload, state),
        currency: returnProperty('currency', action.payload, state),
        language: returnProperty('language', action.payload, state),
        isLoggedIn: returnProperty('isLoggedIn', action.payload, state),
        firstName: returnProperty('firstName', action.payload, state),
        fullName: returnProperty('fullName', action.payload, state),
        dob: returnProperty('dob', action.payload, state),
        gender: returnProperty('gender', action.payload, state),
        lastName: returnProperty('lastName', action.payload, state),
        address: returnProperty('address', action.payload, state),
        GpsLocation: returnProperty('GpsLocation', action.payload, state),
        savedAddresses: returnProperty('savedAddresses', action.payload, state),
        phoneNumber: returnProperty('phoneNumber', action.payload, state),
        vehicleDate: returnProperty('vehicleDate', action.payload, state),
        email: returnProperty('email', action.payload, state),
        profileImage: returnProperty('profileImage', action.payload, state),
        isVerified: returnProperty('isVerified', action.payload, state),
        verificationDetails: returnProperty('verificationDetails', action.payload, state),
        verificationImage: returnProperty('verificationImage', action.payload, state),
        vehicleVerificationImage: returnProperty('vehicleVerificationImage', action.payload, state),
        isAvailable: returnProperty('isAvailable', action.payload, state),
        onTrip: returnProperty('onTrip', action.payload, state),
        bankDetails: returnProperty('bankDetails', action.payload, state),
        vehicleDetails: returnProperty('vehicleDetails', action.payload, state),
      };
      return temp;
    }
    case SET_CURRENT_LOCATION: {
      return { ...state, GpsLocation: action.payload.coordinate };
    }
    case USER_SIGNOUT: {
      return initialState;
    }
    case SET_AVAILABLE: {
      return { ...state, isAvailable: action.payload.value };
    }
    case SET_TRIP_STATUS: {
      return { ...state, onTrip: action.payload.value };
    }
    case ADD_SECOND_ADDRESS: {
      return {
        ...state,
        savedAddresses: [...state.savedAddresses, action.payload.newaddress],
      };
    }
    default: {
      return state;
    }
  }
}
