import {
  UPDATE_PROFILE,
  UPDATE_KYC,
  UPDATE_BANK_DETAILS,
  SET_EDIT_PROFILE,
  UPDATE_VEHICLE_DETAILS,
} from '../actionType/index';

const initialState = {};

export default function(state = initialState, action) {
  switch (action.type) {
    case UPDATE_PROFILE: {
      return { ...state, ...action.payload };
    }
    case UPDATE_KYC: {
      return { ...state, ...action.payload };
    }
    case UPDATE_BANK_DETAILS: {
      return { ...state, ...action.payload };
    }
    case UPDATE_VEHICLE_DETAILS: {
      return { ...state, ...action.payload };
    }
    case SET_EDIT_PROFILE: {
      return {};
    }
    default: {
      return state;
    }
  }
}
