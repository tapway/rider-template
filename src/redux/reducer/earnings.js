import { SET_EARNINGS, CLEAR_EARNINGS } from '../actionType';

const initialState = {
  todayEarn: 0,
  yesterdayEarn: 0,
  weekEarn: 0,
  monthEarn: 0,
  walletBalance: 0,
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SET_EARNINGS: {
      return action.payload;
    }
    case CLEAR_EARNINGS: {
      return initialState;
    }
    default: {
      return state;
    }
  }
}
