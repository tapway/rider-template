import { FETCH_CURRENT_USER, SET_REQUEST, DUMP_REQUEST } from '../actionType';
import returnProperty from '../helper';

const initialState = {
  id: '',
  userId: {},
  type: '',
  category: {},
  deliveryInstructions: {},
  processingStatus: {},
  paymentMode: '',
  paymentAmount: '',
  endUserGpslocation: '',
  deliveryLocation: '',
  pickUpLocation: '',
  deliveryAddress: '',
  pickUpAddress: '',
};

export default function(state = initialState, action) {
  switch (action.type) {
    case FETCH_CURRENT_USER: {
      return { ...state };
    }
    case SET_REQUEST: {
      return {
        id: returnProperty('_id', action.payload, state),
        userId: returnProperty('userId', action.payload, state),
        category: returnProperty('category', action.payload, state),
        type: returnProperty('type', action.payload, state),
        processingStatus: returnProperty('processingStatus', action.payload, state),
        paymentMode: returnProperty('paymentMode', action.payload, state),
        paymentAmount: returnProperty('paymentAmount', action.payload, state),
        endUserGpslocation: returnProperty('endUserGpslocation', action.payload, state),
        deliveryLocation: returnProperty('deliveryLocation', action.payload, state),
        deliveryInstructions: returnProperty('deliveryInstructions', action.payload, state),
        pickUpLocation: returnProperty('pickUpLocation', action.payload, state),
        deliveryAddress: returnProperty('deliveryAddress', action.payload, state),
        pickUpAddress: returnProperty('pickUpAddress', action.payload, state),
      };
    }
    case DUMP_REQUEST: {
      return initialState;
    }
    default: {
      return state;
    }
  }
}
