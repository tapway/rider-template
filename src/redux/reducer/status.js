import { SET_STATUS, DUMP_STATUS } from '../actionType';

const initialState = {
  status: '',
};

export default function(state = initialState, action) {
  // debugger;
  switch (action.type) {
    case SET_STATUS: {
      return {
        status: action.payload.status,
      };
    }
    case DUMP_STATUS: {
      return initialState;
    }
    default: {
      return state;
    }
  }
}
