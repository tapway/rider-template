import { combineReducers } from 'redux';
import currentUser from './currentUser';
import currentOrder from './currentOrder';
import dataOrderType from './dataOrderType';
import orderHistory from './orderHistory';
import deviceInfo from './deviceInfo';
import earnings from './earnings';
import config from './config';
import editProfile from './editProfile';
import request from './request';
import status from './status';

const user = combineReducers({
  currentUser,
  currentOrder,
  request,
  dataOrderType,
  deviceInfo,
  orderHistory,
  earnings,
  config,
  editProfile,
  status,
});

export default user;
