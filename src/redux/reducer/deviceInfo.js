import { SET_DEVICE_INFO, DUMP_DEVICE_INFO } from '../actionType';

const initialState = {
  userId: '',
  pushToken: '',
};

export default function(state = initialState, action) {
  // debugger;
  switch (action.type) {
    case SET_DEVICE_INFO: {
      return {
        userId: action.payload.userId,
        pushToken: action.payload.pushToken,
      };
    }
    case DUMP_DEVICE_INFO: {
      return initialState;
    }
    default: {
      return state;
    }
  }
}
