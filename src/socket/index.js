import io from 'socket.io-client';
import { store } from '../redux/configureStore';
import SERVER from '../config/config';

let socket = null;
let timeoutId = null;

export function socketRiderInit() {
  socket = io(SERVER, {
    jsonp: false,
    transports: ['websocket'],
    query: `token=${store.getState().currentUser.jwtAccessToken}&&id=${
      store.getState().currentUser.id
    }&&type=delivery`,
  });

  socket.heartbeatTimeout = 10000;

  socket.on('connect', () => {
    console.log('socket started');
  });

  socket.on('disconnect', () => {
    console.log('socket started');
    socket.on('connect', () => {});
  });

  socket.on('Trip', data => {
    store.dispatch({
      type: 'SET_REQUEST',
      payload: data,
    });
    timeoutId = setTimeout(() => {
      store.dispatch({
        type: 'DUMP_REQUEST',
      });
    }, 20000);
  });

  socket.on('getOrder', data => {
    store.dispatch({
      type: 'SET_ORDER',
      payload: data,
    });
    store.dispatch({
      type: 'SET_STATUS',
      payload: { status: 'START' },
    });
  });
}

export function makeOrder(payload) {
  socket.emit('makeOrder', payload);
  clearTimeout(timeoutId);
}

export function requestDriverResponse(payload) {
  socket.emit('requestDriverResponse', payload);
}

export function cancelRequest(tripRequest) {
  socket.emit('cancelRequest', tripRequest);
}

export function socketDeliveryStatus(payload) {
  socket.emit('deliveryStatus', payload);
}

export function updateLocation(user, newPos) {
  socket.emit('updateLocation', { id: user.id, coords: newPos });
}

export function endUserRatingUpdate(payload) {
  socket.emit('EndUserRating', payload);
}

export function statusUpdate(payload) {
  socket.emit('statusUpdate', payload);
}
