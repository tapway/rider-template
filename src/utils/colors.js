const Colors = {
  // * Primary Theme colors
  primaryColor: '#35d8fb',
  secondaryColor: '#fca487',
  themeBackgroundColor: '#FFF',
  highlightedContainer: '#F7F3F3',
  flatListItemBgColor: '#FFF',
  flatListPlaceholderColor: '#eeeeee',

  // * Border colors
  primaryBorderColor: 'lightgrey',
  secondaryBorderColor: 'silver',
  redBorderColor: 'red',
  noFoucsBorderColor: '#D8D8D8',
  modalBorderColor: 'rgba(0, 0, 0, 0.05)',
  borderWithPrimaryBgColor: 'white',
  imageContainerBorderColor: '#f7f7f7',

  // * Special colors
  crossContainerColor: '#f2f2f2',
  imageContainerColor: '#f2f2f2',
  blurEffect: 'rgba(0, 0, 0, 0.65)',

  // * Modal's colors
  modalBackgroundColor: '#00000040',
  modalRejectinContainerColor: 'red',
  modalContainerColor: '#FFF',

  // * Constant colors
  white: 'white',
  black: 'black',
  red: 'red',
  transparent: 'transparent',
  lightgrey: 'lightgrey',

  // * Font colors
  // -----------> Font colors for containers
  textForPrimaryBgColor: 'white',
  textForModalContainerColor: 'white',
  textForThemeBgColor: 'black',
  textForTabBar: 'grey',
  textGrey: 'grey',
  textForNonHighlightedContainer: 'silver',
  textForHighlightedContainer: 'black',

  // -----------> Font colors for buttons
  textForPrimaryBtnBgColor: 'white',
  textForThemeBgBtn: 'black',

  // -----------> Font colors general
  placeHolderTextColor: 'grey',
  sureText: '#544F52',
  placeHolderTextColorForPrimaryBg: 'rgb(200, 252, 230)',
  secondarySubHeadingVerifyScreen: '#A6A4A3',

  // * Button colors
  primaryBtnBgColor: '#35d8fb',
  secondaryBtnBgColor: '#fca487',
  thirdBtnBgColor: 'white',

  // * Shadow colors
  primaryShadowColor: 'silver',
  secondaryShadowColor: '#000',

  // * Toast colors
  toastBgColor: 'black',
  toastTextColor: 'orange',
};

export default Colors;
