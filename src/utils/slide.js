import getImage from './getImage';

const slides = [
  {
    key: 'page1',
    title: 'Welcome to Tapway',
    text: 'Now you can make quick and affordable deliveries at the tap of a button',
    image: getImage.onTheWayIcon,
    backgroundColor: '#1891D0',
  },
  {
    key: 'page2',
    title: 'Request a Rider',
    text:
      'Enter your pickup location, destination, item name and quantity and tap on proceed to view delivery price.',
    image: getImage.requestIcon,
    backgroundColor: '#1891D0',
  },
  {
    key: 'page3',
    title: 'Get Paired',
    text: 'Get paired with a rider then make payment to view rider’s details and ETA.',
    image: getImage.getPairedIcon,
    backgroundColor: '#1891D0',
  },
  {
    key: 'page4',
    title: 'Relax',
    text:
      'This is a secured way to getting your item delivered hassle free, once your item is delivered you can rate your rider experience',
    image: getImage.relaxIcon,
    backgroundColor: '#1891D0',
  },
];

export default slides;
