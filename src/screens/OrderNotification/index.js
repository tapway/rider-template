import React, { PureComponent } from 'react';
import { Text, View, Modal, TouchableOpacity, Image } from 'react-native';
import MapView from 'react-native-maps';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import styles from './style';
import getImage from '../../utils/getImage';
import Header from '../../components/Header';
import { makeOrder, socketDeliveryStatus } from '../../socket/index';
import Empty from '../../components/Oops';

class OrdernNotification extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
    };
    this.home = this.home.bind(this);
    this.readyForDelivery = this.readyForDelivery.bind(this);
    this.mapRef = null;
    this.setModalVisible = this.setModalVisible.bind(this);
    this.func = this.func.bind(this);
    this.onmapready = this.onmapready.bind(this);
  }

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  statusUpdate = value => {
    const { props } = this;
    socketDeliveryStatus({
      statusId: props.request.processingStatus,
      userId: props.request.userId,
      status: value,
    });
  };

  onmapready = () => {
    const { modalVisible: Modalvisi } = this.state;
    this.setModalVisible(!Modalvisi);
  };

  cancel = () => {
    const { modalVisible: Modalvisi } = this.state;
    this.setModalVisible(!Modalvisi);
    const { props } = this;
    makeOrder({ deliveryId: props.user.id, success: false });
    this.statusUpdate('Rejected');
    props.dumpRequest();
    props.navigation.navigate('home');
  };

  home() {
    const { modalVisible: Modalvisi } = this.state;
    this.setModalVisible(!Modalvisi);
    const { props } = this;
    props.navigation.navigate('home');
  }

  readyForDelivery() {
    const { props } = this;
    makeOrder({
      ...props.request,
      deliveryId: props.user.id,
      pos: props.user.GpsLocation,
      success: true,
    });
    this.statusUpdate('Accepted');
    props.setAvailable(false);
    if (props.request.category.name === 'Cab') {
      props.dumpRequest();
      props.navigation.navigate('readyForDelivery', { category: props.request.category.name });
    } else {
      props.navigation.navigate('orderList', { category: props.request.category.name });
    }
  }

  func() {
    const { modalVisible: Modalvisi } = this.state;
    this.setModalVisible(!Modalvisi);
    this.readyForDelivery();
  }

  render() {
    const { modalVisible: Modalvisi } = this.state;
    const { props } = this;
    const { deliveryAddress, id, category } = props.request;
    return (
      <View style={styles.container}>
        <View style={styles.container}>
          <Header onlyBack={false} back={this.home} title="Order Notification" />
          {id ? (
            <React.Fragment>
              <Modal visible={Modalvisi} animationType="fadeIn" transparent>
                <View style={styles.secondConatiner}>
                  <View style={styles.modalView}>
                    <Text style={styles.heading2}>{category.name} Request</Text>
                    <Image
                      resizeMode="contain"
                      style={styles.extraButton}
                      source={getImage.locationPoint}
                    />
                    <View style={styles.addressText}>
                      <Text style={styles.addText}>{deliveryAddress}</Text>
                    </View>
                  </View>
                </View>
                <View style={styles.thirdContainer}>
                  <TouchableOpacity
                    style={[styles.loginButton, styles.rejectButton]}
                    onPress={() => this.cancel()}
                  >
                    <Text style={styles.loginText}>Reject</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={[styles.loginButton, styles.acceptButton]}
                    onPress={() => {
                      this.func();
                    }}
                  >
                    <Text style={styles.loginText}>Accept</Text>
                  </TouchableOpacity>
                </View>
              </Modal>
              <View style={styles.mapContainer}>
                <MapView
                  style={styles.map}
                  onMapReady={this.onmapready}
                  ref={ref => {
                    this.mapRef = ref;
                  }}
                />
              </View>
            </React.Fragment>
          ) : (
            <View style={styles.noOrder}>
              <Empty title="No new orders" />
            </View>
          )}
        </View>
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    request: state.request,
    user: state.currentUser,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    dumpRequest: () => {
      dispatch({
        type: 'DUMP_REQUEST',
      });
    },
    setAvailable: val => {
      dispatch({
        type: 'SET_AVAILABLE',
        payload: { value: val },
      });
    },
  };
};

OrdernNotification.propTypes = {
  dumpRequest: PropTypes.func.isRequired,
  setAvailable: PropTypes.func.isRequired,
  request: PropTypes.objectOf(PropTypes.any).isRequired,
  user: PropTypes.objectOf(PropTypes.any).isRequired,
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OrdernNotification);
