import { StyleSheet, Dimensions } from 'react-native';
import colors from '../../utils/colors';

const { height, width } = Dimensions.get('window');
export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.themeBackgroundColor,
  },
  loginButton: {
    width: width / 3,
    alignItems: 'center',
    padding: width / 30,
    borderRadius: 50,
    justifyContent: 'center',
    borderColor: colors.transparent,
  },
  noOrder: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  noOrderText: {
    fontSize: width / 8,
    marginBottom: height / 20,
  },
  map: { flex: 1 },
  heading2: {
    fontSize: width / 20,
    color: colors.primaryColor,
    fontWeight: 'bold',
  },
  extraButton: {
    height: height / 20,
    width: width / 15,
    marginTop: height / 24,
  },
  modalView: {
    backgroundColor: colors.modalContainerColor,
    marginLeft: width / 9,
    marginRight: width / 9,
    borderRadius: 30,
    position: 'absolute',
    alignItems: 'center',
    paddingTop: height / 15,
    paddingLeft: width / 13,
    paddingRight: width / 13,
    shadowOpacity: 0.75,
    shadowColor: colors.primaryShadowColor,
    elevation: 25,
  },
  mapContainer: {
    flex: 14,
  },
  addressText: {
    flex: 1,
    marginTop: height / 50,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: height / 25,
  },
  addText: {
    fontSize: width / 24,
    textAlign: 'center',
  },
  secondConatiner: {
    flex: 9,
    justifyContent: 'center',
    marginTop: height / 5,
    alignItems: 'center',
  },
  acceptButton: {
    backgroundColor: colors.primaryColor,
  },
  rejectButton: {
    backgroundColor: colors.red,
  },
  thirdContainer: {
    backgroundColor: colors.themeBackgroundColor,
    flex: 3,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingRight: width / 9,
    paddingLeft: width / 9,
    alignItems: 'center',
  },
  loginText: {
    color: colors.textForPrimaryBgColor,
    fontSize: width / 18,
  },
});
