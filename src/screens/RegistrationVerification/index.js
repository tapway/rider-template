import React, { PureComponent } from 'react';
import CodeInput from 'react-native-confirmation-code-input';
import Toast from 'react-native-easy-toast';
import { connect } from 'react-redux';
import { View, Text, TouchableOpacity, TouchableWithoutFeedback, Keyboard } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import PropTypes from 'prop-types';
import styles from './style';
import colors from '../../utils/colors';
import setUser from '../../redux/action/loginPage';
import { store } from '../../redux/configureStore';
import register from '../../apiService/register';
import { resetActionHome } from '../../navigation/ResetNavigation';
import registrationResendOtp from '../../apiService/registrationResendOtp';
import Header from '../../components/Header';
import Loader from '../../components/Loader';
import Button from '../../components/Button';
import { heightRatio } from '../../utils/stylesheet';

class Verify extends PureComponent {
  constructor() {
    super();
    this.state = {
      verificationCode: '',
      imageLoader: false,
    };
    this.goBackToLogin = this.goBackToLogin.bind(this);
  }

  handleRegister = res => {
    const { refs, props } = this;
    this.setState({
      verificationCode: '',
      imageLoader: false,
    });
    if (res.success) {
      const { user } = res.data;
      const { jwtAccessToken } = res.data;
      props.setUserDispatcher(user, jwtAccessToken);
      props.navigation.dispatch(resetActionHome);
      props.navigation.navigate('home');
    } else {
      refs.toast.show('Incorrect Otp');
    }
  };

  submit = () => {
    const { verificationCode, imageLoader } = this.state;
    const { user } = this.props;
    this.setState({ imageLoader: !imageLoader });
    register(store.getState().currentUser, verificationCode, user.phoneNumber, this.handleRegister);
  };

  otpRequest = () => {
    const { user } = this.props;
    const { phoneNumber } = user;
    registrationResendOtp(phoneNumber);
  };

  goBackToLogin() {
    const { props } = this;
    store.dispatch({
      type: 'USER_SIGNOUT',
    });
    props.navigation.navigate('login');
  }

  render() {
    const { user } = this.props;
    const { imageLoader } = this.state;
    const { phoneNumber } = user;
    return (
      <KeyboardAwareScrollView scrollEnabled={false}>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
          <View style={styles.container}>
            <Loader loading={imageLoader} />
            <Header onlyBack back={this.goBackToLogin} />
            <View style={styles.firstContainer}>
              <Text style={styles.mainVerificationHeading}>Verify your mobile number</Text>
              <Text style={styles.verificationDescription}>
                Please enter the verification code sent to {phoneNumber}
              </Text>
            </View>
            <View style={styles.secondContainer}>
              <Text style={styles.mainVerificationHeading}>Verification code</Text>
              <CodeInput
                autoFocus={false}
                secureTextEntry
                className="border-box"
                space={30}
                codeLength={4}
                size={40}
                keyboardType="numeric"
                inputPosition="left"
                activeColor="black"
                inactiveColor="grey"
                onFulfill={code => this.setState({ verificationCode: code })}
              />
            </View>
            <View style={styles.thirdContainer}>
              <View style={[styles.loginBtnContainer]}>
                <Button submit={this.submit} title="Next" />
              </View>
              <View style={styles.resendContainer}>
                <Text style={styles.resendCodeText}>Not Recieved? </Text>
                <TouchableOpacity onPress={this.otpRequest}>
                  <Text style={[styles.resendCodeText, styles.boldResend]}>Resend Code</Text>
                </TouchableOpacity>
              </View>
            </View>
            <Toast
              // eslint-disable-next-line react/no-string-refs
              ref="toast"
              style={{ backgroundColor: colors.toastBgColor }}
              position="bottom"
              positionValue={335 * heightRatio}
              fadeInDuration={750}
              fadeOutDuration={1000}
              opacity={0.8}
              textStyle={{ color: colors.toastTextColor, fontSize: 15 }}
            />
          </View>
        </TouchableWithoutFeedback>
      </KeyboardAwareScrollView>
    );
  }
}
const mapStateToProps = state => {
  return {
    user: state.currentUser,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    setUserDispatcher: (user, jwt) => {
      dispatch(setUser(user, jwt));
    },
  };
};

Verify.propTypes = {
  setUserDispatcher: PropTypes.func.isRequired,
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
  user: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Verify);
