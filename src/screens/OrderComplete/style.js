import { StyleSheet, Dimensions } from 'react-native';
import colors from '../../utils/colors';

const { height, width } = Dimensions.get('window');
export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.themeBackgroundColor,
  },
  headingConatiner: {
    flex: 0.1,
    alignItems: 'center',
  },
  headingText: {
    marginTop: height / 20,
    fontSize: height / 40,
    fontWeight: '300',
  },
  firstContainer: {
    flex: 0.2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  amountPaidText: {
    fontSize: height / 40,
    fontWeight: '800',
  },
  amountText: {
    marginTop: height / 150,
    fontSize: height / 20,
    fontWeight: '800',
    color: colors.secondaryColor,
  },
  ratingContainer: {
    flex: 0.25,
    alignItems: 'center',
    justifyContent: 'center',
  },
  userIconImage: {
    height: width / 4,
    width: width / 4,
    borderRadius: width / 4 / 2,
  },
  userName: {
    marginTop: height / 50,
    fontSize: height / 40,
  },
  ratingDescritionContainer: {
    flex: 0.2,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: width / 15,
    marginRight: width / 15,
  },
  ratingDescritionText: {
    fontSize: height / 50,
    textAlign: 'center',
    marginBottom: height / 100,
  },
  lastContainer: {
    flex: 0.2,
    alignItems: 'center',
  },
  loginButton: {
    width: width / 2,
    alignItems: 'center',
    padding: width / 30,
    borderRadius: 50,
    backgroundColor: colors.primaryColor,
    justifyContent: 'center',
    borderColor: 'transparent',
  },
  loginBtnContainer: {
    marginTop: height / 30,
    flex: 2,
    alignItems: 'center',
  },
  loginText: {
    color: colors.textForPrimaryBgColor,
    fontSize: height / 40,
  },
  skipFeedbackButton: {
    marginTop: height / 13,
  },
  skipFeedbackText: {
    fontSize: height / 50,
  },
});
