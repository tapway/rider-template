/* eslint-disable react/no-unused-state */
import React, { PureComponent } from 'react';
import { Text, View, TouchableOpacity, Image } from 'react-native';
import { Rating } from 'react-native-elements';
import { connect } from 'react-redux';
import RadioGroup from 'react-native-radio-buttons-group';
import PropTypes from 'prop-types';
import styles from './style';
import colors from '../../utils/colors';
import getImage from '../../utils/getImage';
import setStatus from '../../redux/action/readyForDelivery';
import { endUserRatingUpdate } from '../../socket';
import setTripStatus from '../../apiService/setTripStatus';

class OrderComplete extends PureComponent {
  constructor() {
    super();
    this.state = {
      rating: null,
      data: [
        { label: 'Very Good', color: colors.primaryColor, size: 15 },
        { label: 'Good', color: colors.primaryColor, size: 15 },
        { label: 'Fair', color: colors.primaryColor, size: 15 },
        { label: 'Poor', color: colors.primaryColor, size: 15 },
      ],
      ratingComment: '',
    };
    this.home = this.home.bind(this);
    this.orderFinished = this.orderFinished.bind(this);
    this.ratingCompleted = this.ratingCompleted.bind(this);
    this.ratingComment = this.ratingComment.bind(this);
  }

  ratingComment(data) {
    const selectedButton = data.find(e => e.selected === true);
    this.setState({ ratingComment: selectedButton.value });
  }

  home() {
    const { props } = this;
    props.navigation.navigate('home');
  }

  orderFinished() {
    const { props } = this;
    const { rating, ratingComment } = this.state;
    setTripStatus(false, props.user.jwtAccessToken);
    props.setAvailable(true);
    endUserRatingUpdate({
      userRating: rating,
      userReview: ratingComment,
      _id: props.order._id,
    });
    props.dumpOrder();
    this.home();
  }

  ratingCompleted(rating) {
    this.setState({ rating });
  }

  render() {
    // eslint-disable-next-line react/prop-types
    const { user, order } = this.props;
    const { data } = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.headingConatiner}>
          <Text style={styles.headingText}>Order Complete</Text>
        </View>
        <View style={styles.firstContainer}>
          <Text style={styles.amountPaidText}>Amount Paid</Text>
          <Text style={styles.amountText}>$ {order.paymentAmount}</Text>
        </View>
        <View style={styles.ratingContainer}>
          <Image
            style={styles.userIconImage}
            source={user.profileImage ? { uri: user.profileImage } : getImage.secondDriver}
          />
          <Text style={styles.userName}>{user.fullName}</Text>
          <Rating onFinishRating={this.ratingCompleted} style={{ paddingVertical: 10 }} />
        </View>
        <View style={styles.ratingDescritionContainer}>
          <Text style={styles.ratingDescritionText}>
            How would you rate your overall experience with our service?
          </Text>
          <RadioGroup radioButtons={data} flexDirection="row" onPress={this.ratingComment} />
        </View>
        <View style={styles.lastContainer}>
          <View style={[styles.loginBtnContainer]}>
            <TouchableOpacity style={styles.loginButton} onPress={this.orderFinished}>
              <Text style={styles.loginText}>Submit</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    user: state.currentUser,
    order: state.currentOrder,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    dumpOrder: () => {
      dispatch({
        type: 'DUMP_ORDER',
      });
    },
    setAvailable: val => {
      dispatch({
        type: 'SET_AVAILABLE',
        payload: { value: val },
      });
    },
    setStatusDispatcher: curr => {
      dispatch(setStatus(curr));
    },
  };
};

OrderComplete.propTypes = {
  user: PropTypes.objectOf(PropTypes.any).isRequired,
  order: PropTypes.objectOf(PropTypes.any).isRequired,
  setAvailable: PropTypes.func.isRequired,
  dumpOrder: PropTypes.func.isRequired,
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OrderComplete);
