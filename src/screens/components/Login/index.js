/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { View, TouchableNativeFeedback, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import Image from 'react-native-remote-svg';
import {
  Container,
  Content,
  Button,
  Item,
  Input,
  Text,
  Header,
  Grid,
  Row,
  Col,
  Left,
  Body,
  // Image,
} from 'native-base';

import styles from './styles';
import Colors from '../../constants/Colors';
import { width, height } from '../../constants/Layout';
import { Actions } from 'react-native-router-flux';

export class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      hidepassword: true,
    };
  }

  render() {
    const { hidepassword } = this.state;
    const {navigation}= this.props
    return (
      <Container style={styles.container}>
        <Header
          transparent
          // hasTabs
          iosBarStyle={'light-content'}
          style={{ height: 5 }}
          androidStatusBarColor={Colors.primary}>
          <Left />
          <Body />
        </Header>
        <Content
          scrollEnabled={true}
          contentContainerStyle={{
            padding: 16,
            minHeight: height,
          }}>
          <View style={{ marginVertical: 5 }}>
            <Text style={styles.welcomeHd}>Welcome Back, William</Text>
          </View>

          <View style={{ justifyContent: 'center', marginVertical: 20 }}>
            <View style={styles.inputContainer}>
              <Text style={styles.inputLabel}>Email ID</Text>
              <Item regular style={styles.inputItem}>
                <Input
                  placeholder="willywale@gmail.com"
                  placeholderTextColor={'#FFF9'}
                  style={styles.inputText}
                />
              </Item>
            </View>

            <View style={styles.inputContainer}>
              <Text style={styles.inputLabel}>Password</Text>
              <Item regular style={styles.inputItem}>
                <Input
                  placeholder="Password"
                  placeholderTextColor={'#FFF9'}
                  secureTextEntry={hidepassword}
                  style={styles.inputText}
                />
                <TouchableNativeFeedback
                  onPressIn={() => this.setState({ hidepassword: false })}
                  onPressOut={() => this.setState({ hidepassword: true })}
                  background={TouchableNativeFeedback.SelectableBackground()}>
                  <View style={{ padding: 10 }}>
                    <Text style={[styles.inputText, { fontSize: 14 }]}>
                      SHOW
                    </Text>
                  </View>
                </TouchableNativeFeedback>
              </Item>
            </View>

            <View>
              <TouchableOpacity
                style={{ padding: 0, margin: 0 }}
                transparent
                onPress={() => navigation.navigate('ForgetPassword')}>
                <Text
                  style={{
                    color: '#FFFFFF',
                    fontSize: 16,
                    fontFamily: 'Nunito-Regular',
                  }}>
                  Forgot Password?
                </Text>
              </TouchableOpacity>
            </View>

            <Button
              block
              primary
              style={styles.submitBtn}
              onPress={() => navigation.navigate('Home')}>
              <Text style={styles.btnText}>LOGIN</Text>
            </Button>

            <View>
              <Text
                style={{
                  color: '#FFFFFF',
                  textAlign: 'center',
                  fontFamily: 'Nunito-Regular',
                }}>
                Don't have an account?
              </Text>
              <TouchableOpacity onPress={() => navigation.navigate('SignUp')}>
                <Text
                  style={{
                    color: '#FFFFFF',
                    textAlign: 'center',
                    fontWeight: '600',
                    fontFamily: 'Nunito-SemiBold',
                  }}>
                  Signup Now
                </Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={styles.actionContainer}>
            <View style={{ marginVertical: 12 }}>
              <Text style={styles.actionTitle}>Quick Actions</Text>
            </View>
            <Grid>
              <Row>
                <Col style={{ alignSelf: 'center' }}>
                  <TouchableOpacity
                    transparent
                    onPress={() => navigation.navigate('CheckPrice')}>
                    <View style={styles.actionColView}>
                      <Image
                        style={styles.actionImageSize}
                        source={require('../../assets/icons/naira.svg')}
                      />
                    </View>
                  </TouchableOpacity>
                  <View
                    style={{
                      alignSelf: 'center',
                    }}>
                    <Text style={styles.actionText}>{'Check Price\n'}</Text>
                  </View>
                </Col>
                <Col style={{ alignSelf: 'center' }}>
                  <TouchableOpacity
                    transparent
                    onPress={() => navigation.navigate('CheckDeliveryTime')}>
                    <View style={styles.actionColView}>
                      <Image
                        style={styles.actionImageSize}
                        source={require('../../assets/icons/clock.svg')}
                      />
                    </View>
                  </TouchableOpacity>
                  <View
                    style={{
                      alignSelf: 'center',
                    }}>
                    <Text style={styles.actionText}>
                      {'Check Deliver\n Time'}
                    </Text>
                  </View>
                </Col>
                <Col style={{ alignSelf: 'center' }}>
                  <TouchableOpacity
                    transparent
                    onPress={() => navigation.navigate('CheckNearestDriver')}>
                    <View style={styles.actionColView}>
                      <Image
                        style={styles.actionImageSize}
                        source={require('../../assets/icons/clock.svg')}
                      />
                    </View>
                  </TouchableOpacity>
                  <View
                    style={{
                      alignSelf: 'center',
                    }}>
                    <Text style={styles.actionText}>
                      {'Check Price\n Driver'}
                    </Text>
                  </View>
                </Col>
              </Row>
            </Grid>
          </View>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Login);
