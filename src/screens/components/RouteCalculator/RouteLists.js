/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react/react-in-jsx-scope */
import React, { Component } from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import styles from './styles';
import { Card, Left, Body, Right, Icon } from 'native-base';

const RouteList = ({ toggleOrderDetails }) => {
  return (
    <View>
      <TouchableOpacity onPress={toggleOrderDetails}>
        <Card style={{ borderRadius: 10 }}>
          <View
            style={{
              padding: 10,
              backgroundColor: '#FFFFFF',
              borderBottomColor: '#0001',
              borderBottomWidth: 1.5,
            }}>
            <View style={{ flexDirection: 'row' }}>
              <Left>
                <Text style={{ color: '#030E1A', opacity: 0.7 }}>
                  Aug 23, 17:36
                </Text>
              </Left>
              <Body />
              <Right>
                <View
                  style={{
                    backgroundColor: 'rgba(41, 47, 61, 0.10999999940395355)',
                    padding: 5,
                    borderRadius: 10,
                  }}>
                  <Text style={{ color: '#030E1A' }}>PENDING</Text>
                </View>
              </Right>
            </View>
            <View style={{ flexDirection: 'row', marginVertical: 5 }}>
              <Text>Parcel ID:</Text>
              <Text
                style={{
                  marginLeft: 10,
                  fontFamily: 'Nunito-SemiBold',
                  color: '#030E1A',
                }}>
                FX2948TPW
              </Text>
            </View>
          </View>
          <View style={{ padding: 10 }}>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ flexDirection: 'column' }}>
                <Icon
                  type="MaterialCommunityIcons"
                  name="map-marker"
                  style={{ color: '#030E1A', fontSize: 16 }}
                />
                <Text />
                <Icon
                  type="MaterialCommunityIcons"
                  name="map-marker"
                  style={{ color: '#3FAF5D', fontSize: 16 }}
                />
              </View>

              <View style={{ flexDirection: 'column', marginLeft: 10 }}>
                <View>
                  <Text
                    style={{
                      fontFamily: 'Nunito-SemiBold',
                      color: '#030E1A',
                    }}>
                    5 Ogunbamila Street, Lady Lak, Bariga.
                  </Text>
                </View>
                <View>
                  <Text />
                </View>
                <View>
                  <Text
                    style={{
                      fontFamily: 'Nunito-SemiBold',
                      color: '#030E1A',
                    }}>
                    Akilo Street, Lady Lak, Bariga
                  </Text>
                </View>
              </View>
            </View>
          </View>
        </Card>
      </TouchableOpacity>
    </View>
  );
};

export default RouteList;
