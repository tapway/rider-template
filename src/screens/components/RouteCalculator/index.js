/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput } from 'react-native';
import { connect } from 'react-redux';
import Image from 'react-native-remote-svg';
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Content,
  Thumbnail,
  Item,
  Input,
  Card,
  Grid,
  Row,
  Col,
  Picker,
  CardItem,
  //   Image,
} from 'native-base';
import styles from './styles';
import { Actions } from 'react-native-router-flux';
import Modal from 'react-native-modal';
import getImage from '../../../utils/getImage';
import RouteList from './RouteLists';

class RouteCalculator extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isOrderDetailsVisible: false,
      isCancelOtherVisible: false,
      reasonType: '',
      isCancelSuccessful: false,
    };
  }

  gotoRouteCalc = () => {
    this.props.navigation.navigate('routeCalc', { routeCalc: 'routeCalc' })
    //Actions.HomePage({ routeCalc: 'routeCalc' });
  }

  toggleOrderDetails = () => {
    // this.gotoRouteCalc();
    this.setState({ isOrderDetailsVisible: !this.state.isOrderDetailsVisible });
  };

  toggleCancelOther = () => {
    this.setState({ isCancelOtherVisible: !this.state.isCancelOtherVisible });
  };

  cancelOther = () => {
    this.setState({ isCancelSuccessful: true });
  };

  cancelDone = () => {
    this.setState({
      isCancelSuccessful: false,
      isCancelOtherVisible: false,
      isOrderDetailsVisible: false,
    });
  };

  render() {
    const {
      isOrderDetailsVisible,
      isCancelOtherVisible,
      reasonType,
      isCancelSuccessful,
    } = this.state;
    const { navigation } = this.props
    return (
      <Container>
        <Header
          transparent
          hasTabs
          style={{
            backgroundColor: '#FFFFFF',
            borderBottomColor: '#0001',
            borderBottomWidth: 1.5,
          }}
          androidStatusBarColor={'#0001'}
          iosBarStyle="dark-content">
          <Left>
            <Button transparent onPress={() => navigation.navigate('home')}>
              <Icon
                style={{
                  paddingHorizontal: 10,
                  color: '#000000',
                }}
                name={'arrow-back'}
              />
            </Button>
          </Left>
          <Body style={styles.headtxtdiv}>
            <Text
              style={styles.headtxt}>
              Route Calculator
            </Text>
          </Body>
          <Right />
        </Header>
        <Content padder style={{ padding: 10 }}>
          <View style={{ padding: 10 }}>
            <Text
              style={{
                fontSize: 20,
                color: '#030E1A',
                fontFamily: 'Nunito-SemiBold',
              }}>
              Available orders along your current order route
            </Text>
          </View>
          <RouteList toggleOrderDetails={this.toggleOrderDetails} />
        </Content>
        {isOrderDetailsVisible && (
          <View
            style={{
              position: 'absolute',
              top: 0,
              bottom: 0,
              right: 0,
              left: 0,
              backgroundColor: '#ffffff',
            }}>
            <Header
              transparent
              hasTabs
              style={{
                backgroundColor: '#FFFFFF',
                borderBottomColor: '#0001',
                borderBottomWidth: 1.5,
              }}
              androidStatusBarColor={'#0001'}
              iosBarStyle="dark-content">
              <Left>
                <Button transparent onPress={this.toggleOrderDetails}>
                  <Icon name="arrow-back" style={{ color: '#030E1A' }} />
                </Button>
              </Left>
              <Body>
                <Text
                  numberOfLines={1}
                  style={{
                    fontSize: 18,
                    color: '#030E1A',
                    fontFamily: 'Nunito-SemiBold',
                  }}>
                  Order Details
                </Text>
              </Body>
              <Right />
            </Header>
            <Content>
              <View style={{ padding: 10 }}>
                <View>
                  <Left
                    style={{ alignSelf: 'flex-start', paddingHorizontal: 10 }}>
                    <View
                      style={{
                        backgroundColor:
                          'rgba(41, 47, 61, 0.10999999940395355)',
                        padding: 5,
                        borderRadius: 10,
                      }}>
                      <Text style={{ color: '#030E1A' }}>PENDING</Text>
                    </View>
                  </Left>
                </View>

                <View style={{ padding: 10 }}>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ flexDirection: 'column' }}>
                      <Icon
                        type="MaterialCommunityIcons"
                        name="map-marker"
                        style={{ color: '#030E1A', fontSize: 16 }}
                      />
                      <Text />
                      <Icon
                        type="MaterialCommunityIcons"
                        name="map-marker"
                        style={{ color: '#3FAF5D', fontSize: 16 }}
                      />
                    </View>

                    <View style={{ flexDirection: 'column', marginLeft: 10 }}>
                      <View>
                        <Text
                          style={{
                            fontFamily: 'Nunito-SemiBold',
                            color: '#030E1A',
                          }}>
                          5 Ogunbamila Street, Lady Lak, Bariga.
                        </Text>
                      </View>
                      <View>
                        <Text />
                      </View>
                      <View>
                        <Text
                          style={{
                            fontFamily: 'Nunito-SemiBold',
                            color: '#030E1A',
                          }}>
                          Akilo Street, Lady Lak, Bariga
                        </Text>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
              <View>
                <Image
                  source={getImage.map}
                  style={{ height: 120, width: null, flex: 1 }}
                />
              </View>
              <View style={{ padding: 20 }}>
                <View style={[styles.inputContainer, styles.containerBorder]}>
                  <View style={{ flexDirection: 'row', marginBottom: 5 }}>
                    <Left
                      style={{
                        flexDirection: 'row',
                        alignContent: 'flex-end',
                        alignItems: 'flex-end',
                      }}>
                      <View
                        style={{
                          backgroundColor: '#ffff',
                          borderRadius: 5,
                          padding: 2,
                          shadowColor: '#0000',
                          shadowOffset: {
                            width: 0,
                            height: 2,
                          },
                          shadowOpacity: 0.23,
                          shadowRadius: 2.0,
                          elevation: 4,
                        }}>
                        <Thumbnail
                          style={{ borderRadius: 5, height: 50, width: 50 }}
                          square
                          small
                          source={getImage.man_hat}
                        />
                      </View>
                      <View
                        style={{
                          marginHorizontal: 20,
                          justifyContent: 'flex-end',
                          alignContent: 'flex-end',
                        }}>
                        <Text
                          style={{
                            color: '#030E1A',
                            fontFamily: 'Nunito-SemiBold',
                          }}>
                          Gregory Samuel
                        </Text>
                        <Left
                          transparent
                          style={{
                            backgroundColor: '#1B2E5A',
                            flexDirection: 'row',
                            borderRadius: 18,
                            paddingHorizontal: 10,
                            alignSelf: 'flex-start',
                            // margin: 2,
                            // paddingVertical: -10,
                          }}>
                          <Text
                            style={{
                              color: '#ffff',
                              fontSize: 16,
                              alignSelf: 'center',
                              paddingHorizontal: 5,
                            }}>
                            Call
                          </Text>
                          <Icon
                            name="call"
                            style={{
                              color: '#ffffff',
                              fontSize: 16,
                              alignSelf: 'center',
                              paddingHorizontal: 5,
                            }}
                          />
                        </Left>
                      </View>
                    </Left>
                    <Right>
                      <Text
                        style={{
                          color: '#030E1A',
                          fontFamily: 'Nunito-SemiBold',
                          fontSize: 20,
                        }}>
                        N9,700.00
                      </Text>
                    </Right>
                  </View>
                </View>
                <View style={[styles.inputContainer, styles.containerBorder]}>
                  <Text style={styles.inputLabel2}>Additional Information</Text>
                  <View style={{ flex: 1, marginVertical: 2 }}>
                    <Text
                      style={{
                        fontSize: 16,
                        color: '#030E1A',
                        fontFamily: 'Nunito-Regular',
                      }}>
                      when an unknown printer took a galley of type and
                      scrambled it to make a type specimen book.
                    </Text>
                  </View>
                  <View style={{ flexDirection: 'row', marginVertical: 5 }}>
                    <View style={{ flex: 1 }}>
                      <Text>Person in Charge:</Text>
                    </View>
                    <View style={{ flex: 1 }}>
                      <Text style={{ marginLeft: -50 }}>Femi Ajiboye</Text>
                    </View>
                  </View>
                  <View style={{ flexDirection: 'row', marginVertical: 5 }}>
                    <View style={{ flex: 1 }}>
                      <Text>Phone Number</Text>
                    </View>
                    <View style={{ flex: 1 }}>
                      <Text style={{ marginLeft: -50 }}>070567376830</Text>
                    </View>
                  </View>
                </View>
                <View style={[styles.inputContainer, styles.containerBorder]}>
                  <Grid>
                    <Row>
                      <Col>
                        <Text style={styles.inputLabel2}>Size of Package</Text>
                        <Card transparent>
                          <View style={{ flexDirection: 'row' }}>
                            <Icon
                              type="AntDesign"
                              name="inbox"
                              style={styles.packageIcon}
                            />
                            <View>
                              <Text style={styles.packageSize}>Large</Text>
                              <Text style={styles.packageWeight}>
                                Up to 30kg
                              </Text>
                            </View>
                          </View>
                        </Card>
                      </Col>
                      <Col>
                        <Text style={styles.inputLabel2}>Quantity</Text>
                        <Card transparent>
                          <View>
                            <Text style={styles.inputText2}>2</Text>
                          </View>
                        </Card>
                      </Col>
                    </Row>
                  </Grid>
                </View>
                <View style={[styles.inputContainer, styles.containerBorder]}>
                  <Grid>
                    <Row>
                      <Col>
                        <Text style={styles.inputLabel2}>Total Distance</Text>
                        <Card transparent>
                          <View>
                            <Text style={styles.inputText2}>5.4 km</Text>
                          </View>
                        </Card>
                      </Col>
                      <Col>
                        <Text style={styles.inputLabel2}>
                          Est. Delivery time
                        </Text>
                        <Card transparent>
                          <View>
                            <Text style={styles.inputText2}>45 min</Text>
                          </View>
                        </Card>
                      </Col>
                    </Row>
                  </Grid>
                </View>

                <Button
                  block
                  primary
                  style={styles.submitBtn}
                // onPress={() => this.toggleCancelOther()}
                >
                  <Text style={styles.btnText}>ACCEPT ORDER</Text>
                </Button>
              </View>
            </Content>
          </View>
        )}
        {isCancelOtherVisible && (
          <View
            style={{
              position: 'absolute',
              top: 0,
              bottom: 0,
              right: 0,
              left: 0,
              backgroundColor: '#ffffff',
              zIndex: 2,
            }}>
            <Header
              transparent
              androidStatusBarColor={'#0001'}
              iosBarStyle="dark-content">
              <Left />
              <Body />
              <Right>
                <Button transparent onPress={this.toggleCancelOther}>
                  <Icon name="close" style={{ color: '#030E1A' }} />
                </Button>
              </Right>
            </Header>
            <Content>
              <View style={{ paddingHorizontal: 20 }}>
                <View>
                  <Text style={[styles.cancelOrderText, { fontSize: 20 }]}>
                    Cancel Order
                  </Text>
                </View>
                <View>
                  <Text
                    style={[
                      styles.cancelOrderText,
                      { fontSize: 18, marginVertical: 20 },
                    ]}>
                    You order is still within 5 minute after it was requested.
                  </Text>
                </View>
                <View style={styles.inputContainer}>
                  <Text
                    style={[
                      styles.cancelOrderText,
                      { fontSize: 16, margin: 5 },
                    ]}>
                    Why do you want to cancel?
                  </Text>
                  <Item regular style={styles.inputItem}>
                    <Picker
                      note
                      mode="dropdown"
                      // style={{ width: 120 }}
                      selectedValue={reasonType}
                      onValueChange={item =>
                        this.setState({ reasonType: item })
                      }>
                      <Picker.Item label="Select reason" value="" />
                      <Picker.Item label="1" value="Reason 1" />
                      <Picker.Item label="2" value="Reason 2" />
                      <Picker.Item label="3" value="Reason 3" />
                      <Picker.Item label="4" value="Reason 4" />
                      <Picker.Item label="5" value="Reason 5" />
                    </Picker>
                  </Item>
                </View>
                <View style={[styles.inputContainer]}>
                  <Text style={[styles.cancelOrderText, { fontSize: 16 }]}>
                    Tell us more (optional)
                  </Text>
                  <View
                    style={[
                      styles.inputItem,
                      {
                        flexDirection: 'column',
                        paddingHorizontal: 10,
                        height: 100,
                      },
                    ]}>
                    <View>
                      <TextInput
                        placeholder={'Add comment'}
                        placeholderTextColor={'#0005'}
                        multiline
                        onChangeText={text => this.setState({ addInfo: text })}
                        style={[styles.inputText, { padding: 0 }]}
                      />
                    </View>
                  </View>
                </View>
                <Button
                  block
                  primary
                  style={{ backgroundColor: '#3FAF5D', marginTop: 30 }}
                  onPress={() => this.toggleCancelOther()}>
                  <Text style={styles.btnText}>NEVERMIND</Text>
                </Button>
                <Button
                  block
                  primary
                  style={[styles.submitBtn, { marginTop: 20 }]}
                  onPress={() => this.cancelOther()}>
                  <Text style={styles.btnText}>CANCEL ORDER</Text>
                </Button>
              </View>
            </Content>
          </View>
        )}
        <Modal
          isVisible={isCancelSuccessful}
          coverScreen={true}
          style={{ backgroundColor: '#FFFFFF' }}
          backdropColor={'#FFFFFF'}>
          <View style={styles.modalContainer}>
            <View style={{ alignSelf: 'center' }}>
              <Icon name={'checkmark-circle'} style={styles.modalIcon} />
            </View>
            <Card transparent>
              <CardItem style={{ justifyContent: 'center' }}>
                <Text style={styles.modalTitle}>
                  Order cancelled successfully!
                </Text>
              </CardItem>

              <CardItem style={{ justifyContent: 'center' }}>
                <Text style={styles.modalSubtitle}>
                  Your order with Transaction/Payment ID xxxx has been cancelled
                </Text>
              </CardItem>

              <CardItem style={{ justifyContent: 'center' }}>
                <Text style={styles.modalSubtitle}>
                  You will be notified vai email of a successful refund
                </Text>
              </CardItem>

              <View style={{ padding: 16 }}>
                <Button
                  block
                  full
                  onPress={this.cancelDone}
                  style={styles.modalButton}>
                  <Text style={styles.modalButtonText}>DONE</Text>
                </Button>
              </View>
            </Card>
          </View>
        </Modal>
      </Container>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(RouteCalculator);
