/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, Dimensions } from 'react-native';
import { connect } from 'react-redux';

import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Segment,
  Content,
  Thumbnail,
  Item,
  Input,
  Card,
} from 'native-base';
import styles from './styles';
import { Actions } from 'react-native-router-flux';
import getImage from '../../../utils/getImage';
import { resetActionLogin } from '../../../navigation/ResetNavigation';
import { setLocation } from '../../../redux/action/home';
import setAvailable from '../../../apiService/available';
import logoutApiService from '../../../apiService/logout';
import LogOut from '../../LogOut';
import Snackbar from 'react-native-snackbar';

const { height, width } = Dimensions.get('window');

export class ProfileSetting extends Component {
  constructor(props) {
    super(props);

    this.state = {
      logoutModal: false,
      profileSegment: 1,
      editIsVisble: false,
    };
  }

  handleLogoutResult = res => {
    console.log('handleLogoutResult', res);

    const { props, refs } = this;
    if (res.success || res.message === 'Unauthorized') {
      props.signout();
      props.navigation.dispatch(resetActionLogin);
      props.navigation.navigate('login');
    } else {
      this.setState({ logoutModal: false }, () => {
        Snackbar.show({
          title: 'Error in logging out!',
          duration: Snackbar.LENGTH_SHORT,
          backgroundColor: 'red',
          color: 'white',
        });
      });
    }
  };

  handleLogout = () => {
    const { props } = this;
    logoutApiService(props.user.jwtAccessToken, this.handleLogoutResult);
  };

  setLogoutModal = () => {
    this.setState(prevState => {
      return { logoutModal: !prevState.logoutModal };
    });
  };

  setLogoutModal = () => {
    this.setState(prevState => {
      return { logoutModal: !prevState.logoutModal };
    });
  };

  toggleEdit = () => this.setState({ editIsVisble: !this.state.editIsVisble });

  toggleSegment = num => this.setState({ profileSegment: num });

  render() {
    const { profileSegment, editIsVisble } = this.state;
    const { user: userobj, navigation } = this.props;
    // console.log('userobj', userobj);

    return (
      <Container>
        <Header
          transparent
          hasTabs
          style={{
            backgroundColor: '#FFFFFF',
            borderBottomColor: '#0001',
            borderBottomWidth: 1.5,
          }}
          androidStatusBarColor={'#0001'}
          iosBarStyle="dark-content">
          <Left>
            <Button transparent onPress={() => navigation.navigate('home')}>
              <Icon
                style={{
                  paddingHorizontal: 10,
                  color: '#000000',
                }}
                name={'arrow-back'}
              />
            </Button>
          </Left>
          <Body>
            <Text
              style={{
                fontSize: 18,
                color: '#030E1A',
                fontFamily: 'Nunito-SemiBold',
              }}>
              Profile
            </Text>
          </Body>
          <Right />
          {/* <Right>
            <TouchableOpacity onPress={this.toggleEdit}>
              <Icon
                type={'AntDesign'}
                name={'edit'}
                style={{
                  color: '#030E1A',
                  fontSize: 20,
                  padding: 10,
                }}
              />
            </TouchableOpacity>
          </Right> */}
        </Header>
        <Segment style={{ backgroundColor: 'transparent', marginVertical: 10 }}>
          <Button
            first
            onPress={() => this.toggleSegment(1)}
            style={{
              borderColor: 'black',
              backgroundColor: profileSegment === 1 ? '#030E1A' : '#FFFFFF',
              justifyContent: 'center',
              borderTopLeftRadius: 5,
              borderBottomLeftRadius: 5,
              width: width / 3.15,
            }}>
            <Text
              style={{
                color: profileSegment === 1 ? '#FFFFFF' : '#030E1A',
              }}>
              Profile
            </Text>
          </Button>
          <Button
            last
            onPress={() => this.toggleSegment(2)}
            style={{
              borderColor: 'black',
              backgroundColor: profileSegment === 2 ? '#030E1A' : '#FFFFFF',
              justifyContent: 'center',
              width: width / 3.15,
            }}>
            <Text
              style={{
                color: profileSegment === 2 ? '#FFFFFF' : '#030E1A',
              }}>
              Rider Details
            </Text>
          </Button>
          <Button
            onPress={() => this.toggleSegment(3)}
            style={{
              backgroundColor: profileSegment === 3 ? '#030E1A' : '#FFFFFF',
              justifyContent: 'center',
              borderColor: 'black',
              borderTopRightRadius: 5,
              borderBottomRightRadius: 4,
              width: width / 3.15,
            }}
            last>
            <Text
              style={{
                color: profileSegment === 3 ? '#FFFFFF' : '#030E1A',
              }}>
              Security Settings
            </Text>
          </Button>
        </Segment>
        <Content padder contentContainerStyle={{ padding: 16 }}>
          {profileSegment === 1 && (
            <View style={{ flex: 1 }}>
              <View
                style={{
                  alignItems: 'center',
                }}>
                <View>
                  <Thumbnail
                    square
                    large
                    source={
                      userobj.profileImage
                        ? { uri: userobj.profileImage }
                        : getImage.secondDriver
                    }
                  />
                  {editIsVisble && (
                    <View
                      style={{
                        position: 'absolute',
                        right: -10,
                        bottom: -10,
                        borderRadius: 30,
                        borderColor: '#FFFFFF',
                        borderWidth: 1,
                        backgroundColor: '#1B2E5A',
                      }}>
                      <Icon
                        type={'AntDesign'}
                        name={'edit'}
                        style={{
                          color: '#FFFFFF',
                          fontSize: 16,
                          padding: 5,
                        }}
                      />
                    </View>
                  )}
                </View>
              </View>
              <View style={{ marginVertical: 30 }}>
                <View style={styles.inputContainer}>
                  <Text style={styles.inputLabel}>Full Name</Text>
                  <Item regular style={styles.inputItem}>
                    <Input
                      value={userobj.fullName}
                      style={styles.inputText}
                      disabled={!editIsVisble}
                    />
                  </Item>
                </View>

                <View style={styles.inputContainer}>
                  <Text style={styles.inputLabel}>Phone number</Text>
                  <Item regular style={styles.inputItem}>
                    <Input
                      value={`${userobj.phoneNumber}`}
                      style={styles.inputText}
                      disabled={!editIsVisble}
                    />
                  </Item>
                </View>

                <View style={styles.inputContainer}>
                  <Text style={styles.inputLabel}>Email Address</Text>
                  <Item regular style={styles.inputItem}>
                    <Input
                      value={`${userobj.email}`}
                      style={styles.inputText}
                      disabled={!editIsVisble}
                    />
                  </Item>
                </View>

                <View style={styles.inputContainer}>
                  <Text style={styles.inputLabel}>Home Address</Text>
                  <Item regular style={styles.inputItem}>
                    <Input
                      value={`${userobj.address}`}
                      style={styles.inputText}
                      disabled={!editIsVisble}
                    />
                  </Item>
                </View>
                {editIsVisble && (
                  <View style={{ marginVertical: 10 }}>
                    <Button
                      block
                      success
                      // style={{ backgroundColor: 'red' }}
                      onPress={this.toggleEdit}>
                      <Text
                        style={{
                          color: '#FFFFFF',
                          fontFamily: 'Nunito-SemiBold',
                        }}>
                        UPDATE
                      </Text>
                    </Button>
                  </View>
                )}

                <View style={{ marginVertical: 20 }}>
                  <Button
                    block
                    style={{ backgroundColor: 'red' }}
                    onPress={() => this.setLogoutModal()}>
                    <Text
                      style={{
                        color: '#FFFFFF',
                        fontFamily: 'Nunito-SemiBold',
                      }}>
                      LOGOUT
                    </Text>
                  </Button>
                </View>
              </View>
            </View>
          )}
          {profileSegment === 2 && (
            <View>
              <View style={{ justifyContent: 'center', marginVertical: 20 }}>
                <View style={styles.inputContainer}>
                  <Text style={styles.inputLabel}>License Number</Text>
                  <Item regular style={styles.inputItem}>
                    <Input
                      value={`${userobj.verificationDetails.licenseNumber}`}
                      placeholderTextColor={'#383838'}
                      style={styles.inputText}
                      disabled={!editIsVisble}
                    />
                  </Item>
                </View>
                <View style={styles.inputContainer}>
                  <Text style={styles.inputLabel}>National ID Card Number</Text>
                  <Item regular style={styles.inputItem}>
                    <Input
                      value={`${userobj.verificationDetails.nationalIdNumber}`}
                      placeholderTextColor={'#383838'}
                      style={styles.inputText}
                      disabled={!editIsVisble}
                    />
                  </Item>
                </View>
              </View>

              {editIsVisble && (
                <View style={{ marginVertical: 30 }}>
                  <Button block style={{ backgroundColor: '#B6B6B6' }}>
                    <Text
                      style={{
                        color: '#FFFFFF',
                        fontFamily: 'Nunito-SemiBold',
                      }}>
                      UPDATE
                    </Text>
                  </Button>
                </View>
              )}
            </View>
          )}
          {profileSegment === 3 && (
            <View>
              <Card
                transparent
                style={{ backgroundColor: '#F1F2F5', padding: 30 }}>
                <Text style={{ color: '#858585' }}>
                  Your verification information helps us secure your account.
                </Text>
              </Card>

              <View style={{ justifyContent: 'center', marginVertical: 20 }}>
                <View style={styles.inputContainer}>
                  <Text style={styles.inputLabel}>Account Name</Text>
                  <Item regular style={styles.inputItem}>
                    <Input
                      value={`${userobj.bankDetails.accountName}`}
                      placeholderTextColor={'#383838'}
                      style={styles.inputText}
                      disabled={!editIsVisble}
                    />
                  </Item>
                </View>

                <View style={styles.inputContainer}>
                  <Text style={styles.inputLabel}>Bank Name</Text>
                  <Item regular style={styles.inputItem}>
                    <Input
                      value={`${userobj.bankDetails.bankName}`}
                      placeholderTextColor={'#383838'}
                      style={styles.inputText}
                      disabled={!editIsVisble}
                    />
                  </Item>
                </View>

                <View style={styles.inputContainer}>
                  <Text style={styles.inputLabel}>Account Number</Text>
                  <Item regular style={styles.inputItem}>
                    <Input
                      value={`${userobj.bankDetails.accountNumber}`}
                      placeholderTextColor={'#383838'}
                      style={styles.inputText}
                      disabled={!editIsVisble}
                    />
                  </Item>
                </View>
              </View>

              {editIsVisble && (
                <View style={{ marginVertical: 30 }}>
                  <Button block style={{ backgroundColor: '#B6B6B6' }}>
                    <Text
                      style={{
                        color: '#FFFFFF',
                        fontFamily: 'Nunito-SemiBold',
                      }}>
                      UPDATE
                    </Text>
                  </Button>
                </View>
              )}
            </View>
          )}
        </Content>
        <LogOut
          logoutModal={this.state.logoutModal}
          signOutUser={this.handleLogout}
          setLogoutModal={this.setLogoutModal}
        />
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.currentUser,
    order: state.currentOrder,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    signout: () => {
      dispatch({ type: 'USER_SIGNOUT' });
      // dispatch({ type: 'CLEAR_EARNINGS' });
      // dispatch({ type: 'SET_EDIT_PROFILE' });
      // dispatch({ type: 'DUMP_ORDER' });
      // dispatch({ type: 'DUMP_STATUS' });
      // dispatch({ type: 'DUMP_REQUEST' });
      // dispatch({ type: 'CLEAR_ORDER_HISTORY' });
    },
    setLocDispatcher: coor => {
      dispatch(setLocation(coor));
    },
    setAvailableDispatcher: val => {
      dispatch(setAvailable(val));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileSetting);
