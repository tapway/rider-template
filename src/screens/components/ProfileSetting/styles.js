import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  inputContainer: {
    flex: 1,
    marginVertical: 10,
    // backgroundColor: 'red',
  },
  inputLabel: {
    fontSize: 18,
    fontWeight: '100',
    color: '#858F99',
    // opacity: 0.7,
  },
  inputItem: {
    color: '#383838',
    borderWidth: 0,
    fontSize: 22,
    marginTop: 3,
    paddingHorizontal: 10,
    height: 45,
    fontFamily: 'Nunito-Regular',
    backgroundColor: '#EFF4F5',
    borderColor: '#EFF4F5',
  },
  inputText: {
    color: '#383838',
    // opacity: 0.9,
  },
});

export default styles;
