/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { View, Text, TouchableNativeFeedback, TextInput } from 'react-native';
import { connect } from 'react-redux';
import {
  Container,
  Header,
  Left,
  Button,
  Icon,
  Body,
  Right,
  Content,
  Grid,
  Row,
  Col,
  Card,
  Item,
  Input,
  Picker,
  Thumbnail,
  CardItem,
  H1,
  Accordion,
  Segment,
} from 'native-base';
import { Actions } from 'react-native-router-flux';
import styles from './styles';

class Wallet extends Component {
  render() {
    return (
      <Container>
        <Header
          transparent
          hasTabs
          style={{
            backgroundColor: '#FFFFFF',
            borderBottomColor: '#0001',
            borderBottomWidth: 1.5,
          }}
          androidStatusBarColor={'#0001'}
          iosBarStyle="dark-content">
          <Left>
            <Button transparent onPress={() => navigation.navigate('home')}>
              <Icon
                style={{
                  paddingHorizontal: 10,
                  color: '#000000',
                }}
                name={'arrow-back'}
              />
            </Button>
          </Left>
          <Body>
            <Text
              style={{
                fontSize: 18,
                fontFamily: 'Nunito-SemiBold',
                color: '#030E1A',
              }}>
              Wallet
            </Text>
          </Body>
          <Right />
        </Header>
        <Content padder>
          <View>
            <View style={{ padding: 5 }}>
              <Card>
                <View
                  style={{
                    flex: 1,
                    flexDirection: 'column',
                    alignItems: 'center',
                  }}>
                  <View style={{ marginTop: 20 }}>
                    <Text style={styles.greenTitle}>Current Balance</Text>
                  </View>
                  <View style={{ marginVertical: 10 }}>
                    <Text style={styles.walletAmount}>N2,050.45</Text>
                  </View>
                  <View style={{ marginBottom: 20 }}>
                    <Button success style={styles.walletButton}>
                      <Text style={{ color: '#ffffff' }}>MAKE AN ORDER</Text>
                    </Button>
                  </View>
                </View>
              </Card>
            </View>
            <View style={{ flex: 1, padding: 5, flexDirection: 'row' }}>
              <View style={{ flex: 1, marginRight: 5 }}>
                <Card>
                  <View
                    style={{
                      flex: 1,
                      flexDirection: 'column',
                      alignItems: 'center',
                    }}>
                    <View style={{ marginTop: 20 }}>
                      <Text style={styles.greenTitle}>Total Withdrawn</Text>
                    </View>
                    <View style={{ marginVertical: 10 }}>
                      <Text style={styles.walletAmount}>N10,050.45</Text>
                    </View>
                  </View>
                </Card>
              </View>
              <View style={{ flex: 1, marginLeft: 5 }}>
                <Card>
                  <View
                    style={{
                      flex: 1,
                      flexDirection: 'column',
                      alignItems: 'center',
                    }}>
                    <View style={{ marginTop: 20 }}>
                      <Text style={styles.greenTitle}>Total Revenue</Text>
                    </View>
                    <View style={{ marginVertical: 10 }}>
                      <Text style={styles.walletAmount}>N12,100.45</Text>
                    </View>
                  </View>
                </Card>
              </View>
            </View>
            <View style={{ padding: 5 }}>
              <View>
                <Text style={styles.historyText}>Transactions History</Text>
              </View>
              <Card>
                <View
                  style={{
                    flex: 1,
                    flexDirection: 'row',
                    borderBottomColor: 'grey',
                    borderBottomWidth: 0.5,
                  }}>
                  <View style={{ flex: 1, flexDirection: 'row', margin: 5 }}>
                    <View style={{ margin: 5 }}>
                      <View
                        style={{
                          backgroundColor: '#E69494577',
                          padding: 5,
                          borderRadius: 50,
                          height: 25,
                          margin: 5,
                        }}>
                        <Icon
                          type="Feather"
                          name="rotate-ccw"
                          style={{ fontSize: 15, color: '#DA0A0A' }}
                        />
                      </View>
                    </View>
                    <View style={{ marginVertical: 5 }}>
                      <View>
                        <Text style={styles.historyTitle}>Made a request</Text>
                      </View>
                      <View>
                        <Text style={styles.historytype}>
                          Make an order request
                        </Text>
                      </View>
                    </View>
                  </View>

                  <View
                    style={{
                      flex: 1,
                      flexDirection: 'column',
                      alignItems: 'flex-end',
                      margin: 10,
                    }}>
                    <View>
                      <Text style={[styles.historyTitle, { color: '#C50404' }]}>
                        N10,500.00
                      </Text>
                    </View>
                    <View>
                      <Text style={styles.historytype}>3 Mar 2018</Text>
                    </View>
                  </View>
                </View>
                <View
                  style={{
                    flex: 1,
                    flexDirection: 'row',
                  }}>
                  <View style={{ flex: 1, flexDirection: 'row', margin: 5 }}>
                    <View style={{ margin: 5 }}>
                      <View
                        style={{
                          backgroundColor: '#00A52C55',
                          padding: 5,
                          borderRadius: 50,
                          height: 25,
                          margin: 5,
                        }}>
                        <Icon
                          type="Feather"
                          name="plus"
                          style={{
                            fontSize: 15,
                            color: '#3FAF5D',
                            fontWeight: 'bold',
                            zIndex: 2,
                          }}
                        />
                      </View>
                    </View>
                    <View style={{ marginVertical: 5 }}>
                      <View>
                        <Text style={styles.historyTitle}>Top up</Text>
                      </View>
                      <View>
                        <Text style={styles.historytype}>Refund</Text>
                      </View>
                    </View>
                  </View>

                  <View
                    style={{
                      flex: 1,
                      flexDirection: 'column',
                      alignItems: 'flex-end',
                      margin: 10,
                    }}>
                    <View>
                      <Text style={[styles.historyTitle, { color: '#00A52C' }]}>
                        N12,100.00
                      </Text>
                    </View>
                    <View>
                      <Text style={styles.historytype}>3 Mar 2018</Text>
                    </View>
                  </View>
                </View>
              </Card>
            </View>
          </View>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Wallet);
