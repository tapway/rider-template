import { StyleSheet } from 'react-native';
import { FontNames } from '../../../../theme';
import { RFPercentage, RFValue } from 'react-native-responsive-fontsize';
const styles = StyleSheet.create({
  inputContainer: {
    flex: 1,
    marginVertical: 10,
    // backgroundColor: 'red',
  },
  containerBorder: {
    borderBottomColor: '#DBDBDB',
    borderBottomWidth: 1,
    paddingBottom: 5,
  },
  headerbottom:{
    backgroundColor: '#FFFFFF',
    borderBottomColor: '#0001',
    borderBottomWidth: 0.5,
  },
  headtxtdiv:{position:'absolute', left:0,right:0,top:RFPercentage(5.5),zIndex:-100},
  headtxt:{
    fontSize: 18,
    color: '#030E1A',
    width:'100%',
    textAlign:'center',
    //paddingLeft:40,
    fontFamily: FontNames.semibold,
  },
  intransit:{
    backgroundColor:
      '#f9f2e4',
    padding: 4,
    borderRadius: 15,
  },
  intransitxt:{ color: '#D4A53D',fontFamily:FontNames.semibold, paddingHorizontal:RFValue(10) },
  inputLabel: {
    fontSize: 20,
    fontWeight: '100',
    color: '#858F99',
    marginVertical: 5,
    // opacity: 0.7,
  },
  inputLabel2: {
    fontSize: 16,
    fontFamily: FontNames.semibold,
    color: '#858F99',
    marginVertical: 2,
    // opacity: 0.7,
  },
  inputItem: {
    color: '#383838',
    borderWidth: 0,
    fontSize: 22,
    marginTop: 3,
    paddingHorizontal: 10,
    height: 45,
    fontFamily: 'Nunito-Regular',
    backgroundColor: '#EFF4F5',
    borderColor: '#EFF4F5',
  },
  inputText: {
    color: '#383838',
    // opacity: 0.9,
  },
  inputText2: {
    color: '#383838',
    fontSize: 18,
    fontFamily: FontNames.regular,
    marginVertical: RFValue(6),
  },
  packageCard: {
    padding: 5,
    elevation: 1,
  },
  selectedCard: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.9,
    shadowRadius: 4.95,

    elevation: 10,
  },
  packageIcon: {
    color: '#A98258',
    fontSize: 50,
    alignSelf: 'center',
  },
  packageSize: {
    color: '#030E1A',
    fontSize: 20,
    fontFamily: FontNames.semibold,
    alignSelf: 'center',
  },
  packageWeight: {
    color: '#030E1A',
    fontSize: 18,
    fontFamily: FontNames.regular,
   paddingLeft: RFValue(25),
    opacity: 0.4,
  },
  submitBtn: {
    backgroundColor: '#B6B6B6',
    borderRadius: 4,
    elevation:0,
    marginVertical: 30,
  },
  btnText: {
    color: '#FFFFFF',
    fontSize: 18,
    fontFamily: 'Nunito-SemiBold',
    fontWeight: '400',
    letterSpacing: 0.5,
  },
  cancelOrderText: { fontFamily: 'Nunito-SemiBold', color: '#030E1A' },
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignContent: 'center',
  },
  modalIcon: { fontSize: 180, color: '#1B2E5A' },
  modalTitle: {
    textAlign: 'center',
    color: '#030E1A',
    fontSize: 25,
    fontFamily: 'Nunito-Bold',
  },
  modalSubtitle: {
    textAlign: 'center',
    color: '#858F99',
    fontSize: 16,
  },
  modalButton: {
    backgroundColor: '#3FAF5D',
    borderRadius: 5,
    padding: 50,
  },
  modalButtonText: {
    color: '#FFFFFF',
    fontSize: 16,
    letterSpacing: 0.2,
  },
});

export default styles;
