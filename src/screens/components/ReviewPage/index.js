/* eslint-disable prettier/prettier */
/* eslint-disable react/jsx-no-undef */
/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput } from 'react-native';
import { connect } from 'react-redux';
import Image from 'react-native-remote-svg';
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Content,
  Thumbnail,
  Item,
  Input,
  Card,
  Grid,
  Row,
  Col,
  Picker,
  CardItem,
  //   Image,
} from 'native-base';
import styles from './styles';
import { Actions } from 'react-native-router-flux';
import StarRating from 'react-native-star-rating';
import Modal from 'react-native-modal';
import getImage from '../../../utils/getImage';

class ReviewPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      starCount: 0,
      isSubmitReviewSuccessful: false,
    };
  }

  onStarRatingPress(rating) {
    this.setState({
      starCount: rating,
    });
  }

  toggleSubmitReview = () => {
    this.setState({
      isSubmitReviewSuccessful: !this.state.isSubmitReviewSuccessful,
    });
  };

  render() {
    const { isSubmitReviewSuccessful } = this.state;
    const {navigation}= this.props
    return (
      <Container>
        <Header
          transparent
          androidStatusBarColor={'#0001'}
          iosBarStyle="dark-content">
          <Left />
          <Body />
          <Right>
            <Button transparent onPress={() => navigation.goBack()}>
              <Icon name="close" style={{ color: '#030E1A' }} />
            </Button>
          </Right>
        </Header>
        <Content>
          <View style={{ paddingHorizontal: 20 }}>
            <View style={{ alignItems: 'center' }}>
              <Thumbnail square large source={getImage.man} />
            </View>
            <View>
              <Text
                style={[
                  styles.cancelOrderText,
                  { fontSize: 18, marginVertical: 20, textAlign: 'center' },
                ]}>
                Your package was just recently delivered by Gregory, rate your
                experience
              </Text>
            </View>

            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              <Text>Today at 11:00am</Text>
              <Text>N9,700.00</Text>
            </View>

            <View style={{ alignItems: 'center', margin: 20 }}>
              <StarRating
                disabled={false}
                maxStars={5}
                starSize={30}
                // emptyStarColor={'red'}
                // starStyle={{ color: 'red' }}
                rating={this.state.starCount}
                selectedStar={rating => this.onStarRatingPress(rating)}
              />
            </View>

            <View style={[styles.inputContainer]}>
              <Text style={[styles.cancelOrderText, { fontSize: 16 }]}>
                Enter comment:
              </Text>
              <View
                style={[
                  styles.inputItem,
                  {
                    flexDirection: 'column',
                    paddingHorizontal: 10,
                    height: 100,
                  },
                ]}>
                <View>
                  <TextInput
                    placeholder={'Add comment'}
                    placeholderTextColor={'#0005'}
                    multiline
                    onChangeText={text => this.setState({ addInfo: text })}
                    style={[styles.inputText, { padding: 0 }]}
                  />
                </View>
              </View>
            </View>

            <View style={[styles.inputContainer]}>
              <Text
                style={[styles.cancelOrderText, { fontSize: 16, margin: 5 }]}>
                Upload photos(Optional)
              </Text>
              <View
                style={[
                  styles.inputItem,
                  { alignItems: 'center', paddingVertical: 10 },
                ]}>
                <Icon name="camera" />
                <Text>Click to add photo</Text>
              </View>
            </View>

            <Button
              block
              primary
              style={{ backgroundColor: '#3FAF5D', marginTop: 30 }}
              onPress={() => this.toggleSubmitReview()}>
              <Text style={styles.btnText}>SUBMIT REVIEW</Text>
            </Button>
            {/* <Button
              block
              primary
              style={[styles.submitBtn, { marginTop: 20 }]}
              onPress={() => Actions.RefundPage()}>
              <Text style={styles.btnText}>RAISE A COMPLAINT</Text>
            </Button> */}
          </View>
        </Content>
        <Modal
          isVisible={isSubmitReviewSuccessful}
          coverScreen={true}
          style={{ backgroundColor: '#FFFFFF' }}
          backdropColor={'#FFFFFF'}>
          <View style={styles.modalContainer}>
            <View style={{ alignSelf: 'center' }}>
              <Icon name={'checkmark-circle'} style={styles.modalIcon} />
            </View>
            <Card transparent>
              <CardItem style={{ justifyContent: 'center' }}>
                <Text style={styles.modalTitle}>
                  Thank you for your feedback
                </Text>
              </CardItem>

              <CardItem style={{ justifyContent: 'center' }}>
                <Text style={styles.modalSubtitle}>
                  You can share your feedback on your social media profile. It
                  could help other users.
                </Text>
              </CardItem>
              {/* <CardItem> */}
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  alignContent: 'center',
                  justifyContent: 'center',
                }}>
                <Button
                  style={{
                    borderRadius: 50,
                    height: 50,
                    width: 50,
                    alignSelf: 'center',
                    marginHorizontal: 2,
                    backgroundColor: '#3B5998',
                  }}>
                  <Icon
                    type="MaterialCommunityIcons"
                    name="facebook"
                    style={{ fontSize: 25, textAlign: 'center' }}
                  />
                </Button>
                <Button
                  style={{
                    borderRadius: 50,
                    height: 50,
                    width: 50,
                    marginHorizontal: 2,
                    backgroundColor: '#55ACEE',
                    alignSelf: 'center',
                  }}>
                  <Icon
                    type="MaterialCommunityIcons"
                    name="twitter"
                    style={{ fontSize: 25, textAlign: 'center' }}
                  />
                </Button>
                <Button
                  style={{
                    borderRadius: 50,
                    height: 50,
                    width: 50,
                    marginHorizontal: 2,
                    backgroundColor: '#25D366',
                    alignSelf: 'center',
                  }}>
                  <Icon
                    type="MaterialCommunityIcons"
                    name="whatsapp"
                    style={{ fontSize: 25, textAlign: 'center' }}
                  />
                </Button>
                <Button
                  style={{
                    borderRadius: 50,
                    height: 50,
                    width: 50,
                    marginHorizontal: 2,
                    backgroundColor: '#0C88B1',
                    alignSelf: 'center',
                  }}>
                  <Icon
                    type="MaterialCommunityIcons"
                    name="share-variant"
                    style={{ fontSize: 25, textAlign: 'center' }}
                  />
                </Button>
              </View>
              {/* </CardItem> */}

              <View style={{ padding: 16 }}>
                <Button
                  block
                  full
                  onPress={() => {
                    this.setState({ isSubmitReviewSuccessful: false }, () => {
                      navigation.navigate('HomePage');
                    });
                  }}
                  style={styles.modalButton}>
                  <Text style={styles.modalButtonText}>DONE</Text>
                </Button>
              </View>
            </Card>
          </View>
        </Modal>
      </Container>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(ReviewPage);
