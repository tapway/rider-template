import { StyleSheet } from 'react-native';
import {RFValue} from "react-native-responsive-fontsize";
import { FontNames } from '../../../../theme';

const styles = StyleSheet.create({
  listText: {
    color: '#030E1A',
    fontFamily: FontNames.semibold,
    fontSize: RFValue(16),
  },
  drawerbg:{width:RFValue(350), height:RFValue(400),right:0, position:'absolute',top:RFValue(-150), left:0},
  listIcon: {
    color: '#1B2E5A',
    marginRight: RFValue(10),
    fontFamily: 'Nunito-SemiBold',
  },
  linerow:{marginBottom:RFValue(9)}
});

export default styles;
