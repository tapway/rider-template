/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { View, Text, Alert, Image, TouchableOpacity, Switch } from 'react-native';
import { connect } from 'react-redux';
import {
  Container,
  Content,
  Header,
  Icon,
  Left,
  Body,
  Right,
  Card,
  Thumbnail,
  Button,
  CardItem,
} from 'native-base';
import { RFValue } from "react-native-responsive-fontsize";
import setAvailableDb from '../../../apiService/available';
import styles from './styles';
import getImage from '../../../utils/getImage';
import { setAvailable } from '../../../redux/action/home';
import { ScrollView } from 'react-native-gesture-handler';

export class DrawerContent extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  toggleSwitch1 = value => {
    const { props } = this;
    props.setAvailableDispatcher(value);
    setAvailableDb(value, props.user.jwtAccessToken);
  };
  render() {
    const { user: userobj, navigation } = this.props;
    return (
      <Container style={{ backgroundColor: '#1b2e5a' }}>
        <Header transparent androidStatusBarColor="transparent">
          <Left>
            <Button transparent onPress={() => navigation.closeDrawer()}>
              <Icon name="close" style={{ color: '#FFFF' }} />
            </Button>
          </Left>
          <Body />
          <Right />
        </Header>
        <Card transparent>
          <Image source={require('../../../../assets/drawerBG.png')} resizeMode='contain' style={styles.drawerbg} />
          <View style={{ flexDirection: 'row', padding: 16 }}>
            <Left
              style={{
                flexDirection: 'column',
                alignContent: 'flex-start',
                alignItems: 'flex-start',
              }}>
              <View
                style={{
                  backgroundColor: '#ffff',
                  borderRadius: 5,
                  padding: 2,
                  shadowColor: '#0000',
                  shadowOffset: {
                    width: 0,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 2.0,
                  elevation: 4,
                }}>
                <Thumbnail
                  style={{ borderRadius: 5, height: 50, width: 50 }}
                  square
                  large
                  source={
                    userobj.profileImage
                      ? { uri: userobj.profileImage }
                      : getImage.secondDriver
                  }
                />
              </View>
              <View
                style={{
                  justifyContent: 'flex-end',
                  alignContent: 'flex-end',
                }}>
                <Text
                  style={{
                    color: '#ffffff',
                    fontFamily: 'Nunito-SemiBold',
                    fontSize: 20,
                    marginTop: 10,
                  }}>
                  {userobj.fullName}
                </Text>
                <TouchableOpacity
                  onPress={() => {
                    navigation.closeDrawer();
                    navigation.navigate('ProfileSetting');
                  }}>
                  <Text
                    style={{
                      color: '#ffffff',
                      opacity: 0.8,
                      fontSize: 15,
                    }}>
                    View Profile
                  </Text>
                </TouchableOpacity>
                <View
                  style={{
                    flexDirection: 'row',
                    width: 250,
                    justifyContent: 'space-between',
                  }}>
                  <Text
                    style={{
                      color: '#ffffff',
                      fontSize: 15,
                      marginTop: 10,
                    }}>
                    You're {userobj && userobj.isAvailable ? 'Online' : 'Offline'}
                  </Text>
                  <Switch
                    value={userobj.isAvailable}
                    onValueChange={this.toggleSwitch1}
                  />
                </View>
              </View>
            </Left>
          </View>
        </Card>
        <Content style={{ backgroundColor: '#FFFF', paddingTop: RFValue(20) }}>
          <ScrollView>
            <TouchableOpacity style={styles.linerow} onPress={() => navigation.navigate('home')}>
              <CardItem>
                <Icon name="home" style={styles.listIcon} />
                <Text style={styles.listText}>Home</Text>
              </CardItem>
            </TouchableOpacity>

            <TouchableOpacity style={styles.linerow} onPress={() => navigation.navigate('OrderHistoryMenu')}>
              <CardItem>
                <Icon
                  type="MaterialCommunityIcons"
                  name="restore-clock"
                  style={styles.listIcon}
                />
                <Text style={styles.listText}>Order History</Text>
              </CardItem>
            </TouchableOpacity>

            <TouchableOpacity style={styles.linerow} onPress={() => navigation.navigate('RiderPayment')}>
              <CardItem>
                <Icon
                  type="MaterialCommunityIcons"
                  name="credit-card"
                  style={[styles.listIcon]}
                />
                <Text style={[styles.listText]}>
                  Rider Payment
                </Text>
              </CardItem>
            </TouchableOpacity>

            <TouchableOpacity style={styles.linerow} onPress={() => navigation.navigate('RouteCalculator')}>
              <CardItem>
                <Icon
                  type="MaterialCommunityIcons"
                  name="map-marker-path"
                  style={[styles.listIcon, { marginRight: 10, marginLeft: -5 }]}
                />
                <Text style={[styles.listText, { paddingLeft: 9 }]}>
                  New orders
                </Text>
              </CardItem>
            </TouchableOpacity>

            <TouchableOpacity style={styles.linerow} onPress={() => navigation.navigate('earning')}>
              <CardItem>
                <Icon
                  type="MaterialCommunityIcons"
                  name="cash"
                  style={[styles.listIcon, ]}
                />
                <Text style={[styles.listText, { paddingLeft: 5 }]}>
                  Earnings
                </Text>
              </CardItem>
            </TouchableOpacity>

            {/* <TouchableOpacity style={styles.linerow} onPress={() => Actions.Wallet()}>
              <CardItem>
                <Icon name="wallet" style={styles.listIcon} />
                <Text style={styles.listText}>Rider Payment</Text>
              </CardItem>
            </TouchableOpacity> */}

            <TouchableOpacity style={styles.linerow} onPress={() => navigation.navigate('FAQ')}>
              <CardItem>
                <Icon
                  type="MaterialCommunityIcons"
                  name="comment-question"
                  style={styles.listIcon}
                />
                <Text style={styles.listText}>FAQs</Text>
              </CardItem>
            </TouchableOpacity>

            <TouchableOpacity style={styles.linerow} onPress={() => navigation.navigate('CustomerCare')}>
              <CardItem>
                <Icon
                  type="AntDesign"
                  name="customerservice"
                  style={styles.listIcon}
                />
                <Text style={styles.listText}>Customer Care</Text>
              </CardItem>
            </TouchableOpacity>

            <TouchableOpacity style={styles.linerow} onPress={() => navigation.navigate('Review')}>
              <CardItem>
                <Icon type="AntDesign" name="star" style={styles.listIcon} />
                <Text style={styles.listText}>Review</Text>
              </CardItem>
            </TouchableOpacity>

            <CardItem />
          </ScrollView>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.currentUser,
    order: state.currentOrder,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    signout: () => {
      dispatch({ type: 'USER_SIGNOUT' });
      // dispatch({ type: 'CLEAR_EARNINGS' });
      // dispatch({ type: 'SET_EDIT_PROFILE' });
      // dispatch({ type: 'DUMP_ORDER' });
      // dispatch({ type: 'DUMP_STATUS' });
      // dispatch({ type: 'DUMP_REQUEST' });
      // dispatch({ type: 'CLEAR_ORDER_HISTORY' });
    },

    setAvailableDispatcher: val => {
      dispatch(setAvailable(val));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DrawerContent);
