/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import {
  Container,
  Content,
  Button,
  Item,
  Input,
  Text,
  FooterTab,
  Icon,
  Header,
  Left,
  Body,
  Right,
} from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import {
  commonStyles,
  loginStyles,
  signUpStyles,
} from '../../assets/css/styles';
import { Actions } from 'react-native-router-flux';
import styles from './styles';

export class SignUp extends Component {
  render() {
    const {navigation}= this.props
    return (
      <Container style={styles.container}>
        <Header transparent androidStatusBarColor={'#0003'}>
          <Left style={{ paddingHorizontal: 5 }}>
            <Button transparent onPress={() => navigation.navigate('login')}>
              <Icon name={'close'} style={styles.closeIcon} />
            </Button>
          </Left>
          <Body />
          <Right />
        </Header>
        <Content
          padder
          style={{ flex: 1, alignContent: 'space-between', padding: 10 }}>
          <View style={{ flex: 1 }}>
            <Text style={signUpStyles.welcomeHd}>Create account </Text>
          </View>

          <Grid>
            <Row>
              <Col>
                <Text style={signUpStyles.textLabel}>First Name</Text>
                <Item regular style={signUpStyles.textInput}>
                  <Input />
                </Item>
              </Col>
              <Col>
                <Text style={signUpStyles.textLabel}>Last Name</Text>
                <Item regular style={signUpStyles.textInput}>
                  <Input />
                </Item>
              </Col>
            </Row>
            <Row>
              <Col>
                <Text style={signUpStyles.textLabel}>Email ID</Text>
                <Item regular style={signUpStyles.textInput}>
                  <Input />
                </Item>
              </Col>
            </Row>
            <Row>
              <Col>
                <Text style={signUpStyles.textLabel}>Phone Number</Text>
                <Item regular style={signUpStyles.textInput}>
                  <Input />
                </Item>
              </Col>
            </Row>
            <Row>
              <Col>
                <Text style={signUpStyles.textLabel}>Password</Text>
                <Item regular style={signUpStyles.textInput}>
                  <Input />
                </Item>
              </Col>
            </Row>
            <Row>
              <Col>
                <Button
                  block
                  primary
                  style={signUpStyles.submitBtn}
                  onPress={() => navigation.navigate('VerifySignUp')}>
                  <Text style={signUpStyles.btnText}>SIGN UP</Text>
                </Button>
              </Col>
            </Row>
          </Grid>

          <View style={{ flex: 1, bottom: 0 }}>
            <Text
              style={{
                color: '#000000',
                textAlign: 'center',
                marginTop: 30,
                marginBottom: 10,
              }}>
              <Text style={[commonStyles.regularFont]}>
                By signing up, you agree with the{' '}
              </Text>
              <Text style={[commonStyles.greenColor, commonStyles.regularFont]}>
                Terms of Service{' '}
              </Text>
              <Text style={[commonStyles.regularFont]}>and </Text>
              <Text style={[commonStyles.greenColor, commonStyles.regularFont]}>
                Privacy Policy{' '}
              </Text>
            </Text>
          </View>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SignUp);
