/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { View, Text, StatusBar, } from 'react-native';
import { connect } from 'react-redux';
import Image from 'react-native-remote-svg';
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Content,
  Card,
  //   Image,
} from 'native-base';
import styles from './styles';
import getImage from '../../../utils/getImage';
import { FontNames } from '../../../../theme';
import { RFPercentage, RFValue } from 'react-native-responsive-fontsize';
import DatePicker from "react-native-datepicker"
import moment from 'moment';

class RiderPayment extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isOrderDetailsVisible: false,
      past_wk_from:new Date(),
      past_wk_to:new Date(),
      past_mth_from:new Date(),
      past_mth_to:new Date()
    };
  }
  componentDidMount() {
    //StatusBar.setBarStyle( 'light-content',true)
    StatusBar.setBackgroundColor("#FFF")
  }
  updateDate=(key, date)=> {
    this.setState({[key]:date})
  }
  render() {
    const {
      isOrderDetailsVisible,
    } = this.state;
    const { navigation } = this.props
    return (
      <Container>
        <Header
          transparent
          hasTabs
          style={styles.headerbottom}
          androidStatusBarColor={'#0001'}
          iosBarStyle="dark-content">
          <Left>
            <Button transparent onPress={() => navigation.openDrawer()}>
              <Icon
                style={{ paddingHorizontal: 10, color: '#000000' }}
                name={'menu'}
              />
            </Button>
          </Left>
          <Body style={styles.headtxtdiv}>
            <Text
              style={styles.headtxt}>
              Rider Payment
            </Text>
          </Body>
          <Right />
        </Header>
        <Content padder style={{ padding: 10, backgroundColor: '#f7f7f7' }}>
          <View>
            <Card style={styles.cardiv}>
              <Text style={styles.cardhead_txt}>Pick your withdrawal preference</Text>
              <View style={styles.cardinnerdiv}>
                <Text style={styles.cardinner_txt}>Pickup + Delivery for the past week</Text>
                <View style={styles.calendar_div}>
                  <DatePicker style={styles.calendar_input}
                    date={ this.state.past_wk_from} androidMode="spinner" mode="date" showIcon={true} iconSource={getImage.calendaricon} confirmBtnText="OK" cancelBtnText="CANCEL" format="MMM DD, YYYY" maxDate={moment().format('YYYY-MM-DD')}
                    //placeholder="Date of Birth"
                    customStyles={{ dateIcon:styles.calendaricon, dateInput:styles.datebr,dateText: styles.calendartext,
                      //placeholderText: styles.calendartext
                    }}
                    onDateChange={date => { this.updateDate('past_wk_from',date) }}
                  />
                  <Text style={{ color: '#030E1A', fontFamily: FontNames.semibold, fontSize: RFValue(15) }}>-</Text>
                  <DatePicker style={styles.calendar_input}
                    date={ this.state.past_wk_to} androidMode="spinner" mode="date" showIcon={true} iconSource={getImage.calendaricon} confirmBtnText="OK" cancelBtnText="CANCEL" format="MMM DD, YYYY" maxDate={moment().format('YYYY-MM-DD')}
                    //placeholder="Date of Birth"
                    customStyles={{ dateIcon:styles.calendaricon, dateInput:styles.datebr,dateText: styles.calendartext,
                      //placeholderText: styles.calendartext
                    }}
                    onDateChange={date => { this.updateDate('past_wk_to',date) }}
                  />
                </View>
                <Button
                  //block
                  //primary
                  style={[styles.submitBtn, { backgroundColor: '#3FAF5D', alignContent: 'center', alignSelf: 'center' }]}
                  onPress={() => navigation.navigate('RiderPaymentDetails')}
                >
                  <Text style={[styles.btnText, { paddingHorizontal: 17, letterSpacing: 1 }]}>CALCULATE</Text>
                </Button>

              </View>
              <View style={styles.cardinnerdiv}>
                <Text style={styles.cardinner_txt}>Pickup + Delivery for the past month</Text>
                <View style={styles.calendar_div}>
                <DatePicker style={styles.calendar_input}
                    date={ this.state.past_mth_from} androidMode="spinner" mode="date" showIcon={true} iconSource={getImage.calendaricon} confirmBtnText="OK" cancelBtnText="CANCEL" format="MMM DD, YYYY" maxDate={moment().format('YYYY-MM-DD')}
                    //placeholder="Date of Birth"
                    customStyles={{ dateIcon:styles.calendaricon, dateInput:styles.datebr,dateText: styles.calendartext,
                      //placeholderText: styles.calendartext
                    }}
                    onDateChange={date => { this.updateDate('past_mth_from',date) }}
                  />
                  <Text style={{ color: '#030E1A', fontFamily: FontNames.semibold, fontSize: RFValue(15) }}>-</Text>
                  <DatePicker style={styles.calendar_input}
                    date={ this.state.past_mth_to} androidMode="spinner" mode="date" showIcon={true} iconSource={getImage.calendaricon} confirmBtnText="OK" cancelBtnText="CANCEL" format="MMM DD, YYYY" maxDate={moment().format('YYYY-MM-DD')}
                    //placeholder="Date of Birth"
                    customStyles={{ dateIcon:styles.calendaricon, dateInput:styles.datebr,dateText: styles.calendartext,
                      //placeholderText: styles.calendartext
                    }}
                    onDateChange={date => { this.updateDate('past_mth_to',date) }}
                  />
                </View>
                <Button
                  //block
                  //primary
                  style={[styles.submitBtn, { backgroundColor: '#3FAF5D', alignContent: 'center', alignSelf: 'center' }]}
                  onPress={() => navigation.navigate('RiderPaymentDetails')}
                >
                  <Text style={[styles.btnText, { paddingHorizontal: 17, letterSpacing: 1 }]}>CALCULATE</Text>
                </Button>

              </View>
            </Card>
            <Text style={[styles.cardhead_txt, { paddingTop: 30, paddingBottom: 15 }]}>Previous Withdrawals</Text>
            <Card style={[styles.cardiv, { paddingHorizontal: 0, paddingVertical: 0 }]}>
              <View style={styles.rowdiv}>
                <View style={styles.rowbox}>
                  <View style={styles.rowleft}>
                    <Image source={getImage.orange_refresh} resizeMode='contain' style={styles.refresh_icon} />
                    <View>
                      <Text style={styles.cardinner_txt2}>Withdrawl Request</Text>
                      <Text style={[styles.cardinner_txt2, styles.cardinner_txt3]}>Order Processing</Text>
                    </View>
                  </View>
                  <View style={styles.rowright}>
                    <View>
                      <Text style={[styles.cardinner_txt2, { color: '#E88200', paddingLeft: 0 }]}>N10,500.00</Text>
                      <Text style={[styles.cardinner_txt2, styles.cardinner_txt4]}>3 Mar 2018</Text>
                    </View>
                  </View>
                </View>
              </View>
              <View style={styles.rowdiv}>
                <View style={styles.rowbox}>
                  <View style={styles.rowleft}>
                    <Image source={getImage.red_refresh} resizeMode='contain' style={styles.refresh_icon} />
                    <View>
                      <Text style={styles.cardinner_txt2}>Withdrawl Request</Text>
                      <Text style={[styles.cardinner_txt2, styles.cardinner_txt3]}>Order Cancelled</Text>
                    </View>
                  </View>
                  <View style={styles.rowright}>
                    <View>
                      <Text style={[styles.cardinner_txt2, { color: '#C50404', paddingLeft: 0 }]}>N10,500.00</Text>
                      <Text style={[styles.cardinner_txt2, styles.cardinner_txt4]}>3 Mar 2018</Text>
                    </View>
                  </View>
                </View>
              </View>
              <View style={styles.rowdiv}>
                <View style={styles.rowbox}>
                  <View style={styles.rowleft}>
                    <Image source={getImage.green_refresh} resizeMode='contain' style={styles.refresh_icon} />
                    <View>
                      <Text style={styles.cardinner_txt2}>Withdrawl Request</Text>
                      <Text style={[styles.cardinner_txt2, styles.cardinner_txt3]}>Order Processed</Text>
                    </View>
                  </View>
                  <View style={styles.rowright}>
                    <View>
                      <Text style={[styles.cardinner_txt2, { color: '#009839', paddingLeft: 0 }]}>N10,500.00</Text>
                      <Text style={[styles.cardinner_txt2, styles.cardinner_txt4]}>3 Mar 2018</Text>
                    </View>
                  </View>
                </View>
              </View>
            </Card>
          </View>
        </Content>

      </Container>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RiderPayment);
