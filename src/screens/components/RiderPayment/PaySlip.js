/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput, StatusBar, Linking } from 'react-native';
import { connect } from 'react-redux';
import Image from 'react-native-remote-svg';
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Content,
  Card,
  //   Image,
} from 'native-base';
import styles from './styles';
import getImage from '../../../utils/getImage';
import { FontNames } from '../../../../theme';
import { RFPercentage, RFValue } from 'react-native-responsive-fontsize';

class RiderPaymentSlip extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isOrderDetailsVisible: false,
    };
  }
  componentDidMount() {
    //StatusBar.setBarStyle( 'light-content',true)
    StatusBar.setBackgroundColor("#FFF")
  }
  render() {
    const {
      isOrderDetailsVisible,
    } = this.state;
    const { navigation } = this.props
    return (
      <Container>
        <Header
          transparent
          hasTabs
          style={styles.headerbottom}
          androidStatusBarColor={'#0001'}
          iosBarStyle="dark-content">
          <Left>
            <Button transparent onPress={() => navigation.pop()}>
              <Icon
                style={{
                  paddingHorizontal: 10,
                  color: '#000000',
                }}
                name={'arrow-back'}
              />
            </Button>
          </Left>
          <Body style={styles.headtxtdiv}>
            <Text
              style={styles.headtxt}>
              Rider Payment
            </Text>
          </Body>
          <Right />
        </Header>
        <Content padder style={{ padding: 10, backgroundColor: '#f7f7f7' }}>
          <View>
            <Card style={[styles.cardiv, {paddingHorizontal:0,paddingVertical:0}]}>
              <View style={{backgroundColor:'#F4F4F4'}}> 
                <View style={[styles.cardinnerdiv2, styles.payslipdiv]}>
                  <View style={styles.rowmode}>
                    <View style={{ width: '50%' }}>
                      <Text style={[styles.cardinnerdiv2_txt, styles.payslip_bold]}>Date Requested:</Text>
                    </View>
                    <View style={{ width: '50%' }}>
                      <Text style={[styles.cardinnerdiv2_txt, styles.payslip_bold]}>Date Confirmed:</Text>
                    </View>
                  </View>
                  <View style={styles.rowmode}>
                    <View style={{ width: '50%' }}>
                      <Text style={styles.cardinnerdiv2_txt}>3rd March, 2019</Text>
                    </View>
                    <View style={{ width: '50%' }}>
                      <Text style={[styles.cardinnerdiv2_txt]}>4th March, 2019</Text>
                    </View>
                  </View>
                </View>
                <View style={[styles.cardinnerdiv2, styles.payslipdiv]}>
                  <View style={styles.rowmode}>
                    <View style={{ width: '50%' }}>
                      <Text style={[styles.cardinnerdiv2_txt, styles.payslip_bold]}>Amount</Text>
                    </View>
                    <View style={{ width: '50%' }}>
                      <Text style={[styles.cardinnerdiv2_txt, styles.payslip_bold]}>Duration</Text>
                    </View>
                  </View>
                  <View style={styles.rowmode}>
                    <View style={{ width: '50%' }}>
                      <Text style={styles.cardinnerdiv2_txt}>N17,500.00</Text>
                    </View>
                    <View style={{ width: '50%' }}>
                      <Text style={[styles.cardinnerdiv2_txt]}>Mar 29 - Apr 5, 2019 </Text>
                    </View>
                  </View>
                </View>
              </View>
              
              <View style={styles.payslip_table}>
                <View style={styles.payslip_table_head}>
                  <View style={styles.slip_row}> 
                  <Text numberOfLines={1} style={[styles.cardinnerdiv2_txt, styles.payslip_boldx]}>Address</Text>
                  </View>
                  <View style={styles.slip_row}> 
                  <Text style={[styles.cardinnerdiv2_txt, styles.payslip_boldx]}>Type</Text>
                  </View>
                  <View style={styles.slip_row}> 
                  <Text style={[styles.cardinnerdiv2_txt, styles.payslip_boldx]}>Rate</Text>
                  </View>
                  <View style={styles.slip_row}> 
                  <Text style={[styles.cardinnerdiv2_txt, styles.payslip_boldx]}>Time</Text>
                  </View>
                  <View style={styles.slip_row}> 
                  <Text numberOfLines={1} style={[styles.cardinnerdiv2_txt, styles.payslip_boldx]}>Amount</Text>
                  </View>
                </View>
                <View style={styles.payslip_row_white}>
                  <View style={styles.slip_row}> 
                  <Text numberOfLines={1} style={[styles.cardinnerdiv2_txt, styles.payslip_boldx]}>Lekki Phase 1</Text>
                  </View>
                  <View style={styles.slip_row}> 
                  <Text numberOfLines={1} style={[styles.cardinnerdiv2_txt, styles.payslip_boldx]}>Salary</Text>
                  </View>
                  <View style={styles.slip_row}> 
                  <Text numberOfLines={1} style={[styles.cardinnerdiv2_txt, styles.payslip_boldx]}>200</Text>
                  </View>
                  <View style={styles.slip_row}> 
                  <Text numberOfLines={1} style={[styles.cardinnerdiv2_txt, styles.payslip_boldx]}>30 min</Text>
                  </View>
                  <View style={styles.slip_row}> 
                  <Text numberOfLines={1} style={[styles.cardinnerdiv2_txt, styles.payslip_boldx]}>N6,000</Text>
                  </View>
                </View>
                <View style={[styles.payslip_row_white, {backgroundColor:'#F4F4F4'}]}>
                  <View style={styles.slip_row}> 
                  <Text numberOfLines={1} style={[styles.cardinnerdiv2_txt, styles.payslip_boldx]}>Lekki Ho</Text>
                  </View>
                  <View style={styles.slip_row}> 
                  <Text numberOfLines={1} style={[styles.cardinnerdiv2_txt, styles.payslip_boldx]}>Salary</Text>
                  </View>
                  <View style={styles.slip_row}> 
                  <Text numberOfLines={1} style={[styles.cardinnerdiv2_txt, styles.payslip_boldx]}>200</Text>
                  </View>
                  <View style={styles.slip_row}> 
                  <Text numberOfLines={1} style={[styles.cardinnerdiv2_txt, styles.payslip_boldx]}>30 min</Text>
                  </View>
                  <View style={styles.slip_row}> 
                  <Text numberOfLines={1} style={[styles.cardinnerdiv2_txt, styles.payslip_boldx]}>N6,000</Text>
                  </View>
                </View>
                <View style={[styles.payslip_row_white]}>
                  <View style={styles.slip_row}> 
                  <Text numberOfLines={1} style={[styles.cardinnerdiv2_txt, styles.payslip_boldx]}>Lekki Ho</Text>
                  </View>
                  <View style={styles.slip_row}> 
                  <Text numberOfLines={1} style={[styles.cardinnerdiv2_txt, styles.payslip_boldx]}>Salary</Text>
                  </View>
                  <View style={styles.slip_row}> 
                  <Text numberOfLines={1} style={[styles.cardinnerdiv2_txt, styles.payslip_boldx]}>200</Text>
                  </View>
                  <View style={styles.slip_row}> 
                  <Text numberOfLines={1} style={[styles.cardinnerdiv2_txt, styles.payslip_boldx]}>30 min</Text>
                  </View>
                  <View style={styles.slip_row}> 
                  <Text numberOfLines={1} style={[styles.cardinnerdiv2_txt, styles.payslip_boldx]}>N6,000</Text>
                  </View>
                </View>
              </View>

            </Card>

            <View style={styles.slip_bottom}>
            <TouchableOpacity style={{alignItems:'center'}}>
                  <Image source={require('../../../../assets/share.png')} resizeMode='contain' style={styles.slip_icon}/>
                  <Text style={[styles.cardinnerdiv2_txt, styles.payslip_bold, styles.slip_icon_txt]}>Share</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{alignItems:'center'}}>
                  <Image source={require('../../../../assets/print.png')} resizeMode='contain' style={styles.slip_icon}/>
                  <Text style={[styles.cardinnerdiv2_txt, styles.payslip_bold, styles.slip_icon_txt]}>Print</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{alignItems:'center'}}>
                  <Image source={require('../../../../assets/download.png')} resizeMode='contain' style={styles.slip_icon}/>
                  <Text style={[styles.cardinnerdiv2_txt, styles.payslip_bold, styles.slip_icon_txt]}>Download</Text>
                </TouchableOpacity>
            </View>
          </View>
        </Content>

      </Container>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RiderPaymentSlip);
