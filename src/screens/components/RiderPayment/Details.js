/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput, StatusBar, Linking } from 'react-native';
import { connect } from 'react-redux';
import Image from 'react-native-remote-svg';
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Content,
  Card,
  //   Image,
} from 'native-base';
import styles from './styles';
import getImage from '../../../utils/getImage';
import { FontNames } from '../../../../theme';
import { RFPercentage, RFValue } from 'react-native-responsive-fontsize';

class RiderPayment extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isOrderDetailsVisible: false,
    };
  }
  componentDidMount() {
    //StatusBar.setBarStyle( 'light-content',true)
    StatusBar.setBackgroundColor("#FFF")
  }
  render() {
    const {
      isOrderDetailsVisible,
    } = this.state;
    const { navigation } = this.props
    return (
      <Container>
        <Header
          transparent
          hasTabs
          style={styles.headerbottom}
          androidStatusBarColor={'#0001'}
          iosBarStyle="dark-content">
          <Left>
            <Button transparent onPress={() => navigation.pop()}>
              <Icon
                style={{
                  paddingHorizontal: 10,
                  color: '#000000',
                }}
                name={'arrow-back'}
              />
            </Button>
          </Left>
          <Body style={styles.headtxtdiv}>
            <Text
              style={styles.headtxt}>
              Rider Payment
            </Text>
          </Body>
          <Right />
        </Header>
        <Content padder style={{ padding: 10, backgroundColor: '#f7f7f7' }}>
          <View>
            <Card style={styles.cardiv}>
              <View style={{marginBottom:15}}>
                <Text style={[styles.cardhead_txt, { fontSize: 20 }]}>Customer order 1 (Williams Adebowale)</Text>
                <View style={styles.cardinnerdiv2}>
                  <View style={styles.rowmode}>
                    <View style={{ width: '40%' }}>
                      <Text style={styles.cardinnerdiv2_txt}>Pickup details:</Text>
                    </View>
                    <View style={{ width: '60%' }}>
                      <Text style={[styles.cardinnerdiv2_txt, { fontFamily: FontNames.semibold }]}>5 Akilo Street, Lady Lak</Text>
                    </View>
                  </View>
                  <View style={styles.rowmode}>
                    <View style={{ width: '40%' }}>
                      <Text style={styles.cardinnerdiv2_txt}>Delivery details:</Text>
                    </View>
                    <View style={{ width: '60%' }}>
                      <Text style={[styles.cardinnerdiv2_txt, { fontFamily: FontNames.semibold }]}>Divine Estate,  Amuwo Odo</Text>
                    </View>
                  </View>
                </View>
              </View>

              <View style={{marginBottom:15}}>
                <Text style={[styles.cardhead_txt, { fontSize: 20 }]}>Customer order 2 (Williams Adebowale)</Text>
                <View style={styles.cardinnerdiv2}>
                  <View style={styles.rowmode}>
                    <View style={{ width: '40%' }}>
                      <Text style={styles.cardinnerdiv2_txt}>Pickup details:</Text>
                    </View>
                    <View style={{ width: '60%' }}>
                      <Text style={[styles.cardinnerdiv2_txt, { fontFamily: FontNames.semibold }]}>5 Akilo Street, Lady Lak</Text>
                    </View>
                  </View>
                  <View style={styles.rowmode}>
                    <View style={{ width: '40%' }}>
                      <Text style={styles.cardinnerdiv2_txt}>Delivery details:</Text>
                    </View>
                    <View style={{ width: '60%' }}>
                      <Text style={[styles.cardinnerdiv2_txt, { fontFamily: FontNames.semibold }]}>Divine Estate,  Amuwo Odo</Text>
                    </View>
                  </View>
                </View>
              </View>

              <View style={{ marginTop:30 }}>
                <View style={[styles.rowmode, {marginBottom:10}]}>
                  <View style={{ width: '70%' }}>
                    <Text style={styles.cardinnerdiv2_txt}>No of Tips collected:</Text>
                  </View>
                  <View style={{ width: '30%', alignItems: 'center' }}>
                    <Text style={[styles.cardinnerdiv2_txt, { fontFamily: FontNames.semibold }]}>2</Text>
                  </View>
                </View>
                <View style={[styles.rowmode, {marginBottom:10}]}>
                  <View style={{ width: '70%' }}>
                    <Text style={styles.cardinnerdiv2_txt}>Amount of Tips:</Text>
                  </View>
                  <View style={{ width: '30%', alignItems: 'center' }}>
                    <Text style={[styles.cardinnerdiv2_txt, { fontFamily: FontNames.semibold }]}>1500</Text>
                  </View>
                </View>
                <View style={[styles.rowmode, {marginBottom:10}]}>
                  <View style={{ width: '70%' }}>
                    <Text style={styles.cardinnerdiv2_txt}>No of Wrong Delivery or Accident:</Text>
                  </View>
                  <View style={{ width: '30%', alignItems: 'center' }}>
                    <Text style={[styles.cardinnerdiv2_txt, { fontFamily: FontNames.semibold }]}>0</Text>
                  </View>
                </View>
                <View style={[styles.rowmode, {marginBottom:10}]}>
                  <View style={{ width: '70%' }}>
                    <Text style={styles.cardinnerdiv2_txt}>Total Amount to be paid:</Text>
                  </View>
                  <View style={{ width: '30%', alignItems: 'center' }}>
                    <Text style={[styles.cardinnerdiv2_txt, { fontFamily: FontNames.semibold }]}>17,500.00</Text>
                  </View>
                </View>
              </View>

              <Button
                block
                //primary
                style={[styles.submitBtn, { backgroundColor: '#3FAF5D',height:55 }]}
               onPress={() => navigation.navigate('RiderPaymentSuccess')}
              >
                <Text style={[styles.btnText, { letterSpacing: 1 }]}>REQUEST PAYMENT</Text>
              </Button>


            </Card>
          </View>
        </Content>

      </Container>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RiderPayment);
