/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput, StatusBar, Linking } from 'react-native';
import { connect } from 'react-redux';
import Image from 'react-native-remote-svg';
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Content,
  Card,
  //   Image,
} from 'native-base';
import styles from './styles';
import getImage from '../../../utils/getImage';
import { FontNames } from '../../../../theme';
import { RFPercentage, RFValue } from 'react-native-responsive-fontsize';

class RiderPaymentSuccess extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isOrderDetailsVisible: false,
    };
  }
  componentDidMount() {
    //StatusBar.setBarStyle( 'light-content',true)
    StatusBar.setBackgroundColor("#FFF")
  }
  render() {
    const {
      isOrderDetailsVisible,
    } = this.state;
    const { navigation } = this.props
    return (
      <Container>
        <Header
          transparent
          hasTabs
          style={styles.headerbottom}
          androidStatusBarColor={'#0001'}
          iosBarStyle="dark-content">
          <Left>
            <Button transparent onPress={() => navigation.pop()}>
              <Icon
                style={{
                  paddingHorizontal: 10,
                  color: '#000000',
                }}
                name={'arrow-back'}
              />
            </Button>
          </Left>
          <Body style={styles.headtxtdiv}>
            <Text
              style={styles.headtxt}>
              Rider Payment
            </Text>
          </Body>
          <Right />
        </Header>
       
          <View style={styles.success_div}>
            <Image source={getImage.success_svg} resizeMode='contain' style={{width:RFValue(132),height:RFValue(132)}} />
            <Text style={styles.success_head}>Your request is succeful</Text>
            <Text style={styles.success_text}>Your payour request is successful, and will be processed.</Text>

            <Button
                block
                //primary
                style={[styles.submitBtn, { backgroundColor: '#3FAF5D',width:'85%',alignSelf:'center',height:55 }]}
               onPress={() => navigation.navigate('RiderPaymentSlip')}
              >
                <Text style={[styles.btnText, { letterSpacing: 1 }]}>DONE</Text>
              </Button>
          </View>

      </Container>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RiderPaymentSuccess);
