/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput } from 'react-native';
import { connect } from 'react-redux';
import Image from 'react-native-remote-svg';
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Content,
  Thumbnail,
  Item,
  Input,
  Card,
  Grid,
  Row,
  Col,
  Picker,
  CardItem,
  //   Image,
} from 'native-base';
import styles from './styles';
import { Actions } from 'react-native-router-flux';
import Modal from 'react-native-modal';

class RefundPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      reasonType: '',
      isRequestFundSuccessful: false,
    };
  }

  render() {
    const { reasonType, isRequestFundSuccessful } = this.state;
    return (
      <Container>
        <Header
          transparent
          androidStatusBarColor={'#0001'}
          iosBarStyle="dark-content">
          <Left />
          <Body />
          <Right>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="close" style={{ color: '#030E1A' }} />
            </Button>
          </Right>
        </Header>
        <Content>
          <View style={{ paddingHorizontal: 20 }}>
            <View>
              <Text
                style={[
                  styles.cancelOrderText,
                  { fontSize: 20, marginVertical: 20 },
                ]}>
                What is the issue with your parcel?
              </Text>
            </View>
            <View style={styles.inputContainer}>
              <Text
                style={[styles.cancelOrderText, { fontSize: 16, margin: 5 }]}>
                Reason
              </Text>
              <Item regular style={styles.inputItem}>
                <Picker
                  note
                  mode="dropdown"
                  // style={{ width: 120 }}
                  selectedValue={reasonType}
                  onValueChange={item => this.setState({ reasonType: item })}>
                  <Picker.Item label="Select reason" value="" />
                  <Picker.Item label="Late" value="Reason 1" />
                  <Picker.Item label="Bad" value="Reason 2" />
                  <Picker.Item label="Slow" value="Reason 3" />
                  <Picker.Item label="Others" value="Reason 4" />
                </Picker>
              </Item>
            </View>

            <View style={[styles.inputContainer]}>
              <Text style={[styles.cancelOrderText, { fontSize: 16 }]}>
                Tell us more (optional)
              </Text>
              <View
                style={[
                  styles.inputItem,
                  {
                    flexDirection: 'column',
                    paddingHorizontal: 10,
                    height: 100,
                  },
                ]}>
                <View>
                  <TextInput
                    placeholder={'Add comment'}
                    placeholderTextColor={'#0005'}
                    multiline
                    onChangeText={text => this.setState({ addInfo: text })}
                    style={[styles.inputText, { padding: 0 }]}
                  />
                </View>
              </View>
            </View>

            <View style={[styles.inputContainer]}>
              <Text
                style={[styles.cancelOrderText, { fontSize: 16, margin: 5 }]}>
                Upload photos(Optional)
              </Text>
              <View
                style={[
                  styles.inputItem,
                  { alignItems: 'center', paddingVertical: 10 },
                ]}>
                <Icon name="camera" />
                <Text>Click to add photo</Text>
              </View>
            </View>

            <Button
              block
              primary
              style={{ backgroundColor: '#3FAF5D', marginTop: 30 }}
              onPress={() => this.setState({ isRequestFundSuccessful: true })}>
              <Text style={styles.btnText}>REQUEST FUND</Text>
            </Button>
          </View>
        </Content>
        <Modal
          isVisible={isRequestFundSuccessful}
          coverScreen={true}
          style={{ backgroundColor: '#FFFFFF' }}
          backdropColor={'#FFFFFF'}>
          <View style={styles.modalContainer}>
            <View style={{ alignSelf: 'center' }}>
              <Icon name={'checkmark-circle'} style={styles.modalIcon} />
            </View>
            <Card transparent>
              <CardItem style={{ justifyContent: 'center' }}>
                <Text style={styles.modalTitle}>Refund request successful</Text>
              </CardItem>

              <CardItem style={{ justifyContent: 'center' }}>
                <Text style={styles.modalSubtitle}>
                  You will be notified vai email and your refunded cash will
                  reflect in your wallet.
                </Text>
              </CardItem>

              <View style={{ padding: 16 }}>
                <Button
                  block
                  full
                  onPress={() =>
                    this.setState({ isRequestFundSuccessful: false }, () => {
                      this.props.navigation.navigate('HomePage');
                    })
                  }
                  style={styles.modalButton}>
                  <Text style={styles.modalButtonText}>DONE</Text>
                </Button>
              </View>
            </Card>
          </View>
        </Modal>
      </Container>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RefundPage);
