import React, { Component } from "react";
import { Container, Header, Content, Card, CardItem, Text, Body, Left, Button, Icon, Right } from "native-base";
import styles from './styles';

export default class CardItemBordered extends Component {
    constructor(props) {
        super(props);

    }
    render() {
        const { navigation } = this.props;
        return (
            <Container>
                <Header transparent androidStatusBarColor="#0003">
                    <Left style={{ paddingHorizontal: 5 }}>
                        <Button transparent onPress={() => navigation.goBack()}>
                            <Icon name="close" style={styles.closeIcon} />
                        </Button>
                    </Left>
                    <Body style={styles.headtxtdiv}>
                        <Text
                            style={{
                                fontFamily: 'Nunito-SemiBold',
                                fontSize: 18,
                                color: '#030E1A',
                                width: '100%',
                                textAlign: 'center',
                            }}>
                            Privacy Policy
                </Text>
                    </Body>
                    <Right />
                </Header>
                <Content padder>
                    <Card>
                        <CardItem header >
                            <Text>Privacy Policy</Text>
                        </CardItem>
                        <CardItem >
                            <Body>
                                <Text>
                                    Effective date: October, 2019
                                    {'\n'}{'\n'}
                                    Tapway Logistics ("us", "we", or "our") operates the http://www.tapwaylogistics.com website (the "Service").
                                     {'\n'}{'\n'}
                                    This page informs you of our policies regarding the collection, use, and disclosure of personal data when you use our Service and the choices you have associated with that data.
                                     {'\n'}{'\n'}
                                    We use your data to provide and improve the Service. By using the Service, you agree to the collection and use of information in accordance with this policy.
                                    {'\n'}{'\n'}
                                    Unless otherwise defined in this Privacy Policy, terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, accessible from http://www.tapwaylogistics.com

                </Text>
                            </Body>
                        </CardItem>

                    </Card>

                    <Card>

                        <CardItem header >
                            <Text>Information Collection And Use</Text>
                        </CardItem>
                        <CardItem >
                            <Body>
                                <Text>
                                    We collect several different types of information for various purposes to provide and improve our Service to you.
                                    {'\n'}{'\n'}

                                </Text>
                            </Body>
                        </CardItem>

                    </Card>

                    <Card>
                        <CardItem header >
                            <Text>Types of Data Collected</Text>
                        </CardItem>
                        <CardItem header >
                            <Text>Personal Data</Text>
                        </CardItem>

                        <CardItem >
                            <Body>
                                <Text>
                                    While using our Service, we may ask you to provide us with certain, personally identifiable information that can be used to contact or identify you ("Personal Data"). Personally, identifiable information may include, but is not limited to:
                                    {'\n'}{'\n'}
                                    *  Email address {'\n'}
                                    *  First name and last name  {'\n'}
                                    *  Phone number  {'\n'}
                                    *  Address, State, Province, ZIP/Postal code, City  {'\n'}
                                    *  Cookies and Usage Data  {'\n'}
                                </Text>
                            </Body>
                        </CardItem>

                    </Card>

                    <Card>
                        <CardItem header >
                            <Text>Usage Data</Text>
                        </CardItem>

                        <CardItem >
                            <Body>
                                <Text>
                                    We may also collect information about how the Service is accessed and used ("Usage Data"). This Usage Data may include information such as your computer's Internet Protocol address (e.g., IP address), browser type, browser version, the pages of our Service that you visit, the time and date of your visit, the time spent on those pages, unique device identifiers and other diagnostic data.

                </Text>
                            </Body>
                        </CardItem>

                    </Card>

                    <Card>
                        <CardItem header >
                            <Text>Tracking and Cookies Data</Text>
                        </CardItem>

                        <CardItem >
                            <Body>
                                <Text>
                                    We use cookies and similar tracking technologies to track the activity on our Service and hold certain information.
                                    {'\n'}{'\n'}
                                    Cookies are files with a small amount of data, which may include a unique anonymous identifier. Cookies are sent to your browser from a website and stored on your device. Tracking technologies also used are beacons, tags, and scripts to collect and track information and to improve and analyze our Service.
                                    {'\n'}{'\n'}
                                    You can instruct your browser to refuse all cookies or to indicate when a cookie is sent. However, if you do not accept cookies, you may not be able to use some portions of our Service.


                </Text>
                            </Body>
                        </CardItem>

                    </Card>

                    <Card>
                        <CardItem header >
                            <Text>Examples of Cookies we use:</Text>
                        </CardItem>

                        <CardItem >
                            <Body>
                                <Text>
                                    Session Cookies. We use Session Cookies to operate our Service.
                                    {'\n'}
                                    Preference Cookies. We use Preference Cookies to remember your preferences and various settings.
                                    {'\n'}
                                    Security Cookies. We use Security Cookies for security purposes.

                </Text>
                            </Body>
                        </CardItem>

                    </Card>

                    <Card>
                        <CardItem header >
                            <Text>Use of Data</Text>
                        </CardItem>

                        <CardItem header >
                            <Text>Tapway Logistics uses the collected data for various purposes:
</Text>
                        </CardItem>

                        <CardItem >
                            <Body>
                                <Text>
                                    * To provide and maintain the Service.
                                    {'\n'}
                                    * To notify you about changes to our Service.
                                    {'\n'}
                                    * To allow you to participate in interactive features of our Service when you choose to do so.
                                    {'\n'}
                                    * To provide customer care and support
                                    {'\n'}
                                    * To provide analysis or valuable information so that we can improve the Service
                                    {'\n'}
                                    * To monitor the usage of the Service
                                    {'\n'}
                                    * To detect, prevent and address technical issues

                </Text>
                            </Body>
                        </CardItem>

                    </Card>

                    <Card>
                        <CardItem header >
                            <Text>Transfer of Data</Text>
                        </CardItem>

                        <CardItem >
                            <Body>
                                <Text>
                                    Your information, including Personal Data, may be transferred to — and maintained on — computers located outside of your state, province, country or other governmental jurisdiction where the data protection laws may differ than those from your jurisdiction.
                                    {'\n'}{'\n'}
                                    If you are located outside the Central African Republic and choose to provide information to us, please note that we transfer the data, including Personal Data, to the Central African Republic and process it there.
                                    {'\n'}{'\n'}
                                    Your consent to this Privacy Policy followed by your submission of such information represents your agreement to that transfer.
                                    {'\n'}{'\n'}
                                    Tapway Logistics will take all steps that are reasonably necessary to ensure that your data is treated securely and under this Privacy Policy. Also, no transfer of your Personal Data will take place to an organization or a country unless there are adequate controls in place, including the security of your data and other personal information.



                </Text>
                            </Body>
                        </CardItem>

                    </Card>

                    <Card>
                        <CardItem header >
                            <Text>Disclosure of Data</Text>
                        </CardItem>

                        <CardItem header >
                            <Text>Legal Requirements</Text>
                        </CardItem>

                        <CardItem >
                            <Body>
                                <Text>
                                    Tapway Logistics may disclose your Data in the good faith belief that such action is necessary to:
                                    {'\n'}{'\n'}
                                    * To comply with a legal obligation.
                                    {'\n'}
                                    * To protect and defend the rights or property of Tapway Logistics.
                                    {'\n'}
                                    * To prevent or investigate possible wrongdoing in connection with the Service.
                                    {'\n'}
                                    * To protect the personal safety of users of the Service or the public.
                                    {'\n'}
                                    * To protect against legal liability.
                </Text>
                            </Body>
                        </CardItem>

                    </Card>

                    <Card>
                        <CardItem header >
                            <Text>Security of Data</Text>
                        </CardItem>

                        <CardItem >
                            <Body>
                                <Text>
                                    The securities of your data are essential to us, but remember that no method of transmission over the Internet or method of electronic storage is 100% secure. While we strive to use commercially acceptable means to protect your Personal Data, we cannot guarantee its absolute security.
                </Text>
                            </Body>
                        </CardItem>

                    </Card>

                    <Card>
                        <CardItem header >
                            <Text>Service Providers</Text>
                        </CardItem>

                        <CardItem >
                            <Body>
                                <Text>
                                    We may employ third party companies and individuals to facilitate our Service ("Service Providers"), to provide the Service on our behalf, to perform Service-related services or to assist us in analyzing how our Service is used.
                                     {'\n'}{'\n'}
                                    These third parties have access to your Personal Data only to perform these tasks on our behalf and are obligated not to disclose or use it for any other purpose.
                </Text>
                            </Body>
                        </CardItem>

                    </Card>

                    <Card>
                        <CardItem header >
                            <Text>Analytics</Text>
                        </CardItem>

                        <CardItem >
                            <Body>
                                <Text>
                                    We may use third-party Service Providers to monitor and analyze the use of our Service.

                </Text>
                            </Body>
                        </CardItem>

                    </Card>

                    <Card>
                        <CardItem header >
                            <Text>Links to Other Sites</Text>
                        </CardItem>

                        <CardItem >
                            <Body>
                                <Text>
                                    Our Service may contain links to other sites that are not operated by us. If you click on a third party link, you will be directed to that third party's site. We strongly advise you to review the Privacy Policy of every site you visit.
                                    {'\n'}{'\n'}
                                    We have no control over and assume no responsibility for the content, privacy policies or practices of any third party sites or services.
                </Text>
                            </Body>
                        </CardItem>

                    </Card>

                    <Card>
                        <CardItem header >
                            <Text>Children's Privacy</Text>
                        </CardItem>

                        <CardItem >
                            <Body>
                                <Text>
                                    Our Service does not address anyone under the age of 18 ("Children").
                                    {'\n'}{'\n'}
                                    We do not knowingly collect personally identifiable information from anyone under the age of 18. If you are a parent or guardian and you are aware that your Children has provided us with Personal Data, please contact us. If we become aware that we have collected Personal Data from children without verification of parental consent, we take steps to remove that information from our servers.
                </Text>
                            </Body>
                        </CardItem>

                    </Card>

                    <Card>
                        <CardItem header >
                            <Text>Changes to This Privacy Policy</Text>
                        </CardItem>

                        <CardItem >
                            <Body>
                                <Text>
                                    We may update our Privacy Policy from time to time. We will notify you of any changes by posting the new Privacy Policy on this page.
                                    {'\n'}{'\n'}
                                    We will let you know via email and a prominent notice on our Service, before the change becoming effective and update the "effective date" at the top of this Privacy Policy.
                                    {'\n'}{'\n'}
                                    You are advised to review this Privacy Policy periodically for any changes. Changes to this Privacy Policy are effective when they are posted on this page.
                </Text>
                            </Body>
                        </CardItem>

                    </Card>

                    <Card>
                        <CardItem header >
                            <Text>Contact Us</Text>
                        </CardItem>

                        <CardItem >
                            <Body>
                                <Text>
                                    If you have any questions about this Privacy Policy, please contact us:
                                    {'\n'}{'\n'}
                                    By email: tapway@mail.com
                                    {'\n'}
                                    By visiting this page on our website: www.tapwaylogistics.com
                                    
                </Text>
                            </Body>
                        </CardItem>

                    </Card>
                </Content>
            </Container>
        );
    }
}