import { StyleSheet } from 'react-native';
import { FontNames } from '../../../../theme';
import { RFValue, RFPercentage } from "react-native-responsive-fontsize"

const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: '#FFFFFF' },
    closeIcon: { fontSize: 25, color: '#030E1A' },
    headtxtdiv: { position: 'absolute', left: 0, right: 0, top: RFPercentage(5.5), zIndex: -100 },
});

export default styles;
