/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { connect } from 'react-redux';
import {
  Container,
  Header,
  Left,
  Icon,
  Body,
  Right,
  Content,
  Button,
  Item,
  Input,
  Card,
  Thumbnail,
} from 'native-base';
import { Actions } from 'react-native-router-flux';
import styles from './styles';

export class CheckNearestDriver extends Component {
  constructor(props) {
    super(props);

    this.state = {
      checkRider: false,
    };
  }

  togglecheckRider = () => {
    this.setState({ checkRider: !this.state.checkRider });
  };

  render() {
    const { checkRider } = this.state;
    const {navigation}=this.props
    return (
      <Container>
        <Header
          style={{ backgroundColor: '#FFFFFF', marginTop: 35 }}
          androidStatusBarColor={'#FFFFFF'}>
          <Left>
            <Button transparent onPress={() => navigation.goBack()}>
              <Icon name="arrow-back" style={{ color: '#000000' }} />
            </Button>
          </Left>
          <Body>
            <Text
              numberOfLines={1}
              style={{
                fontSize: 18,
                color: '#030E1A',
                fontFamily: 'Nunito-SemiBold',
              }}>
              Check Nearest Rider
            </Text>
          </Body>
          <Right />
        </Header>
        <Content padder style={{ padding: 16 }}>
          <Card transparent>
            <View style={styles.inputContainer}>
              <Text style={styles.inputLabel}>Pickup Location</Text>
              <Item regular style={styles.inputItem}>
                <Icon
                  type="FontAwesome"
                  name="map-marker"
                  style={{ color: '#1B2E5A', fontSize: 20 }}
                />
                <Input
                  placeholder="Enter Location"
                  placeholderTextColor={'#383838'}
                  style={styles.inputText}
                />
              </Item>
            </View>
            <View style={styles.inputContainer}>
              <Text style={styles.inputLabel}>Drop Location</Text>
              <Item regular style={styles.inputItem}>
                <Icon
                  type="FontAwesome"
                  name="map-marker"
                  style={{ color: '#3FAF5D', fontSize: 20 }}
                />
                <Input
                  placeholder="Enter Location"
                  placeholderTextColor={'#383838'}
                  style={styles.inputText}
                />
              </Item>
            </View>
          </Card>
          <Button
            block
            primary
            style={styles.submitBtn}
            onPress={this.togglecheckRider}>
            <Text style={styles.btnText}>Find Rider</Text>
          </Button>
        </Content>
        {checkRider && (
          <View
            style={{
              position: 'absolute',
              top: 0,
              bottom: 0,
              right: 0,
              left: 0,
              backgroundColor: '#1B2E5A',
            }}>
            <Header
              style={{ backgroundColor: '#1B2E5A', marginTop: 35 }}
              iosBarStyle="light-content"
              androidStatusBarColor={'#1B2E5A'}>
              <Left>
                <Button transparent onPress={() => this.togglecheckRider()}>
                  <Icon name="arrow-back" style={{ color: '#ffffff' }} />
                </Button>
              </Left>
              <Body>
                <Text
                  numberOfLines={1}
                  style={{
                    fontSize: 18,
                    color: '#FFFFFF',
                    fontFamily: 'Nunito-SemiBold',
                  }}>
                  Check Rider
                </Text>
              </Body>
              <Right />
            </Header>
            <Content padder style={{ padding: 10 }}>
              <Card style={{ padding: 10, borderRadius: 5 }}>
                <View style={{ flexDirection: 'row' }}>
                  <Left
                    style={{
                      flexDirection: 'row',
                      alignContent: 'flex-end',
                      alignItems: 'flex-end',
                    }}>
                    <View
                      style={{
                        backgroundColor: '#ffff',
                        borderRadius: 5,
                        padding: 2,
                        shadowColor: '#0000',
                        shadowOffset: {
                          width: 0,
                          height: 2,
                        },
                        shadowOpacity: 0.23,
                        shadowRadius: 2.0,
                        elevation: 4,
                      }}>
                      <Thumbnail
                        style={{ borderRadius: 5 }}
                        square
                        small
                        source={require('../../assets/icons/man_hat.png')}
                      />
                    </View>
                    <View
                      style={{
                        marginHorizontal: 20,
                        justifyContent: 'flex-end',
                        alignContent: 'flex-end',
                      }}>
                      <Text
                        style={{
                          color: '#030E1A',
                          opacity: 0.4,
                          fontSize: 14,
                        }}>
                        Pickup Rider
                      </Text>
                      <Text
                        style={{
                          color: '#030E1A',
                          fontFamily: 'Nunito-SemiBold',
                        }}>
                        Gregory Samuel
                      </Text>
                    </View>
                  </Left>
                  <Right>
                    <View
                      style={{
                        backgroundColor: '#1B2E5A',
                        flexDirection: 'row',
                        borderRadius: 25,
                      }}>
                      <Text
                        style={{ color: '#ffff', fontSize: 16, padding: 5 }}>
                        Call
                      </Text>
                      <Icon
                        name="call"
                        style={{ color: '#ffffff', fontSize: 16, padding: 5 }}
                      />
                    </View>
                  </Right>
                </View>

                <Button
                  block
                  primary
                  style={styles.submitBtn}
                  onPress={() => navigation.navigate('login')}>
                  <Text style={styles.btnText}>LOGIN TO MAKE ORDER</Text>
                </Button>
              </Card>
            </Content>
          </View>
        )}
      </Container>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CheckNearestDriver);
