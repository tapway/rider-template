import { StyleSheet } from 'react-native';
import { FontNames } from '../../../../theme';
import { RFPercentage, RFValue } from 'react-native-responsive-fontsize';

const styles = StyleSheet.create({
  inputContainer: {
    flex: 1,
    marginVertical: 10,
    // backgroundColor: 'red',
  },
  headtxtdiv: { position: 'absolute', left: 0, right: 0, top: RFPercentage(5.5), zIndex: -100 },
  headtxt: {
    fontSize: 18,
    color: '#030E1A',
    width: '100%',
    textAlign: 'center',
    //paddingLeft:40,
    fontFamily: FontNames.semibold,
  },
});

export default styles;
