/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { View, Text, TouchableNativeFeedback, TextInput } from 'react-native';
import { connect } from 'react-redux';
import {
  Container,
  Header,
  Left,
  Button,
  Icon,
  Body,
  Right,
  Content,
  Grid,
  Row,
  Col,
  Card,
  Item,
  Input,
  Picker,
  Thumbnail,
  CardItem,
  H1,
  Accordion,
} from 'native-base';
import { Actions } from 'react-native-router-flux';
import styles from './styles';

const dataArray = [
  {
    title: 'How do I sign up?',
    content:
      `It’s a simple process! All it takes it’s a few minutes of your time. You can reach out to the Community team at any point.${'\n'} Email rider@tapwaylogistics.com ${'\n'}${'\n'}All you need to do is: ${'\n'}${'\n'}Start your application via our app.Click here to begin 
Complete our online screening process ${'\n'}${'\n'}
Once qualified, you will need to submit all required documentation ${'\n'}${'\n'}
Pass a background check ${'\n'}${'\n'}
Complete our training and get inducted ${'\n'}${'\n'}
Start delivering `,
  },
  {
    title: 'How will I be paid?',
    content:
      'Lorem ipsum dolor amet typewriter keytar vape meggings. Man braid pickled helvetica, Craft beer hammock selfies meditation, roof party seitan waistcoat before they sold out offal taxidermy cloud bread art party. ',
  },
  {
    title: 'Rider related questions',
    content:
      `You will be paid weekly for every accepted and completed deliveries. You will receive a statement of your weekly pay for your own reference and your wallet will be credited. ${'\n'}${'\n'}

All partner riders are paid based on their time on task and delivery miles. Tapway takes 20% off every transaction.${'\n'}${'\n'}
If you have any further questions. You can refer to the independent contractor agreement or contact our support team. `,
  },
  {
    title: 'Are the working hours flexible?',
    content:
      `Absolutely! We pride ourselves by offering our partner riders an opportunity for an alternative source of income. So autonomously, you control your hours and schedule.${'\n'}${'\n'}
We have a block system of 2-4 hours per cycle. So that means, for every time a cycle  completes. You can get fresh requests.${'\n'}${'\n'}
We also provide a system where you can book your hours a week ahead. With this you’re guaranteed a fixed income and you avoid redundancy.  So keep an eye out for our weekly rooster.` ,
  },

  {
    title: 'What will I be delivering?',
    content:
      `Items from small medium enterprises including but not limited to, clothing, electronics, food and many more.${'\n'}${'\n'}
How long are delivery routes and where will items be delivered?${'\n'}${'\n'}
Deliveries are made within the Island or to the mainland. 
Usually items are delivered to residence or offices, but we go wherever the customer wants.` ,
  },

  {
    title: 'What type of smart phone is required?',
    content:
      `Android or iPhone 
      ${'\n'}${'\n'}
Data plan required` ,
  },

  {
    title: 'What if I feel unsafe while making a delivery?',
    content:
      `Your safety is paramount and we pride ourselves for that. The slightest inclination you sense concern. Please call our rider support line through your rider app immediately.`,
  },
];

class FAQ extends Component {
  render() {
    const { navigation } = this.props
    return (
      <Container>
        <Header
          transparent
          hasTabs
          style={{
            backgroundColor: '#FFFFFF',
            borderBottomColor: '#0001',
            borderBottomWidth: 1.5,
          }}
          androidStatusBarColor={'#0001'}
          iosBarStyle="dark-content">
          <Left>
            <Button transparent onPress={() => navigation.navigate('home')}>
              <Icon
                style={{
                  paddingHorizontal: 10,
                  color: '#000000',
                }}
                name={'arrow-back'}
              />
            </Button>
          </Left>
          <Body style={styles.headtxtdiv}>
            <Text
              style={styles.headtxt}>
              FAQ
            </Text>
          </Body>
          <Right />
        </Header>
        <Content contentContainerStyle={{ padding: 20 }}>
          <View style={styles.inputContainer}>
            <H1>Frequently Asked Questions:</H1>
          </View>
          <View style={styles.inputContainer}>
            <Accordion dataArray={dataArray} expanded={0} />
          </View>
          <View style={styles.inputContainer}>
            <Button
              block
              full
              style={{
                backgroundColor: '#3FAF5D',
                borderRadius: 5,
                marginTop: 50,
              }}
              onPress={() => { }}>
              <Text
                style={{
                  color: '#ffffff',
                  fontFamily: 'Nunito-SemiBold',
                  fontSize: 14,
                }}>
                SUBMIT YOUR QUESTION
              </Text>
            </Button>
          </View>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(FAQ);
