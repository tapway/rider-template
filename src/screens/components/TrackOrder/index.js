/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { View, Text, TouchableNativeFeedback, TextInput } from 'react-native';
import { connect } from 'react-redux';
import {
  Container,
  Header,
  Left,
  Button,
  Icon,
  Body,
  Right,
  Content,
  Grid,
  Row,
  Col,
  Card,
  Item,
  Input,
  Picker,
  Thumbnail,
  CardItem,
} from 'native-base';
import { Actions } from 'react-native-router-flux';
import styles from './styles';
import TrackResult from './TrackResult';

class TrackOrder extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isTrackingVisible: false,
      trackType: 'parcel',
    };
  }

  toggleTrackOrder = () => {
    this.setState({ isTrackingVisible: !this.state.isTrackingVisible });
  };
  toggleTrackType = trackType => {
    this.setState({ trackType });
  };

  render() {
    const { isTrackingVisible, trackType } = this.state;
    return (
      <>
        {(!isTrackingVisible && (
          <Container>
            <Header
              transparent
              hasTabs
              style={{
                backgroundColor: '#FFFFFF',
                borderBottomColor: '#0001',
                borderBottomWidth: 1.5,
              }}
              androidStatusBarColor={'#0001'}
              iosBarStyle="dark-content">
              <Left>
                <Button transparent onPress={() => navigation.navigate('home')}>
                  <Icon
                    style={{
                      paddingHorizontal: 10,
                      color: '#000000',
                    }}
                    name={'arrow-back'}
                  />
                </Button>
              </Left>
              <Body>
                <Text
                  style={{
                    fontSize: 18,
                    fontFamily: 'Nunito-SemiBold',
                    color: '#030E1A',
                  }}>
                  Track Order
                </Text>
              </Body>
              <Right />
            </Header>
            <Content contentContainerStyle={{ padding: 20 }}>
              <View style={styles.inputContainer}>
                <Text style={styles.inputLabel}>
                  Enter Parcel ID / Rider Name{' '}
                </Text>
                <Item regular style={styles.inputItem}>
                  <Input
                    placeholder=""
                    placeholderTextColor={'#383838'}
                    style={styles.inputText}
                    onChangeText={text =>
                      this.setState({ personInCharge: text })
                    }
                  />
                </Item>
              </View>
              <View style={styles.inputContainer}>
                <Text style={styles.inputLabel}>Track Type</Text>
                <Item regular style={[styles.inputItem, { padding: 2 }]}>
                  <Grid>
                    <Row>
                      <TouchableNativeFeedback
                        onPress={() => this.toggleTrackType('rider')}>
                        <Col
                          style={{
                            backgroundColor:
                              trackType === 'rider' ? '#1B2E5A' : 'transparent',
                            justifyContent: 'center',
                            borderRadius: 5,
                          }}>
                          <View style={{ alignSelf: 'center' }}>
                            <Text
                              style={{
                                color:
                                  trackType === 'rider' ? '#ffffff' : '#030E1A',
                                fontWeight: 'bold',
                              }}>
                              Rider
                            </Text>
                          </View>
                        </Col>
                      </TouchableNativeFeedback>
                      <TouchableNativeFeedback
                        onPress={() => this.toggleTrackType('parcel')}>
                        <Col
                          style={{
                            backgroundColor:
                              trackType === 'parcel'
                                ? '#1B2E5A'
                                : 'transparent',
                            justifyContent: 'center',
                            borderRadius: 5,
                          }}>
                          <View
                            style={{
                              alignSelf: 'center',
                            }}>
                            <Text
                              style={{
                                color:
                                  trackType === 'parcel'
                                    ? '#ffffff'
                                    : '#030E1A',
                                fontWeight: 'bold',
                              }}>
                              Parcel
                            </Text>
                          </View>
                        </Col>
                      </TouchableNativeFeedback>
                    </Row>
                  </Grid>
                </Item>
              </View>
              <View style={{ flex: 1, padding: 5 }}>
                <Button
                  block
                  full
                  style={{
                    backgroundColor: '#3FAF5D',
                    borderRadius: 5,
                    marginTop: 50,
                  }}
                  onPress={() => this.toggleTrackOrder()}>
                  <Text
                    style={{
                      color: '#ffffff',
                      fontFamily: 'Nunito-SemiBold',
                      fontSize: 14,
                    }}>
                    TRACK
                  </Text>
                </Button>
              </View>
            </Content>
          </Container>
        )) || (
          <TrackResult
            toggleTrackOrder={this.toggleTrackOrder}
            trackType={trackType}
          />
        )}
      </>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TrackOrder);
