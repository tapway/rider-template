/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { View, Text, TouchableNativeFeedback, TextInput } from 'react-native';
import { connect } from 'react-redux';
import {
  Container,
  Header,
  Left,
  Button,
  Icon,
  Body,
  Right,
  Content,
  Grid,
  Row,
  Col,
  Card,
  Item,
  Input,
  Picker,
  Thumbnail,
  CardItem,
} from 'native-base';
import { Actions } from 'react-native-router-flux';
import styles from './styles';

class TrackResult extends Component {
  componentDidMount = () => {
    console.log(this.props);
  };

  render() {
    return (
      <Container>
        <Header
          transparent
          hasTabs
          style={{
            backgroundColor: '#FFFFFF',
            borderBottomColor: '#0001',
            borderBottomWidth: 1.5,
          }}
          androidStatusBarColor={'#0001'}
          iosBarStyle="dark-content">
          <Left>
            <Button transparent onPress={this.props.toggleTrackOrder}>
              <Icon
                style={{ paddingHorizontal: 5, color: '#000000' }}
                name={'arrow-back'}
              />
            </Button>
          </Left>
          <Body>
            <Text
              style={{
                fontSize: 18,
                fontFamily: 'Nunito-SemiBold',
                color: '#030E1A',
              }}>
              Track Result
            </Text>
          </Body>
          <Right />
        </Header>
        <Content contentContainerStyle={{ padding: 20 }}>
          <View style={styles.inputContainer}>
            <Grid>
              <Row>
                <Col style={{ alignItems: 'flex-start' }}>
                  <Text
                    style={{
                      fontSize: 20,
                      fontWeight: 'bold',
                      fontFamily: 'Nunito-SemiBold',
                      color: '#1B2E5A',
                    }}>
                    FX2948TPW
                  </Text>
                </Col>
                <Col style={{ alignItems: 'flex-end' }}>
                  <View>
                    <Text>Est Delivery Time</Text>
                    <Text
                      style={{
                        fontSize: 20,
                        fontWeight: 'bold',
                        fontFamily: 'Nunito-SemiBold',
                        color: '#1B2E5A',
                      }}>
                      45 min
                    </Text>
                  </View>
                </Col>
              </Row>
            </Grid>
          </View>
          <View
            style={[
              styles.inputContainer,
              {
                marginHorizontal: -20,
                backgroundColor: '#dbede0',
                paddingVertical: 20,
              },
            ]}>
            <Text
              style={{
                fontSize: 20,
                fontFamily: 'Nunito-SemiBold',
                alignSelf: 'center',
                color: '#1B2E5A',
              }}>
              In Transit
            </Text>
          </View>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TrackResult);
