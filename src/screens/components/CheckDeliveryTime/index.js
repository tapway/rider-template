/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { View, Text, TouchableNativeFeedback } from 'react-native';
import { connect } from 'react-redux';
import {
  Container,
  Header,
  Left,
  Icon,
  Body,
  Right,
  Content,
  Button,
  Card,
  Item,
  Input,
  Col,
  Picker,
  Form,
  CardItem,
} from 'native-base';
import { Actions } from 'react-native-router-flux';
import styles from './styles';
import { Grid, Row } from 'react-native-easy-grid';

export class CheckDeliveryTime extends Component {
  constructor(props) {
    super(props);

    this.state = {
      itemQuality: 0,
      selectedCard: '',
      checkPriceOrder: false,
    };
  }

  togglecheckPriceOrder = () => {
    this.setState({ checkPriceOrder: !this.state.checkPriceOrder });
  };

  handleSelectedCard = selectedCard => {
    this.setState({ selectedCard });
  };

  render() {
    const { itemQuality, selectedCard, checkPriceOrder } = this.state;
    const {navigation}= this.props
    return (
      <Container>
        <Header
          style={{ backgroundColor: '#FFFFFF', marginTop: 35 }}
          androidStatusBarColor={'#FFFFFF'}>
          <Left>
            <Button transparent onPress={() => navigation.goBack()}>
              <Icon name="arrow-back" style={{ color: '#000000' }} />
            </Button>
          </Left>
          <Body>
            <Text
              numberOfLines={1}
              style={{
                fontSize: 18,
                color: '#030E1A',
                fontFamily: 'Nunito-SemiBold',
              }}>
              Check Delivery Time
            </Text>
          </Body>
          <Right />
        </Header>
        <Content contentContainerStyle={{ padding: 10 }}>
          <Card transparent>
            <View style={styles.inputContainer}>
              <Text style={styles.inputLabel}>Pickup Location</Text>
              <Item regular style={styles.inputItem}>
                <Icon
                  type="FontAwesome"
                  name="map-marker"
                  style={{ color: '#1B2E5A', fontSize: 20 }}
                />
                <Input
                  placeholder="Enter Location"
                  placeholderTextColor={'#383838'}
                  style={styles.inputText}
                />
              </Item>
            </View>
            <View style={styles.inputContainer}>
              <Text style={styles.inputLabel}>Drop Location</Text>
              <Item regular style={styles.inputItem}>
                <Icon
                  type="FontAwesome"
                  name="map-marker"
                  style={{ color: '#3FAF5D', fontSize: 20 }}
                />
                <Input
                  placeholder="Enter Location"
                  placeholderTextColor={'#383838'}
                  style={styles.inputText}
                />
              </Item>
            </View>
            <View style={styles.inputContainer}>
              <Text style={styles.inputLabel}>Select size of package</Text>
              <Grid>
                <Row>
                  <Col style={{ margin: 5 }}>
                    <Card
                      style={[
                        styles.packageCard,
                        selectedCard === 'small' ? styles.selectedCard : {},
                      ]}>
                      <TouchableNativeFeedback
                        onPress={() => this.handleSelectedCard('small')}>
                        <View>
                          <Icon
                            type="AntDesign"
                            name="inbox"
                            style={styles.packageIcon}
                          />
                          <Text style={styles.packageSize}>Small</Text>
                          <Text style={styles.packageWeight}>Up to 10kg</Text>
                        </View>
                      </TouchableNativeFeedback>
                    </Card>
                  </Col>
                  <Col style={{ margin: 5 }}>
                    <Card
                      style={[
                        styles.packageCard,
                        selectedCard === 'medium' ? styles.selectedCard : {},
                      ]}>
                      <TouchableNativeFeedback
                        onPress={() => this.handleSelectedCard('medium')}>
                        <View>
                          <Icon
                            type="AntDesign"
                            name="inbox"
                            style={styles.packageIcon}
                          />
                          <Text style={styles.packageSize}>Medium</Text>
                          <Text style={styles.packageWeight}>Up to 20kg</Text>
                        </View>
                      </TouchableNativeFeedback>
                    </Card>
                  </Col>
                  <Col style={{ margin: 5 }}>
                    <Card
                      style={[
                        styles.packageCard,
                        selectedCard === 'large' ? styles.selectedCard : {},
                      ]}>
                      <TouchableNativeFeedback
                        onPress={() => this.handleSelectedCard('large')}>
                        <View>
                          <Icon
                            type="AntDesign"
                            name="inbox"
                            style={styles.packageIcon}
                          />
                          <Text style={styles.packageSize}>Large</Text>
                          <Text style={styles.packageWeight}>Up to 30kg</Text>
                        </View>
                      </TouchableNativeFeedback>
                    </Card>
                  </Col>
                </Row>
              </Grid>
            </View>
            <View style={styles.inputContainer}>
              <Text style={styles.inputLabel}>Select size of package</Text>
              <Item regular style={styles.inputItem}>
                <Picker
                  note
                  mode="dropdown"
                  // style={{ width: 120 }}
                  selectedValue={itemQuality}
                  onValueChange={item => this.setState({ itemQuality: item })}>
                  <Picker.Item label="Select Number" value="0" />
                  <Picker.Item label="1" value="1" />
                  <Picker.Item label="2" value="2" />
                  <Picker.Item label="3" value="3" />
                  <Picker.Item label="4" value="4" />
                  <Picker.Item label="5" value="5" />
                </Picker>
              </Item>
            </View>
          </Card>

          <Button
            block
            primary
            style={styles.submitBtn}
            onPress={() => this.togglecheckPriceOrder()}>
            <Text style={styles.btnText}>CHECK DELIVERY TIME</Text>
          </Button>
        </Content>
        {checkPriceOrder && (
          <View
            style={{
              position: 'absolute',
              top: 0,
              bottom: 0,
              right: 0,
              left: 0,
              backgroundColor: '#1B2E5A',
            }}>
            <Header
              style={{ backgroundColor: '#1B2E5A', marginTop: 35 }}
              iosBarStyle="light-content"
              androidStatusBarColor={'#1B2E5A'}>
              <Left>
                <Button
                  transparent
                  onPress={() => this.togglecheckPriceOrder()}>
                  <Icon name="arrow-back" style={{ color: '#ffffff' }} />
                </Button>
              </Left>
              <Body>
                <Text
                  numberOfLines={1}
                  style={{
                    fontSize: 18,
                    color: '#FFFFFF',
                    fontFamily: 'Nunito-SemiBold',
                  }}>
                  Check Price
                </Text>
              </Body>
              <Right />
            </Header>
            <Content padder>
              <Card style={{ paddingHorizontal: 10 }}>
                <View style={[styles.inputContainer, styles.containerBorder]}>
                  <Text style={styles.inputLabel2}>Pickup Location</Text>
                  <Text style={styles.inputText2}>
                    5 Ogunbamila Street, Lady Lak, Bariga
                  </Text>
                </View>
                <View style={[styles.inputContainer, styles.containerBorder]}>
                  <Text style={styles.inputLabel2}>Drop Location</Text>
                  <Text style={styles.inputText2}>
                    Akilo Street, Lady Lak, Bariga
                  </Text>
                </View>
                <View style={[styles.inputContainer, styles.containerBorder]}>
                  <Grid>
                    <Row>
                      <Col>
                        <Text style={styles.inputLabel2}>Size of Package</Text>
                        <Card transparent>
                          <View style={{ flexDirection: 'row' }}>
                            <Icon
                              type="AntDesign"
                              name="inbox"
                              style={styles.packageIcon}
                            />
                            <View>
                              <Text style={styles.packageSize}>Large</Text>
                              <Text style={styles.packageWeight}>
                                Up to 30kg
                              </Text>
                            </View>
                          </View>
                        </Card>
                      </Col>
                      <Col>
                        <Text style={styles.inputLabel2}>Quantity</Text>
                        <Card transparent>
                          <View>
                            <Text style={styles.inputText2}>2</Text>
                          </View>
                        </Card>
                      </Col>
                    </Row>
                  </Grid>
                </View>
                <View style={[styles.inputContainer, styles.containerBorder]}>
                  <Grid>
                    <Row>
                      <Col>
                        <Text style={styles.inputLabel2}>Total Distance</Text>
                        <Card transparent>
                          <View>
                            <Text style={styles.inputText2}>5.4 km</Text>
                          </View>
                        </Card>
                      </Col>
                      <Col>
                        <Text style={styles.inputLabel2}>
                          Est. Delivery time
                        </Text>
                        <Card transparent>
                          <View>
                            <Text style={styles.inputText2}>45 min</Text>
                          </View>
                        </Card>
                      </Col>
                    </Row>
                  </Grid>
                </View>
                <View style={[styles.inputContainer]}>
                  <View>
                    <Text style={styles.inputLabel2}>Size of Package</Text>
                  </View>
                  <Text
                    style={{
                      alignSelf: 'center',
                      fontSize: 30,
                      color: '#030E1A',
                    }}>
                    N24,000
                  </Text>
                </View>

                <Button
                  block
                  primary
                  style={styles.submitBtn}
                  onPress={() => navigation.navigate('login')}>
                  <Text style={styles.btnText}>LOGIN TO MAKE ORDER</Text>
                </Button>
              </Card>
            </Content>
          </View>
        )}
      </Container>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CheckDeliveryTime);
