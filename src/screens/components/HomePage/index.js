/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import {
  View, Text, StyleSheet, TouchableOpacity, Switch, Platform,
  PermissionsAndroid
} from 'react-native';
import { connect } from 'react-redux';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import RNGooglePlaces from 'react-native-google-places';
import PropTypes from 'prop-types';
//import Geolocation from '@react-native-community/geolocation';
import Geolocation from 'react-native-geolocation-service';

import Toast from 'react-native-easy-toast';
//import styles from './style';
import {
  Container,
  Header,
  Left,
  Icon,
  Body,
  Right,
  Content,
  Button,
  Card,
  Input,
} from 'native-base';
//import getImage from '../../utils/getImage';
//import LogOut from '../LogOut';
import colors from '../../../utils/colors';
import { Actions } from 'react-native-router-flux';
import mapStyle from './mapStyle';
import { heightRatio, widthRatio } from '../../../utils/stylesheet';
import { resetActionLogin } from '../../../navigation/ResetNavigation';
import { socketRiderInit, updateLocation } from '../../../socket/index';
import { setAvailable } from '../../../redux/action/home';
import { setLocation } from '../../../redux/action/home';
import setTripStatus from '../../../apiService/setTripStatus';
import setAvailableDb from '../../../apiService/available';
import RequestCard from './RequestCard';
import GotoPickUpCard from './GotoPickUpCard';
import GotoDropOffCard from './GotoDropOffCard';
import FinishJobCard from './FinishJobCard';

export class HomePage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      logoutModal: false,
      isPackageDetailsVisible: false,
      fromLocation: '',
      fromLocationSet: false,
      toLocation: '',
      toLocationSet: false,
      predictions: [],
      currentInput: '',
      region: {
        latitude: 6.599033,
        longitude: 3.3411348,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
      },
      isRequestCardVisible: false,
      isPickUpCardVisible: false,
      isDropOffCardVisible: false,
      isFinishJobCardVisible: false,
      isDropOffContentVisible: false,
      isFinishJobContentVisible: false,
    };
    setTripStatus(false, props.user.jwtAccessToken);
    socketRiderInit();
    this.orderNotification = this.orderNotification.bind(this);
  }

  componentDidMount() {
    const that = this;
    if (Platform.OS === 'ios') {
      this.callLocation(that);
    } else {
      // eslint-disable-next-line no-inner-declarations
      async function requestLocationPermission() {
        try {
          const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
            {
              title: 'Location Access Required',
              message: 'This App needs to Access your location',
            },
          );
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            that.callLocation(that);
            //console.log(this.props);

          }
        } catch (err) {
          throw err;
        }
      }
      requestLocationPermission();
    }
  }

  orderPickUp = () => {
    const { fromLocation, toLocation } = this.state;
    if (fromLocation.length < 0 || toLocation.length < 0) {
      alert.alert('Select location from dropdown');
    } else {
      this.props.navigation.navigate('PackageDetails', { fromLocation, toLocation })
      //Actions.PackageDetails({ fromLocation, toLocation });
    }
  };

  checkInput = () => {
    const { fromLocationSet, toLocationSet } = this.state;
    let inputState = true;
    if (toLocationSet && fromLocationSet) {
      inputState = false;
    }
    return inputState;
  };

  togglePackageDetails = () => {
    this.setState({
      isPackageDetailsVisible: !this.state.isPackageDetailsVisible,
    });
  };

  getPredictions = place => {
    RNGooglePlaces.getAutocompletePredictions(place, {
      //  type: 'cities',
      country: 'NG',
    })
      .then(results =>
        this.setState(
          { predictions: results },
          () => { },
          // console.log('Predictions', results),
        ),
      )
      .catch(error => console.log(error.message));
  };

  clearPredictions = () => {
    this.setState({ predictions: [] });
  };

  setLocationPlace = (place, locationType) => {
    if (locationType === 'from') {
      RNGooglePlaces.lookUpPlaceByID(place.placeID)
        .then(results => {
          console.log('logging lookUpPlaceByID', results);
          const { latitude, longitude, viewport } = results.location;
          this.setState(
            {
              fromLocationSet: true,
              fromLocation: results.name,
              fromLocation_lat: latitude,
              fromLocation_lng: longitude,
              region: {
                latitude,
                longitude: this.state.toLocation_lng
                  ? this.state.toLocation_lng
                  : longitude,
                latitudeDelta: 0.3699098778279124,
                longitudeDelta: 0.1909481361508365,
              },
            },
            () => {
              // console.log('region', JSON.stringify(region));
              this.clearPredictions();
            },
          );
        })
        .catch(error => console.log(error.message));
    } else {
      RNGooglePlaces.lookUpPlaceByID(place.placeID)
        .then(results => {
          const { latitude, longitude } = results.location;
          this.setState(
            {
              toLocationSet: true,
              toLocation: results.name,
              toLocation_lat: latitude,
              toLocation_lng: longitude,
              region: {
                latitude: this.state.fromLocation_lat
                  ? this.state.fromLocation_lat
                  : latitude,
                longitude,
                latitudeDelta: 0.3699098778279124,
                longitudeDelta: 0.1909481361508365,
              },
            },
            () => {
              this.clearPredictions();
            },
          );
        })
        .catch(error => console.log(error.message));
    }
  };

  placesLists = (place, locationType) => {
    if (locationType === 'from') {
      this.setState(
        {
          locationType,
          fromLocation: place,
          fromLocationSet: false,
          currentInput: locationType,
        },
        () => {
          this.getPredictions(place, locationType);
        },
      );
    } else {
      this.setState(
        {
          locationType,
          toLocationSet: false,
          toLocation: place,
          currentInput: locationType,
        },
        () => {
          this.getPredictions(place, locationType);
        },
      );
    }
  };

  toggleSwitch1 = value => {
    const { props } = this;
    props.setAvailableDispatcher(value);
    setAvailableDb(value, props.user.jwtAccessToken);
  };

  navigation = nav => {
    const { props } = this;
    props.navigation.navigate(nav);
  };

  callLocation() {
    console.log('call location fired ')
    const { props } = this;
    this.watchId = Geolocation.watchPosition(
      position => {
        console.log(position)
        updateLocation(props.user, [
          position.coords.longitude,
          position.coords.latitude,
        ]);
        props.setLocDispatcher([
          position.coords.longitude,
          position.coords.latitude,
        ]);
      },
      error => console.log(error.message),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
    );
  }

  orderNotification() {
    const { props } = this;
    // eslint-disable-next-line no-underscore-dangle
    if (props.order._id === '') {
      props.navigation.navigate('orderNotification');
    } else {
      props.navigation.navigate('readyForDelivery');
    }
  }

  toggleCards = (valueTrue, valueFalse) => {
    const { routeCalc } = this.props;
    if (
      routeCalc &&
      routeCalc === 'routeCalc' &&
      valueTrue === 'isRequestCardVisible'
    ) {
      this.setState({ isPickUpCardVisible: false });
    } else {
      this.setState({ [valueTrue]: true, [valueFalse]: false });
    }
  };

  toggleDropOffContent = () => {
    this.setState({
      isDropOffContentVisible: !this.state.isDropOffContentVisible,
    });
  };

  toggleFinishJobContent = () => {
    this.setState({
      isFinishJobContentVisible: !this.state.isFinishJobContentVisible,
    });
  };

  resetCards = () => {
    this.setState(
      {
        isDropOffCardVisible: false,
        isFinishJobCardVisible: false,
        isPickUpCardVisible: false,
        isRequestCardVisible: false,
      },
      () => {
        this.toggleSwitch1(false);
        this.props.navigation.navigate('ReviewPage')
      },
    );
  };

  componentWillMount() {
    const { routeCalc } = this.props;
    console.log('routeCalc', routeCalc);

    if (routeCalc && routeCalc === 'routeCalc') {
      this.setState({ isPickUpCardVisible: true });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.user.isAvailable !== this.props.user.isAvailable) {
      console.log('this.props.user.isAvailable', this.props.user.isAvailable);

      if (this.props.user.isAvailable) {
        this.setState(
          { isRequestCardVisible: this.props.user.isAvailable },
          () => {
            console.log(
              'isRequestCardVisible',
              this.state.isRequestCardVisible,
            );
          },
        );
      } else {
        this.setState(
          { isRequestCardVisible: this.props.user.isAvailable },
          () => {
            console.log(
              'isRequestCardVisible',
              this.state.isRequestCardVisible,
            );
          },
        );
      }
    }
  }

  render() {
    const {
      toLocation,
      toLocationSet,
      toLocation_lat,
      toLocation_lng,

      fromLocation,
      fromLocationSet,
      fromLocation_lat,
      fromLocation_lng,

      predictions,
      currentInput,
      region,
      isRequestCardVisible,
      isPickUpCardVisible,
      isDropOffCardVisible,
      isFinishJobCardVisible,
      isDropOffContentVisible,
      isFinishJobContentVisible,
    } = this.state;

    const { user: userobj } = this.props;


    return (
      <Container style={{ flex: 1, backgroundColor: '#ffffff' }}>
        <MapView
          provider={PROVIDER_GOOGLE}
          style={styles.map}
          region={region}
          // onRegionChangeComplete={res => console.log('region change', res)}
          customMapStyle={mapStyle}>
          {fromLocationSet && (
            <Marker
              key={1}
              coordinate={{
                latitude: Number(fromLocation_lat),
                longitude: Number(fromLocation_lng),
              }}
              pinColor={'#1B2E5A'}
            />
          )}
          {toLocationSet && (
            <Marker
              key={2}
              coordinate={{
                latitude: Number(toLocation_lat),
                longitude: Number(toLocation_lng),
              }}
              pinColor={'#ffb600'}
            />
          )}
        </MapView>
        <Header transparent androidStatusBarColor={'#0001'}>
          <Left>
            <Button transparent onPress={() => this.props.navigation.openDrawer()}>
              <Icon
                style={{ paddingHorizontal: 10, color: '#000000' }}
                name={'menu'}
              />
            </Button>
          </Left>
          <Body />
          <Right />
        </Header>
        <View
          style={{
            flex: 1,
            position: 'absolute',
            bottom: 0,
            left: 0,
            right: 0,
            padding: 20,
          }}>
          {!isRequestCardVisible &&
            !isPickUpCardVisible &&
            !isDropOffCardVisible &&
            !isFinishJobCardVisible && (
              <Button
                success
                style={{
                  backgroundColor: userobj.isAvailable ? 'grey' : '#3FAF5D',
                  alignSelf: 'center',
                  paddingHorizontal: 10,
                }}
                onPress={() => this.toggleSwitch1(!userobj.isAvailable)}>
                <Text
                  style={{
                    color: '#ffffff',
                    fontFamily: 'Nunito-SemiBold',
                    fontSize: 16,
                  }}>
                  {userobj.isAvailable ? 'GO OFFLINE' : 'GO ONLINE'}
                </Text>
              </Button>
            )}
          {isRequestCardVisible && (
            <RequestCard
              userobj={userobj}
              toggleSwitch1={this.toggleSwitch1}
              toggleCards={this.toggleCards}
            />
          )}
          {isPickUpCardVisible && !isRequestCardVisible && (
            <GotoPickUpCard userobj={userobj} toggleCards={this.toggleCards} />
          )}
          {isDropOffCardVisible && !isPickUpCardVisible && (
            <GotoDropOffCard
              userobj={userobj}
              toggleCards={this.toggleCards}
              isDropOffContentVisible={isDropOffContentVisible}
              toggleDropOffContent={this.toggleDropOffContent}
            />
          )}
          {isFinishJobCardVisible && !isDropOffCardVisible && (
            <FinishJobCard
              userobj={userobj}
              toggleCards={this.toggleCards}
              resetCards={this.resetCards}
              isFinishJobContentVisible={isFinishJobContentVisible}
              toggleFinishJobContent={this.toggleFinishJobContent}
            />
          )}
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});

const mapStateToProps = state => {
  return {
    user: state.currentUser,
    order: state.currentOrder,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setLocDispatcher: coor => {
      dispatch(setLocation(coor));
    },
    setAvailableDispatcher: val => {
      dispatch(setAvailable(val));
    },
  };
};

HomePage.propTypes = {
  setAvailableDispatcher: PropTypes.func.isRequired,
  setLocDispatcher: PropTypes.func.isRequired,
  signout: PropTypes.func.isRequired,
  order: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.object,
    PropTypes.array,
  ]).isRequired,
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
  user: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
