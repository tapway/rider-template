/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import {
  Container,
  Header,
  Left,
  Icon,
  Body,
  Right,
  Content,
  Button,
  Card,
  Input,
  Grid,
  Row,
  Col,
  Thumbnail,
} from 'native-base';
import styles from './styles';
import getImage from '../../../utils/getImage';

const FinishJobCard = ({
  userobj,
  toggleSwitch1,
  toggleCards,
  resetCards,
  isFinishJobContentVisible,
  toggleFinishJobContent,
}) => {
  return (
    <Card
      style={{
        backgroundColor: '#1891D0',
        borderRadius: 5,
        paddingHorizontal: 10,
      }}>
      {!isFinishJobContentVisible && (
        <View
          style={[
            styles.inputContainer,
            styles.containerBorder,
            { backgroundColor: '#1891D0' },
          ]}>
          <TouchableOpacity
            onPress={toggleFinishJobContent}
            style={{
              alignItems: 'center',
            }}>
            <Icon
              name="expand-more"
              type="MaterialIcons"
              style={{ color: '#ffffff' }}
            />
          </TouchableOpacity>
        </View>
      )}
      {isFinishJobContentVisible && (
        <View style={[styles.inputContainer, { backgroundColor: '#1891D0' }]}>
          <TouchableOpacity onPress={toggleFinishJobContent}>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ flex: 1 }}>
                <Text
                  numberOfLines={1}
                  ellipsizeMode="tail"
                  style={[
                    styles.inputLabel2,
                    styles.whiteText,
                    { overflow: 'hidden' },
                  ]}>
                  Pick up 5 Ogunbamila Street, Lady Lak, Bariga
                </Text>
              </View>
              <View style={{ marginHorizontal: 10 }}>
                <Icon
                  name="expand-less"
                  type="MaterialIcons"
                  style={{ color: '#ffffff' }}
                />
              </View>
            </View>
          </TouchableOpacity>
        </View>
      )}

      {!isFinishJobContentVisible && (
        <View>
          <View style={[styles.inputContainer, { backgroundColor: '#1891D0' }]}>
            <Grid>
              <Row>
                <Col>
                  <Text style={[styles.inputLabel2, { fontSize: 12 }]}>
                    DROP LOCATION
                  </Text>
                  <View>
                    <Text style={[styles.inputLabel2, styles.whiteText]}>
                      {'5 Ogunbamila Street, Lady Lak, Bariga'}
                    </Text>
                  </View>
                </Col>
                <Col>
                  <Text style={[styles.inputLabel2, { fontSize: 12 }]}>
                    EST. DELIVERY TIME
                  </Text>
                  <Text style={[styles.inputLabel2, styles.whiteText]}>
                    10 min
                  </Text>
                </Col>
              </Row>
            </Grid>
          </View>

          <View style={[styles.inputContainer, { marginVertical: 1 }]}>
            <View style={{ flexDirection: 'row', marginVertical: 2 }}>
              <Text style={[styles.inputLabel2, { fontSize: 12 }]}>
                Person in Charge:
              </Text>
              <Text
                style={[
                  styles.inputLabel2,
                  styles.whiteText,
                  { marginLeft: 10, fontSize: 14 },
                ]}>
                {'Femi Ajiboye'}
              </Text>
            </View>
            <View style={{ flexDirection: 'row', marginVertical: 2 }}>
              <Text style={[styles.inputLabel2, { fontSize: 12 }]}>
                Phone Number:
              </Text>
              <Text
                style={[
                  styles.inputLabel2,
                  styles.whiteText,
                  { marginLeft: 20, fontSize: 14 },
                ]}>
                {'070567376830'}
              </Text>
            </View>
          </View>

          <View
            style={[
              styles.inputContainer,
              {
                flexDirection: 'row',
                marginVertical: 1,
                marginBottom: 5,
                padding: 10,
              },
              styles.containerBorder,
            ]}>
            <Left
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <View
                style={{
                  backgroundColor: '#ffff',
                  borderRadius: 5,
                  padding: 2,
                  shadowColor: '#0000',
                  shadowOffset: {
                    width: 0,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 2.0,
                  elevation: 4,
                }}>
                <Thumbnail
                  style={{ borderRadius: 5, height: 50, width: 50 }}
                  square
                  small
                  source={getImage.man_hat}
                />
              </View>
              <View
                style={{
                  marginLeft: 20,
                }}>
                <Text style={[styles.inputLabel2, { fontSize: 14 }]}>
                  Customer name
                </Text>
                <Text style={[styles.inputLabel2, styles.whiteText]}>
                  Gregory Samuel
                </Text>
              </View>
            </Left>
            <Right>
              <View
                transparent
                style={{
                  backgroundColor: '#ffffff',
                  flexDirection: 'row',
                  borderRadius: 18,
                  padding: 10,
                  alignSelf: 'flex-end',
                  // margin: 2,
                  // paddingVertical: -10,
                }}>
                <Text
                  style={{
                    color: '#1891D0',
                    fontSize: 16,
                    alignSelf: 'center',
                    paddingHorizontal: 5,
                  }}>
                  Call
                </Text>
                <Icon
                  name="call"
                  style={{
                    color: '#1891D0',
                    fontSize: 16,
                    alignSelf: 'center',
                    paddingHorizontal: 5,
                  }}
                />
              </View>
            </Right>
          </View>

          <View
            style={[
              styles.inputContainer,
              {
                backgroundColor: '#1891D0',
                marginVertical: 10,
                flexDirection: 'row',
                justifyContent: 'center',
              },
            ]}>
            {/* <Button
          transparent
          small
          success
          style={{
            // backgroundColor: userobj.isAvailable ? 'grey' : '#3FAF5D',
            alignSelf: 'center',
            paddingHorizontal: 10,
          }}
          onPress={() =>
            toggleCards('isPickUpCardVisible', 'isDropOffCardVisible')
          }>
          <Text
            style={{
              color: '#ffffff',
              fontFamily: 'Nunito-SemiBold',
              fontSize: 16,
            }}>
            CANCEL JOB
          </Text>
        </Button> */}
            <Button
              small
              success
              style={{
                backgroundColor: '#3FAF5D',
                alignSelf: 'center',
                paddingHorizontal: 10,
              }}
              onPress={resetCards}>
              <Text
                style={{
                  color: '#ffffff',
                  fontFamily: 'Nunito-SemiBold',
                  fontSize: 16,
                }}>
                FINISH JOB
              </Text>
            </Button>
          </View>
        </View>
      )}
    </Card>
  );
};

export default FinishJobCard;
