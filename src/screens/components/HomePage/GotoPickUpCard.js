/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import {
  Container,
  Header,
  Left,
  Icon,
  Body,
  Right,
  Content,
  Button,
  Card,
  Input,
  Grid,
  Row,
  Col,
  Thumbnail,
} from 'native-base';
import styles from './styles';
import getImage from '../../../utils/getImage';

const GotoPickUpCard = ({ userobj, toggleSwitch1, toggleCards }) => {
  return (
    <Card
      style={{
        backgroundColor: '#1891D0',
        borderRadius: 5,
        paddingHorizontal: 10,
      }}>
      <View
        style={[styles.inputContainer, { marginTop: 10, marginVertical: 5 }]}>
        <Text style={[styles.inputLabel2, { fontSize: 12 }]}>
          PICKUP LOCATION
        </Text>
        <View>
          <Text style={[styles.inputLabel2, styles.whiteText]}>
            {'5 Ogunbamila Street, Lady Lak, Bariga'}
          </Text>
        </View>
      </View>

      <View style={[styles.inputContainer, { marginVertical: 5 }]}>
        <Text style={[styles.inputLabel2, { fontSize: 12 }]}>
          EST. DELIVERY TIME
        </Text>
        <Text style={[styles.inputLabel2, styles.whiteText]}>10 min</Text>
      </View>

      <View style={[styles.inputContainer, { marginVertical: 5 }]}>
        <Text style={[styles.inputLabel2, { fontSize: 12 }]}>
          ADDITIONAL INFORMATION
        </Text>
        <Text style={[styles.inputLabel2, styles.whiteText]}>
          when an unknown printer took a galley of type and scrambled it to make
          a type specimen book.
        </Text>
        <View style={{ flexDirection: 'row', marginVertical: 5 }}>
          <Text style={[styles.inputLabel2, { fontSize: 12 }]}>
            Person in Charge:
          </Text>
          <Text
            style={[
              styles.inputLabel2,
              styles.whiteText,
              { marginLeft: 10, fontSize: 14 },
            ]}>
            {'Femi Ajiboye'}
          </Text>
        </View>
        <View style={{ flexDirection: 'row', marginVertical: 5 }}>
          <Text style={[styles.inputLabel2, { fontSize: 12 }]}>
            Phone Number:
          </Text>
          <Text
            style={[
              styles.inputLabel2,
              styles.whiteText,
              { marginLeft: 20, fontSize: 14 },
            ]}>
            {'070567376830'}
          </Text>
        </View>
      </View>

      <View
        style={[
          styles.inputContainer,
          styles.containerBorder,
          {
            backgroundColor: '#1891D0',
            marginVertical: 2,
            // borderBottomColor:
          },
        ]}>
        <Grid>
          <Row>
            <Col>
              <Text style={[styles.inputLabel2, { fontSize: 12 }]}>
                SIZE OF PACKAGE
              </Text>
              <Card transparent>
                <View style={{ flexDirection: 'row' }}>
                  <Icon
                    type="AntDesign"
                    name="inbox"
                    style={styles.packageIcon}
                  />
                  <View>
                    <Text style={[styles.packageSize, styles.whiteText]}>
                      {'Medium'}
                    </Text>
                    <Text style={[styles.whiteText]}>{'Up to 20Kg'}</Text>
                  </View>
                </View>
              </Card>
            </Col>
            <Col>
              <Text style={[styles.inputLabel2, { fontSize: 12 }]}>
                QUANTITY
              </Text>
              <Card transparent>
                <View>
                  <Text style={[styles.inputLabel2, styles.whiteText]}>2</Text>
                </View>
              </Card>
            </Col>
          </Row>
        </Grid>
      </View>

      <View
        style={[
          styles.inputContainer,
          {
            flexDirection: 'row',
            marginBottom: 5,
            padding: 10,
          },
          styles.containerBorder,
        ]}>
        <Left
          style={{
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <View
            style={{
              backgroundColor: '#ffff',
              borderRadius: 5,
              padding: 2,
              shadowColor: '#0000',
              shadowOffset: {
                width: 0,
                height: 2,
              },
              shadowOpacity: 0.23,
              shadowRadius: 2.0,
              elevation: 4,
            }}>
            <Thumbnail
              style={{ borderRadius: 5, height: 50, width: 50 }}
              square
              small
              source={getImage.man_hat}
            />
          </View>
          <View
            style={{
              marginLeft: 20,
            }}>
            <Text style={[styles.inputLabel2, { fontSize: 14 }]}>
              Customer name
            </Text>
            <Text style={[styles.inputLabel2, styles.whiteText]}>
              Gregory Samuel
            </Text>
          </View>
        </Left>
        <Right>
          <View
            transparent
            style={{
              backgroundColor: '#ffffff',
              flexDirection: 'row',
              borderRadius: 18,
              padding: 10,
              alignSelf: 'flex-end',
              // margin: 2,
              // paddingVertical: -10,
            }}>
            <Text
              style={{
                color: '#1891D0',
                fontSize: 16,
                alignSelf: 'center',
                paddingHorizontal: 5,
              }}>
              Call
            </Text>
            <Icon
              name="call"
              style={{
                color: '#1891D0',
                fontSize: 16,
                alignSelf: 'center',
                paddingHorizontal: 5,
              }}
            />
          </View>
        </Right>
      </View>

      <View
        style={[
          styles.inputContainer,
          {
            backgroundColor: '#1891D0',
            marginVertical: 10,
            flexDirection: 'row',
            justifyContent: 'flex-end',
          },
        ]}>
        <Button
          transparent
          small
          success
          style={{
            // backgroundColor: userobj.isAvailable ? 'grey' : '#3FAF5D',
            alignSelf: 'center',
            paddingHorizontal: 20,
          }}
          onPress={() =>
            toggleCards('isRequestCardVisible', 'isPickUpCardVisible')
          }>
          <Text
            style={{
              color: '#ffffff',
              fontFamily: 'Nunito-SemiBold',
              fontSize: 16,
            }}>
            CANCEL JOB
          </Text>
        </Button>
        <Button
          small
          success
          style={{
            backgroundColor: '#3FAF5D',
            alignSelf: 'center',
            paddingHorizontal: 20,
          }}
          onPress={() =>
            toggleCards('isDropOffCardVisible', 'isPickUpCardVisible')
          }>
          <Text
            style={{
              color: '#ffffff',
              fontFamily: 'Nunito-SemiBold',
              fontSize: 16,
            }}>
            GO TO PICKUP
          </Text>
        </Button>
      </View>
    </Card>
  );
};

export default GotoPickUpCard;
