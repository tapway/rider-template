/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import {
  Container,
  Header,
  Left,
  Icon,
  Body,
  Right,
  Content,
  Button,
  Card,
  Input,
  Grid,
  Row,
  Col,
} from 'native-base';
import styles from './styles';

const RequestCard = ({ userobj, toggleSwitch1, toggleCards }) => {
  return (
    <Card
      style={{
        backgroundColor: '#1891D0',
        borderRadius: 5,
        paddingHorizontal: 10,
      }}>
      <View style={[styles.inputContainer, { backgroundColor: '#1891D0' }]}>
        <Grid>
          <Row>
            <Col>
              <Text style={[styles.inputLabel2, { fontSize: 12 }]}>
                PICKUP LOCATION
              </Text>
              <View>
                <Text style={[styles.inputLabel2, styles.whiteText]}>
                  {'5 Ogunbamila Street, Lady Lak, Bariga'}
                </Text>
              </View>
            </Col>
            <Col>
              <Text style={[styles.inputLabel2, { fontSize: 12 }]}>
                EST. DELIVERY TIME
              </Text>
              <Text style={[styles.inputLabel2, styles.whiteText]}>10 min</Text>
            </Col>
          </Row>
        </Grid>
      </View>
      <View
        style={[
          styles.inputContainer,
          {
            backgroundColor: '#1891D0',
            marginVertical: 1,
          },
          styles.containerBorder,
        ]}>
        <Grid>
          <Row>
            <Col>
              <Text style={[styles.inputLabel2, { fontSize: 12 }]}>
                DROP LOCATION
              </Text>
              <Card transparent>
                <View>
                  <Text style={[styles.inputLabel2, styles.whiteText]}>
                    {'Akilo Street, Lady Lak, Bariga'}
                  </Text>
                </View>
              </Card>
            </Col>
            <Col>
              <Text style={[styles.inputLabel2, { fontSize: 12 }]}>
                QUANTITY
              </Text>
              <Card transparent>
                <View>
                  <Text style={[styles.inputLabel2, styles.whiteText]}>2</Text>
                </View>
              </Card>
            </Col>
          </Row>
        </Grid>
      </View>
      <View
        style={[
          styles.inputContainer,
          {
            backgroundColor: '#1891D0',
            marginVertical: 10,
            flexDirection: 'row',
            justifyContent: 'flex-end',
          },
        ]}>
        <Button
          transparent
          small
          success
          style={{
            // backgroundColor: userobj.isAvailable ? 'grey' : '#3FAF5D',
            alignSelf: 'center',
            paddingHorizontal: 10,
          }}
          onPress={() => toggleSwitch1(!userobj.isAvailable)}>
          <Text
            style={{
              color: '#ffffff',
              fontFamily: 'Nunito-SemiBold',
              fontSize: 16,
            }}>
            IGNORE
          </Text>
        </Button>
        <Button
          small
          success
          style={{
            backgroundColor: '#3FAF5D',
            alignSelf: 'center',
            paddingHorizontal: 10,
          }}
          onPress={() =>
            toggleCards('isPickUpCardVisible', 'isRequestCardVisible')
          }>
          <Text
            style={{
              color: '#ffffff',
              fontFamily: 'Nunito-SemiBold',
              fontSize: 16,
            }}>
            ACCEPT
          </Text>
        </Button>
      </View>
    </Card>
  );
};

export default RequestCard;
