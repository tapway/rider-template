/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { View, Text, TouchableNativeFeedback, TextInput } from 'react-native';
import { connect } from 'react-redux';
import {
  Container,
  Header,
  Left,
  Button,
  Icon,
  Body,
  Right,
  Content,
  Grid,
  Row,
  Col,
  Card,
  Item,
  Input,
  Picker,
  Thumbnail,
  CardItem,
} from 'native-base';
import { Actions } from 'react-native-router-flux';
import styles from './styles';
import getImage from '../../../utils/getImage';

class PackageDetails extends Component {
  constructor(props) {
    super(props);

    this.state = {
      itemQuality: 0,
      selectedCard: '',
      checkPriceOrder: false,
      isReviewVisible: false,
      isPayCardVisible: false,
      isSuccessful: false,
      packageSize: { title: '', weight: 0 },
      addInfo: '',
      personInCharge: '',
      contactNumber: '',
    };
  }

  toggleReviewVisible = () => {
    this.setState({
      isReviewVisible: !this.state.isReviewVisible,
      isPayCardVisible: false,
    });
  };
  togglePayCard = () => {
    this.setState({
      isPayCardVisible: !this.state.isPayCardVisible,
    });
  };
  toggleSuccessfulVisible = () => {
    this.setState({
      isSuccessful: !this.state.isSuccessful,
    });
  };

  handleSelectedCard = (selectedCard, weight) => {
    this.setState({
      selectedCard,
      packageSize: { title: selectedCard, weight },
    });
  };
  render() {
    const { fromLocation, toLocation,navigation } = this.props;

    const {
      selectedCard,
      itemQuality,
      isReviewVisible,
      isPayCardVisible,
      isSuccessful,
      addInfo,
      personInCharge,
      contactNumber,
      packageSize,
    } = this.state;

    return (
      <Container>
        <Header
          transparent
          hasTabs
          style={{
            backgroundColor: '#FFFFFF',
            borderBottomColor: '#0001',
            borderBottomWidth: 1.5,
          }}
          androidStatusBarColor={'#0001'}
          iosBarStyle="dark-content">
          <Left>
            <Button transparent onPress={() => navigation.goBack()}>
              <Icon
                style={{ paddingHorizontal: 5, color: '#000000' }}
                name={'arrow-back'}
              />
            </Button>
          </Left>
          <Body>
            <Text
              style={{
                fontSize: 18,
                fontFamily: 'Nunito-SemiBold',
                color: '#030E1A',
              }}>
              Package Details
            </Text>
          </Body>
          <Right />
        </Header>
        <Content contentContainerStyle={{ padding: 10 }}>
          <View style={styles.inputContainer}>
            <Text style={styles.inputLabel}>Select size of package</Text>
            <Grid>
              <Row>
                <Col style={{ margin: 5 }}>
                  <Card
                    style={[
                      styles.packageCard,
                      selectedCard === 'small' ? styles.selectedCard : {},
                    ]}>
                    <TouchableNativeFeedback
                      onPress={() => this.handleSelectedCard('small', 10)}>
                      <View>
                        <Icon
                          type="AntDesign"
                          name="inbox"
                          style={styles.packageIcon}
                        />
                        <Text style={styles.packageSize}>Small</Text>
                        <Text style={styles.packageWeight}>Up to 10kg</Text>
                      </View>
                    </TouchableNativeFeedback>
                  </Card>
                </Col>
                <Col style={{ margin: 5 }}>
                  <Card
                    style={[
                      styles.packageCard,
                      selectedCard === 'medium' ? styles.selectedCard : {},
                    ]}>
                    <TouchableNativeFeedback
                      onPress={() => this.handleSelectedCard('medium', 20)}>
                      <View>
                        <Icon
                          type="AntDesign"
                          name="inbox"
                          style={styles.packageIcon}
                        />
                        <Text style={styles.packageSize}>Medium</Text>
                        <Text style={styles.packageWeight}>Up to 20kg</Text>
                      </View>
                    </TouchableNativeFeedback>
                  </Card>
                </Col>
                <Col style={{ margin: 5 }}>
                  <Card
                    style={[
                      styles.packageCard,
                      selectedCard === 'large' ? styles.selectedCard : {},
                    ]}>
                    <TouchableNativeFeedback
                      onPress={() => this.handleSelectedCard('large', 30)}>
                      <View>
                        <Icon
                          type="AntDesign"
                          name="inbox"
                          style={styles.packageIcon}
                        />
                        <Text style={styles.packageSize}>Large</Text>
                        <Text style={styles.packageWeight}>Up to 30kg</Text>
                      </View>
                    </TouchableNativeFeedback>
                  </Card>
                </Col>
              </Row>
            </Grid>
          </View>
          <View style={[styles.inputContainer]}>
            <Text style={styles.inputLabel}>Additional Information</Text>
            <Item
              regular
              style={[
                styles.inputItem,
                {
                  alignContent: 'flex-start',
                  height: 80,
                  padding: 0,
                  margin: 0,
                },
              ]}>
              <Input
                placeholder=""
                multiline={true}
                numberOfLines={10}
                maxLength={100}
                placeholderTextColor={'#383838'}
                style={[styles.inputText, { padding: 0 }]}
                onChangeText={text => this.setState({ addInfo: text })}
              />
            </Item>
          </View>
          <View style={styles.inputContainer}>
            <Text style={styles.inputLabel}>Quantity of item</Text>
            <Item regular style={styles.inputItem}>
              <Picker
                note
                mode="dropdown"
                // style={{ width: 120 }}
                selectedValue={itemQuality}
                onValueChange={item => this.setState({ itemQuality: item })}>
                <Picker.Item label="Select Number" value="0" />
                <Picker.Item label="1" value="1" />
                <Picker.Item label="2" value="2" />
                <Picker.Item label="3" value="3" />
                <Picker.Item label="4" value="4" />
                <Picker.Item label="5" value="5" />
              </Picker>
            </Item>
          </View>
          <View style={styles.inputContainer}>
            <Text style={styles.inputLabel}>Person/Shop in Charge</Text>
            <Item regular style={styles.inputItem}>
              <Input
                placeholder=""
                placeholderTextColor={'#383838'}
                style={styles.inputText}
                onChangeText={text => this.setState({ personInCharge: text })}
              />
            </Item>
          </View>
          <View style={styles.inputContainer}>
            <Text style={styles.inputLabel}>Contact Phone Number</Text>
            <Item regular style={styles.inputItem}>
              <Input
                placeholder=""
                placeholderTextColor={'#383838'}
                style={styles.inputText}
                onChangeText={text => this.setState({ contactNumber: text })}
              />
            </Item>
          </View>
          <View style={styles.inputContainer}>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ flex: 1, padding: 5 }}>
                <Button block full onPress={() => navigation.goBack()}>
                  <Text
                    style={{
                      color: '#030E1A',
                      fontFamily: 'Nunito-SemiBold',
                      fontSize: 14,
                    }}>
                    Cancel
                  </Text>
                </Button>
              </View>
              <View style={{ flex: 1, padding: 5 }}>
                <Button
                  block
                  full
                  style={{ backgroundColor: '#3FAF5D' }}
                  onPress={() => this.toggleReviewVisible()}>
                  <Text
                    style={{
                      color: '#ffffff',
                      fontFamily: 'Nunito-SemiBold',
                      fontSize: 14,
                    }}>
                    CONTINUE
                  </Text>
                </Button>
              </View>
            </View>
          </View>
        </Content>
        {/* {isReviewVisible && } */}
        {isReviewVisible && (
          <View
            style={{
              position: 'absolute',
              top: 0,
              bottom: 0,
              right: 0,
              left: 0,
              backgroundColor: '#1B2E5A',
            }}>
            <Header
              style={{ backgroundColor: '#1B2E5A', marginTop: 35 }}
              iosBarStyle="light-content"
              androidStatusBarColor={'#1B2E5A'}>
              <Left>
                <Button transparent onPress={() => this.toggleReviewVisible()}>
                  <Icon name="arrow-back" style={{ color: '#ffffff' }} />
                </Button>
              </Left>
              <Body>
                <Text
                  numberOfLines={1}
                  style={{
                    fontSize: 18,
                    color: '#FFFFFF',
                    fontFamily: 'Nunito-SemiBold',
                  }}>
                  Review and Pay
                </Text>
              </Body>
              <Right />
            </Header>
            <Content padder>
              {!isPayCardVisible && (
                <Card style={{ paddingHorizontal: 10, borderRadius: 5 }}>
                  <View
                    style={[
                      styles.inputContainer,
                      {
                        backgroundColor: '#F4F4F4',
                        marginTop: 0,
                        marginHorizontal: -10,
                        borderRadius: 5,
                      },
                    ]}>
                    <Text style={[styles.inputLabel2, { padding: 10 }]}>
                      Pickup Rider & Price
                    </Text>
                    <View
                      style={{
                        flexDirection: 'row',
                        marginBottom: 5,
                        padding: 10,
                      }}>
                      <Left
                        style={{
                          flexDirection: 'row',
                          alignContent: 'flex-end',
                          alignItems: 'flex-end',
                        }}>
                        <View
                          style={{
                            backgroundColor: '#ffff',
                            borderRadius: 5,
                            padding: 2,
                            shadowColor: '#0000',
                            shadowOffset: {
                              width: 0,
                              height: 2,
                            },
                            shadowOpacity: 0.23,
                            shadowRadius: 2.0,
                            elevation: 4,
                          }}>
                          <Thumbnail
                            style={{ borderRadius: 5, height: 50, width: 50 }}
                            square
                            small
                            source={getImage.man_hat}
                          />
                        </View>
                        <View
                          style={{
                            marginHorizontal: 20,
                            justifyContent: 'flex-end',
                            alignContent: 'flex-end',
                          }}>
                          <Text
                            style={{
                              color: '#030E1A',
                              fontFamily: 'Nunito-SemiBold',
                            }}>
                            Gregory Samuel
                          </Text>
                          <Left
                            transparent
                            style={{
                              backgroundColor: '#1B2E5A',
                              flexDirection: 'row',
                              borderRadius: 18,
                              paddingHorizontal: 10,
                              alignSelf: 'flex-start',
                              // margin: 2,
                              // paddingVertical: -10,
                            }}>
                            <Text
                              style={{
                                color: '#ffff',
                                fontSize: 16,
                                alignSelf: 'center',
                                paddingHorizontal: 5,
                              }}>
                              Call
                            </Text>
                            <Icon
                              name="call"
                              style={{
                                color: '#ffffff',
                                fontSize: 16,
                                alignSelf: 'center',
                                paddingHorizontal: 5,
                              }}
                            />
                          </Left>
                        </View>
                      </Left>
                      <Right>
                        <Text
                          style={{
                            color: '#030E1A',
                            fontFamily: 'Nunito-SemiBold',
                            fontSize: 20,
                          }}>
                          N9,700.00
                        </Text>
                      </Right>
                    </View>
                  </View>
                  <View style={[styles.inputContainer, styles.containerBorder]}>
                    <Text style={styles.inputLabel2}>Pickup Location</Text>
                    <Text style={styles.inputText2}>{fromLocation}</Text>
                  </View>
                  <View style={[styles.inputContainer, styles.containerBorder]}>
                    <Text style={styles.inputLabel2}>Drop Location</Text>
                    <Text style={styles.inputText2}>{toLocation}</Text>
                  </View>
                  <View style={[styles.inputContainer, styles.containerBorder]}>
                    <Text style={styles.inputLabel2}>
                      Additional Information
                    </Text>
                    <View style={{ flex: 1, marginVertical: 2 }}>
                      <Text
                        style={{
                          fontSize: 16,
                          color: '#030E1A',
                          fontFamily: 'Nunito-Regular',
                        }}>
                        {addInfo}
                      </Text>
                    </View>
                    <View style={{ flexDirection: 'row', marginVertical: 5 }}>
                      <View style={{ flex: 1 }}>
                        <Text>Person in Charge:</Text>
                      </View>
                      <View style={{ flex: 1 }}>
                        <Text style={{ marginLeft: -50, color: '#030E1A' }}>
                          {personInCharge}
                        </Text>
                      </View>
                    </View>
                    <View style={{ flexDirection: 'row', marginVertical: 5 }}>
                      <View style={{ flex: 1 }}>
                        <Text>Phone Number</Text>
                      </View>
                      <View style={{ flex: 1 }}>
                        <Text style={{ marginLeft: -50, color: '#030E1A' }}>
                          {contactNumber}
                        </Text>
                      </View>
                    </View>
                  </View>
                  <View style={[styles.inputContainer, styles.containerBorder]}>
                    <Grid>
                      <Row>
                        <Col>
                          <Text style={styles.inputLabel2}>
                            Size of Package
                          </Text>
                          <Card transparent>
                            <View style={{ flexDirection: 'row' }}>
                              <Icon
                                type="AntDesign"
                                name="inbox"
                                style={styles.packageIcon}
                              />
                              <View>
                                <Text style={styles.packageSize}>
                                  {packageSize.title}
                                </Text>
                                <Text style={styles.packageWeight}>
                                  {`Up to ${packageSize.weight}kg`}
                                </Text>
                              </View>
                            </View>
                          </Card>
                        </Col>
                        <Col>
                          <Text style={styles.inputLabel2}>Quantity</Text>
                          <Card transparent>
                            <View>
                              <Text style={styles.inputText2}>
                                {itemQuality}
                              </Text>
                            </View>
                          </Card>
                        </Col>
                      </Row>
                    </Grid>
                  </View>
                  <View style={[styles.inputContainer, styles.containerBorder]}>
                    <Grid>
                      <Row>
                        <Col>
                          <Text style={styles.inputLabel2}>Total Distance</Text>
                          <Card transparent>
                            <View>
                              <Text style={styles.inputText2}>5.4 km</Text>
                            </View>
                          </Card>
                        </Col>
                        <Col>
                          <Text style={styles.inputLabel2}>
                            Est. Delivery time
                          </Text>
                          <Card transparent>
                            <View>
                              <Text style={styles.inputText2}>45 min</Text>
                            </View>
                          </Card>
                        </Col>
                      </Row>
                    </Grid>
                  </View>
                  <View style={styles.inputContainer}>
                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ flex: 1, padding: 5 }}>
                        <Button block full onPress={this.toggleReviewVisible}>
                          <Text
                            style={{
                              color: '#030E1A',
                              fontFamily: 'Nunito-SemiBold',
                              fontSize: 14,
                            }}>
                            Cancel
                          </Text>
                        </Button>
                      </View>
                      <View style={{ flex: 1, padding: 5 }}>
                        <Button
                          block
                          full
                          style={{ backgroundColor: '#3FAF5D' }}
                          onPress={this.togglePayCard}>
                          <Text
                            style={{
                              color: '#ffffff',
                              fontFamily: 'Nunito-SemiBold',
                              fontSize: 14,
                            }}>
                            CONTINUE
                          </Text>
                        </Button>
                      </View>
                    </View>
                  </View>
                </Card>
              )}
              {isPayCardVisible && (
                <Card style={{ paddingHorizontal: 10, borderRadius: 5 }}>
                  <View
                    style={[
                      styles.inputContainer,
                      {
                        backgroundColor: '#F4F4F4',
                        marginTop: 0,
                        marginHorizontal: -10,
                        borderRadius: 5,
                      },
                    ]}>
                    <Text style={[styles.inputLabel2, { padding: 10 }]}>
                      Pickup Rider & Price
                    </Text>
                    <View
                      style={{
                        flexDirection: 'row',
                        marginBottom: 5,
                        padding: 10,
                      }}>
                      <Left
                        style={{
                          flexDirection: 'row',
                          alignContent: 'flex-end',
                          alignItems: 'flex-end',
                        }}>
                        <View
                          style={{
                            backgroundColor: '#ffff',
                            borderRadius: 5,
                            padding: 2,
                            shadowColor: '#0000',
                            shadowOffset: {
                              width: 0,
                              height: 2,
                            },
                            shadowOpacity: 0.23,
                            shadowRadius: 2.0,
                            elevation: 4,
                          }}>
                          <Thumbnail
                            style={{ borderRadius: 5, height: 50, width: 50 }}
                            square
                            small
                            source={getImage.man_hat}
                          />
                        </View>
                        <View
                          style={{
                            marginHorizontal: 20,
                            justifyContent: 'flex-end',
                            alignContent: 'flex-end',
                          }}>
                          <Text
                            style={{
                              color: '#030E1A',
                              fontFamily: 'Nunito-SemiBold',
                            }}>
                            Gregory Samuel
                          </Text>
                          <Left
                            transparent
                            style={{
                              backgroundColor: '#1B2E5A',
                              flexDirection: 'row',
                              borderRadius: 18,
                              paddingHorizontal: 10,
                              alignSelf: 'flex-start',
                              // margin: 2,
                              // paddingVertical: -10,
                            }}>
                            <Text
                              style={{
                                color: '#ffff',
                                fontSize: 16,
                                alignSelf: 'center',
                                paddingHorizontal: 5,
                              }}>
                              Call
                            </Text>
                            <Icon
                              name="call"
                              style={{
                                color: '#ffffff',
                                fontSize: 16,
                                alignSelf: 'center',
                                paddingHorizontal: 5,
                              }}
                            />
                          </Left>
                        </View>
                      </Left>
                      <Right>
                        <Text
                          style={{
                            color: '#030E1A',
                            fontFamily: 'Nunito-SemiBold',
                            fontSize: 20,
                          }}>
                          N9,700.00
                        </Text>
                      </Right>
                    </View>
                  </View>
                  <View style={[styles.inputContainer, { marginRight: 2 }]}>
                    <Text style={styles.inputLabel2}>Card Number</Text>
                    <Item regular style={styles.inputItem}>
                      <Input
                        placeholder="**********"
                        placeholderTextColor={'#383838'}
                        secureTextEntry={true}
                        keyboardType="numeric"
                        style={styles.inputText}
                        maxLength={16}
                      />
                    </Item>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={[styles.inputContainer, { marginRight: 2 }]}>
                      <Text style={styles.inputLabel2}>CVV</Text>
                      <Item regular style={styles.inputItem}>
                        <Input
                          placeholder="***"
                          placeholderTextColor={'#383838'}
                          maxLength={3}
                          secureTextEntry={true}
                          style={styles.inputText}
                          keyboardType="numeric"
                        />
                      </Item>
                    </View>
                    <View style={[styles.inputContainer, { marginLeft: 2 }]}>
                      <Text style={styles.inputLabel2}>Expiry Date</Text>
                      <Item regular style={styles.inputItem}>
                        <Input
                          placeholder="MM/YY"
                          placeholderTextColor={'#383838'}
                          style={styles.inputText}
                          keyboardType="numeric"
                          maxLength={4}
                        />
                      </Item>
                    </View>
                  </View>
                  <Button
                    block
                    primary
                    style={styles.submitBtn}
                    onPress={this.toggleSuccessfulVisible}>
                    <Text style={styles.btnText}>PAY</Text>
                  </Button>
                </Card>
              )}
            </Content>
          </View>
        )}
        {isSuccessful && (
          <View
            style={{
              position: 'absolute',
              top: 0,
              bottom: 0,
              right: 0,
              left: 0,
              backgroundColor: '#1B2E5A',
              zIndex: 10,
            }}>
            <Header
              style={{ backgroundColor: '#1B2E5A', marginTop: 35 }}
              iosBarStyle="light-content"
              androidStatusBarColor={'#1B2E5A'}>
              {/* <Left /> */}
              <View style={{ justifyContent: 'center' }}>
                <Text
                  numberOfLines={1}
                  style={{
                    fontSize: 20,
                    color: '#FFFFFF',
                    fontFamily: 'Nunito-SemiBold',
                  }}>
                  Success
                </Text>
              </View>
              {/* <Right /> */}
            </Header>
            <Content padder>
              <Card style={{ borderRadius: 5 }}>
                <View style={styles.modalContainer}>
                  <View style={{ alignSelf: 'center' }}>
                    <Icon name={'checkmark-circle'} style={styles.modalIcon} />
                  </View>
                  <Card transparent>
                    <CardItem style={{ justifyContent: 'center' }}>
                      <Text style={styles.modalTitle}>
                        Order placed successfully!
                      </Text>
                    </CardItem>

                    <CardItem style={{ justifyContent: 'center' }}>
                      <Text style={styles.modalSubtitle}>
                        {
                          'Your order with has been successfully request, you can track the progress of the order in the with the parcel ID generated.'
                        }
                      </Text>
                    </CardItem>

                    <View style={{ padding: 16 }}>
                      <Button
                        block
                        full
                        onPress={() => navigation.navigate('HomePage')}
                        style={styles.modalButton}>
                        <Text style={styles.modalButtonText}>DONE</Text>
                      </Button>
                    </View>
                  </Card>
                </View>
              </Card>
            </Content>
          </View>
        )}
      </Container>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PackageDetails);
