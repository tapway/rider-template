import { StyleSheet } from 'react-native';
import { RFValue, RFPercentage } from 'react-native-responsive-fontsize';

const styles = StyleSheet.create({
  inputContainer: {
    flex: 1,
    marginVertical: 10,
    // backgroundColor: 'red',
  },
  containerBorder: {
    borderBottomColor: '#DBDBDB',
    borderBottomWidth: 1,
    paddingBottom: 5,
  },
  inputLabel: {
    fontSize: 20,
    fontWeight: '100',
    color: '#858F99',
    marginVertical: 5,
    // opacity: 0.7,
  },
  bgimg: { position: 'absolute', bottom: 0, height: RFPercentage(70), zIndex: -100, width: '100%' },
  inputLabel2: {
    fontSize: 16,
    fontWeight: '100',
    color: '#02011a',
    marginVertical: 2,
    // opacity: 0.7,
  },
  inputItem: {
    color: '#383838',
    borderWidth: 0,
    fontSize: 22,
    marginTop: 3,
    paddingHorizontal: 10,
    height: 45,
    fontFamily: 'Nunito-Regular',
    backgroundColor: '#EFF4F5',
    borderColor: '#EFF4F5',
  },
  inputText: {
    color: '#383838',
    // opacity: 0.9,
  },
  whiteText: {
    color: '#FFFFFF',
  },
  inputText2: {
    color: '#383838',
    fontSize: 18,
    marginVertical: 8,
  },
  packageCard: {
    padding: 5,
    elevation: 1,
  },
  selectedCard: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.9,
    shadowRadius: 4.95,

    elevation: 10,
  },
  packageIcon: {
    color: '#A98258',
    fontSize: 50,
    alignSelf: 'center',
  },
  packageSize: {
    color: '#030E1A',
    fontSize: 20,
    alignSelf: 'center',
  },
  packageWeight: {
    color: '#030E1A',
    fontSize: 18,
    alignSelf: 'center',
    opacity: 0.4,
  },
  submitBtn: {
    backgroundColor: '#3FAF5D',
    borderRadius: 4,
    marginVertical: 30,
  },
  btnText: {
    color: '#FFFFFF',
    fontSize: 18,
    fontFamily: 'Nunito-SemiBold',
    fontWeight: '400',
    letterSpacing: 0.5,
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignContent: 'center',
  },
  modalIcon: { fontSize: 150, color: '#1B2E5A' },
  modalTitle: {
    textAlign: 'center',
    color: '#030E1A',
    fontSize: 25,
    fontFamily: 'Nunito-Bold',
  },
  modalSubtitle: {
    textAlign: 'center',
    color: '#858F99',
    fontSize: 16,
  },
  modalButton: {
    backgroundColor: '#3FAF5D',
    borderRadius: 5,
    padding: 50,
  },
  modalButtonText: {
    color: '#FFFFFF',
    fontSize: 16,
    letterSpacing: 0.2,
  },
});

export default styles;
