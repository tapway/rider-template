/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Modal, Image } from 'react-native';
import PropTypes from 'prop-types';
import styles from './style';
import getImage from '../../utils/getImage';

export default class LogOut extends Component {
  handleBackPress = () => {
    const { props } = this;
    props.setLogoutModal(); // works best when the goBack is async
    return true;
  };

  render() {
    const { props } = this;
    return (
      <Modal
        transparent
        visible={props.logoutModal}
        onRequestClose={() => {
          props.setLogoutModal();
        }}>
        <View style={styles.backgroundContainer}>
          <View style={styles.mainContainer}>
            <Image
              resizeMode="contain"
              source={getImage.logOut}
              style={styles.signOutImg}
            />
            <Text style={styles.sureText}>
              Are you Sure that you want to Sign out?
            </Text>
            <View
              style={{
                width: '100%',
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <TouchableOpacity
                style={[styles.btns, styles.primaryColor]}
                onPress={() => props.signOutUser()}>
                <Text style={styles.btnText}>Yes</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.btns}
                onPress={() => {
                  props.setLogoutModal();
                }}>
                <Text style={styles.btnText}>No</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}

LogOut.propTypes = {
  signOutUser: PropTypes.func.isRequired,
  logoutModal: PropTypes.bool.isRequired,
  setLogoutModal: PropTypes.func.isRequired,
};
