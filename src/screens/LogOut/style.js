import { StyleSheet } from 'react-native';
import { heightRatio, widthRatio } from '../../utils/stylesheet';
import colors from '../../utils/colors';

export default StyleSheet.create({
  backgroundContainer: {
    flex: 1,
    backgroundColor: colors.blurEffect,
    justifyContent: 'center',
  },
  mainContainer: {
    height: 280 * heightRatio,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.themeBackgroundColor,
    marginHorizontal: 45 * widthRatio,
    borderRadius: 20 * heightRatio,
    elevation: 80,
    flexDirection: 'column',
    paddingHorizontal: 15 * widthRatio,
    paddingVertical: 40 * heightRatio,
  },
  signOutImg: { height: 50 * heightRatio, width: 45 * heightRatio },
  sureText: {
    marginTop: 21 * heightRatio,
    fontSize: 20,
    color: colors.sureText,
    textAlign: 'center',
    marginBottom: 40 * heightRatio,
  },
  btns: {
    height: 46 * heightRatio,
    width: '45%',
    borderRadius: (46 / 2) * heightRatio,
    borderColor: colors.secondaryColor,
    backgroundColor: colors.secondaryColor,
    alignItems: 'center',
    justifyContent: 'center',
    // marginTop: 20 * heightRatio,
  },
  primaryColor: { backgroundColor: colors.primaryColor },
  btnText: { fontSize: 16 * heightRatio, color: colors.textForPrimaryBtnBgColor },
});
