/* eslint-disable react/no-unused-state */
import React, { PureComponent } from 'react';
import CodeInput from 'react-native-confirmation-code-input';
import Toast from 'react-native-easy-toast';
import { View, Text, TouchableWithoutFeedback, Keyboard, Modal } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import PropTypes from 'prop-types';
import styles from './style';
import colors from '../../utils/colors';
import verifyRide from '../../apiService/verifyRide';
import Header from '../../components/Header';
import Loader from '../../components/Loader';
import Button from '../../components/Button';
import { heightRatio } from '../../utils/stylesheet';

export default class VerifyRideClass extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      verificationCode: '',
      imageLoader: false,
    };
  }

  handleVerifyRide = res => {
    const { imageLoader } = this.state;
    this.setState(
      {
        verificationCode: '',
        imageLoader: !imageLoader,
      },
      () => {
        if (res.success) {
          const { props } = this;
          if (props.category === 'Cab') {
            props.function();
          } else {
            props.endTrip();
          }
        } else {
          const { refs } = this;
          refs.toast.show('Incorrect Otp');
        }
      }
    );
  };

  submit = () => {
    const { verificationCode, imageLoader } = this.state;
    const { props } = this;
    this.setState({ imageLoader: !imageLoader });
    verifyRide(verificationCode, props.orderId, this.handleVerifyRide);
  };

  render() {
    const { imageLoader } = this.state;
    const { props } = this;
    return (
      <Modal transparent visible={props.loadVerify} animated animationType="slide">
        <KeyboardAwareScrollView scrollEnabled={false}>
          <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
            <View style={styles.container}>
              <Loader loading={imageLoader} />
              <Header onlyBack back={props.verifyBack} />
              <View style={styles.firstContainer}>
                <Text style={styles.mainVerificationHeading}>Verify Ride</Text>
                <Text style={styles.verificationDescription}>
                  Please enter the verification code sent to the customer.
                </Text>
              </View>
              <View style={styles.secondContainer}>
                <Text style={styles.mainVerificationHeading}>Verification code</Text>
                <CodeInput
                  secureTextEntry
                  autoFocus={false}
                  className="border-box"
                  space={30}
                  codeLength={4}
                  size={40}
                  keyboardType="numeric"
                  inputPosition="left"
                  activeColor="black"
                  inactiveColor="grey"
                  onFulfill={code => this.setState({ verificationCode: code })}
                />
              </View>
              <View style={styles.thirdContainer}>
                <Button submit={this.submit} title="Next" />
              </View>
              <Toast
                // eslint-disable-next-line react/no-string-refs
                ref="toast"
                style={{ backgroundColor: colors.toastBgColor }}
                position="bottom"
                positionValue={335 * heightRatio}
                fadeInDuration={750}
                fadeOutDuration={1000}
                opacity={0.8}
                textStyle={{ color: colors.toastTextColor, fontSize: 15 }}
              />
            </View>
          </TouchableWithoutFeedback>
        </KeyboardAwareScrollView>
      </Modal>
    );
  }
}

VerifyRideClass.propTypes = {
  verifyBack: PropTypes.func.isRequired,
  loadVerify: PropTypes.bool.isRequired,
  endTrip: PropTypes.func.isRequired,
  function: PropTypes.func.isRequired,
  category: PropTypes.string.isRequired,
  orderId: PropTypes.string.isRequired,
};
