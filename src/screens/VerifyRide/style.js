import { StyleSheet, Dimensions } from 'react-native';
import colors from '../../utils/colors';
import { heightRatio } from '../../utils/stylesheet';

const { height, width } = Dimensions.get('window');
export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.themeBackgroundColor,
  },
  firstContainer: {
    height: heightRatio * 100,
    paddingLeft: width / 6,
    paddingRight: width / 6,
    justifyContent: 'center',
  },
  secondContainer: {
    height: heightRatio * 200,
    justifyContent: 'center',
    paddingTop: height / 20,
    paddingLeft: width / 6,
  },
  thirdContainer: {
    height: heightRatio * 300,
    justifyContent: 'center',
    paddingHorizontal: width / 6,
    alignItems: 'center',
  },
  mainVerificationHeading: {
    fontSize: height / 35,
    fontWeight: '200',
  },
  verificationDescription: {
    fontSize: height / 60,
    marginTop: height / 50,
    color: colors.textForNonHighlightedContainer,
    fontWeight: '400',
  },
  resendContainer: {
    flexDirection: 'row',
  },
});
