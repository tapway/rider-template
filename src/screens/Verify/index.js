/* eslint-disable prettier/prettier */
/* eslint-disable react/no-unused-state */
/* eslint-disable react/prop-types */
import React, { PureComponent } from 'react';
import CodeInput from 'react-native-confirmation-code-input';
import Toast from 'react-native-easy-toast';
import { connect } from 'react-redux';
import {
  View,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import PropTypes from 'prop-types';
import { Container, Header as Head, Left, Body,Button } from 'native-base';
import styles from './style';
import colors from '../../utils/colors';
import setUser from '../../redux/action/loginPage';
import onVerificationCode from '../../apiService/otpVerification';
import { resetActionHome } from '../../navigation/ResetNavigation';
import resendOtp from '../../apiService/resendOtp';
import Header from '../../components/Header';
import Loader from '../../components/Loader';
import { Actions } from 'react-native-router-flux';
// import Button from '../../components/Button';
import { heightRatio } from '../../utils/stylesheet';
import Colors from '../../utils/colors';
import { RFValue } from 'react-native-responsive-fontsize';

class Verify extends PureComponent {
  constructor() {
    super();
    this.state = {
      verificationCode: '',
      imageLoader: false,
    };
    this.goBackToLogin = this.goBackToLogin.bind(this);
  }

  handleLogin = res => {
    const { imageLoader } = this.state;
    console.log(res)
    this.setState({
      verificationCode: '',
      imageLoader: !imageLoader,
    });
    if (res.success) {
      const { props } = this;
      const { user } = res.data;
      const { jwtAccessToken } = res.data;
      props.setUserDispatcher(user, jwtAccessToken);
      props.navigation.dispatch(resetActionHome);
      props.navigation.navigate('home');
    }else{
      const { refs } = this;
      refs.toast.show(res.message);
    }
  };
onSubmit=(code)=> {
  this.setState({ verificationCode: code });
  setTimeout(() => {
    this.submit()
  }, 100);
}
  submit = () => {
    const { verificationCode, imageLoader } = this.state;
    const { navigation } = this.props;
    this.setState({ imageLoader: !imageLoader });
    const phoneNumber = navigation.getParam('phoneNumber');
    onVerificationCode(phoneNumber, verificationCode, this.handleLogin);
  };

  otpRequest = () => {
    const { refs } = this;
    const { navigation } = this.props;
    const phoneNumber = navigation.getParam('phoneNumber');
    resendOtp(phoneNumber);
    refs.toast.show('OTP has been resent!');
  };

  goBackToLogin() {
    const { props } = this;
    props.navigation.navigate('login');
  }

  render() {
    const { navigation } = this.props;
    const { imageLoader } = this.state;
    const phoneNumber = navigation.getParam('phoneNumber');

    return (
      <Container style={styles.container}>
        <Head
          transparent // hasTabs
          iosBarStyle="light-content"
          style={{ height: 5 }}
          androidStatusBarColor={'#1891D0'}>
          <Left />
          <Body />
        </Head>
        
        <Header onlyBack back={this.goBackToLogin} customstyle={{ borderBottomWidth: 0,marginTop:RFValue(30)}} />

        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
          <KeyboardAvoidingView
            style={{ flex: 1 }}
            enabled={Platform.OS === 'ios'}
            behavior="padding">
            <Loader loading={imageLoader} />
            <View style={styles.firstContainer}>
              <Text style={styles.mainVerificationHeading}>
              Enter 4 digit code sent to the email address provided
              </Text>
              {/* <Text style={styles.verificationDescription}>
                Please enter the verification code sent to {phoneNumber} or your email
              </Text> */}
            </View>
            <View style={styles.secondContainer}>
              <CodeInput
                secureTextEntry
                autoFocus={false}
                className="border-box"
                space={30}
                codeLength={4}
                size={40}
                keyboardType="numeric"
                inputPosition="left"
                activeColor="black"
                inactiveColor="lightgrey"
                 codeInputStyle={styles.codeinput}
                containerStyle={styles.codecontainer}
                onFulfill={code => this.onSubmit(code)}
              />
            </View>
            <View style={styles.thirdContainer}>
              <View style={[styles.loginBtnContainer]}>
                {/* <Button submit={this.submit} title="Next" /> */}
                <Button block primary style={styles.submitBtn} onPress={() => this.submit()}>
                <Text style={styles.btnText}>VERIFY</Text>
              </Button>
              </View>
              <View style={styles.resendCodeButton}>
                <View style={styles.resendContainer}>
                  <Text style={styles.resendCodeText}>Didn’t recieve a verification code?</Text>
                  <TouchableOpacity onPress={this.otpRequest}>
                    <Text style={[styles.resendCodeText, styles.boldResend]}>
                      Resend Code
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
            <Toast
              // eslint-disable-next-line react/no-string-refs
              ref="toast"
              style={{ backgroundColor: colors.toastBgColor }}
              position="bottom"
              positionValue={335 * heightRatio}
              fadeInDuration={750}
              fadeOutDuration={1000}
              opacity={0.8}
              textStyle={{ color: colors.toastTextColor, fontSize: 15 }}
            />
          </KeyboardAvoidingView>
        </TouchableWithoutFeedback>
      </Container>
    );
  }
}
const mapStateToProps = state => {
  return {
    user: state.currentUser,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    setUserDispatcher: (user, jwt) => {
      dispatch(setUser(user, jwt));
    },
  };
};
Verify.propTypes = {
  setUserDispatcher: PropTypes.func.isRequired,
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
};
export default connect(mapStateToProps, mapDispatchToProps)(Verify);
