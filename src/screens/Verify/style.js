import { StyleSheet, Dimensions, Platform } from 'react-native';
import colors from '../../utils/colors';
import { heightRatio } from '../../utils/stylesheet';
import { FontNames } from '../../../theme';
import {RFValue} from "react-native-responsive-fontsize"

const { width } = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.themeBackgroundColor,
    paddingBottom: Platform.OS === 'ios' ? 30 * heightRatio : 20 * heightRatio,
  },
  firstContainer: {
    marginTop: RFValue(30),
    paddingLeft: width / RFValue(18),
    paddingRight: width / RFValue(18),
    justifyContent: 'center',
  },
  secondContainer: {
   // flex: 1,
    width:'100%',
    marginTop: RFValue(10),
    //backgroundColor:'red',
    flexDirection: 'row',
    justifyContent:'space-between',
    justifyContent: 'center',
    paddingHorizontal: width / RFValue(18),
  },
  codeinput:{ width:RFValue(58),height:RFValue(50), borderColor: 'silver',backgroundColor:'#EFF4F5',  borderWidth: 0.8,borderRadius:5 },
  codecontainer:{marginBottom:RFValue(30), width:'100%',flexDirection:'row',justifyContent:'space-between'},
  btnText: {
    color: '#FFFFFF',
    fontSize: 18,
    fontFamily: FontNames.regular,
  },
  submitBtn: {
    backgroundColor: '#3FAF5D',
    borderRadius: 4,
    height:RFValue(50),
    elevation:0,
    marginTop: 40,
    marginBottom: 30,
  },
  thirdContainer: {
    justifyContent: 'center',
    paddingHorizontal: width / RFValue(18),
  },
  mainVerificationHeading: {
    fontFamily: FontNames.bold,
    fontSize: RFValue(24),
    color:'#030E1A'
  },
  verificationDescription: {
    fontSize: 12 * heightRatio,
    marginTop: 10 * heightRatio,
    marginBottom: 40 * heightRatio,
    color: colors.textGrey,
    fontWeight: '400',
    lineHeight: 16 * heightRatio,
  },
  loginBtnContainer: {
    marginBottom: RFValue(5),
    alignItems: 'center',
    justifyContent: 'center',
  },
  resendContainer: {
    flexDirection: 'column',
    alignItems: 'center',
    marginBottom: 20 * heightRatio,
  },
  resendCodeButton: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  resendCodeText: {
    fontSize: RFValue(14),
    fontFamily: FontNames.regular,
    color: '#1B2E5A',
  },
  boldResend: {
    fontFamily:FontNames.medium,
    color: '#3FAF5D',
  },
});
