import React, { PureComponent } from 'react';
import { View } from 'react-native';
import MapView from 'react-native-maps';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Card from './Card';
import InputField from './InputField';
import Button from './Button';
import styles from './style';
import navigate from './navigate';
import setStatus from '../../redux/action/readyForDelivery';
import Header from '../../components/Header';
import { socketDeliveryStatus } from '../../socket/index';
import VerifyRide from '../VerifyRide';
import setTripStatus from '../../apiService/setTripStatus';

class ReadyForDelivery extends PureComponent {
  constructor() {
    super();
    this.state = {
      loadVerify: false,
      showCard: false,
    };
    this.mapRef = null;
    this.startPickUp = this.startPickUp.bind(this);
    this.startTrip = this.startTrip.bind(this);
    this.endTrip = this.endTrip.bind(this);
    this.endPickUp = this.endPickUp.bind(this);
    this.home = this.home.bind(this);
  }

  setLoadVerify = () => {
    this.setState({ modalVisible: false }, () => {
      this.setState({ loadVerify: true });
    });
  };

  statusUpdate = value => {
    const { props } = this;
    socketDeliveryStatus({
      orderId: props.order._id,
      userId: props.order.userId,
      statusId: props.order.processingStatus,
      status: value,
    });
  };

  verifyBack = () => {
    this.setState({ loadVerify: false }, () => {
      this.setState({ modalVisible: true });
    });
  };

  home() {
    const { props } = this;
    props.navigation.navigate('home');
  }

  startPickUp() {
    const { props } = this;
    const { order, user } = props;
    const { GpsLocation } = user;
    const { pickUpLocation } = order;
    const destination = {
      latitude: pickUpLocation[1],
      longitude: pickUpLocation[0],
    };
    const source = { latitude: GpsLocation[1], longitude: GpsLocation[0] };
    setTripStatus(true, props.user.jwtAccessToken);
    this.statusUpdate('Started');
    navigate({ destination, source });
  }

  endPickUp() {
    const { props } = this;
    this.setState({ loadVerify: false });
    props.setStatusDispatcher('PICKUPDONE');
    this.statusUpdate('Picked');
  }

  startTrip() {
    const { props } = this;
    const { order, user } = props;
    const { GpsLocation } = user;
    const { deliveryLocation } = order;
    const destination = {
      latitude: deliveryLocation[1],
      longitude: deliveryLocation[0],
    };
    const source = { latitude: GpsLocation[1], longitude: GpsLocation[0] };
    navigate({ destination, source });
  }

  endTrip() {
    const { props } = this;
    props.setStatusDispatcher('Completed');
    this.setState({ loadVerify: false });
    this.statusUpdate('Completed');
    props.navigation.navigate('orderComplete');
  }

  render() {
    const { props, state } = this;
    const { order, status } = props;
    const { loadVerify, showCard } = state;
    const category = props.navigation.getParam('category');
    return (
      <View style={styles.container}>
        <Header onlyBack={false} back={this.home} title="Ready For Delivery" />
        <MapView
          ref={ref => {
            this.mapRef = ref;
          }}
          style={styles.map}
        />
        <View style={styles.overlay}>
          {showCard ? (
            <Card />
          ) : (
            <View style={styles.secondConatiner}>
              <View style={styles.modalView}>
                <InputField
                  value={order.pickUpAddress}
                  heading="Pickup Location"
                  style={styles.pickImage}
                  image="pickUp"
                />
                <InputField
                  value={order.deliveryAddress}
                  heading="Drop Location"
                  image="blueLocation"
                  style={styles.dropImage}
                />
                {status.status === 'START' ? (
                  <View style={styles.buttonContainer}>
                    <Button title="Start PickUp" function={this.startPickUp} />
                    <Button
                      redButton
                      title="End PickUp"
                      function={
                        category === 'Cab' ? this.setLoadVerify : this.endPickUp
                      }
                    />
                  </View>
                ) : (
                  <View style={styles.buttonContainer}>
                    <Button title="Start Trip" function={this.startTrip} />
                    <Button
                      redButton
                      title="End Trip"
                      function={
                        category === 'Cab' ? this.endTrip : this.setLoadVerify
                      }
                    />
                  </View>
                )}
              </View>
            </View>
          )}
        </View>
        <VerifyRide
          loadVerify={loadVerify}
          orderId={props.order._id}
          function={this.endPickUp}
          endTrip={this.endTrip}
          category={category}
          verifyBack={this.verifyBack}
        />
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    user: state.currentUser,
    order: state.currentOrder,
    status: state.status,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    setStatusDispatcher: curr => {
      dispatch(setStatus(curr));
    },
  };
};

ReadyForDelivery.propTypes = {
  setStatusDispatcher: PropTypes.func.isRequired,
  order: PropTypes.objectOf(PropTypes.any).isRequired,
  status: PropTypes.objectOf(PropTypes.any).isRequired,
  user: PropTypes.objectOf(PropTypes.any).isRequired,
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ReadyForDelivery);
