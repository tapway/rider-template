import React, { PureComponent } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import ProductList from '../OrderList/ProductList';
import styles from './style';

class Card extends PureComponent {
  constructor() {
    super();
    this.state = {};
  }

  render() {
    const { props } = this;
    const { order } = props;
    return (
      <View style={styles.card}>
        <View style={styles.cardHeadingsContainer}>
          <Text style={styles.orderDetailsHeading}>Order Details</Text>
          <View style={styles.locDesc}>
            <Text style={styles.locHeading1}>Items</Text>
            <ProductList data={order.deliveryInstructions.itemDetails} style={styles.productList} />
          </View>
        </View>
        <View style={styles.locationHeadingsContiner}>
          <Text style={styles.orderDetailsHeading}>Location Details</Text>
          <View style={styles.locDesc}>
            <Text style={styles.locHeading1}>House/Flat No.</Text>
            <Text style={styles.locHeading2}>{order.deliveryInstructions.detailAddress}</Text>
          </View>
          <View style={styles.locDesc}>
            <Text style={styles.locHeading1}>Locality</Text>
            <Text style={styles.locHeading2}>{order.deliveryInstructions.detailAddress2}</Text>
          </View>
          <View style={styles.locDesc}>
            <Text style={styles.locHeading1}>Address</Text>
            <Text style={styles.locHeading2}>{order.deliveryAddress}</Text>
          </View>
        </View>
        <View style={styles.callCont}>
          <TouchableOpacity style={[styles.loginButton, styles.acceptButton]}>
            <Text style={styles.loginText}>Call For Help</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    user: state.currentUser,
    order: state.currentOrder,
    status: state.status,
  };
};

Card.propTypes = {
  order: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default connect(
  mapStateToProps,
  null
)(Card);
