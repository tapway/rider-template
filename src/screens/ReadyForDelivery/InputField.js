import React, { PureComponent } from 'react';
import { Text, View, TextInput, Image } from 'react-native';
import PropTypes from 'prop-types';
import styles from './style';
import getImage from '../../utils/getImage';

class InputField extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { props } = this;
    return (
      <View style={styles.singleInput}>
        <Image resizeMode="contain" style={props.style} source={getImage[props.image]} />
        <View>
          <Text style={styles.liHeading}>{props.heading}</Text>
          <TextInput value={props.value} style={styles.textInputBox} editable={false} />
        </View>
      </View>
    );
  }
}

InputField.propTypes = {
  style: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  heading: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
};

export default InputField;
