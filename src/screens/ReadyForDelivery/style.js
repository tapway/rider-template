import { StyleSheet, Dimensions } from 'react-native';
import colors from '../../utils/colors';
import { heightRatio, widthRatio } from '../../utils/stylesheet';

const { height, width } = Dimensions.get('window');
export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.themeBackgroundColor,
  },
  loginButton: {
    width: width / 3.2,
    alignItems: 'center',
    padding: 13,
    marginTop: height / 50,
    marginRight: width / 30,
    marginLeft: width / 30,
    borderRadius: 50,
    justifyContent: 'center',
    borderColor: colors.transparent,
  },
  extraButton: {
    height: height / 30,
    width: width / 30,
    overflow: 'visible',
    marginRight: width / 30,
  },
  modalView: {
    backgroundColor: colors.modalContainerColor,
    marginLeft: width / 8,
    marginRight: width / 8,
    padding: 20,
    paddingTop: 25,
    position: 'absolute',
    alignItems: 'center',
    shadowOpacity: 1,
    shadowColor: colors.primaryShadowColor,
    borderRadius: 5,
  },
  boldb: {
    fontWeight: 'bold',
  },
  locDesc: {
    marginTop: height / 100,
  },
  productList: {
    marginTop: heightRatio * 24,
  },
  locHeading1: {
    fontSize: height / 48,
    fontWeight: '500',
  },
  locHeading2: {
    fontSize: height / 60,
  },
  callCont: {
    flex: 1,
    alignItems: 'center',
  },
  cardHeadingsContainer: {
    flex: 1,
    borderColor: colors.modalBorderColor,
    borderBottomWidth: 1,
    paddingBottom: height / 50,
  },
  locationHeadingsContiner: {
    flex: 1,
    paddingTop: height / 50,
  },
  row: {
    flexDirection: 'row',
  },
  map: {
    flex: 1,
  },
  markerView: {
    backgroundColor: colors.themeBackgroundColor,
    padding: width / 25,
    borderRadius: 10,
    width: width / 1.7,
  },
  markerHeading: {
    color: colors.secondaryColor,
    fontSize: height / 42,
    marginBottom: height / 150,
  },
  topPading: {
    marginTop: height / 125,
  },
  userImage: {
    backgroundColor: colors.primaryColor,
  },
  textWhite: {
    color: colors.white,
    fontSize: height / 55,
  },
  mapContainer: {
    flex: 14,
  },
  singleInput: {
    flexDirection: 'row',
    marginBottom: height / 40,
  },
  liHeading: {
    fontSize: 18,
    fontWeight: '200',
  },
  textInputBox: {
    borderStyle: 'solid',
    borderBottomWidth: 1,
    borderColor: colors.secondaryBorderColor,
    width: widthRatio * 220,
    padding: width / 30,
    paddingLeft: 0,
    fontSize: height / 60,
    fontWeight: '200',
    color: colors.textForTabBar,
  },
  secondConatiner: {
    flex: 1,
    marginTop: height / 1.8,
    alignItems: 'center',
  },
  dropImage: {
    height: heightRatio * 20,
    marginRight: widthRatio * 7,
    marginTop: heightRatio * 1,
  },
  pickImage: {
    height: heightRatio * 15,
    marginRight: widthRatio * 7,
    marginTop: heightRatio * 1,
  },
  card: {
    backgroundColor: '#fff',
    marginLeft: width / 12,
    marginRight: width / 12,
    padding: height / 30,
    paddingLeft: width / 10,
    paddingRight: width / 8,
    shadowOpacity: 1,
    shadowColor: colors.primaryShadowColor,
    borderRadius: 5,
    marginTop: height / 4,
    marginBottom: height / 15,
  },
  loginBtnContainer: {
    flex: 2,
    alignItems: 'center',
  },
  loginText: {
    color: colors.textForPrimaryBgColor,
    fontSize: height / 50,
  },
  acceptButton: {
    backgroundColor: colors.primaryColor,
  },
  rejectButton: {
    backgroundColor: colors.red,
  },
  orderDetailsHeading: {
    fontSize: height / 35,
    color: colors.secondaryColor,
  },
  thirdContainer: {
    backgroundColor: colors.themeBackgroundColor,
    flex: 3,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingRight: width / 9,
    paddingLeft: width / 9,
    alignItems: 'center',
  },
  userIconImage: {
    height: height / 22,
    width: width / 10,
    marginRight: width / 20,
    overflow: 'visible',
  },
  buttonContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  overlay: { flex: 1, width: '100%', position: 'absolute' },
});
