import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Text, TouchableOpacity } from 'react-native';
import styles from './style';

class Button extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { props } = this;
    return (
      <TouchableOpacity
        style={[styles.loginButton, props.redButton ? styles.rejectButton : styles.acceptButton]}
        onPress={props.function}
      >
        <Text style={styles.loginText}>{props.title}</Text>
      </TouchableOpacity>
    );
  }
}

Button.propTypes = {
  redButton: PropTypes.bool,
  function: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
};
Button.defaultProps = {
  redButton: false,
};

export default Button;
