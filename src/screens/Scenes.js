import React from 'react';
import {
  // Actions,
  Scene,
  Router,
  Drawer,
  // Modal,
} from 'react-native-router-flux';

import HomePage from './components/HomePage';
import ProfileSetting from './components/ProfileSetting';
import OrderHistory from './components/OrderHistory';

import DrawerContent from './components/DrawerContent';
import PackageDetails from './components/HomePage/PackageDetails';
import TrackOrder from './components/TrackOrder';
import FAQ from './components/FAQ';
import CustomerCare from './components/CustomerCare';
import Wallet from './components/Wallet';

import ReviewPage from './components/ReviewPage';
import RefundPage from './components/RefundPage';
import RouteCalculator from './components/RouteCalculator';


// const transitionConfig = () => ({
//   screenInterpolator: StackViewStyleInterpolator.forFadeFromBottomAndroid,
// });

const Scenes = props => (
  <Router>
    {/* {console.log('logging Scenes props', props)} */}
    <Scene key="root" hideNavBar>
      <Drawer
        hideNavBar
        key="drawer"
        contentComponent={DrawerContent}
        drawerWidth={300}>
        <Scene key="Home" hideNavBar>
          <Scene
            key="HomePage"
            component={HomePage}
            {...props}
            hideNavBar
            initial
          />
          <Scene
            key="ProfileSetting"
            component={ProfileSetting}
            hideNavBar
            {...props}
          />
          <Scene key="OrderHistory" component={OrderHistory} hideNavBar />
          <Scene key="PackageDetails" component={PackageDetails} hideNavBar />
          <Scene key="RouteCalculator" component={RouteCalculator} hideNavBar />
          <Scene key="FAQ" component={FAQ} hideNavBar />
          <Scene key="CustomerCare" component={CustomerCare} hideNavBar />
          <Scene key="Wallet" component={Wallet} hideNavBar />
          <Scene key="ReviewPage" component={ReviewPage} hideNavBar />
          <Scene key="RefundPage" component={RefundPage} hideNavBar />
        </Scene>
      </Drawer>
    </Scene>
  </Router>
);

export default Scenes;
