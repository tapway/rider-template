import React, { PureComponent } from 'react';
import { Text, View, Image } from 'react-native';
import MapView from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import styles from './style';
import Header from '../../components/Header';
import Oops from '../../components/Oops';

class CurrentStatus extends PureComponent {
  constructor() {
    super();
    this.home = this.home.bind(this);
    this.mapRef = null;
    this.state = {};
  }

  /**
   * Fit The Map According To Coordinates Of PickUp And Delivery Loaction
   * Requires PickUp and Delivery Coords
   */

  onMapReady = () => {
    const { props } = this;
    const { order, user } = props;
    const { deliveryLocation, pickUpLocation } = order;
    const { GpsLocation } = user;
    const destination = {
      latitude: deliveryLocation[1],
      longitude: deliveryLocation[0],
    };
    const pickUpLoc = {
      latitude: pickUpLocation[1],
      longitude: pickUpLocation[0],
    };
    const currLoc = { latitude: GpsLocation[1], longitude: GpsLocation[0] };

    this.mapRef.fitToCoordinates([destination, pickUpLoc, currLoc], {
      edgePadding: { top: 120, right: 120, bottom: 120, left: 120 },
      animated: true,
    });
  };

  // Navigate To Home Page
  home() {
    const { props } = this;
    props.navigation.navigate('home');
  }

  render() {
    const { props } = this;
    const { order, user } = props;
    const { GpsLocation } = user;
    const { deliveryLocation, pickUpLocation } = order;
    const destination = { latitude: deliveryLocation[1], longitude: deliveryLocation[0] };
    const pickUpLoc = { latitude: pickUpLocation[1], longitude: pickUpLocation[0] };
    const currLoc = { latitude: GpsLocation[1], longitude: GpsLocation[0] };
    const { _id: id } = order;
    const imgobj = { uri: user.profileImage };
    return (
      <View style={styles.container}>
        <View style={styles.container}>
          <Header onlyBack={false} back={this.home} title="Current Status" />
          {id ? (
            <React.Fragment>
              <View style={styles.secondConatiner}>
                <MapView
                  ref={ref => {
                    this.mapRef = ref;
                  }}
                  style={styles.map}
                  onMapReady={this.onMapReady}
                >
                  <MapView.Marker
                    coordinate={{
                      latitude: destination.latitude,
                      longitude: destination.longitude,
                    }}
                  >
                    <View style={styles.markerView}>
                      <Text style={styles.markerHeading}>Destination Location</Text>
                      <Text>{order.deliveryAddress}</Text>
                    </View>
                  </MapView.Marker>
                  <MapView.Marker
                    coordinate={{
                      latitude: currLoc.latitude,
                      longitude: currLoc.longitude,
                    }}
                  >
                    <View style={[styles.markerView, styles.userImage]}>
                      <View style={styles.row}>
                        <View>
                          <Image style={styles.userIconImage} source={imgobj} />
                        </View>
                        <View>
                          <Text style={[styles.textWhite, styles.boldb]}>Pickup</Text>
                          <Text style={styles.textWhite}>{user.fullName}</Text>
                        </View>
                      </View>
                      <Text style={[styles.textWhite, styles.topPading]}>{user.address}</Text>
                    </View>
                  </MapView.Marker>
                  <MapView.Marker
                    coordinate={{
                      latitude: pickUpLoc.latitude,
                      longitude: pickUpLoc.longitude,
                    }}
                  >
                    <View style={styles.markerView}>
                      <Text style={styles.markerHeading}>Pickup Location</Text>
                      <Text>{order.pickUpAddress}</Text>
                    </View>
                  </MapView.Marker>
                  <MapViewDirections
                    origin={currLoc}
                    destination={pickUpLoc}
                    apikey={props.config.googleMapsApiKey}
                    strokeWidth={3}
                    strokeColor="#144787"
                  />
                  <MapViewDirections
                    origin={currLoc}
                    destination={destination}
                    apikey={props.config.googleMapsApiKey}
                    strokeWidth={3}
                    strokeColor="#144787"
                  />
                </MapView>
              </View>
              <View style={styles.thirdContainer}>
                <View>
                  <Image style={styles.userIconImage} source={imgobj} />
                </View>
                <View>
                  <Text>{user.fullName}</Text>
                  <Text>MID : 234567</Text>
                </View>
              </View>
            </React.Fragment>
          ) : (
            <View style={styles.noOrder}>
              <Oops title="No Current Order" />
            </View>
          )}
        </View>
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    user: state.currentUser,
    order: state.currentOrder,
    config: state.config,
  };
};

CurrentStatus.propTypes = {
  config: PropTypes.objectOf(PropTypes.any).isRequired,
  order: PropTypes.objectOf(PropTypes.any).isRequired,
  user: PropTypes.objectOf(PropTypes.any).isRequired,
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default connect(
  mapStateToProps,
  null
)(CurrentStatus);
