import { StyleSheet, Dimensions } from 'react-native';
import colors from '../../utils/colors';

const { height, width } = Dimensions.get('window');
export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.themeBackgroundColor,
  },
  loginButton: {
    width: width / 3,
    alignItems: 'center',
    padding: width / 30,
    borderRadius: 50,
    justifyContent: 'center',
    borderColor: colors.transparent,
  },
  secondConatiner: {
    flex: 11,
  },
  markerView: {
    backgroundColor: colors.white,
    padding: width / 25,
    borderRadius: 10,
    width: width / 1.7,
  },
  markerHeading: {
    color: colors.secondaryColor,
    fontSize: height / 42,
    marginBottom: height / 150,
  },
  topPading: {
    marginTop: height / 125,
  },
  userImage: {
    backgroundColor: colors.primaryColor,
  },
  textWhite: {
    color: colors.textForModalContainerColor,
    fontSize: height / 55,
  },
  acceptButton: {
    backgroundColor: colors.primaryColor,
  },
  boldb: {
    fontWeight: 'bold',
  },
  row: {
    flexDirection: 'row',
  },
  rejectButton: {
    backgroundColor: colors.modalRejectinContainerColor,
  },
  thirdContainer: {
    flex: 2,
    flexDirection: 'row',
    marginLeft: width / 22,
    alignItems: 'center',
  },
  noOrder: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  noOrderText: {
    fontSize: width / 8,
    marginBottom: height / 20,
  },
  loginText: {
    color: colors.white,
    fontSize: width / 18,
  },
  map: {
    flex: 1,
  },

  userIconImage: {
    height: height / 22,
    width: height / 22,
    borderRadius: height / 22 / 2,
    marginRight: width / 20,
  },
});
