import React, { PureComponent } from 'react';
import { Text, View, SafeAreaView, ScrollView, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import styles from './style';
import Profile from './Profile';
import DL from './DrivingLicense';
import BankDetails from './BankDetails';
import VehicleDetail from './VehicleDetail';

class EditProfile extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      active: 0,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.checkActive = this.checkActive.bind(this);
  }

  componentDidMount() {
    const { props } = this;
    props.setEditProfile();
  }

  scroller = (active, clicked) => {
    if (active === clicked) {
      return;
    }
    switch (active) {
      case 0:
        this.Profile.scrollToBtns();
        break;
      case 1:
        this.DL.scrollToBtns();
        break;
      case 2:
        this.VehicleDetail.scrollToBtns();
        break;
      case 3:
        this.BankDetails.scrollToBtns();
        break;
      default:
    }
  };

  checkActive(active) {
    switch (active) {
      case 0:
        return (
          <Profile
            handleChange={this.handleChange}
            handleCancel={this.handleCancel}
            onRef={ref => {
              this.Profile = ref;
            }}
          />
        );
      case 1:
        return (
          <DL
            handleChange={this.handleChange}
            handleCancel={this.handleCancel}
            onRef={ref => {
              this.DL = ref;
            }}
          />
        );
      case 2:
        return (
          <VehicleDetail
            handleChange={this.handleChange}
            handleCancel={this.handleCancel}
            onRef={ref => {
              this.VehicleDetail = ref;
            }}
          />
        );
      case 3:
        return (
          <BankDetails
            handleChange={this.handleChange}
            handleCancel={this.handleCancel}
            onRef={ref => {
              this.BankDetails = ref;
            }}
          />
        );

      default:
        return '';
    }
  }

  handleChange(x) {
    this.setState({ active: x });
  }

  handleCancel() {
    const { props } = this;
    props.handleCancel();
  }

  render() {
    const { active } = this.state;

    const self = this;
    return (
      <View style={styles.container}>
        <SafeAreaView />
        <View style={styles.crossCont}>
          <TouchableOpacity
            onPress={() => {
              this.handleCancel();
            }}
            style={styles.crossContainer}
          >
            <Text style={styles.crossText}>X</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.mainContainer}>
          <View style={{ height: '8%' }}>
            <ScrollView
              horizontal
              showsHorizontalScrollIndicator={false}
              // style={styles.tabBar}
              style={{ flex: 1 }}
              contentContainerStyle={styles.tabBar}
            >
              <TouchableOpacity
                disabled={active === 0}
                style={[styles.tabBarBtn, active === 0 ? styles.activeTabBarBtn : {}]}
                onPress={() => {
                  this.scroller(active, 0);
                }}
              >
                <Text style={[styles.tabBarText, active === 0 ? styles.activeTabText : {}]}>
                  Profile
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                disabled={active === 1}
                style={[styles.tabBarBtn, active === 1 ? styles.activeTabBarBtn : {}]}
                onPress={() => {
                  this.scroller(active, 1);
                }}
              >
                <Text style={[styles.tabBarText, active === 1 ? styles.activeTabText : {}]}>
                  Driving License
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                disabled={active === 2}
                style={[styles.tabBarBtn, active === 2 ? styles.activeTabBarBtn : {}]}
                onPress={() => {
                  this.scroller(active, 2);
                }}
              >
                <Text style={[styles.tabBarText, active === 2 ? styles.activeTabText : {}]}>
                  Vehicle Details
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                disabled={active === 3}
                style={[styles.tabBarBtn, active === 3 ? styles.activeTabBarBtn : {}]}
                onPress={() => {
                  this.scroller(active, 3);
                }}
              >
                <Text style={[styles.tabBarText, active === 3 ? styles.activeTabText : {}]}>
                  Bank Details
                </Text>
              </TouchableOpacity>
            </ScrollView>
          </View>
          <View style={{ flex: 1 }}>{self.checkActive(active)}</View>
        </View>
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    user: state.currentUser,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    setEditProfile: () => {
      dispatch({
        type: 'SET_EDIT_PROFILE',
      });
    },
  };
};

EditProfile.propTypes = {
  setEditProfile: PropTypes.func.isRequired,
  handleCancel: PropTypes.func.isRequired,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditProfile);
