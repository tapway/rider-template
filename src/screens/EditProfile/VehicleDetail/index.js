/* eslint-disable react/no-string-refs */
/* eslint-disable no-nested-ternary */
import React, { PureComponent } from 'react';
import { Text, ScrollView, View, TouchableOpacity, Image } from 'react-native';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-picker';
import DatePicker from 'react-native-datepicker';
import Toast from 'react-native-easy-toast';
import PropTypes from 'prop-types';
import styles from '../style';
import { updateVehicleDetail } from '../../../redux/action/editProfile';
import { vehicleDetailsInitialState } from '../initialState';
import MyTextInput from '../../../components/TextInput';
import validation from './validation';
import colors from '../../../utils/colors';
import { heightRatio } from '../../../utils/stylesheet';

const options = {
  title: 'Select Avatar',
  quality: 0.1,
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};
let ref = null;
class VehicleDetail extends PureComponent {
  constructor() {
    super();
    this.state = vehicleDetailsInitialState;
  }

  componentDidMount() {
    const { refs } = this;
    ref = refs;
    const { props } = this;
    props.onRef(this);
  }

  scrollToBtns = () => {
    ref.scrollView.scrollTo({ y: 1000, animated: true });
  };

  handlerFocus = (input, value) => {
    this.setState({
      [input]: value,
    });
  };

  handlerInput = (input, value) => {
    this.setState({
      [input]: value,
    });
  };

  uploadimage = () => {
    ImagePicker.showImagePicker(options, response => {
      if (!response.didCancel && !response.error && !response.customButton) {
        this.setState({ avatarSource: response.uri });
      }
    });
  };

  submit = () => {
    const { props } = this;
    const { vehicleModel, registrationNumber, date, insurancePolicy, vehicleNumber } = this.state;
    const { avatarSource } = this.state;
    props.setVehicleDetailDispatcher(
      vehicleModel,
      registrationNumber,
      date,
      insurancePolicy,
      vehicleNumber,
      avatarSource
    );
    props.handleChange(3);
  };

  goBack = () => {
    const { props } = this;
    // startNavigation(0);
    props.handleChange(1);
  };

  render() {
    const { modelFocus, registrationFocus, insuranceFocus } = this.state;
    const { date: chooseDate, insurancePolicy, vehicleModel, dateFocus } = this.state;
    const { numberFocus, registrationNumber, vehicleNumber } = this.state;
    const { props } = this;
    const { user } = props;
    return (
      <View style={styles.fullFlex}>
        <ScrollView
          ref="scrollView"
          showsVerticalScrollIndicator={false}
          style={[styles.scroll, styles.contentContainer, { paddingTop: 10 * heightRatio }]}
        >
          <MyTextInput
            heading="Vehicles Model"
            placeholder={
              user.vehicleDetails.vehicleVerificationModal
                ? user.vehicleDetails.vehicleVerificationModal
                : 'Your Vehicle Model'
            }
            field="vehicleModel"
            value={vehicleModel}
            focusInput={modelFocus}
            focus="modelFocus"
            handlerFocus={this.handlerFocus}
            handlerInput={this.handlerInput}
          />
          <MyTextInput
            heading="Registration Number"
            placeholder={
              user.vehicleDetails.vehicleRegistrationNumber
                ? user.vehicleDetails.vehicleRegistrationNumber
                : 'Your Registration Number'
            }
            field="registrationNumber"
            value={registrationNumber}
            focusInput={registrationFocus}
            focus="registrationFocus"
            handlerFocus={this.handlerFocus}
            handlerInput={this.handlerInput}
          />
          <Text style={[styles.headingText2, styles.margin]}>Date of Registration</Text>
          <DatePicker
            style={[
              styles.dateInputBox,
              dateFocus === 0 ? styles.nofocus : dateFocus === 1 ? styles.focus : styles.redFocus,
            ]}
            date={chooseDate}
            mode="date"
            placeholder={
              user.vehicleDetails.vehicleDate
                ? user.vehicleDetails.vehicleDate
                : 'Date of Registration'
            }
            format="YYYY-MM-DD"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            customStyles={{
              dateIcon: styles.datePickerCustomStylesDateIcon,
              dateInput: styles.datePickerCustomStylesDateInput,
            }}
            onOpenModal={() => this.handlerFocus('dateFocus', 1)}
            onCloseModal={() => this.handlerFocus('dateFocus', 0)}
            onDateChange={date => {
              this.setState({ date });
            }}
          />
          <MyTextInput
            heading="Insurance Policy"
            placeholder={
              user.vehicleDetails.vehicleInsurancePolicy
                ? user.vehicleDetails.vehicleInsurancePolicy
                : 'Your Insurance Policy'
            }
            field="insurancePolicy"
            value={insurancePolicy}
            focusInput={insuranceFocus}
            focus="insuranceFocus"
            handlerFocus={this.handlerFocus}
            handlerInput={this.handlerInput}
          />
          <MyTextInput
            heading="Vehicle Number"
            placeholder={
              user.vehicleDetails.vehicleNumber
                ? user.vehicleDetails.vehicleNumber
                : 'Your Vehicle Number'
            }
            field="vehicleNumber"
            value={vehicleNumber}
            focusInput={numberFocus}
            focus="numberFocus"
            handlerFocus={this.handlerFocus}
            handlerInput={this.handlerInput}
          />
          {props.user.vehicleVerificationImage ? (
            <TouchableOpacity onPress={this.uploadimage}>
              <View style={styles.imageDocument}>
                <Image
                  style={styles.imageDocumentPic}
                  source={{ uri: props.user.vehicleVerificationImage }}
                />
              </View>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity style={styles.imageDocument} onPress={this.uploadimage}>
              <View style={styles.imageDocumentContainer}>
                <Text style={styles.uploadText}>Registration Certificate</Text>
                <Text style={styles.uploadText2}>upload your registration certificate</Text>
              </View>
            </TouchableOpacity>
          )}
          <View style={styles.nextBtnContainer}>
            <TouchableOpacity onPress={this.goBack} style={styles.buttonContainerBack}>
              <Text style={styles.backbutton}>Back</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => validation(this)} style={styles.buttonContainerNext}>
              <Text style={styles.nextButton}>Next</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
        <Toast
          ref="toast"
          style={{ backgroundColor: colors.toastBgColor }}
          position="bottom"
          positionValue={400}
          fadeInDuration={750}
          fadeOutDuration={1000}
          opacity={0.8}
          textStyle={{ color: colors.toastTextColor, fontSize: 15 }}
        />
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    user: state.currentUser,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    setVehicleDetailDispatcher: (modal, registrationNumber, date, insurance, number, img) => {
      dispatch(updateVehicleDetail(modal, registrationNumber, date, insurance, number, img));
    },
  };
};

VehicleDetail.propTypes = {
  onRef: PropTypes.func.isRequired,
  handleChange: PropTypes.func.isRequired,
  setVehicleDetailDispatcher: PropTypes.func.isRequired,
  user: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VehicleDetail);
