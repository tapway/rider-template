import React, { PureComponent } from 'react';
import { Text, ScrollView, View, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { Dropdown } from 'react-native-material-dropdown';
import Toast from 'react-native-easy-toast';

import PropTypes from 'prop-types';
import styles from '../style';
import colors from '../../../utils/colors';

import { updateBankDetails, updateDeliveryUser } from '../../../redux/action/editProfile';

import Button from '../../../components/Button';
import MyTextInput from '../../../components/TextInput';
import Loader from '../../../components/Loader';

import { bankDetailsInitialState } from '../initialState';
import updateDetailsDb from '../../../apiService/updateProfile';
import { store } from '../../../redux/configureStore';
import { heightRatio } from '../../../utils/stylesheet';

let ref = null;
class BankDetails extends PureComponent {
  constructor() {
    super();
    this.state = bankDetailsInitialState;
  }

  componentDidMount() {
    const { refs } = this;
    ref = refs;
    const { props } = this;
    props.onRef(this);
  }

  scrollToBtns = () => {
    ref.scrollView.scrollTo({ y: 1000, animated: true });
  };

  handlerFocus = (input, value) => {
    this.setState({
      [input]: value,
    });
  };

  handlerInput = (input, value) => {
    this.setState({
      [input]: value,
    });
  };

  handleSubmit = () => {
    const { props } = this;
    const { accountName, accountNum, bankName, IFSCcode } = this.state;
    props.updateBankDetailsDispatcher(accountName, accountNum, bankName, IFSCcode);
    updateDetailsDb(
      props.user.jwtAccessToken,
      store.getState().editProfile,
      props.updateDeliveryUserDispatcher,
      props.handleCancel
    );
  };

  render() {
    const { props } = this;
    const { nameFocus, numberFocus, codeFocus } = this.state;
    const { accountName, accountNum, IFSCcode, imageLoader } = this.state;
    const { Banks } = this.state;
    const { user } = props;
    return (
      <View style={styles.fullFlex}>
        <Loader loading={imageLoader} />
        <ScrollView
          ref="scrollView"
          showsVerticalScrollIndicator={false}
          style={[styles.scroll, { paddingTop: 10 * heightRatio }]}
        >
          <Text style={[styles.headingText2]}>Bank Name</Text>
          <Dropdown
            data={Banks}
            value={user.bankDetails.bankName ? user.bankDetails.bankName : 'Kotak Mahindra'}
            // eslint-disable-next-line react/no-unused-state
            onChangeText={value => this.setState({ bankName: value })}
            containerStyle={{ marginTop: -10 * heightRatio }}
          />
          <MyTextInput
            heading="Account Name"
            placeholder={
              user.bankDetails.accountName ? user.bankDetails.accountName : 'Your Account Name'
            }
            field="accountName"
            value={accountName}
            focusInput={nameFocus}
            focus="nameFocus"
            handlerFocus={this.handlerFocus}
            handlerInput={this.handlerInput}
          />
          <MyTextInput
            heading="Account Number"
            placeholder={
              user.bankDetails.accountNumber
                ? user.bankDetails.accountNumber
                : 'Your Account Number'
            }
            field="accountNum"
            value={accountNum}
            focusInput={numberFocus}
            focus="numberFocus"
            handlerFocus={this.handlerFocus}
            handlerInput={this.handlerInput}
          />
          <MyTextInput
            heading="IFSC Code"
            placeholder={user.bankDetails.IFSC_Code ? user.bankDetails.IFSC_Code : 'Your IFSC Code'}
            field="IFSCcode"
            value={IFSCcode}
            focusInput={codeFocus}
            focus="codeFocus"
            handlerFocus={this.handlerFocus}
            handlerInput={this.handlerInput}
          />
          <View style={[styles.loginBtnContainer, styles.topPad]}>
            <Button submit={this.handleSubmit} title="Update" />
          </View>
          <View style={[styles.backKycBtnContainer, styles.botpad]}>
            <TouchableOpacity
              style={styles.backKycButton}
              onPress={() => {
                props.handleChange(2);
              }}
            >
              <Text style={styles.backKycText}>Back</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
        <Toast
          ref="toast"
          style={{ backgroundColor: colors.toastBgColor }}
          position="bottom"
          positionValue={400}
          fadeInDuration={750}
          fadeOutDuration={1000}
          opacity={0.8}
          textStyle={{ color: colors.toastTextColor, fontSize: 15 }}
        />
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.currentUser,
    edit: state.editProfile,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    updateBankDetailsDispatcher: (accname, accnum, bankname, ifsccode) => {
      dispatch(updateBankDetails(accname, accnum, bankname, ifsccode));
    },
    updateDeliveryUserDispatcher: res => {
      dispatch(updateDeliveryUser(res));
    },
  };
};

BankDetails.propTypes = {
  updateBankDetailsDispatcher: PropTypes.func.isRequired,
  updateDeliveryUserDispatcher: PropTypes.func.isRequired,
  handleChange: PropTypes.func.isRequired,
  handleCancel: PropTypes.func.isRequired,
  onRef: PropTypes.func.isRequired,
  user: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BankDetails);
