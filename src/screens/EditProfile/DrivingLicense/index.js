import React, { PureComponent } from 'react';
import { Text, ScrollView, View, TouchableOpacity, Image } from 'react-native';
import { connect } from 'react-redux';
import DatePicker from 'react-native-datepicker';
import ImagePicker from 'react-native-image-picker';
import PropTypes from 'prop-types';
import Toast from 'react-native-easy-toast';
import colors from '../../../utils/colors';
import styles from '../style';
import { updateKYC } from '../../../redux/action/editProfile';
import { kycInitialState } from '../initialState';
import MyTextInput from '../../../components/TextInput';
import validation from './validation';
import { changeVerificationPicAsync } from '../../../redux/action/imageUpload';
import { heightRatio } from '../../../utils/stylesheet';

const options = {
  title: 'Select Avatar',
  quality: 0.1,
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};
let ref = null;
class DL extends PureComponent {
  constructor() {
    super();
    this.state = kycInitialState;
  }

  componentDidMount() {
    const { refs } = this;
    ref = refs;
    const { props } = this;
    props.onRef(this);
  }

  scrollToBtns = () => {
    ref.scrollView.scrollTo({ y: 1000, animated: true });
  };

  handlerFocus = (input, value) => {
    this.setState({
      [input]: value,
    });
  };

  handlerInput = (input, value) => {
    this.setState({
      [input]: value,
    });
  };

  uploadimage = () => {
    ImagePicker.showImagePicker(options, response => {
      if (!response.didCancel && !response.error && !response.customButton) {
        changeVerificationPicAsync({ localUrl: response.uri }, response.type);
      }
    });
  };

  submit = () => {
    const { props } = this;
    const {
      address,
      licenceNumber,
      cardHolder,
      dateOfIssue,
      validity,
    } = this.state;
    props.updateKycDispatcher(
      address,
      licenceNumber,
      cardHolder,
      dateOfIssue,
      validity,
    );
    props.handleChange(2);
  };

  goBack = () => {
    const { props } = this;
    props.handleChange(0);
  };

  render() {
    const {
      cardHolderFocus,
      licenceNumberFocus,
      dateFocus,
      validityFocus,
    } = this.state;
    const {
      address,
      licenceNumber,
      cardHolder,
      dateOfIssue,
      validity,
      addressFocus,
    } = this.state;
    const { props } = this;
    const { user } = props;
    return (
      <View style={styles.fullFlex}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          style={[
            styles.scroll,
            styles.contentContainer,
            { paddingTop: 10 * heightRatio },
          ]}
          ref="scrollView">
          <MyTextInput
            heading="Card Holder"
            placeholder={
              user.verificationDetails.cardHolder
                ? user.verificationDetails.cardHolder
                : 'Card Holder Name '
            }
            field="cardHolder"
            value={cardHolder}
            focusInput={cardHolderFocus}
            focus="cardHolderFocus"
            handlerFocus={this.handlerFocus}
            handlerInput={this.handlerInput}
          />
          {/* <MyTextInput
            heading="Licence Number"
            placeholder={
              user.verificationDetails.licenceNumber
                ? 'x'.repeat(8) +
                  user.verificationDetails.licenceNumber.substr(
                    user.verificationDetails.licenceNumber.length - 4
                  )
                : 'Your License Name'
            }
            field="licenceNumber"
            value={licenceNumber}
            focusInput={licenceNumberFocus}
            focus="licenceNumberFocus"
            handlerFocus={this.handlerFocus}
            handlerInput={this.handlerInput}
          /> */}
          <Text style={[styles.headingText2, styles.margin]}>
            Date Of Issue
          </Text>
          <DatePicker
            style={[
              styles.dateInputBox,
              dateFocus === 0
                ? styles.nofocus
                : dateFocus === 1
                ? styles.focus
                : styles.redFocus,
            ]}
            date={dateOfIssue}
            mode="date"
            placeholder={
              user.verificationDetails.dateOfIssue
                ? user.verificationDetails.dateOfIssue
                : 'Date of Issue'
            }
            format="DD-MM-YYYY"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            customStyles={{
              dateIcon: styles.datePickerCustomStylesDateIcon,
              dateInput: styles.datePickerCustomStylesDateInput,
            }}
            onOpenModal={() => this.handlerFocus('dateFocus', 1)}
            onCloseModal={() => this.handlerFocus('dateFocus', 0)}
            onDateChange={date => {
              this.setState({ dateOfIssue: date });
            }}
          />
          <Text style={[styles.headingText2, styles.margin]}>
            Validity Of Licence
          </Text>
          <DatePicker
            style={[
              styles.dateInputBox,
              validityFocus === 0
                ? styles.nofocus
                : validityFocus === 1
                ? styles.focus
                : styles.redFocus,
            ]}
            date={validity}
            mode="date"
            placeholder={
              user.verificationDetails.validity
                ? user.verificationDetails.validity
                : 'Validity of License'
            }
            format="DD-MM-YYYY"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            customStyles={{
              dateIcon: styles.datePickerCustomStylesDateIcon,
              dateInput: styles.datePickerCustomStylesDateInput,
            }}
            onOpenModal={() => this.handlerFocus('validityFocus', 1)}
            onCloseModal={() => this.handlerFocus('validityFocus', 0)}
            onDateChange={date => {
              this.setState({ validity: date });
            }}
          />
          <MyTextInput
            heading="Address"
            placeholder={user.address ? user.address : 'Your Address'}
            field="address"
            value={address}
            focusInput={addressFocus}
            focus="addressFocus"
            handlerFocus={this.handlerFocus}
            handlerInput={this.handlerInput}
          />
          {props.changedUser.verificationImage || user.verificationImage ? (
            <TouchableOpacity onPress={this.uploadimage}>
              <View style={styles.imageDocument}>
                <Image
                  style={styles.imageDocumentPic}
                  source={
                    props.changedUser.verificationImage
                      ? { uri: props.changedUser.verificationImage }
                      : { uri: user.verificationImage }
                  }
                />
              </View>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              style={styles.imageDocument}
              onPress={this.uploadimage}>
              <View style={styles.imageDocumentContainer}>
                <Text style={styles.uploadText}>Driving License</Text>
                <Text style={styles.uploadText2}>
                  upload your driving license
                </Text>
              </View>
            </TouchableOpacity>
          )}
          <View style={styles.nextBtnContainer}>
            <TouchableOpacity
              onPress={this.goBack}
              style={styles.buttonContainerBack}>
              <Text style={styles.backbutton}>Back</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => validation(this)}
              style={styles.buttonContainerNext}>
              <Text style={styles.nextButton}>Next</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
        <Toast
          ref="toast"
          style={{ backgroundColor: colors.toastBgColor }}
          position="bottom"
          positionValue={400}
          fadeInDuration={750}
          fadeOutDuration={1000}
          opacity={0.8}
          textStyle={{ color: colors.toastTextColor, fontSize: 15 }}
        />
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    user: state.currentUser,
    changedUser: state.editProfile,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    updateKycDispatcher: (
      address,
      licenceNumber,
      cardHolder,
      dateOfIssue,
      validity,
      img,
    ) => {
      dispatch(
        updateKYC(
          address,
          licenceNumber,
          cardHolder,
          dateOfIssue,
          validity,
          img,
        ),
      );
    },
  };
};

DL.propTypes = {
  onRef: PropTypes.func.isRequired,
  handleChange: PropTypes.func.isRequired,
  updateKycDispatcher: PropTypes.func.isRequired,
  changedUser: PropTypes.objectOf(PropTypes.any).isRequired,
  user: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DL);
