import { StyleSheet, Dimensions } from 'react-native';
import { heightRatio, widthRatio } from '../../utils/stylesheet';
import colors from '../../utils/colors';

const { height, width } = Dimensions.get('window');
export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: colors.blurEffect,
    padding: height / 40,
    paddingBottom: height / 30,
  },
  crossCont: {
    height: 25 * heightRatio,
    alignItems: 'flex-end',
  },
  crossContainer: {
    height: '100%',
    width: 25 * heightRatio,
    backgroundColor: colors.crossContainerColor,
    borderRadius: 12.5 * heightRatio,
    alignItems: 'center',
    justifyContent: 'center',
  },
  crossText: {
    fontSize: 14 * heightRatio,
  },
  mainContainer: {
    backgroundColor: colors.themeBackgroundColor,
    flex: 1,
    marginTop: 20,
    paddingHorizontal: height / 35,
    borderRadius: 5,
  },
  imageDocument: {
    marginTop: 15 * heightRatio,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 2,
    borderColor: colors.imageContainerColor,
    height: height / 4.5,
    width: width / 1.35,
  },
  imageDocumentContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnContainer: {
    justifyContent: 'flex-end',
    flexDirection: 'row',
    height: height / 10,
    alignItems: 'center',
  },
  cancelBtn: {
    color: colors.secondaryColor,
    fontSize: height / 50,
    fontWeight: '600',
    marginRight: width / 10,
  },
  nextBtn: {
    color: colors.primaryColor,
    fontSize: height / 50,
    fontWeight: '600',
  },

  scroll: {
    marginTop: 0,
  },
  headingContainer: {
    flex: 1,
    top: height / 8,
    paddingLeft: width / 10,
    alignItems: 'baseline',
  },
  contentContainer: {
    flex: 3,
    paddingRight: width / 30,
  },
  headingText1: {
    fontSize: height / 20,
  },
  headingText2: {
    fontSize: 14 * heightRatio,
    paddingTop: height / 80,
    fontWeight: '200',
  },
  focus: {
    borderColor: colors.primaryColor,
  },
  nofocus: {
    borderColor: colors.primaryBorderColor,
  },
  textInputBox: {
    borderStyle: 'solid',
    borderBottomWidth: 1,
    padding: width / 30,
    paddingLeft: 0,
    fontSize: height / 60,
    color: colors.textForThemeBgColor,
  },
  backSignContainer: {
    top: height / 18,
    paddingLeft: width / 15,
  },
  margin: {
    marginTop: 10 * heightRatio,
  },
  operations: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  nextButton: {
    fontSize: 14 * heightRatio,
    color: colors.themeBackgroundColor,
    fontWeight: '500',
  },
  opfocus: {
    borderStyle: 'solid',
    borderBottomWidth: 1,
    borderColor: colors.primaryColor,
    fontWeight: 'bold',
  },
  noopfocus: {
    fontWeight: '200',
  },
  tabBar: {
    flexDirection: 'row',
    marginTop: height / 30,
    overflow: 'hidden',
  },
  tabBarText: {
    fontSize: 14 * heightRatio,
    fontWeight: '200',
    color: colors.textForTabBar,
  },
  tabBarBtn: {
    color: colors.textForThemeBgColor,
    marginRight: width / 10,
  },
  activeTabBarBtn: {
    fontWeight: '400',
    color: colors.textForThemeBgColor,
    borderBottomColor: colors.primaryColor,
    borderBottomWidth: 3,
  },
  activeTabText: {
    fontWeight: '400',
    color: colors.textForThemeBgColor,
  },
  // new
  imageUpload: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: height / 30,
    marginBottom: 20 * heightRatio,
    marginTop: 10 * heightRatio,
  },
  uploadText: {
    fontSize: 14 * heightRatio,
    color: colors.textForTabBar,
  },
  uploadText2: {
    marginTop: height / 150,
    fontSize: 12 * heightRatio,
    color: colors.textForTabBar,
    fontWeight: '200',
  },
  uploadText3: {
    marginTop: height / 150,
    fontSize: 12 * heightRatio,
    color: colors.primaryColor,
    fontWeight: 'bold',
  },
  imageDocumentPic: {
    height: height / 4.5,
    width: width / 1.35,
  },
  userIconImage: {
    height: width / 5,
    width: width / 5,
    borderRadius: width / 5 / 2,
  },
  dropdownStyle: {
    width: width / 1.25,
  },

  dropdown: {
    color: colors.primaryColor,
    borderColor: colors.textForModalContainerColor,
    fontSize: width / 18,
  },
  dropdownTextStyle: {
    fontSize: width / 18,
  },
  pad: {
    paddingBottom: height / 140,
  },
  backButton: {
    height: height / 48,
    width: width / 19,
    marginTop: height / 140,
    overflow: 'visible',
  },
  operationsText: {
    fontSize: height / 39,
  },
  nextBtnContainer: {
    alignItems: 'center',
    paddingTop: 30 * heightRatio,
    paddingBottom: height / 20,
    paddingHorizontal: 10 * widthRatio,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  botpad: {
    marginBottom: height / 12,
  },
  backbutton: {
    fontSize: 14 * heightRatio,
    // paddingRight: width / 10,
    color: colors.secondaryColor,
    fontWeight: '500',
  },
  topPad: {
    marginTop: height / 15,
  },
  loginButton: {
    width: width / 2,
    alignItems: 'center',
    padding: width / 30,
    borderRadius: 50,
    backgroundColor: colors.primaryColor,
    justifyContent: 'center',
    borderColor: colors.transparent,
  },
  loginBtnContainer: {
    flex: 2,
    alignItems: 'center',
  },
  loginText: {
    color: colors.textForPrimaryBgColor,
    fontSize: height / 40,
  },
  backKycBtnContainer: {
    marginTop: height / 40,
    flex: 2,
    alignItems: 'center',
  },
  datePickerCustomStylesDateIcon: {
    position: 'absolute',
    left: 0,
    top: 4,
    marginLeft: 0,
  },
  datePickerCustomStylesDateInput: {
    marginLeft: 40 * widthRatio,
    borderWidth: 0,
    alignItems: 'flex-start',
  },
  dateInputBox: {
    borderStyle: 'solid',
    borderBottomWidth: 1,
    padding: 4 * heightRatio,
    marginTop: 6 * heightRatio,
    width: '100%',
    paddingLeft: 0,
    fontSize: height / 60,
    color: colors.primaryColor,
  },
  fullFlex: { flex: 1 },
  backKycButton: {
    width: '100%',
    height: 44 * heightRatio,
    alignItems: 'center',
    padding: width / 30,
    borderRadius: 50,
    backgroundColor: colors.thirdBtnBgColor,
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: colors.secondaryColor,
  },
  backKycText: {
    color: colors.secondaryColor,
    fontSize: height / 40,
  },
  buttonContainerBack: {
    borderWidth: 1,
    borderColor: colors.secondaryColor,
    borderRadius: 25,
    width: '45%',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10 * widthRatio,
  },
  buttonContainerNext: {
    backgroundColor: colors.primaryColor,
    borderRadius: 25,
    width: '45%',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10 * widthRatio,
  },
});
