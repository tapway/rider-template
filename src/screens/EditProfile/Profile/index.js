import React, { PureComponent } from 'react';
import { Text, ScrollView, View, TouchableOpacity, Image } from 'react-native';
import { Dropdown } from 'react-native-material-dropdown';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-picker';
import Toast from 'react-native-easy-toast';
import DatePicker from 'react-native-datepicker';
import PropTypes from 'prop-types';
import styles from '../style';
import { updateProfile } from '../../../redux/action/editProfile';
import getImage from '../../../utils/getImage';
import { profileInitialState } from '../initialState';
import MyTextInput from '../../../components/TextInput';
import validation from './validation';
import { heightRatio } from '../../../utils/stylesheet';
import { changeUserProfilePicAsync } from '../../../redux/action/imageUpload';
import colors from '../../../utils/colors';

const options = {
  title: 'Select Avatar',
  quality: 0.1,
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};
let ref = null;
class Profile extends PureComponent {
  constructor() {
    super();
    this.state = profileInitialState;
    this.phone = null;
  }

  componentDidMount() {
    const { refs } = this;
    ref = refs;
    const { props } = this;
    props.onRef(this);
  }

  componentWillUnmount() {
    const { props } = this;
    props.onRef(undefined);
  }

  scrollToBtns = () => {
    ref.scrollView.scrollTo({ y: 1000, animated: true });
  };

  uploadImage = () => {
    ImagePicker.showImagePicker(options, response => {
      if (!response.didCancel && !response.error && !response.customButton) {
        changeUserProfilePicAsync({ localUrl: response.uri }, response.type);
      }
    });
  };

  handlerFocus = (input, value) => {
    this.setState({
      [input]: value,
    });
  };

  handlerInput = (input, value) => {
    this.setState({
      [input]: value,
    });
  };

  submit = () => {
    const { props } = this;
    const { mobile, password, fullName, email, dob, confirmPassword } = this.state;
    const { Currency, Language, avatarSource } = this.state;
    props.updateProfileDispatcher(
      mobile,
      password,
      fullName,
      email,
      dob,
      confirmPassword,
      Currency,
      Language,
      avatarSource
    );
    props.handleChange(1);
  };

  render() {
    const { nameFocus, emailFocus, dobFocus } = this.state;
    const { fullName, email, dob } = this.state;
    const { Genderopt } = this.state;
    const { props } = this;
    const { user } = props;
    return (
      <View style={styles.fullFlex}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          style={[styles.scroll, styles.contentContainer]}
          ref="scrollView"
        >
          <View style={styles.imageUpload}>
            <TouchableOpacity onPress={this.uploadImage}>
              <Image
                style={styles.userIconImage}
                source={
                  props.changedUser.profileImage
                    ? { uri: props.changedUser.profileImage }
                    : props.user.profileImage
                    ? { uri: props.user.profileImage }
                    : getImage.secondDriver
                }
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={this.uploadImage}>
              <Text style={styles.uploadText3}>Upload Image</Text>
            </TouchableOpacity>
          </View>
          <MyTextInput
            heading="Full Name"
            placeholder={user.fullName ? user.fullName : 'Your Name'}
            field="fullName"
            value={fullName}
            focusInput={nameFocus}
            focus="nameFocus"
            handlerFocus={this.handlerFocus}
            handlerInput={this.handlerInput}
          />
          <MyTextInput
            heading="Email"
            placeholder={user.email ? user.email : 'Enter Email'}
            field="email"
            defaultValue={user.fullName}
            value={email}
            focusInput={emailFocus}
            focus="emailFocus"
            handlerFocus={this.handlerFocus}
            handlerInput={this.handlerInput}
          />
          <Text style={[styles.headingText2, styles.margin]}>DOB</Text>
          <DatePicker
            style={[
              styles.dateInputBox,
              dobFocus === 0 ? styles.nofocus : dobFocus === 1 ? styles.focus : styles.redFocus,
            ]}
            date={dob}
            mode="date"
            placeholder={user.dob ? user.dob : 'Enter Date of Birth'}
            format="YYYY-MM-DD"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            customStyles={{
              dateIcon: styles.datePickerCustomStylesDateIcon,
              dateInput: styles.datePickerCustomStylesDateInput,
            }}
            onOpenModal={() => this.handlerFocus('dobFocus', 1)}
            onCloseModal={() => this.handlerFocus('dobFocus', 0)}
            onDateChange={date => {
              this.setState({ dob: date });
            }}
          />
          <Text style={[styles.headingText2, styles.margin]}>Gender</Text>
          <Dropdown
            data={Genderopt}
            value={user.gender ? user.gender : 'Male'}
            onChangeText={value => this.setState({ gender: value })}
            containerStyle={{ marginTop: -10 * heightRatio }}
          />
          <View style={styles.nextBtnContainer}>
            <TouchableOpacity
              onPress={() => {
                props.handleCancel();
              }}
              style={styles.buttonContainerBack}
            >
              <Text style={styles.backbutton}>Cancel</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => validation(this)} style={styles.buttonContainerNext}>
              <Text style={styles.nextButton}>Next</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
        <Toast
          ref="toast"
          style={{ backgroundColor: colors.toastBgColor }}
          position="bottom"
          positionValue={400}
          fadeInDuration={750}
          fadeOutDuration={1000}
          opacity={0.8}
          textStyle={{ color: colors.toastTextColor, fontSize: 15 }}
        />
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    user: state.currentUser,
    changedUser: state.editProfile,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    updateProfileDispatcher: (mob, pass, fname, mail, birth, gende, curr, lang, img) => {
      dispatch(updateProfile(mob, pass, fname, mail, birth, gende, curr, lang, img));
    },
  };
};

Profile.propTypes = {
  onRef: PropTypes.func.isRequired,
  handleChange: PropTypes.func.isRequired,
  handleCancel: PropTypes.func.isRequired,
  updateProfileDispatcher: PropTypes.func.isRequired,
  changedUser: PropTypes.objectOf(PropTypes.any).isRequired,
  user: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
