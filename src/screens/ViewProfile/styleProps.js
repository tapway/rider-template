import { StyleSheet, Dimensions } from 'react-native';
import colors from '../../utils/colors';
import { heightRatio } from '../../utils/stylesheet';

const { height, width } = Dimensions.get('window');
export default StyleSheet.create({
  container: {
    padding: width / 20,
    paddingTop: 20 * heightRatio,
    flexDirection: 'column',
    backgroundColor: colors.themeBackgroundColor,
  },
  profileProp: {
    marginTop: 12 * heightRatio,
    flexDirection: 'row',
    alignItems: 'center',
  },
  propImg: {
    height: 12 * heightRatio,
    width: width / 20,
    overflow: 'visible',
  },
  propTxt: {
    fontWeight: '300',
    fontSize: 12 * heightRatio,
    marginLeft: width / 22,
    letterSpacing: 0.2,
  },

  Heading: {
    fontSize: 16 * heightRatio,
    fontWeight: '200',
    letterSpacing: 0.4,
  },
  pDetails: {
    flexDirection: 'row',
    marginBottom: 22 * heightRatio,
    justifyContent: 'space-between',
  },
  pDetail: {
    flex: 1,
    // marginTop: height / 55,
  },
  bDetail: {
    marginBottom: 22 * heightRatio,
  },
  pdType: {
    fontWeight: '100',
    fontSize: 10 * heightRatio,
    marginBottom: height / 200,
    marginTop: 2 * heightRatio,
    letterSpacing: 0.4,
  },
  pdValue: {
    fontWeight: '500',
    letterSpacing: 0.4,
    fontSize: 12 * heightRatio,
  },
  IdDetails: {
    marginTop: height / 40,
  },
  bDetails: {
    flexDirection: 'column',
    marginTop: height / 55,
  },
  detailsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  addBtn: {
    backgroundColor: colors.primaryColor,
    width: 'auto',
    justifyContent: 'center',
    alignItems: 'center',
    height: height / 20,
    borderRadius: height / 180,
    marginTop: height / 28,
  },
  addBtnText: {
    color: colors.white,
  },
});
