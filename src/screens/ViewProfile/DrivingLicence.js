import React, { PureComponent } from 'react';
import { View, Text } from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import styles from './styleProps';
import { heightRatio } from '../../utils/stylesheet';

class DL extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { user: userobj } = this.props;
    return (
      <View style={styles.container}>
        <View style={[styles.pDetails, { marginTop: 12 * heightRatio }]}>
          <View style={styles.pDetail}>
            <Text style={styles.pdValue}>Card Holder</Text>
            <Text style={styles.pdType}>
              {userobj.verificationDetails.cardHolder}
            </Text>
          </View>
          <View style={styles.pDetail}>
            <Text style={styles.pdValue}>Licence Number</Text>
            {/* <Text style={styles.pdType}>
              {` ${'X'.repeat(8)}`}
              {userobj.verificationDetails.licenceNumber.substr(
                userobj.verificationDetails.licenceNumber.length - 4
              )}
            </Text> */}
          </View>
        </View>

        <View style={styles.pDetails}>
          <View style={styles.pDetail}>
            <Text style={styles.pdValue}>Address</Text>
            <Text style={styles.pdType}>{userobj.address}</Text>
          </View>
        </View>

        <View style={styles.pDetails}>
          <View style={styles.pDetail}>
            <Text style={styles.pdValue}>Date of Issue</Text>
            <Text style={styles.pdType}>
              {userobj.verificationDetails.dateOfIssue}
            </Text>
          </View>
          <View style={styles.pDetail}>
            <Text style={styles.pdValue}>Validity Licence</Text>
            <Text style={styles.pdType}>
              {userobj.verificationDetails.validity}
            </Text>
          </View>
        </View>
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    user: state.currentUser,
  };
};
DL.propTypes = {
  user: PropTypes.objectOf(PropTypes.any).isRequired,
};
export default connect(
  mapStateToProps,
  null,
)(DL);
