import { StyleSheet, Dimensions } from 'react-native';
import colors from '../../utils/colors';
import { heightRatio, widthRatio } from '../../utils/stylesheet';

const { height, width } = Dimensions.get('window');
export default StyleSheet.create({
  container: {
    flexDirection: 'column',
    backgroundColor: colors.themeBackgroundColor,
  },

  nameBar: {
    paddingLeft: width / 23,
    paddingRight: width / 23,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  backButton: {
    height: height / 48,
    width: width / 19,
    marginTop: height / 140,
    overflow: 'visible',
  },
  backBtn: {
    height: height / 48,
    width: width / 19,
    marginTop: height / 140,
    overflow: 'visible',
  },
  nameBarText: {
    fontSize: height / 40,
    fontWeight: '200',
  },
  profile: {
    flexDirection: 'row',
    height: height / 7.5,
    backgroundColor: colors.highlightedContainer,
    alignItems: 'center',
  },
  profilePic: {
    height: height / 12,
    borderRadius: height / 12 / 2,
    width: height / 12,
    marginLeft: width / 16,
  },
  details: {
    flex: 1,
    marginLeft: width / 24,
  },
  name: {
    fontSize: 14 * heightRatio,
    fontWeight: '500',
    paddingBottom: 4 * heightRatio,
  },
  email: {
    fontSize: 10 * heightRatio,
    fontWeight: '300',
  },
  bankButton: {
    marginRight: width / 2,
  },
  tabsContainer: {},
  tabBar: {
    flexDirection: 'row',
    marginTop: 20 * heightRatio,
    paddingHorizontal: 10 * widthRatio,
  },
  tabBarText: {
    fontSize: 14 * heightRatio,
    fontWeight: '200',
    color: colors.textForTabBar,
  },
  tabBarBtn: {
    height: height / 25,

    alignItems: 'center',
    justifyContent: 'center',
  },
  activeTabBarBtn: {
    fontWeight: '600',
    color: colors.textForHighlightedContainer,
  },
  activeTouchable: {
    borderBottomWidth: 3,
    borderColor: colors.primaryColor,
  },
});
