import React, { PureComponent } from 'react';
import { View, Text } from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import styles from './styleProps';
import { heightRatio } from '../../utils/stylesheet';

class VehicleDetail extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    // eslint-disable-next-line react/prop-types
    const { user } = this.props;
    const { vehicleDetails } = user;

    return (
      <View style={styles.container}>
        <View style={[styles.pDetails, { marginTop: 12 * heightRatio }]}>
          <View style={styles.pDetail}>
            <Text style={styles.pdValue}>Vehicle Model</Text>
            <Text style={styles.pdType}>{vehicleDetails.vehicleVerificationModal}</Text>
          </View>
          <View style={styles.pDetail}>
            <Text style={styles.pdValue}>Registration Number</Text>
            <Text style={styles.pdType}>{vehicleDetails.vehicleRegistrationNumber}</Text>
          </View>
        </View>

        <View style={styles.pDetails}>
          <View style={styles.pDetail}>
            <Text style={styles.pdValue}>Date Of Registration</Text>
            <Text style={styles.pdType}>{vehicleDetails.vehicleDate}</Text>
          </View>
          <View style={styles.pDetail}>
            <Text style={styles.pdValue}>Vehicle Number</Text>
            <Text style={styles.pdType}>{vehicleDetails.vehicleNumber}</Text>
          </View>
        </View>

        <View style={styles.pDetails}>
          <View style={styles.pDetail}>
            <Text style={styles.pdValue}>Insurance Policy</Text>
            <Text style={styles.pdType}>{vehicleDetails.vehicleInsurancePolicy}</Text>
          </View>
        </View>
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    user: state.currentUser,
  };
};
VehicleDetail.propTypes = {
  user: PropTypes.objectOf(PropTypes.any).isRequired,
};
export default connect(
  mapStateToProps,
  null
)(VehicleDetail);
