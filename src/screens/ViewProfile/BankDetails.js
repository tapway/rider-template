import React, { PureComponent } from 'react';
import { View, Text } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import styles from './styleProps';

class BankDetails extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { user: userobj } = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.detailsContainer}>
          <View style={styles.bDetails}>
            <View style={styles.bDetail}>
              <Text style={styles.pdValue}>Account name</Text>
              <Text style={styles.pdType}>{userobj.bankDetails.accountName}</Text>
            </View>
            <View style={styles.bDetail}>
              <Text style={styles.pdValue}>Bank name</Text>
              <Text style={styles.pdType}>{userobj.bankDetails.bankName}</Text>
            </View>
          </View>
          <View style={styles.bDetails}>
            <View style={styles.bDetail}>
              <Text style={styles.pdValue}>Account number</Text>
              <Text style={styles.pdType}>
                {Math.floor(userobj.bankDetails.accountNumber / 10 ** 14)}
                {'XXXXXXXXXXXXXX'}
              </Text>
            </View>

            <View style={styles.bDetail}>
              <Text style={styles.pdValue}>IFSC code</Text>
              <Text style={styles.pdType}>{userobj.bankDetails.IFSC_Code}</Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    user: state.currentUser,
  };
};
BankDetails.propTypes = {
  user: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default connect(
  mapStateToProps,
  null
)(BankDetails);
