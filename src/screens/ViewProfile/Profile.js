import React, { PureComponent } from 'react';
import { View, Text, Image } from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import styles from './styleProps';
import getImage from '../../utils/getImage';

class Profile extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { user: userobj } = this.props;

    return (
      <View style={styles.container}>
        <View style={styles.profileProp}>
          <Image
            resizeMode="contain"
            source={getImage.userFirst}
            style={styles.propImg}
          />
          <Text style={styles.propTxt}>{userobj.fullName}</Text>
        </View>
        <View style={styles.profileProp}>
          <Image
            resizeMode="contain"
            source={getImage.call}
            style={styles.propImg}
          />
          <Text style={styles.propTxt}>{userobj.phoneNumber}</Text>
        </View>
        <View style={styles.profileProp}>
          <Image
            resizeMode="contain"
            source={getImage.email}
            style={styles.propImg}
          />
          <Text style={styles.propTxt}>{userobj.email}</Text>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.currentUser,
  };
};
Profile.propTypes = {
  user: PropTypes.objectOf(PropTypes.any).isRequired,
};
export default connect(
  mapStateToProps,
  null,
)(Profile);
