import React, { PureComponent } from 'react';
import { View, Text, Image, Modal, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import styles from './style';
import Header from '../../components/Header';

// import TabNavigator from './tabnav';
import EditProfile from '../EditProfile';
import { widthRatio } from '../../utils/stylesheet';

class ViewProfile extends PureComponent {
  static checkActiveTab(prop) {
    switch (prop.navigation.state.index) {
      case 0:
        return 'Driving Licence';
      case 1:
        return 'Vehicle Details';
      case 2:
        return 'Bank Details';
      default:
        return '';
    }
  }

  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
    };
    this.home = this.home.bind(this);
    this.editProfile = this.editProfile.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
  }

  home() {
    const { props } = this;
    props.navigation.navigate('home');
  }

  editProfile() {
    this.setState({ modalVisible: true });
  }

  handleCancel() {
    this.setState({ modalVisible: false });
  }

  render() {
    const { modalVisible: modalV } = this.state;
    // eslint-disable-next-line react/prop-types
    const { user } = this.props;
    const prop = this.props;
    const imgobj = { uri: user.profileImage };
    return (
      <View style={styles.container}>
        <Header
          twoButtons
          back={this.home}
          next={this.editProfile}
          title={ViewProfile.checkActiveTab(prop)}
        />
        <Modal visible={modalV} transparent>
          <EditProfile handleCancel={this.handleCancel} user={user} />
        </Modal>

        <View style={styles.profile}>
          {/* <Image source={imgobj} style={styles.profilePic} /> */}
          <View style={styles.details}>
            <Text style={styles.name}>{user.fullName}</Text>
            <Text style={styles.email}>{user.email}</Text>
            <Text style={styles.email}>{user.phoneNumber}</Text>
          </View>
        </View>
        <View style={styles.tabBar}>
          <TouchableOpacity
            style={[
              styles.tabBarBtn,
              prop.navigation.state.index === 0 ? styles.activeTouchable : {},
            ]}
            onPress={() => {
              prop.navigation.navigate('dl');
            }}>
            <Text
              style={[
                styles.tabBarText,
                prop.navigation.state.index === 0 ? styles.activeTabBarBtn : {},
              ]}>
              Driving Licence
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[
              styles.tabBarBtn,
              prop.navigation.state.index === 1 ? styles.activeTouchable : {},
              { marginHorizontal: 12.5 * widthRatio },
            ]}
            onPress={() => {
              prop.navigation.navigate('vehicledetail');
            }}>
            <Text
              style={[
                styles.tabBarText,
                prop.navigation.state.index === 1 ? styles.activeTabBarBtn : {},
              ]}>
              Vehicle Details
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[
              styles.tabBarBtn,
              styles.bankButton,
              prop.navigation.state.index === 2 ? styles.activeTouchable : {},
            ]}
            onPress={() => {
              prop.navigation.navigate('bankdetails');
            }}>
            <Text
              style={[
                styles.tabBarText,
                prop.navigation.state.index === 2 ? styles.activeTabBarBtn : {},
              ]}>
              Bank Details
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
function mapStateToProps(state) {
  return { user: state.currentUser };
}
ViewProfile.propTypes = {
  user: PropTypes.objectOf(PropTypes.any).isRequired,
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
};
export default connect(
  mapStateToProps,
  null,
)(ViewProfile);
