import { createAppContainer } from 'react-navigation';
import React from 'react';
import { createMaterialTopTabNavigator } from 'react-navigation-tabs';
import ViewProfile from './index';
import DL from './DrivingLicence';
import BankDetails from './BankDetails';
import VehicleDetail from './VehicleDetail';

const TabNavigator = createMaterialTopTabNavigator(
  {
    dl: {
      screen: DL,
      navigationOptions: () => ({
        tabBarLabel: 'Driving Licence',
      }),
    },
    vehicledetail: {
      screen: VehicleDetail,
      navigationOptions: () => ({
        tabBarLabel: 'Vehicle Detail',
      }),
    },
    bankdetails: {
      screen: BankDetails,
      navigationOptions: () => ({
        tabBarLabel: 'Bank Details',
      }),
    },
  },
  {
    tabBarComponent: props => <ViewProfile {...props} />,
  },
  {
    initialRouteName: 'dl',
    backBehaviour: 'initialRoute',
  }
);
export default createAppContainer(TabNavigator);
