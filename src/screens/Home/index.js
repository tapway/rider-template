/* eslint-disable no-undef */
import React, { PureComponent } from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  Switch,
  Platform,
  PermissionsAndroid,
} from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Dropdown } from 'react-native-material-dropdown';
import Toast from 'react-native-easy-toast';
import styles from './style';
import { socketRiderInit, updateLocation } from '../../socket/index';
import setAvailableDb from '../../apiService/available';
import { resetActionLogin } from '../../navigation/ResetNavigation';
import { setLocation, setAvailable } from '../../redux/action/home';
import getImage from '../../utils/getImage';
import LogOut from '../LogOut';
import Geolocation from '@react-native-community/geolocation';
import colors from '../../utils/colors';
import setTripStatus from '../../apiService/setTripStatus';
import { heightRatio, widthRatio } from '../../utils/stylesheet';
import logoutApiService from '../../apiService/logout';

class HomePage extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      logoutModal: false,
    };
    setTripStatus(false, props.user.jwtAccessToken);

    socketRiderInit();
    this.orderNotification = this.orderNotification.bind(this);
  }

  componentDidMount() {
    const that = this;
    if (Platform.OS === 'ios') {
      this.callLocation(that);
    } else {
      // eslint-disable-next-line no-inner-declarations
      async function requestLocationPermission() {
        try {
          const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
            {
              title: 'Location Access Required',
              message: 'This App needs to Access your location',
            },
          );
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            that.callLocation(that);
          }
        } catch (err) {
          throw err;
        }
      }
      requestLocationPermission();
    }
  }

  handleLogoutResult = res => {
    const { props, refs } = this;
    if (res.success) {
      props.signout();
      props.navigation.dispatch(resetActionLogin);
      props.navigation.navigate('login');
    } else {
      this.setState({ logoutModal: false }, () => {
        refs.toast.show('Error in logging out');
      });
    }
  };

  handleLogout = () => {
    const { props } = this;
    logoutApiService(props.user.jwtAccessToken, this.handleLogoutResult);
  };

  setLogoutModal = () => {
    this.setState(prevState => {
      return { logoutModal: !prevState.logoutModal };
    });
  };

  toggleSwitch1 = value => {
    const { props } = this;
    props.setAvailableDispatcher(value);
    setAvailableDb(value, props.user.jwtAccessToken);
  };

  navigation = nav => {
    const { props } = this;
    props.navigation.navigate(nav);
  };

  callLocation() {
    console.log('call location fired ')
    const { props } = this;
    this.watchId = Geolocation.watchPosition(
      position => {
        // console.log(position)
        updateLocation(props.user, [
          position.coords.longitude,
          position.coords.latitude,
        ]);
        props.setLocDispatcher([
          position.coords.longitude,
          position.coords.latitude,
        ]);
      },
      error => console.log(error.message),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
    );
  }

  orderNotification() {
    const { props } = this;
    // eslint-disable-next-line no-underscore-dangle
    if (props.order._id === '') {
      props.navigation.navigate('orderNotification');
    } else {
      props.navigation.navigate('readyForDelivery');
    }
  }

  render() {
    const { props, state } = this;
    const { user: userobj } = props;
    const imgobj = { uri: userobj.profileImage };
    return (
      <View style={styles.container}>
        <LogOut
          logoutModal={state.logoutModal}
          signOutUser={this.handleLogout}
          setLogoutModal={this.setLogoutModal}
        />
        <View style={styles.signOut}>
          <Switch
            value={userobj.isAvailable}
            onValueChange={this.toggleSwitch1}
          />
          <TouchableOpacity onPress={() => this.setLogoutModal()}>
            <Image
              resizeMode="contain"
              source={getImage.signOut}
              style={styles.signOutIcon}
            />
          </TouchableOpacity>
        </View>
        <View style={styles.profile}>
          <View style={styles.imgContainer}>
            <Image source={imgobj} style={styles.profilePic} />
          </View>
          <View style={styles.details}>
            <Text style={styles.name}>{userobj.fullName}</Text>
            <Text style={styles.email}>{userobj.email}</Text>
            <TouchableOpacity
              style={styles.viewProfile}
              onPress={() => this.navigation('viewProfileTabNav')}>
              <Text style={styles.viewProfileText}>View Profile</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.iconsContainer}>
          <View style={styles.iconsContainerA}>
            <TouchableOpacity
              style={styles.icon}
              onPress={this.orderNotification}>
              <Image
                resizeMode="contain"
                source={getImage.newOrder}
                style={styles.iconImage}
              />
              <Text style={styles.iconText}>New Order</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.icon}
              onPress={() => this.navigation('currentStatus')}>
              <Image
                resizeMode="contain"
                source={getImage.list}
                style={styles.imgCurrentStatus}
              />
              <Text style={styles.iconText}>Current Status</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.iconsContainerB}>
            <TouchableOpacity
              style={styles.icon}
              onPress={() => this.navigation('orderHistory')}>
              <Image
                resizeMode="contain"
                source={getImage.orderHistory}
                style={styles.imgOrderHistory}
              />
              <Text style={styles.iconText}>Order History</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.icon}
              onPress={() => this.navigation('earning')}>
              <Image
                resizeMode="contain"
                source={getImage.usd}
                style={styles.iconImage}
              />
              <Text style={styles.iconText}>Earning</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.emptyContainer}>
          <Image
            resizeMode="contain"
            source={getImage.language}
            style={{ height: 12 * heightRatio }}
          />
          <Dropdown
            data={[
              { value: 'English' },
              { value: 'Spanish' },
              { value: 'French' },
            ]}
            value="English"
            fontSize={12 * heightRatio}
            containerStyle={{
              width: 80 * widthRatio,
              padding: 0,
              borderBottomWidth: 0,
              marginTop: 10,
            }}
            dropdownOffset={{ top: 0, left: 0 }}
            dropdownMargins={{ min: 10 * widthRatio, max: 10 * widthRatio }}
            pickerStyle={{ bottom: 200 }}
          />
        </View>
        <Toast
          ref="toast"
          style={{ backgroundColor: colors.toastBgColor }}
          position="bottom"
          positionValue={250 * heightRatio}
          fadeInDuration={750}
          fadeOutDuration={1000}
          opacity={0.8}
          textStyle={{ color: colors.toastTextColor, fontSize: 15 }}
        />
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    user: state.currentUser,
    order: state.currentOrder,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    signout: () => {
      dispatch({ type: 'USER_SIGNOUT' });
      // dispatch({ type: 'CLEAR_EARNINGS' });
      // dispatch({ type: 'SET_EDIT_PROFILE' });
      // dispatch({ type: 'DUMP_ORDER' });
      // dispatch({ type: 'DUMP_STATUS' });
      // dispatch({ type: 'DUMP_REQUEST' });
      // dispatch({ type: 'CLEAR_ORDER_HISTORY' });
    },
    setLocDispatcher: coor => {
      dispatch(setLocation(coor));
    },
    setAvailableDispatcher: val => {
      dispatch(setAvailable(val));
    },
  };
};
HomePage.propTypes = {
  setAvailableDispatcher: PropTypes.func.isRequired,
  setLocDispatcher: PropTypes.func.isRequired,
  signout: PropTypes.func.isRequired,
  order: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.object,
    PropTypes.array,
  ]).isRequired,
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
  user: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(HomePage);
