import { StyleSheet, Dimensions } from 'react-native';
import { heightRatio } from '../../utils/stylesheet';
import colors from '../../utils/colors';

const { height, width } = Dimensions.get('window');
export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.themeBackgroundColor,
  },
  signOut: {
    alignItems: 'stretch',
    justifyContent: 'space-between',
    paddingLeft: width / 14,
    paddingRight: width / 10,
    paddingTop: 50,
    paddingBottom: 30,
    flexDirection: 'row',
    backgroundColor: colors.highlightedContainer,
  },
  signOutIcon: {
    height: height / 30,
    width: width / 15,
    overflow: 'visible',
  },
  profile: {
    height: height / 7,
    flexDirection: 'row',
    backgroundColor: colors.highlightedContainer,
  },
  imgContainer: {
    width: width / 3.5,
    padding: width / 14,
    paddingTop: width / 30,
  },
  profilePic: {
    height: height / 14,
    borderRadius: height / 28,
    width: height / 14,
  },
  details: {
    flex: 1,
  },
  name: {
    fontSize: 16 * heightRatio,
    fontWeight: '500',
    paddingBottom: 2 * heightRatio,
  },
  email: {
    fontSize: 12 * heightRatio,
    fontWeight: '100',
  },
  viewProfile: {
    backgroundColor: colors.white,
    borderRadius: 50,
    width: width / 3.5,
    height: height / 25,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 12 * heightRatio,
  },
  viewProfileText: {
    textAlign: 'center',
    color: colors.secondaryColor,
    fontSize: 12 * heightRatio,
  },
  iconsContainer: {
    // height: height / 2.3,
    flex: 1,
    flexDirection: 'row',
    paddingBottom: 100 * heightRatio,
  },
  iconsContainerA: {
    width: width / 2,
    flexDirection: 'column',
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  iconsContainerB: {
    width: width / 2,

    flexDirection: 'column',
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  icon: {
    alignItems: 'center',
    justifyContent: 'flex-end',
    height: height / 16,
    overflow: 'visible',
  },
  iconText: {
    marginTop: height / 60,
    fontSize: height / 50,
  },
  iconImage: {
    height: height / 15.5,
    width: height / 19,
    overflow: 'visible',
  },
  imgOrderHistory: {
    width: height / 17.5,
    height: height / 18.4,
    overflow: 'visible',
  },
  imgCurrentStatus: {
    width: height / 20,
    height: height / 16,
    overflow: 'visible',
  },
  emptyContainer: {
    height: 50 * heightRatio,
    borderTopWidth: 0.5,
    borderColor: colors.secondaryBorderColor,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
});
