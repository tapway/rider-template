/* eslint-disable react/prop-types */
import React, { PureComponent } from 'react';
import { Text, View, Image } from 'react-native';
import { connect } from 'react-redux';
import styles from './style';
import setEarnings from '../../redux/action/earnings';
import fetchEarnings from '../../apiService/earnings';
import getImage from '../../utils/getImage';
import Header from '../../components/Header';
import { Actions } from 'react-native-router-flux';

class Earning extends PureComponent {
  constructor(props) {
    super(props);
    this.home = this.home.bind(this);
    this.fetchEarnings();
  }

  fetchEarnings() {
    const { props } = this;
    fetchEarnings(props.user.jwtAccessToken, props.setEarningsDispatcher);
  }

  home() {
    const { props } = this;
    props.navigation.navigate('AuthLoading');
  }

  render() {
    const { earnings } = this.props;
    return (
      <View style={styles.container}>
        <Header onlyBack={false} back={this.home()} title="Earnings" />
        <View style={styles.secondConatiner}>
          <Image resizeMode="contain" source={getImage.wallet} style={styles.iconImage} />
          <View style={styles.headingView}>
            <Text style={styles.h1}>Current Balance</Text>
            <Text style={styles.h2}>₦ {earnings.walletBalance}</Text>
          </View>
        </View>
        <View style={styles.thirdContainer}>
          <Text style={styles.heading2}>Estimated earnings</Text>
          <View style={styles.list}>
            <View style={styles.row}>
              <View style={styles.singleLi}>
                <Text style={styles.light}>Today so far</Text>
                <Text style={styles.bold}>₦ {earnings.todayEarn}</Text>
              </View>
              <View style={styles.singleLi}>
                <Text style={styles.light}>Yesterday</Text>
                <Text style={styles.bold}>₦ {earnings.yesterdayEarn}</Text>
              </View>
            </View>
            <View style={styles.row}>
              <View style={styles.singleLi}>
                <Text style={styles.light}>Last 7 days</Text>
                <Text style={styles.bold}>₦ {earnings.weekEarn}</Text>
              </View>
              <View style={styles.singleLi}>
                <Text style={styles.light}>Last 28 days</Text>
                <Text style={styles.bold}>₦ {earnings.monthEarn}</Text>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
function mapStateToProps(state) {
  return {
    user: state.currentUser,
    earnings: state.earnings,
  };
}
const mapDispatchToProps = dispatch => {
  return {
    setEarningsDispatcher: result => {
      dispatch(
        setEarnings(
          result.todayEarn,
          result.yesterdayEarn,
          result.weekEarn,
          result.monthEarn,
          result.walletBalance
        )
      );
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Earning);
