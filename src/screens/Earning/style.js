import { StyleSheet, Dimensions } from 'react-native';
import { heightRatio } from '../../utils/stylesheet';
import colors from '../../utils/colors';
import { FontNames } from '../../../theme';
import { RFPercentage, RFValue } from 'react-native-responsive-fontsize';

const { height, width } = Dimensions.get('window');
export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.themeBackgroundColor,
  },
  secondConatiner: {
    flex: 3,
    flexDirection: 'row',
    backgroundColor: colors.primaryColor,

    alignItems: 'center',
  },
  headingView: {
    marginLeft: width / 14,
    marginTop: 7 * heightRatio,
  },
  heading2: {
    fontSize: 14 * heightRatio,
    fontWeight: '300',
  },
  h1: {
    color: colors.textForPrimaryBgColor,
    fontSize: 12 * heightRatio,
  },
  h2: {
    color: colors.textForPrimaryBgColor,
    fontWeight: '400',
    fontSize: 18 * heightRatio,
    marginTop: 4 * heightRatio,
  },
  iconImage: {
    height: height / 15,
    width: width / 7,
    overflow: 'visible',
    marginLeft: width / 10,
  },
  thirdContainer: {
    flex: 13,
    marginTop: height / 22,
    marginRight: width / 9,
    marginLeft: width / 9,
  },
  list: {
    marginTop: 20 * heightRatio,
  },
  singleLi: {
    width: width / 2.8,
    alignItems: 'center',
    padding: width / 30,
    justifyContent: 'center',
    borderColor: colors.secondaryBorderColor,
    borderWidth: 0.5,
    borderRadius: 10,
  },
  bold: {
    fontWeight: '600',
    fontSize: 14 * heightRatio,
    marginTop: 4 * heightRatio,
  },
  light: {
    fontWeight: '200',
    fontSize: 12 * heightRatio,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 20 * heightRatio,
  },
  headtxtdiv: { position: 'absolute', left: 0, right: 0, top: RFPercentage(5.5), zIndex: -100 },
  headtxt: {
    fontSize: 18,
    color: '#030E1A',
    width: '100%',
    textAlign: 'center',
    //paddingLeft:40,
    fontFamily: FontNames.semibold,
  },
});
