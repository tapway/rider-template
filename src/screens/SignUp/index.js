/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-picker';
import Snackbar from 'react-native-snackbar';
import SignUpOne from './SignUpOne';
import SignUpTwo from './SignUpTwo';
import SignUpThree from './SignUpThree';
import { updateUserProfilePicAsync } from '../../redux/action/imageUpload';
import { inputValidation } from '../../helper';
import registerUser from '../../apiService/registerUser';
import login from '../../apiService/login';
import loginSendEmail from '../../apiService/loginSendEmail'
import Loader from '../../components/Loader';

const options = {
  title: 'Select Avatar',
  quality: 0.1,
  mediaType: 'photo',
  noData: true,
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

export class SignUp extends Component {
  constructor(props) {
    super(props);

    this.state = {
      backButton: 1,
      phoneNumber: '+234',
      imageLoader: false,
      isLoading: false,
    };
  }

  handleBackButton = (value = 0) => {
    if (value > 0) {
      this.setState({ backButton: value });
    } else {
      this.setState({ backButton: 1 }, () => this.props.navigation.goBack());
    }
  };

  handleNextButton = value => {
    const {
      firstName,
      lastName,
      email,
      phoneNumber,
      gender,
      profileImage,
      dob,
    } = this.state;
    const payload1 = {
      firstName,
      lastName,
      email,
      phoneNumber,
      gender,
      profileImage,
      dob,
    };

    if (value === 2) {
      const checkInput = inputValidation(payload1);
      if (checkInput.result) {
        this.setState({ backButton: value }, () => {
          console.log('stage', value, this.state);
        });
      } else {
        Snackbar.show({
          title: `${checkInput.message}`,
          duration: Snackbar.LENGTH_SHORT,
          backgroundColor: 'red',
          color: 'white',
        });
      }
    } else {
      this.setState({ backButton: value }, () => {
        console.log('stage', value, this.state);
      });
    }
  };

  handleInput = (type, value) => this.setState({ [type]: value });

  uploadimage = (valueType, displayType) => {
    console.log('responseuploadimage');
    ImagePicker.showImagePicker(options, response => {
      if (!response.didCancel && !response.error && !response.customButton) {
        console.log('response', response);
        const source = { uri: response.uri };
        // const profileDataSource = {
        //   uri: `data:${response.type};base64,${response.data}`,
        // };
        this.setState({
          [valueType]: response.uri,
          [displayType]: source,
        });
        // updateUserProfilePicAsync({ localUrl: response.uri }, response.type);
      }
    });
  };

  handleGoToVerify = res => {
    console.log('signup handleGoToVerify res', res);
    const { phoneNumber } = this.state;
    this.setState({ imageLoader: false, isLoading: false }, () => {
      console.log('handleGoToVerify', phoneNumber);
    });
    if (res.success) {
      const { props } = this;
      props.navigation.navigate('verify', { phoneNumber });
    } else {
      console.log(res.message)
      const { refs } = this;
      if (res.errorCode) {
        refs.toast.show(res.message);
      } else {
        refs.toast.show(res.message);
      }
      this.handlerFocus('mobileFocus', 2);
    }
  };

  handleSuccess = res => {
    if (res.success) {
      Snackbar.show({
        title: 'Account created!',
        duration: Snackbar.LENGTH_SHORT,
        backgroundColor: 'green',
        color: 'white',
      });
      const mobile = `${res.data.user.phoneNumber}`;
      const email = `${res.data.user.email}`;
      loginSendEmail(mobile, email, this.handleGoToVerify);
    }
  };

  handleError = res => {
    if (res) {
      Snackbar.show({
        title: res.message || 'Failed!',
        duration: Snackbar.LENGTH_SHORT,
        backgroundColor: 'red',
        color: 'white',
      });
    }
  };

  submit = payload => {
    const payload1 = {
      address: 'yaba, lagos',
      bankDetails: {
        accountName: 'asdfg',
        bankName: 'gtbank',
        accountNumber: '01233637829',
      },
      deviceInfo: {
        userId: '7add8727-d0ee-421e-b2be-df174719d717',
        pushToken:
          'fldNqe_PbaY:APA91bHoAvZZxUuFsBLooiV9OCgXLyM84bcNpt…9d2Wv8rnDE2ZpYduhpxsffhdLG2jepTkcUKnYa8JEW3qXLH0o',
      },
      dob: 'Thu Jan 31 2002 00:00:00 GMT+0100 (West Africa Standard Time)',
      email: 'email@email.com',
      firstName: 'first',
      fullName: 'first last',
      gender: 'female',
      lastName: 'last',
      phoneNumber: '+2349025742933',
      profileImage: '',
      vehicleDetails: {},
      verificationDetails: {
        licenseNumber: '12345',
        nationalIdNumber: '54321',
      },
      verificationImage: '',
    };
    this.setState({ imageLoader: true }, () => {
      registerUser(payload, this.handleSuccess, this.handleError);
    });
  };

  handleSignUp = () => {
    const deviceInfo = this.props.device;
    const {
      firstName,
      lastName,
      address,
      phoneNumber,
      email,
      profileImage,
      verificationImage,
      gender,
      dob,
      licenseNumber,
      nationalIdNumber,
      accountName,
      bankName,
      accountNumber,
    } = this.state;
    const payload = {
      firstName,
      lastName,
      address,
      phoneNumber,
      email,
      profileImage,
      verificationImage,
      gender,
      dob,
      fullName: `${firstName} ${lastName}`,
      verificationDetails: { licenseNumber, nationalIdNumber },
      bankDetails: { accountName, bankName, accountNumber },
      vehicleDetails: {},
      deviceInfo,
    };
    const checkInput = inputValidation({ ...payload });
    if (checkInput.result) {
      console.log('payloadhhhhhhhhhhhhhhhh', payload);
      this.submit(payload);
    } else {
      Snackbar.show({
        title: `${checkInput.message}`,
        duration: Snackbar.LENGTH_SHORT,
        backgroundColor: 'red',
        color: 'white',
      });
    }
  };

  render() {
    const { backButton, imageLoader, isLoading } = this.state;
    return (
      <>
        <Loader loading={imageLoader} />
        {backButton === 1 && (
          <SignUpOne
            onBack={this.handleBackButton}
            handleInput={this.handleInput}
            inputValue={this.state}
            handleNextButton={this.handleNextButton}
            uploadimage={this.uploadimage}
          />
        )}
        {backButton === 2 && (
          <SignUpTwo
            onBack={this.handleBackButton}
            handleInput={this.handleInput}
            inputValue={this.state}
            handleNextButton={this.handleNextButton}
            uploadimage={this.uploadimage}
          />
        )}
        {backButton === 3 && (
          <SignUpThree
            onBack={this.handleBackButton}
            handleInput={this.handleInput}
            inputValue={this.state}
            handleSignUp={this.handleSignUp}
            isLoading={isLoading}
          />
        )}
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    device: state.deviceInfo,
  };
};

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);
