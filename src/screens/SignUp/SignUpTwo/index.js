/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { View, TouchableOpacity ,Image} from 'react-native';
import {
  Container,
  Content,
  Button,
  Item,
  Input,
  Text,
  Icon,
  Header,
  Left,
  Body,
  Right,
  Card,
  Thumbnail,
} from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import styles from '../styles';
import getImage from '../../../utils/getImage';
import { FontNames } from '../../../../theme';


const SignUpTwo = ({
  onBack,
  handleInput,
  inputValue,
  handleNextButton,
  uploadimage,
}) => {
  return (
    <Container style={styles.container}>
      <Header transparent androidStatusBarColor={'#0003'}>
        <Left style={{ paddingHorizontal: 5 }}>
          <Button transparent onPress={() => onBack(1)}>
            <Icon name="arrow-back" style={styles.closeIcon} />
          </Button>
        </Left>
        <Body />
        <Right />
      </Header>
      <Content
        padder
        style={{ flex: 1, alignContent: 'space-between', padding: 10 }}>
        <View style={{ flex: 1 }}>
          <Text style={[styles.welcomeHd, { fontSize: 25 }]}>2 of 3</Text>
          <Text style={styles.welcomeHd}>A little more info.</Text>
        </View>

        <Grid>
          <Row>
            <Col>
              <Text style={styles.textLabel}>Home Address</Text>
              <Item regular style={styles.textInput}>
                <Input
                  keyboardType="default"
                  value={inputValue.address}
                  onChangeText={text => handleInput('address', text)}
                />
              </Item>
            </Col>
          </Row>
          <Row>
            <Col>
              <Text style={styles.textLabel}>License Number</Text>
              <Item regular style={styles.textInput}>
                <Input
                  keyboardType="default"
                  value={inputValue.licenseNumber}
                  onChangeText={text => handleInput('licenseNumber', text)}
                />
              </Item>
            </Col>
          </Row>
          <Row>
            <Col>
              <Text style={styles.textLabel}>National ID Card Number</Text>
              <Item regular style={styles.textInput}>
                <Input
                  value={inputValue.nationalIdNumber}
                  onChangeText={text => handleInput('nationalIdNumber', text)}
                />
              </Item>
            </Col>
          </Row>
          <Row>
            <Col>
                <TouchableOpacity
                  style={{width: 120,
                  padding: 10,
                  alignSelf: 'center',
                  marginTop: 20,}}
                  onPress={() =>
                    uploadimage('verificationImage', 'verificationAvatar')
                  }>
                  {(!inputValue.verificationAvatar && (
                    <View style={{ alignItems: 'center' }}>
                    <Image source={getImage.uploadvehicle} resizeMode='contain' style={{width:42,height:42}}/>
                      <Text style={{ textAlign: 'center', fontSize: 14,fontFamily:FontNames.regular }}>
                        Upload Vehicle Particulars
                      </Text>
                    </View>
                  )) || (
                    <Thumbnail
                      square
                      large
                      source={inputValue.verificationAvatar}
                    />
                  )}
                </TouchableOpacity>
            </Col>
          </Row>
          <Row>
            <Col>
              <Button
                block
                primary
                style={styles.submitBtn}
                onPress={() => handleNextButton(3)}>
                <Text style={styles.btnText}>NEXT</Text>
              </Button>
            </Col>
          </Row>
        </Grid>

        {/* <View style={{ flex: 1, bottom: 0 }}>
          <Text
            style={{
              color: '#000000',
              textAlign: 'center',
              marginTop: 30,
              marginBottom: 10,
            }}>
            <Text style={[styles.regularFont]}>
              By signing up, you agree with the{' '}
            </Text>
            <Text style={[styles.greenColor, styles.regularFont]}>
              Terms of Service{' '}
            </Text>
            <Text style={[styles.regularFont]}>and </Text>
            <Text style={[styles.greenColor, styles.regularFont]}>
              Privacy Policy{' '}
            </Text>
          </Text>
        </View> */}
      </Content>
    </Container>
  );
};

export default SignUpTwo;
