import { StyleSheet } from 'react-native';
import { FontNames } from '../../../theme';
import {RFValue} from "react-native-responsive-fontsize"
const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: '#FFFFFF' },
  closeIcon: { fontSize: 25, color: '#030E1A' },
  greenColor: {
    color: '#3FAF5D',
  },
  regularFont: {
    fontFamily: FontNames.regular,
  },
  // container: {
  //   paddingTop: 20,
  //   paddingBottom: 20,
  //   paddingLeft: 10,
  //   paddingRight: 10,
  //   backgroundColor: '#FFFFFF',
  // },
  welcomeHd: {
    color: '#1B2E5A',
    fontSize: 30,
    fontFamily: FontNames.bold,
    width: '70%',
  },
  textLabel: {
    marginTop: 17,
    fontSize: RFValue(16),
    color: '#858F99',
    paddingLeft: RFValue(4),
    paddingBottom: RFValue(6),
    fontFamily: FontNames.regular,
  },
  textInput: {
    backgroundColor: '#EFF4F5',
    borderColor: 'rgba(27, 46, 90, 0.5)',
    color: '#FFFFFF',
    borderWidth: 0.5,
    borderRadius: 5,
    fontSize: 22,
    marginTop: 3,
    paddingLeft: 10,
    height: 45,
    width:'100%',
    fontFamily: FontNames.regular,
  },
  submitBtn: {
    backgroundColor: '#3FAF5D',
    borderRadius: 4,
    elevation:0,
    marginTop: 40,
    marginBottom: 30,
  },
  btnText: {
    color: '#FFFFFF',
    fontSize: 18,
    fontFamily: FontNames.regular,
  },
});

export default styles;
