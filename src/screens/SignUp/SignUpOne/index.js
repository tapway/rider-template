/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { View, TouchableOpacity } from 'react-native';
import {
  Container,
  Content,
  Button,
  Item,
  Input,
  Text,
  FooterTab,
  Icon,
  Header,
  Left,
  Body,
  Right,
  Picker,
  DatePicker,
  Thumbnail,
} from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import styles from '../styles';
import getImage from '../../../utils/getImage';
import { Actions } from 'react-native-router-flux';

gotoTermsOfService = () => {
  //const { props } = this;
  //props.navigation.navigate('termsOfService');
};

gotoPrivacyPolicy = () => {
  //const { props } = this;
  //props.navigation.navigate('privacyPolicy');
};


const SignUpOne = ({
  onBack,
  handleInput,
  inputValue,
  handleNextButton,
  uploadimage,
}) => {
  return (
    <Container style={styles.container}>
      <Header transparent androidStatusBarColor={'#0003'}>
        <Left style={{ paddingHorizontal: 5 }}>
          <Button transparent onPress={onBack}>
            <Icon name="arrow-back" style={styles.closeIcon} />
          </Button>
        </Left>
        <Body />
        <Right />
      </Header>
      <Content
        padder
        style={{ flex: 1, alignContent: 'space-between', padding: 10 }}>
        <View style={{ flex: 1 }}>
          <Text style={[styles.welcomeHd, { fontSize: 25 }]}>1 of 3</Text>
          <Text style={styles.welcomeHd}>Sign up as a rider</Text>
        </View>

        <Grid>
          <Row>
            <Col
              style={{
                alignContent: 'center',
                alignItems: 'center',
                margin: 20,
              }}>
              <TouchableOpacity
                onPress={() => uploadimage('profileImage', 'avatarImage')}>
                <View>
                  <Thumbnail
                    resizeMode='contain'
                    circular
                    large
                    source={
                      inputValue.avatarImage
                        ? inputValue.avatarImage
                        : getImage.uploadicon
                    }
                  />
                  {/* <View
                    style={{
                      position: 'absolute',
                      right: -1,
                      bottom: -1,
                      borderRadius: 30,
                      borderColor: '#FFFFFF',
                      borderWidth: 1,
                      backgroundColor: '#1B2E5A',
                    }}>
                    <Icon
                      type={'AntDesign'}
                      name={'edit'}
                      style={{
                        color: '#FFFFFF',
                        fontSize: 16,
                        padding: 5,
                      }}
                    />
                  </View> */}
                </View>
              </TouchableOpacity>
            </Col>
          </Row>
          <Row style={{ justifyContent: 'space-between' }}>
            <Col style={{ width: '48%' }}>
              <Text style={styles.textLabel}>First Name</Text>
              <Item regular style={styles.textInput}>
                <Input
                  keyboardType="default"
                  value={inputValue.firstName}
                  onChangeText={text => handleInput('firstName', text)}
                />
              </Item>
            </Col>
            <Col style={{ width: '48%' }}>
              <Text style={styles.textLabel}>Last Name</Text>
              <Item regular style={styles.textInput}>
                <Input
                  value={inputValue.lastName}
                  onChangeText={text => handleInput('lastName', text)}
                />
              </Item>
            </Col>
          </Row>
          <Row>
            <Col>
              <Text style={styles.textLabel}>Email ID</Text>
              <Item regular style={styles.textInput}>
                <Input
                  keyboardType="email-address"
                  value={inputValue.email}
                  onChangeText={text => handleInput('email', text)}
                />
              </Item>
            </Col>
          </Row>
          <Row>
            <Col>
              <Text style={styles.textLabel}>Phone Number</Text>
              <Item regular style={styles.textInput}>
                <Input
                  value={inputValue.phoneNumber}
                  keyboardType="number-pad"
                  onChangeText={text => handleInput('phoneNumber', text)}
                />
              </Item>
            </Col>
          </Row>
          <Row>
            <Col>
              <Text style={styles.textLabel}>Gender</Text>
              <Item regular style={styles.textInput}>
                <Picker
                  note
                  mode="dropdown"
                  style={{ width: 120, color: '#000' }}
                  selectedValue={inputValue.gender}
                  onValueChange={value => handleInput('gender', value)}>
                  <Picker.Item label="Select gender" value="" />
                  <Picker.Item label="Male" value="male" />
                  <Picker.Item label="Female" value="female" />
                </Picker>
              </Item>
            </Col>
          </Row>

          <Row>
            <Col>
              <Text style={styles.textLabel}>Type of Vehicle</Text>
              <Item regular style={styles.textInput}>
                <Picker
                  note
                  mode="dropdown"
                  style={{ width: 120, color: '#000' }}
                  selectedValue={inputValue.vehicle}
                  onValueChange={value => handleInput('vehicle', value)}>
                  <Picker.Item label="Select vehicle" value="" />
                  <Picker.Item label="Bike" value="bike" />
                  <Picker.Item label="Car" value="car" />
                </Picker>
              </Item>
            </Col>
          </Row>

          <Row>
            <Col>
              <Text style={styles.textLabel}>Date Of Birth</Text>
              <Item regular style={styles.textInput}>
                <DatePicker
                  defaultDate={new Date(2018, 4, 4)}
                  minimumDate={new Date(1960, 1, 1)}
                  maximumDate={new Date(2001, 12, 31)}
                  locale={'en'}
                  modalTransparent={false}
                  animationType={'fade'}
                  androidMode='spinner'
                  placeHolderText="Select date"
                  textStyle={{ color: '#000' }}
                  placeHolderTextStyle={{ color: '#000' }}
                  onDateChange={value => handleInput('dob', value)}
                  disabled={false}
                />
              </Item>
            </Col>
          </Row>
          <Row>
            <Col>
              <Button
                block
                primary
                style={styles.submitBtn}
                onPress={() => handleNextButton(2)}>
                <Text style={styles.btnText}>NEXT</Text>
              </Button>
            </Col>
          </Row>
        </Grid>

        <View style={{ flex: 1, bottom: 0 }}>
          <Text
            style={{
              color: '#000000',
              textAlign: 'center',
              marginTop: 10,
              marginBottom: 20,
            }}>
            <Text style={[styles.regularFont]}>
              By signing up, you agree with the{' '}
            </Text>
            <Text style={[styles.greenColor, styles.regularFont]} onPress={gotoTermsOfService()}>
              Terms of Service{' '}
            </Text>
            <Text style={[styles.regularFont]}>and </Text>
            <Text style={[styles.greenColor, styles.regularFont]} onPress={gotoPrivacyPolicy()}>
              Privacy Policy{' '}
            </Text>
          </Text>
        </View>
      </Content>
    </Container>
  );
};

export default SignUpOne;
