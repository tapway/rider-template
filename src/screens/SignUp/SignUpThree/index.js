/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { View } from 'react-native';
import {
  Container,
  Content,
  Button,
  Item,
  Input,
  Text,
  Icon,
  Header,
  Left,
  Body,
  Right,
} from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import styles from '../styles';

const SignUpThree = ({ onBack, handleInput, inputValue, handleSignUp }) => {
  return (
    <Container style={styles.container}>
      <Header transparent androidStatusBarColor={'#0003'}>
        <Left style={{ paddingHorizontal: 5 }}>
          <Button transparent onPress={() => onBack(2)}>
            <Icon name="arrow-back" style={styles.closeIcon} />
          </Button>
        </Left>
        <Body />
        <Right />
      </Header>
      <Content
        padder
        style={{ flex: 1, alignContent: 'space-between', padding: 10 }}>
        <View style={{ flex: 1 }}>
          <Text style={[styles.welcomeHd, { fontSize: 25 }]}>3 of 3</Text>
          <Text style={styles.welcomeHd}>Provide your bank details.</Text>
        </View>

        <Grid>
          <Row>
            <Col>
              <Text style={styles.textLabel}>Account Name</Text>
              <Item regular style={styles.textInput}>
                <Input
                  keyboardType="default"
                  value={inputValue.accountName}
                  onChangeText={text => handleInput('accountName', text)}
                />
              </Item>
            </Col>
          </Row>
          <Row>
            <Col>
              <Text style={styles.textLabel}>Bank Name</Text>
              <Item regular style={styles.textInput}>
                <Input
                  keyboardType="default"
                  value={inputValue.bankName}
                  onChangeText={text => handleInput('bankName', text)}
                />
              </Item>
            </Col>
          </Row>
          <Row>
            <Col>
              <Text style={styles.textLabel}>Account Number</Text>
              <Item regular style={styles.textInput}>
                <Input
                  value={inputValue.accountNumber}
                  onChangeText={text => handleInput('accountNumber', text)}
                />
              </Item>
            </Col>
          </Row>
          <Row>
            <Col>
              <Button
                block
                primary
                style={styles.submitBtn}
                onPress={handleSignUp}>
                <Text style={styles.btnText}>SUBMIT</Text>
              </Button>
            </Col>
          </Row>
        </Grid>

        {/* <View style={{ flex: 1, bottom: 0 }}>
          <Text
            style={{
              color: '#000000',
              textAlign: 'center',
              marginTop: 30,
              marginBottom: 10,
            }}>
            <Text style={[styles.regularFont]}>
              By signing up, you agree with the{' '}
            </Text>
            <Text style={[styles.greenColor, styles.regularFont]}>
              Terms of Service{' '}
            </Text>
            <Text style={[styles.regularFont]}>and </Text>
            <Text style={[styles.greenColor, styles.regularFont]}>
              Privacy Policy{' '}
            </Text>
          </Text>
        </View> */}
      </Content>
    </Container>
  );
};

export default SignUpThree;
