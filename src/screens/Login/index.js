import React, { PureComponent } from 'react';
import { Text, View, TouchableOpacity,Image } from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import PhoneInput from 'react-native-phone-input';
import Toast from 'react-native-easy-toast';
import {
  Spinner,
  Container,
  Left,
  Body,
  Content,
  Button,
  Item,
  Header,
} from 'native-base';
import signInStyles from './style';
import getImage from '../../utils/getImage';
import { validation, validateNumber } from './validation';
import Colors from '../../utils/colors';
import { RFValue, RFPercentage } from 'react-native-responsive-fontsize';
import { FontNames } from '../../../theme';
import styles from '../components/HomePage/styles';

class Login extends PureComponent {
  constructor() {
    super();
    this.state = {
      mobileFocus: 0,
      mobile: '+234',
      imageLoader: false,
      phoneNumberModal: false,
      isLoading: false,
    };
    this.phone = null;
    this.register = this.register.bind(this);
  }

  handleLogin = res => {
    const { mobile } = this.state;
    this.setState({ imageLoader: false, isLoading: false }, () => {
      this.setState({ phoneNumberModal: false });
    });
    if (res.success) {
      const { props } = this;
      props.navigation.navigate('verify', { phoneNumber: mobile });
    } else {
      console.log(res)
      const { refs } = this;
      if (res.errorCode) {
        refs.toast.show(res.message);
      } else {
        refs.toast.show(res.message);
      }
      this.handlerFocus('mobileFocus', 2);
    }
  };

  handlerFocus = (input, value) => {
    this.setState({
      [input]: value,
    });
  };

  handlerInput = (input, value) => {
    this.setState({
      [input]: value,
    });
  };

  register() {
    const { props } = this;
    props.navigation.navigate('signUp');
  }

  render() {
    const { mobile, isLoading } = this.state;
    return (
      <Container style={signInStyles.container}>
        <Header
          transparent
          // hasTabs
          iosBarStyle="light-content"
          style={{ height: 5 }}
          androidStatusBarColor={'#1891D0'}>
          <Left />
          <Body />
        </Header>
        <Content
          scrollEnabled
          keyboardShouldPersistTaps="never"
          contentContainerStyle={{
            padding: 16,
          }}>
          <View style={{ marginVertical: 5 }}>
            <Text style={signInStyles.welcomeHd}>Welcome Back</Text>
          </View>

          <View style={{ justifyContent: 'center', marginVertical: 20 }}>
            <View style={signInStyles.inputContainer}>
              <Text style={signInStyles.inputLabel}>
                Enter Mobile Number to Proceed
              </Text>
              <Item regular style={signInStyles.inputItem}>
                <PhoneInput
                  ref={ref => {
                    this.phone = ref;
                  }}
                  value={mobile}
                  onChangePhoneNumber={text => {
                    this.setState(
                      {
                        mobile: text,
                      },
                      () => {
                        validateNumber(this);
                      },
                    );
                  }}
                  textStyle={{ fontSize: RFValue(16), color: '#fff', fontFamily: FontNames.regular }}
                  textProps={{
                    onSubmitEditing: () => validation(this),
                    placeholder: 'Enter Phone Number',
                    autoFocus: true,
                  }}
                  style={signInStyles.phoneInput}
                />
              </Item>
            </View>

            <Button
              block
              primary
              style={signInStyles.submitBtn}
              onPress={() => validation(this)}>
              {(!isLoading && (
                <Text style={signInStyles.btnText}>LOGIN</Text>
              )) || <Spinner />}
            </Button>

            <View>
              <Text
                style={{
                  color: '#FFFFFF',
                  textAlign: 'center',
                  fontFamily: FontNames.regular,
                }}>
                Don&apos;t have an account?
              </Text>
              <TouchableOpacity onPress={this.register}>
                <Text
                  style={{
                    color: '#FFFFFF',
                    textAlign: 'center',
                    paddingTop: 3,
                    fontFamily: FontNames.bold,
                  }}>
                  Signup Now
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Content>
        <Image
          source={getImage.loginBG}
          resizeMode='cover'
          style={styles.bgimg}
        />
        <Toast
          ref="toast"
          style={{ backgroundColor: 'red' }}
          position="bottom"
          positionValue={100}
          fadeInDuration={750}
          fadeOutDuration={1000}
          opacity={0.8}
          textStyle={{ color: '#ffffff', fontSize: 15 }}
        />
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.currentUser,
  };
};

Login.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default connect(
  mapStateToProps,
  null,
)(Login);
