import {StyleSheet} from 'react-native';
import { FontNames } from '../../../theme';
import { RFValue } from 'react-native-responsive-fontsize';

const signInStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#1891D0',
  },
  white: {
    color: '#FFFFFF',
  },
  welcomeHd: {
    //fontWeight: '600',
    color: '#FFFFFF',
    fontSize: RFValue(30),
    fontFamily: FontNames.bold,
    width: '70%',
  },
  inputContainer: {
    marginVertical: 5,
  },
  inputLabel: {
    fontSize: RFValue(18),
    fontFamily: FontNames.regular,
    color: '#FFFFFF',
    opacity: 0.7,
  },
  inputItem: {
    borderColor: '#FFFFFF',
    color: '#FFFFFF',
    borderWidth: 1,
    fontSize: 22,
    marginTop: 3,
    paddingHorizontal: 10,
    height: 47,
    marginTop: 20,
    fontFamily: 'Nunito-Regular',
  },
  inputText: {
    color: '#FFFFFF',
    opacity: 0.9,
  },
  submitBtn: {
    backgroundColor: '#FFFFFF',
    borderRadius: 4,
    marginTop: 40,
    marginBottom: 30,
  },
  btnText: {
    color: '#030E1A',
    fontSize: RFValue(18),
    fontFamily: FontNames.semibold,
    //letterSpacing: 0.5,
  },
  signUp: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  footerMenu: {
    // bottom: 20,
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  actionContainer: {
    flex: 1,
    marginTop: 30,
  },
  actionTitle: {
    color: '#FFFFFF',
    textAlign: 'center',
    fontSize: 16,
    fontWeight: '200',
    fontFamily: 'Nunito-Regular',
  },
  actionColView: {
    alignSelf: 'center',
    backgroundColor: '#FFF5',
    padding: 20,
    borderRadius: 50,
  },
  actionImageSize: {width: 25, height: 25},
  actionText: {
    color: '#FFFFFF',
    fontSize: 12,
    fontFamily: 'Nunito-Regular',
    textAlign: 'center',
  },
});

export default signInStyles;
