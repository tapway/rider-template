import login from '../../apiService/login';

const validation = incomingThis => {
  const { mobile } = incomingThis.state;
  const { refs } = incomingThis;
  if (mobile !== '') {
    if (incomingThis.phone.isValidNumber()) {
      incomingThis.setState({ imageLoader: true, isLoading: true });
      login(mobile, incomingThis.handleLogin);
    } else {
      refs.toast.show('Invalid Number');
      incomingThis.handlerFocus('mobileFocus', 2);
    }
  } else {
    refs.toast.show('Required Fields Not Filled');
    incomingThis.handlerFocus('mobileFocus', 2);
  }
};

const validateNumber = incomingThis => {
  if (!incomingThis.phone.isValidNumber()) {
    incomingThis.handlerFocus('mobileFocus', 2);
  } else {
    incomingThis.handlerFocus('mobileFocus', 1);
  }
};

export { validation, validateNumber };
