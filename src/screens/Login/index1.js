import React, {PureComponent} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  TouchableWithoutFeedback,
  Keyboard,
  Modal,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import PhoneInput from 'react-native-phone-input';
import Toast from 'react-native-easy-toast';
import styles from './style';
import Loader from '../../components/Loader';
import {validation, validateNumber} from './validation';
import {heightRatio} from '../../utils/stylesheet';
import getImage from '../../utils/getImage';
import Header from '../../components/Header';

class Login extends PureComponent {
  constructor() {
    super();
    this.state = {
      mobileFocus: 0,
      mobile: '+91',
      imageLoader: false,
      phoneNumberModal: false,
    };
    this.phone = null;
    this.register = this.register.bind(this);
  }

  handleLogin = res => {
    const {mobile} = this.state;
    this.setState({imageLoader: false}, () => {
      this.setState({phoneNumberModal: false});
    });
    if (res.success) {
      const {props} = this;
      props.navigation.navigate('verify', {phoneNumber: mobile});
    } else {
      const {refs} = this;
      if (res.errorCode) {
        refs.toast.show('User already logged in');
      } else {
        refs.toast.show('User Not Found');
      }
      this.handlerFocus('mobileFocus', 2);
    }
  };

  handlerFocus = (input, value) => {
    this.setState({
      [input]: value,
    });
  };

  handlerInput = (input, value) => {
    this.setState({
      [input]: value,
    });
  };

  register() {
    const {props} = this;
    props.navigation.navigate('registerTabNavigator');
  }

  render() {
    const {state} = this;
    const {mobile, imageLoader, mobileFocus} = state;
    return (
      <React.Fragment>
        <Modal visible={state.phoneNumberModal} animated animationType="slide">
          <Loader loading={imageLoader} />
          <Header
            onlyBack
            back={() => this.setState({phoneNumberModal: false})}
          />

          <TouchableWithoutFeedback
            onPress={() => {
              Keyboard.dismiss();
            }}
            accessible={false}>
            <KeyboardAvoidingView
              style={styles.mainModalView}
              behavior="padding"
              enabled={Platform.OS === 'ios'}>
              <View style={styles.mainModalView}>
                <View style={{flex: 1}}>
                  <Text style={styles.definingText}>Please enter your</Text>
                  <Text
                    style={[styles.definingText, {marginTop: 4 * heightRatio}]}>
                    mobile number to proceed
                  </Text>
                  <PhoneInput
                    ref={ref => {
                      this.phone = ref;
                    }}
                    value={mobile}
                    onChangePhoneNumber={text => {
                      this.setState(
                        {
                          mobile: text,
                        },
                        () => {
                          validateNumber(this);
                        },
                      );
                    }}
                    style={[
                      styles.textInputBox,
                      mobileFocus === 0
                        ? styles.nofocus
                        : mobileFocus === 1
                        ? styles.focus
                        : styles.redFocus,
                      {marginTop: 25 * heightRatio},
                    ]}
                    textStyle={{fontSize: 18}}
                    textProps={{placeholder: 'Enter Phone Number'}}
                  />
                </View>
                <View style={styles.nextBtn}>
                  <TouchableOpacity
                    style={styles.button}
                    onPress={() => validation(this)}>
                    <Text style={styles.loginText}>Next</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </KeyboardAvoidingView>
          </TouchableWithoutFeedback>
        </Modal>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
          <KeyboardAwareScrollView
            style={styles.container}
            scrollEnabled={false}>
            <View style={styles.headingContainer}>
              <Text style={styles.headingText1}>Welcome</Text>
              <Text style={styles.headingText2}>PartnerZ</Text>
            </View>
            <Image
              source={getImage.signUpImage}
              resizeMode="contain"
              style={styles.image}
            />
            <View style={styles.formContainer}>
              <Text style={styles.definingText}>Please enter your</Text>
              <Text style={[styles.definingText, {marginTop: 2 * heightRatio}]}>
                mobile number to proceed
              </Text>
              <TouchableOpacity
                onPress={() => {
                  this.setState({phoneNumberModal: true});
                }}
                style={[styles.textInputBox, styles.nofocus]}>
                <Image
                  source={getImage.indFlag}
                  resizeMode="contain"
                  style={styles.flagImg}
                />
                <Text style={{fontSize: 20}}>+91</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.registerConatiner}>
              <View style={styles.reg}>
                <Text style={styles.notFound}>Not a member? </Text>
                <TouchableOpacity onPress={this.register}>
                  <Text style={[styles.notFound, styles.bold]}>Register</Text>
                </TouchableOpacity>
              </View>
            </View>
            <Toast
              ref="toast"
              style={{backgroundColor: 'black'}}
              position="bottom"
              positionValue={250 * heightRatio}
              fadeInDuration={750}
              fadeOutDuration={1000}
              opacity={0.8}
              textStyle={{color: 'orange', fontSize: 15}}
            />
          </KeyboardAwareScrollView>
        </TouchableWithoutFeedback>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.currentUser,
  };
};

Login.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default connect(
  mapStateToProps,
  null,
)(Login);
