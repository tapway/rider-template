import React, { PureComponent } from 'react';
import { View } from 'react-native';
import { Placeholder, PlaceholderMedia, PlaceholderLine, Shine } from 'rn-placeholder';
import styles from './style';

export default class Loading extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={styles.mainPlaceholderContainer}>
        <Placeholder Animation={Shine} Left={PlaceholderMedia} style={styles.singlePlaceholder}>
          <PlaceholderLine />
          <PlaceholderLine />
          <PlaceholderLine />
        </Placeholder>
        <Placeholder Animation={Shine} Left={PlaceholderMedia} style={styles.singlePlaceholder}>
          <PlaceholderLine />
          <PlaceholderLine />
          <PlaceholderLine />
        </Placeholder>
        <Placeholder Animation={Shine} Left={PlaceholderMedia} style={styles.singlePlaceholder}>
          <PlaceholderLine />
          <PlaceholderLine />
          <PlaceholderLine />
        </Placeholder>
        <Placeholder Animation={Shine} Left={PlaceholderMedia} style={styles.singlePlaceholder}>
          <PlaceholderLine />
          <PlaceholderLine />
          <PlaceholderLine />
        </Placeholder>
        <Placeholder Animation={Shine} Left={PlaceholderMedia} style={styles.singlePlaceholder}>
          <PlaceholderLine />
          <PlaceholderLine />
          <PlaceholderLine />
        </Placeholder>
        <Placeholder Animation={Shine} Left={PlaceholderMedia} style={styles.singlePlaceholder}>
          <PlaceholderLine />
          <PlaceholderLine />
          <PlaceholderLine />
        </Placeholder>
        <Placeholder Animation={Shine} Left={PlaceholderMedia} style={styles.singlePlaceholder}>
          <PlaceholderLine />
          <PlaceholderLine />
          <PlaceholderLine />
        </Placeholder>
      </View>
    );
  }
}
