import { StyleSheet, Dimensions } from 'react-native';
import colors from '../../utils/colors';
import { heightRatio, widthRatio } from '../../utils/stylesheet';

const { height, width } = Dimensions.get('window');
export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: colors.themeBackgroundColor,
  },
  nameBar: {
    flexDirection: 'row',
    textAlign: 'center',
  },
  loginButton: {
    width: width / 3,
    alignItems: 'center',
    padding: width / 30,
    borderRadius: 50,
    justifyContent: 'center',
    borderColor: colors.transparent,
  },
  mainPlaceholderContainer: {
    padding: width / 30,
  },
  singlePlaceholder: {
    flexDirection: 'row',
    borderWidth: 0.5,
    borderColor: colors.primaryBorderColor,
    paddingTop: height / 40,
    paddingBottom: height / 40,
    justifyContent: 'center',
    alignItems: 'center',
    paddingRight: width / 32,
    paddingLeft: width / 32,
    marginBottom: width / 30,
  },
  nameBarText: {
    flex: 1,
    fontSize: height / 40,
    fontWeight: '300',
    marginLeft: width / 4,
  },
  acceptButton: {
    backgroundColor: colors.primaryColor,
  },
  loginText: {
    color: colors.textForPrimaryBtnBgColor,
    fontSize: width / 18,
  },
  detailsBlock: {
    flexDirection: 'column',
    flex: 1,
    marginBottom: width / 20,
  },
  singleDetail: {
    flex: 1,
    flexDirection: 'row',
    margin: width / 30,
    marginBottom: 0,
    borderWidth: 0.5,
    borderColor: colors.primaryBorderColor,
    paddingTop: height / 40,
    paddingBottom: height / 40,
    paddingRight: width / 32,
  },
  imgContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    width: width / 5,
    marginRight: width / 100,
  },
  dishImg: {
    height: height / 16,
    width: width / 7.4,
    overflow: 'visible',
  },
  ideaImg: {
    height: height / 15,
    width: width / 9,
    overflow: 'visible',
  },
  carImg: {
    height: height / 23,
    width: width / 8,
    overflow: 'visible',
  },
  details: {
    flex: 1,
  },
  nopad: {
    paddingTop: 0,
  },
  noOrder: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  noOrderText: {
    fontSize: width / 8,
    marginBottom: height / 20,
  },
  location: {
    flexDirection: 'row',
  },
  ptrImg: {
    height: height / 18,
    width: width / 22,
    marginRight: width / 50,
  },
  locationTxt: {
    flex: 1,
  },
  address: {
    fontSize: height / 65,
    marginBottom: 14 * heightRatio,
  },

  value: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
  },
  price: {
    color: colors.secondaryShadowColor,
    fontSize: 12 * heightRatio,
    marginLeft: width / 40,
    fontWeight: '600',
  },
  bar: {
    color: colors.primaryBorderColor,
    marginLeft: 5 * widthRatio,
    fontSize: height / 40,
    fontWeight: '300',
  },
  date: {
    marginLeft: 5 * widthRatio,
    fontSize: 10 * heightRatio,
    width: width / 2.5,
    fontWeight: '500',
  },
});
