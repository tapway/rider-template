import React, { PureComponent } from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import { uniqBy } from 'lodash';
import PropTypes from 'prop-types';
import styles from './style';
import Details from './details';
import setOrderHistory from '../../redux/action/orderHistory';
import Header from '../../components/Header';
import fetchOrderHistoryDb from '../../apiService/orderHistory';
import Loading from './Loading';

class OrderHistory extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      orderHistory: [],
      loaded: true,
    };
  }

  async componentDidMount() {
    this.loadMoreHistory();
  }

  loadMoreHistory = async () => {
    const { props, state } = this;
    await fetchOrderHistoryDb(
      props.user.jwtAccessToken,
      props.user.id,
      10,
      state.orderHistory.length,
      this.setOrderHistory,
      this.loader
    );
  };

  setOrderHistory = payload => {
    this.setState(prevState => {
      const order = prevState.orderHistory;
      const arr = uniqBy([...order, ...payload], '_id');
      console.log(arr)
      return { orderHistory: arr };
    });
  };

  loader = () => {
    this.setState({
      loaded: false,
    });
  };

  home = () => {
    const { props } = this;
    props.navigation.navigate('home');
  };

  render() {
    const { state } = this;
    return (
      <View style={styles.container}>
        <View style={styles.container}>
          <Header onlyBack={false} back={this.home} title="Order History" />
          {state.loaded ? (
            <Loading />
          ) : (
            <Details history={state.orderHistory} loadMore={this.loadMoreHistory} />
          )}
        </View>
      </View>
    );
  }
}
function mapStateToProps(state) {
  return {
    user: state.currentUser,
    orders: state.orderHistory,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    setOrderHistoryDispatcher: data => {
      dispatch(setOrderHistory(data));
    },
  };
}

OrderHistory.propTypes = {
  user: PropTypes.objectOf(PropTypes.any).isRequired,
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OrderHistory);
