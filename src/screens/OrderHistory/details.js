import React, { PureComponent } from 'react';
import { View, Text, Image, FlatList } from 'react-native';
import moment from 'moment';
import PropTypes from 'prop-types';
import styles from './style';
import getImage from '../../utils/getImage';
import Oops from '../../components/Oops';

export default class Details extends PureComponent {
  static checkImg(element) {
    switch (element.catagory.name.toLowerCase()) {
      case 'food':
        return <Image resizeMode="contain" source={getImage.dish} style={styles.dishImg} />;
      case 'pets':
        return <Image resizeMode="contain" source={getImage.pet} style={styles.dishImg} />;
      case 'groceries':
        return <Image resizeMode="contain" source={getImage.bag} style={styles.dishImg} />;
      case 'medical':
        return <Image resizeMode="contain" source={getImage.heart} style={styles.dishImg} />;
      case 'sports':
        return <Image resizeMode="contain" source={getImage.football} style={styles.dishImg} />;
      case 'electronics':
        return <Image resizeMode="contain" source={getImage.electronic} style={styles.ideaImg} />;
      case 'car':
        return <Image resizeMode="contain" source={getImage.car} style={styles.carImg} />;
      default:
        return <Image resizeMode="contain" source={getImage.car} style={styles.carImg} />;
    }
  }

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { props } = this;
    const { history } = props;
    return (
      <FlatList
        data={history}
        keyExtractor={item => item._id}
        ListEmptyComponent={Oops}
        onEndReached={() => {
          props.loadMore();
        }}
        onEndReachedThreshold={0}
        // ListFooterComponent={<ActivityIndicator size="small"></ActivityIndicator>}
        renderItem={({ item }) => (
          <View style={styles.singleDetail}>
            <View style={styles.imgContainer}>{Details.checkImg(item)}</View>
            <View style={styles.details}>
              <View style={styles.location}>
                <Image resizeMode="contain" source={getImage.pickDrop} style={styles.ptrImg} />
                <View style={styles.locationTxt}>
                  <Text numberOfLines={1} style={styles.address}>
                    {item.deliveryAddress}
                  </Text>
                  <Text numberOfLines={1} style={styles.address}>
                    {item.pickUpAddress}
                  </Text>
                </View>
              </View>
              <View style={styles.value}>
                <Text style={styles.price}>${item.paymentAmount}</Text>
                <Text style={styles.bar}>|</Text>
                <Text style={styles.date} numberOfLines={1}>
                  {moment(item.createdAt).format('ddd DD-MM-YY hh:mm A')}
                </Text>
              </View>
            </View>
          </View>
        )}
        style={styles.detailsBlock}
      />
    );
  }
}

Details.propTypes = {
  loadMore: PropTypes.func.isRequired,
  history: PropTypes.arrayOf(PropTypes.object).isRequired,
};
