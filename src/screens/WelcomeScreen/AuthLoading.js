import React from "react";
import {
  ActivityIndicator,
  StatusBar,
  StyleSheet,
  View
} from "react-native";
import { store } from '../../redux/configureStore';

class AuthLoadingScreen extends React.Component {
  constructor(props) {
    super(props);
    this._bootstrapAsync();
  }

  // Fetch the token from storage then navigate to our appropriate place
  _bootstrapAsync = async () => {
      // Fetch the token from storage then navigate to our appropriate place
      const userToken = await store.getState().currentUser.id;
      const { props } = this;
      props.navigation.navigate(userToken ? 'home' : 'welcomeScreen');
   
  };

  // Render any loading content that you like here
  render() {
    return (
      <View style={styles.container}>
        <ActivityIndicator color='#fff' size="small" />
        <StatusBar barStyle="default" />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#1891D0",
    justifyContent: "center",
    alignItems: "center"
  }
});
export default AuthLoadingScreen;
