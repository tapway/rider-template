import React, { PureComponent } from 'react';
import { View, Text, Animated } from 'react-native';
import PropTypes from 'prop-types';
import Image from 'react-native-remote-svg';
import AppIntroSlider from 'react-native-app-intro-slider';
import { Header } from 'native-base';
import { welcomeStyles } from './style';
import { store } from '../../redux/configureStore';
import slides from '../../utils/slide';

export default class WelcomeScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      imageLoader: false,
    };
    this.springValue = new Animated.Value(0.3);
  }

  componentDidMount() {
    this.spring();
  }

  check = async () => {
    // Fetch the token from storage then navigate to our appropriate place
    const userToken = await store.getState().currentUser.id;
    // eslint-disable-next-line react/prop-types
    const { props } = this;
    props.navigation.navigate(userToken ? 'home' : 'login');
    // props.navigation.navigate(userToken ? 'home' : 'login');
  };

  _renderItem = ({ item }) => {
    return (
      <View
        style={[
          welcomeStyles.slide,
          { backgroundColor: item.backgroundColor },
        ]}>
        <Header
          transparent
          androidStatusBarColor="transparent"
          iosBarStyle="light-content"
        />
        <Image source={item.image} style={welcomeStyles.image} />
        <Text style={welcomeStyles.title}>{item.title}</Text>
        <Text style={welcomeStyles.text}>{item.text}</Text>
      </View>
    );
  };

  spring() {
    this.springValue.setValue(0.3);
    Animated.spring(this.springValue, {
      toValue: 1,
      friction: 1,
    }).start();
  }

  render() {
    return (
      <AppIntroSlider
        showSkipButton
        renderItem={this._renderItem}
        slides={slides}
        onDone={this.check}
        onSkip={this.check}
      />
      // <View>
      //   <Text>Welcome To Tapway Rider</Text>
      // </View>
    );
  }
}

WelcomeScreen.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
};
