import React, { PureComponent } from 'react';
import { Text, View, Image, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import styles from './style';
import ProductList from './ProductList';
import getImage from '../../utils/getImage';
import Header from '../../components/Header';
import Button from '../../components/Button';

class OrderList extends PureComponent {
  constructor() {
    super();
    this.state = {};
    this.home = this.home.bind(this);
    this.readyForDelivery = this.readyForDelivery.bind(this);
  }

  home() {
    const { props } = this;
    props.navigation.navigate('home');
  }

  readyForDelivery() {
    const { props } = this;
    props.dumpRequest();
    const category = props.navigation.getParam('category');
    props.navigation.navigate('readyForDelivery', { category });
  }

  render() {
    const { props } = this;
    const { deliveryAddress, pickUpAddress, deliveryInstructions } = props.request;
    const { paymentMode, paymentAmount, category } = props.request;
    return (
      <View style={styles.container}>
        <Header onlyBack={false} back={this.home} title="Order Request" />
        <View style={styles.locationView}>
          <View style={styles.pickUpConatiner}>
            <View style={styles.iconContainer}>
              <Image resizeMode="contain" style={styles.image} source={getImage.secondPickup} />
            </View>
            <View style={styles.pickUpHeadingContainer}>
              <Text style={styles.headingPickUpText}>Pickup from</Text>
              <View style={styles.pickUpDescriptionContainer}>
                <Text numberOfLines={1} style={styles.pickUpDescriptionText}>
                  {pickUpAddress}
                </Text>
              </View>
            </View>
          </View>
          <View style={styles.pickUpConatiner}>
            <View style={styles.iconContainer}>
              <Image resizeMode="contain" style={styles.pickupImage} source={getImage.drop} />
            </View>
            <View style={styles.pickUpHeadingContainer}>
              <Text style={styles.headingPickUpText}>Drop location</Text>
              <View style={styles.pickUpDescriptionContainer}>
                <Text numberOfLines={1} style={styles.pickUpDescriptionText}>
                  {deliveryAddress}
                </Text>
              </View>
            </View>
          </View>
        </View>
        <ScrollView style={styles.mainContainer}>
          <View style={styles.productsContainer}>
            <Text style={styles.productsHeading}>Ordered Products</Text>
            <ProductList
              category={category}
              data={deliveryInstructions.itemDetails}
              style={styles.productList}
            />
          </View>
          <View style={styles.invoiceConatiner}>
            <Text style={styles.productsHeading}>Invoice</Text>
            <View style={styles.partnerChargesContainer}>
              <Text style={styles.partnerChargesText}>Partner Delivery Charges</Text>
              <Text style={styles.partnerChargesText}>Free</Text>
            </View>
            <View style={styles.grandTotalContainer}>
              <Text style={styles.partnerChargesText}>Grand Total</Text>
              <View style={styles.paidConatiner}>
                <Text style={styles.paidTextFirst}>$ {paymentAmount} - </Text>
                {paymentMode === 'Cash' ? (
                  <Text style={styles.unpaidTextSecond}>UnPaid</Text>
                ) : (
                  <Text style={styles.paidTextSecond}>Paid</Text>
                )}
              </View>
            </View>
          </View>
        </ScrollView>
        <View style={styles.buttonContainer}>
          <Button submit={this.readyForDelivery} title="Start Trip" />
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    request: state.request,
    user: state.currentUser,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    dumpRequest: () => {
      dispatch({
        type: 'DUMP_REQUEST',
      });
    },
  };
};

OrderList.propTypes = {
  dumpRequest: PropTypes.func.isRequired,
  request: PropTypes.objectOf(PropTypes.any).isRequired,
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OrderList);
