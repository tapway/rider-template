import { StyleSheet } from 'react-native';
import { heightRatio, widthRatio } from '../../utils/stylesheet';
import colors from '../../utils/colors';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.themeBackgroundColor,
  },
  locationView: {
    backgroundColor: colors.primaryColor,
    flex: 0.25,
    paddingLeft: widthRatio * 25,
    paddingRight: widthRatio * 35,
  },
  pickUpConatiner: {
    flex: 1,
    marginTop: heightRatio * 10,
    flexDirection: 'row',
  },
  iconContainer: {},
  image: {
    height: heightRatio * 10,
    width: widthRatio * 10,
    overflow: 'visible',
    marginTop: heightRatio * 3,
    marginRight: widthRatio * 10,
  },
  pickupImage: {
    height: heightRatio * 18.2,
    width: widthRatio * 12.8,
    overflow: 'visible',
    marginTop: heightRatio * 1,
    marginRight: widthRatio * 10,
  },
  headingPickUpText: {
    fontSize: heightRatio * 12,
    color: colors.white,
  },
  pickUpDescriptionContainer: {
    flexDirection: 'row',
    paddingBottom: heightRatio * 5,
    borderBottomWidth: 1,
    borderColor: colors.themeBackgroundColor,
  },
  pickUpDescriptionText: {
    marginTop: heightRatio * 10,
    fontSize: heightRatio * 12,
    fontWeight: '500',
    width: widthRatio * 300,
    color: colors.white,
  },
  mainContainer: {
    flex: 0.75,
    paddingTop: heightRatio * 9,
    paddingLeft: widthRatio * 15,
    paddingRight: widthRatio * 15,
  },
  productsContainer: {
    padding: heightRatio * 15,
    marginBottom: heightRatio * 5,
    borderWidth: 0.25,
    borderColor: colors.secondaryBorderColor,
  },
  invoiceConatiner: {
    padding: heightRatio * 15,
    borderWidth: 0.25,
    borderColor: colors.secondaryBorderColor,
  },
  productsHeading: {
    fontSize: heightRatio * 15,
    fontWeight: '500',
  },
  productList: {
    marginTop: heightRatio * 24,
    height: heightRatio * 150,
  },
  partnerChargesContainer: {
    marginTop: heightRatio * 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  partnerChargesText: {
    fontSize: heightRatio * 12,
  },
  grandTotalContainer: {
    marginTop: heightRatio * 12,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  paidConatiner: {
    flexDirection: 'row',
  },
  paidTextFirst: { fontSize: heightRatio * 12, fontWeight: 'bold' },
  paidTextSecond: {
    fontSize: heightRatio * 12,
    color: colors.primaryColor,
    fontWeight: 'bold',
  },
  unpaidTextSecond: {
    fontSize: heightRatio * 12,
    color: colors.red,
    fontWeight: 'bold',
  },
  buttonContainer: {
    paddingHorizontal: 15 * widthRatio,
    paddingBottom: 10 * heightRatio,
    backgroundColor: colors.themeBackgroundColor,
    justifyContent: 'center',
    alignItems: 'center',
  },
  sigleListContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomWidth: 1,
    borderColor: colors.secondaryBorderColor,
    paddingBottom: heightRatio * 8,
    paddingTop: heightRatio * 8,
  },
  itemDescriptionText: {
    fontSize: heightRatio * 12,
    fontWeight: '400',
    width: widthRatio * 200,
  },
  itemBrand: {
    flexDirection: 'row',
    marginTop: heightRatio * 5,
  },
  itemBrandText: {
    fontWeight: '200',
  },
  itemBrandDescText: {
    fontWeight: 'bold',
  },
  itemPriceText: {
    color: colors.secondaryColor,
    fontWeight: 'bold',
  },
  quantityCont: {
    borderWidth: 0.5,
    borderColor: colors.secondaryBorderColor,
    padding: widthRatio * 5,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: widthRatio * 10,
  },
  quantityText: {
    color: colors.primaryColor,
  },
  itemContainer: {
    height: 44.2 * heightRatio,
    width: '100%',
    paddingRight: 20,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  itemTextActive: { fontSize: 14 * heightRatio, fontWeight: '500' },
  itemTextInActive: { fontSize: 14 * heightRatio, fontWeight: '300' },
});
