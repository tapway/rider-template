import React, { PureComponent } from 'react';
import { View, Text, FlatList } from 'react-native';
import { indexOf } from 'lodash';
import PropTypes from 'prop-types';
import { CheckBox } from 'native-base';
import styles from './style';
import colors from '../../utils/colors';

export default class ProductList extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  _itemSeperator = () => {
    return (
      <View
        style={{
          backgroundColor: 'lightgrey',
          height: 1,
          width: '100%',
        }}
      ></View>
    );
  };

  _renderItem = item => {
    const { props } = this;
    const active = indexOf(props.data, item) !== -1;
    return (
      <View style={styles.itemContainer}>
        <Text style={active ? styles.itemTextActive : styles.itemTextInActive}>{item}</Text>
        <CheckBox style={{ fontSize: 12 }} checked={active} color={colors.primaryColor} disabled />
      </View>
    );
  };

  render() {
    const { props } = this;
    const itemType = ['Documents/Books', 'Foods', 'Sports', 'Electronic Items', 'Others'];
    if (props.category.name === 'Pickup') {
      return (
        <FlatList
          bounces={false}
          showsVerticalScrollIndicator={false}
          ItemSeparatorComponent={this._itemSeperator}
          data={itemType}
          contentContainerStyle={{ marginTop: 10 }}
          renderItem={({ item }) => this._renderItem(item)}
        ></FlatList>
      );
    }
    return (
      <FlatList
        data={props.data}
        renderItem={({ item }) => (
          <View style={styles.sigleListContainer}>
            <View style={styles.sigleListDescription}>
              <Text numberOfLines={1} style={styles.itemDescriptionText}>
                {item.itemDescription}
              </Text>
              {props.category.name === 'Food' ? (
                <React.Fragment />
              ) : (
                <View style={styles.itemBrand}>
                  <Text style={styles.itemBrandText}>Brand : </Text>
                  <Text style={styles.itemBrandDescText}>{item.brand}</Text>
                </View>
              )}
              <View style={styles.itemBrand}>
                <Text style={styles.itemBrandText}>Price : </Text>
                <Text style={styles.itemPriceText}>{item.price}</Text>
              </View>
            </View>
            <View style={styles.sigleListQuantityCont}>
              <View style={styles.quantityCont}>
                <Text style={styles.quantityText}>{item.quantity}</Text>
              </View>
            </View>
          </View>
        )}
        style={props.style}
      />
    );
  }
}

ProductList.propTypes = {
  style: PropTypes.objectOf(PropTypes.any).isRequired,
  data: PropTypes.objectOf(PropTypes.any).isRequired,
  category: PropTypes.objectOf(PropTypes.any).isRequired,
};
