import { createAppContainer } from 'react-navigation';
import React from 'react';
import { View, Dimensions } from 'react-native';
import { createMaterialTopTabNavigator } from 'react-navigation-tabs';
import Profile from './Profile';
import Kyc from './DrivingLicence';
import BankDetails from './BankDetails';
import VehicleDetail from './VehicleDetails';
import { TopComponent } from './TopComponent';
import colors from '../../utils/colors';
import Header from '../../components/Header';
import { store } from '../../redux/configureStore';
import { Header as Head, Left, Body } from 'native-base';

const { width } = Dimensions.get('window');

const RegisterTabNavigator = createMaterialTopTabNavigator(
  {
    profile: {
      screen: Profile,
      navigationOptions: () => ({
        tabBarLabel: 'About',
        swipeEnabled: false,
      }),
    },
    vehicleDetail: {
      screen: VehicleDetail,
      navigationOptions: () => ({
        tabBarLabel: 'Vehicle Detail',
        swipeEnabled: false,
      }),
    },
    kyc: {
      screen: Kyc,
      navigationOptions: () => ({
        tabBarLabel: 'KYC',
        swipeEnabled: false,
      }),
    },
    bankdetails: {
      screen: BankDetails,
      navigationOptions: () => ({
        tabBarLabel: 'Bank Details',
        swipeEnabled: false,
      }),
    },
  },
  {
    tabBarComponent: props => {
      return (
        <View style={{ flex: 0.4 }}>
          <Head
            transparent // hasTabs iosBarStyle="light-content"
            style={{ height: 5 }}
            androidStatusBarColor={'#1891D0'}>
            <Left />
            <Body />
          </Head>
          <Header
            onlyBack
            back={() => {
              // eslint-disable-next-line react/prop-types
              props.navigation.navigate('login');
              store.dispatch({
                type: 'USER_SIGNOUT',
              });
            }}
          />
          <TopComponent {...props} />
        </View>
      );
    },
    tabBarOptions: {
      upperCaseLabel: false,
      labelStyle: {
        fontSize: width / 19,
        color: colors.black,
        padding: 0,
        fontWeight: '700',
      },
      indicatorStyle: {
        backgroundColor: colors.primaryColor,
        justifyContent: 'space-around',
      },
      tabStyle: {
        justifyContent: 'space-between',
        alignItems: 'stretch',
        width: 'auto',
      },
      style: {
        backgroundColor: colors.white,
        justifyContent: 'space-between',
      },
    },
  },
  {
    initialRouteName: 'profile',
    backBehaviour: 'initialRoute',
    tabBarPosition: 'top',
  },
);

export default createAppContainer(RegisterTabNavigator);
