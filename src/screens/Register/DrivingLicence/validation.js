import { startNavigation } from '../TopComponent';

const validation = incomingThis => {
  const { props, refs } = incomingThis;
  const { address, licenceNumber, cardHolder, dateOfIssue, validity } = incomingThis.state;
  let flag = 0;
  if (address === '') {
    flag = 1;
    incomingThis.handlerFocus('addressFocus', 2);
  }
  if (licenceNumber === '') {
    flag = 1;
    incomingThis.handlerFocus('licenceNumberFocus', 2);
  }
  if (cardHolder === '') {
    flag = 1;
    incomingThis.handlerFocus('cardHolderFocus', 2);
  }
  if (dateOfIssue === '') {
    flag = 1;
    incomingThis.handlerFocus('dateFocus', 2);
  }
  if (validity === '') {
    flag = 1;
    incomingThis.handlerFocus('validityFocus', 2);
  }
  if (props.user.verificationImage === '') {
    flag = 1;
    incomingThis.handlerFocus('avatarFocus', 2);
  }
  if (flag === 0) {
    props.setKycDispatcher(address, licenceNumber, cardHolder, dateOfIssue, validity);
    startNavigation(3);
    props.navigation.navigate('bankdetails');
  } else {
    refs.toast.show('Required Fields Not Filled');
  }
};

export default validation;
