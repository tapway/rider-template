import React, { PureComponent } from 'react';
import { Text, View, TouchableOpacity, Image } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { connect } from 'react-redux';
import DatePicker from 'react-native-datepicker';
import ImagePicker from 'react-native-image-picker';
import Toast from 'react-native-easy-toast';
import PropTypes from 'prop-types';
import styles from '../style';
import colors from '../../../utils/colors';
import { updateVerificationPicAsync } from '../../../redux/action/imageUpload';
import { setKYC } from '../../../redux/action/register';
import { startNavigation } from '../TopComponent';
import { kycInitialState } from '../initialState';
import getImage from '../../../utils/getImage';
import MyTextInput from '../../../components/TextInput';
import validation from './validation';

const options = {
  title: 'Select Avatar',
  quality: 0.1,
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

class Kyc extends PureComponent {
  constructor() {
    super();
    this.state = kycInitialState;
  }

  handlerFocus = (input, value) => {
    this.setState({
      [input]: value,
    });
  };

  handlerInput = (input, value) => {
    this.setState({
      [input]: value,
    });
  };

  uploadimage = () => {
    ImagePicker.showImagePicker(options, response => {
      if (!response.didCancel && !response.error && !response.customButton) {
        updateVerificationPicAsync({ localUrl: response.uri }, response.type);
      }
    });
  };

  goBack = () => {
    const { props } = this;
    startNavigation(1);
    props.navigation.navigate('vehicleDetail');
  };

  render() {
    const { cardHolderFocus, licenceNumberFocus, dateFocus, validityFocus } = this.state;
    const { props } = this;
    const { address, licenceNumber, cardHolder, dateOfIssue, validity, addressFocus } = this.state;
    return (
      <View style={styles.container}>
        <KeyboardAwareScrollView style={[styles.scroll, styles.contentContainer]}>
          <MyTextInput
            heading="Card Holder"
            placeholder="Card holder name"
            field="cardHolder"
            value={cardHolder}
            focusInput={cardHolderFocus}
            focus="cardHolderFocus"
            handlerFocus={this.handlerFocus}
            handlerInput={this.handlerInput}
          />
          <MyTextInput
            heading="Licence Number"
            placeholder="Your Licence Number"
            field="licenceNumber"
            value={licenceNumber}
            focusInput={licenceNumberFocus}
            focus="licenceNumberFocus"
            handlerFocus={this.handlerFocus}
            handlerInput={this.handlerInput}
          />
          <Text style={[styles.headingText2, styles.margin]}>Date Of Issue</Text>
          <DatePicker
            style={[
              styles.dateInputBox,
              dateFocus === 0 ? styles.nofocus : dateFocus === 1 ? styles.focus : styles.redFocus,
            ]}
            date={dateOfIssue}
            mode="date"
            placeholder="Date Of Issue"
            format="DD-MM-YYYY"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            customStyles={{
              dateIcon: styles.datePickerCustomStylesDateIcon,
              dateInput: styles.datePickerCustomStylesDateInput,
            }}
            onOpenModal={() => this.handlerFocus('dateFocus', 1)}
            onCloseModal={() => this.handlerFocus('dateFocus', 0)}
            onDateChange={date => {
              this.setState({ dateOfIssue: date });
            }}
          />
          <Text style={[styles.headingText2, styles.margin]}>Validity Of Licence</Text>
          <DatePicker
            style={[
              styles.dateInputBox,
              validityFocus === 0
                ? styles.nofocus
                : validityFocus === 1
                ? styles.focus
                : styles.redFocus,
            ]}
            date={validity}
            mode="date"
            placeholder="Validity Of Licence"
            format="DD-MM-YYYY"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            customStyles={{
              dateIcon: styles.datePickerCustomStylesDateIcon,
              dateInput: styles.datePickerCustomStylesDateInput,
            }}
            onOpenModal={() => this.handlerFocus('validityFocus', 1)}
            onCloseModal={() => this.handlerFocus('validityFocus', 0)}
            onDateChange={date => {
              this.setState({ validity: date });
            }}
          />
          <MyTextInput
            heading="Address"
            placeholder="Your address"
            field="address"
            value={address}
            focusInput={addressFocus}
            focus="addressFocus"
            handlerFocus={this.handlerFocus}
            handlerInput={this.handlerInput}
          />
          {props.user.verificationImage ? (
            <View style={styles.imageDocument}>
              <Image
                style={styles.imageDocumentPic}
                source={{ uri: props.user.verificationImage }}
              />
            </View>
          ) : (
            <TouchableOpacity
              style={[
                styles.imageDocument,
                props.user.verificationImage ? styles.redFocus : styles.noLight,
              ]}
              onPress={this.uploadimage}
            >
              <View style={styles.imageDocumentContainer}>
                <Image style={styles.loadingImageUpload} source={getImage.uploadImage} />
                <Text style={styles.uploadText}>Driving License</Text>
                <Text style={styles.uploadText2}>upload your driving license</Text>
              </View>
            </TouchableOpacity>
          )}
          <View style={styles.nextBtnContainer}>
            <TouchableOpacity onPress={this.goBack}>
              <Text style={styles.backbutton}>Back</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => validation(this)}>
              <Text style={styles.nextButton}>Next</Text>
            </TouchableOpacity>
          </View>
        </KeyboardAwareScrollView>
        <Toast
          ref="toast"
          style={{ backgroundColor: colors.toastBgColor }}
          position="bottom"
          positionValue={400}
          fadeInDuration={750}
          fadeOutDuration={1000}
          opacity={0.8}
          textStyle={{ color: colors.toastTextColor, fontSize: 15 }}
        />
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    user: state.currentUser,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    setKycDispatcher: (address, licenceNumber, cardHolder, dateOfIssue, validity) => {
      dispatch(setKYC(address, licenceNumber, cardHolder, dateOfIssue, validity));
    },
  };
};

Kyc.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
  user: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Kyc);
