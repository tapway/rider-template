import formValidation from '../../../helpers';
import { startNavigation } from '../TopComponent';
import checkUserExistance from '../../../apiService/checkUserExistance';

const navigate = (incomingThis, res) => {
  const { props, refs } = incomingThis;
  const { fullName, email, dob } = incomingThis.state;
  const { Currency, Language, mobile, gender } = incomingThis.state;
  if (!res.success) {
    incomingThis.handlerFocus('mobileFocus', 0);
    props.setProfileDispatcher(mobile, fullName, email, dob, gender, Currency, Language);
    startNavigation(1);
    props.navigation.navigate('vehicleDetail');
  } else {
    incomingThis.handlerFocus('mobileFocus', 2);
    refs.toast.show('User Already Exists');
  }
};

const validation = incomingThis => {
  const { refs, props } = incomingThis;
  const { fullName, email, dob, mobile } = incomingThis.state;
  if (fullName === '') {
    refs.toast.show('Required Fields Not Filled');
    incomingThis.handlerFocus('nameFocus', 2);
  } else if (dob === '') {
    refs.toast.show('Required Fields Not Filled');
    incomingThis.handlerFocus('dobFocus', 2);
  } else if (email === '') {
    refs.toast.show('Required Fields Not Filled');
    incomingThis.handlerFocus('emailFocus', 2);
  } else if (props.user.profileImage === '') {
    refs.toast.show('Image not uploaded');
  } else if (formValidation.emailVaildate(email)) {
    if (incomingThis.phone.isValidNumber()) {
      incomingThis.handlerFocus('mobileFocus', 0);
      checkUserExistance(mobile, navigate, incomingThis);
    } else {
      incomingThis.handlerFocus('mobileFocus', 2);
      refs.toast.show('Invalid Number');
    }
  } else {
    incomingThis.handlerFocus('emailFocus', 2);
    refs.toast.show('Invalid Email');
  }
};

export default validation;
