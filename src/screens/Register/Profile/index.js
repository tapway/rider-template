import React, { PureComponent } from 'react';
import { Text, View, TouchableOpacity, Image } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Dropdown } from 'react-native-material-dropdown';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import ImagePicker from 'react-native-image-picker';
import Toast from 'react-native-easy-toast';
import PhoneInput from 'react-native-phone-input';
import DatePicker from 'react-native-datepicker';
import styles from '../style';
import colors from '../../../utils/colors';
import { updateUserProfilePicAsync } from '../../../redux/action/imageUpload';
import { setProfile } from '../../../redux/action/register';
import getImage from '../../../utils/getImage';
import { profileInitialState } from '../initialState';
import MyTextInput from '../../../components/TextInput';
import validation from './validation';
import { heightRatio } from '../../../utils/stylesheet';

const options = {
  title: 'Select Avatar',
  quality: 0.1,
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

class Profile extends PureComponent {
  constructor() {
    super();
    this.state = profileInitialState;
    this.phone = null;
  }

  uploadImage = () => {
    ImagePicker.showImagePicker(options, response => {
      if (!response.didCancel && !response.error && !response.customButton) {
        updateUserProfilePicAsync({ localUrl: response.uri }, response.type);
      }
    });
  };

  handlerFocus = (input, value) => {
    this.setState({
      [input]: value,
    });
  };

  handlerInput = (input, value) => {
    this.setState({
      [input]: value,
    });
  };

  render() {
    const { nameFocus, emailFocus, mobileFocus, dobFocus } = this.state;
    const { mobile, fullName, email, dob } = this.state;
    const { Genderopt } = this.state;
    const { props } = this;
    return (
      <View style={styles.container}>
        <KeyboardAwareScrollView style={[styles.scroll, styles.contentContainer]}>
          <View style={styles.imageUpload}>
            <Image
              style={styles.userIconImage}
              source={
                props.user.profileImage ? { uri: props.user.profileImage } : getImage.secondDriver
              }
            />
            <TouchableOpacity onPress={this.uploadImage}>
              <Text style={styles.uploadText3}>Upload Image</Text>
            </TouchableOpacity>
          </View>
          <MyTextInput
            heading="Full Name"
            placeholder="Your Name"
            field="fullName"
            value={fullName}
            focusInput={nameFocus}
            focus="nameFocus"
            handlerFocus={this.handlerFocus}
            handlerInput={this.handlerInput}
          />
          <MyTextInput
            heading="Email"
            placeholder="demo@email.com"
            field="email"
            value={email}
            focusInput={emailFocus}
            focus="emailFocus"
            handlerFocus={this.handlerFocus}
            handlerInput={this.handlerInput}
          />
          <Text style={[styles.headingText2, styles.margin]}>Mobile</Text>
          <PhoneInput
            ref={ref => {
              this.phone = ref;
            }}
            value={mobile}
            onChangePhoneNumber={text => this.setState({ mobile: text })}
            style={[
              styles.textInputBox,
              mobileFocus === 0
                ? styles.nofocus
                : mobileFocus === 1
                ? styles.focus
                : styles.redFocus,
            ]}
            textStyle={{ fontSize: 12 * heightRatio }}
            textProps={{ placeholder: 'Enter Phone Number' }}
          />
          <Text style={[styles.headingText2, styles.margin]}>DOB</Text>
          <DatePicker
            style={[
              styles.dateInputBox,
              dobFocus === 0 ? styles.nofocus : dobFocus === 1 ? styles.focus : styles.redFocus,
            ]}
            date={dob}
            mode="date"
            placeholder="Date Of Birth"
            format="YYYY-MM-DD"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            customStyles={{
              dateIcon: styles.datePickerCustomStylesDateIcon,
              dateInput: styles.datePickerCustomStylesDateInput,
            }}
            onOpenModal={() => this.handlerFocus('dobFocus', 1)}
            onCloseModal={() => this.handlerFocus('dobFocus', 0)}
            onDateChange={date => {
              this.setState({ dob: date });
            }}
          />
          <Text style={[styles.headingText2, styles.margin]}>Gender</Text>
          <Dropdown
            data={Genderopt}
            value="Male"
            onChangeText={value => this.setState({ gender: value })}
          />
          <View style={styles.nextBtnContainer}>
            <TouchableOpacity onPress={() => validation(this)}>
              <Text style={styles.nextButton}>Next</Text>
            </TouchableOpacity>
          </View>
        </KeyboardAwareScrollView>
        <Toast
          ref="toast"
          style={{ backgroundColor: colors.toastBgColor }}
          position="bottom"
          positionValue={400}
          fadeInDuration={750}
          fadeOutDuration={1000}
          opacity={0.8}
          textStyle={{ color: colors.toastTextColor, fontSize: 15 }}
        />
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    user: state.currentUser,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    setProfileDispatcher: (mob, fname, mail, birth, gende, curr, lang) => {
      dispatch(setProfile(mob, fname, mail, birth, gende, curr, lang));
    },
  };
};

Profile.propTypes = {
  user: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
