import React, { PureComponent } from 'react';
import { Text, View, ScrollView, Keyboard, Platform } from 'react-native';
import PropTypes from 'prop-types';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import styles from './style';

let scrollRef = null;

export const startNavigation = choice => {
  switch (choice) {
    case 0:
      scrollRef.scrollView.scrollTo({ x: 0, animated: true });
      break;
    case 1:
      scrollRef.scrollView.scrollTo({ x: 0, animated: true });
      break;
    case 2:
      scrollRef.scrollView.scrollTo({ x: 150, animated: true });
      break;
    case 3:
      scrollRef.scrollView.scrollTo({ x: 250, animated: true });
      break;
    default:
      break;
  }
};

export class TopComponent extends PureComponent {
  constructor(props) {
    super(props);
    this.state = { keyboardState: false };
  }

  componentWillMount() {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide);
  }

  componentDidMount() {
    const { refs } = this;
    scrollRef = refs;
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  keyboardDidShow = () => {
    this.setState({
      keyboardState: true,
    });
  };

  keyboardDidHide = () => {
    this.setState({
      keyboardState: false,
    });
  };

  render() {
    const { props } = this;
    const { keyboardState } = this.state;
    return (
      <KeyboardAwareScrollView scrollEnabled={false}>
        <View style={styles.topContainer}>
          {keyboardState && Platform.OS === 'android' ? (
            <React.Fragment />
          ) : (
            <View style={styles.headingContainer}>
              <Text style={styles.headingText1}>Register</Text>
            </View>
          )}
          <ScrollView
            ref="scrollView"
            horizontal
            showsHorizontalScrollIndicator={false}
            style={styles.operations}
          >
            <View
              style={[
                styles.pad,
                props.navigation.state.index === 0 ? styles.focusBorder : styles.noopfocus,
              ]}
            >
              <Text
                style={[
                  styles.operationsText,
                  props.navigation.state.index === 0 ? styles.opfocus : styles.noopfocus,
                ]}
              >
                Profile
              </Text>
            </View>
            <View
              style={[
                styles.pad,
                props.navigation.state.index === 1 ? styles.focusBorder : styles.noopfocus,
              ]}
            >
              <Text
                style={[
                  styles.operationsText,
                  props.navigation.state.index === 1 ? styles.opfocus : styles.noopfocus,
                ]}
              >
                Vehicle Details
              </Text>
            </View>
            <View
              style={[
                styles.pad,
                props.navigation.state.index === 2 ? styles.focusBorder : styles.noopfocus,
              ]}
            >
              <Text
                style={[
                  styles.operationsText,
                  props.navigation.state.index === 2 ? styles.opfocus : styles.noopfocus,
                ]}
              >
                Driving Licence
              </Text>
            </View>
            <View
              style={[
                styles.pad,
                props.navigation.state.index === 3 ? styles.focusBorder : styles.noopfocus,
              ]}
            >
              <Text
                style={[
                  styles.operationsText,
                  props.navigation.state.index === 3 ? styles.opfocus : styles.noopfocus,
                ]}
              >
                Bank Details
              </Text>
            </View>
          </ScrollView>
        </View>
      </KeyboardAwareScrollView>
    );
  }
}

TopComponent.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
};
