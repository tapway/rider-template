export const profileInitialState = {
  nameFocus: 0,
  emailFocus: 0,
  mobileFocus: 0,
  avatarSource: '',
  dobFocus: 0,
  fullName: '',
  email: '',
  mobile: '',
  dob: '',
  gender: 'Male',
  Genderopt: [{ value: 'Male' }, { value: 'Female' }, { value: 'Others' }],
  Currencyopt: [{ value: 'USD' }, { value: 'INR' }],
  Currency: 'USD',
  Languageopt: [{ value: 'English' }, { value: 'Hindi' }],
  Language: 'English',
};

export const kycInitialState = {
  cardHolderFocus: 0,
  licenceNumberFocus: 0,
  dateFocus: 0,
  avatarSource: '',
  avatarFocus: 0,
  validityFocus: 0,
  addressFocus: 0,
  cardHolder: '',
  licenceNumber: '',
  address: '',
  dateOfIssue: '',
  validity: '',
};

export const vehicleDetailsInitialState = {
  modelFocus: 0,
  avatarSource: '',
  registrationFocus: 0,
  avatarFocus: 0,
  insuranceFocus: 0,
  dateFocus: 0,
  numberFocus: 0,
  date: '',
  vehicleModel: '',
  registrationNumber: '',
  insurancePolicy: '',
  vehicleNumber: '',
};

export const bankDetailsInitialState = {
  nameFocus: 0,
  numberFocus: 0,
  codeFocus: 0,
  accountName: '',
  accountNum: '',
  bankName: 'Kotak Mahindra',
  IFSCcode: '',
  Banks: [{ value: 'Kotak Mahindra' }, { value: 'State Bank Of India' }],
  imageLoader: false,
};
