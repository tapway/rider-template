import React, { PureComponent } from 'react';
import { Text, View, TouchableOpacity, Image } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-picker';
import DatePicker from 'react-native-datepicker';
import Toast from 'react-native-easy-toast';
import PropTypes from 'prop-types';
import styles from '../style';
import colors from '../../../utils/colors';
import { setVehicleDetail } from '../../../redux/action/register';
import { updateVehicleVerificationPicAsync } from '../../../redux/action/imageUpload';
import { startNavigation } from '../TopComponent';
import getImage from '../../../utils/getImage';
import { vehicleDetailsInitialState } from '../initialState';
import MyTextInput from '../../../components/TextInput';
import validation from './validation';

const options = {
  title: 'Select Avatar',
  quality: 0.1,
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

class VehicleDetail extends PureComponent {
  constructor() {
    super();
    this.state = vehicleDetailsInitialState;
  }

  handlerFocus = (input, value) => {
    this.setState({
      [input]: value,
    });
  };

  handlerInput = (input, value) => {
    this.setState({
      [input]: value,
    });
  };

  uploadimage = () => {
    ImagePicker.showImagePicker(options, response => {
      if (!response.didCancel && !response.error && !response.customButton) {
        updateVehicleVerificationPicAsync({ localUrl: response.uri }, response.type);
      }
    });
  };

  goBack = () => {
    const { props } = this;
    startNavigation(0);
    props.navigation.navigate('profile');
  };

  render() {
    const { modelFocus, registrationFocus, insuranceFocus, avatarFocus } = this.state;
    const { date: chooseDate, insurancePolicy, vehicleModel, dateFocus } = this.state;
    const { numberFocus, registrationNumber, vehicleNumber } = this.state;
    const { props } = this;
    return (
      <View style={styles.container}>
        <KeyboardAwareScrollView style={[styles.scroll, styles.contentContainer]}>
          <MyTextInput
            heading="Vehicles Model"
            placeholder="Your Vehicle Model"
            field="vehicleModel"
            value={vehicleModel}
            focusInput={modelFocus}
            focus="modelFocus"
            handlerFocus={this.handlerFocus}
            handlerInput={this.handlerInput}
          />
          <MyTextInput
            heading="Registration Number"
            placeholder="Your Registration Number"
            field="registrationNumber"
            value={registrationNumber}
            focusInput={registrationFocus}
            focus="registrationFocus"
            handlerFocus={this.handlerFocus}
            handlerInput={this.handlerInput}
          />
          <Text style={[styles.headingText2, styles.margin]}>Date of Registration</Text>
          <DatePicker
            style={[
              styles.dateInputBox,
              dateFocus === 0 ? styles.nofocus : dateFocus === 1 ? styles.focus : styles.redFocus,
            ]}
            date={chooseDate}
            mode="date"
            placeholder="Date of Registration"
            format="YYYY-MM-DD"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            customStyles={{
              dateIcon: styles.datePickerCustomStylesDateIcon,
              dateInput: styles.datePickerCustomStylesDateInput,
            }}
            onOpenModal={() => this.handlerFocus('dateFocus', 1)}
            onCloseModal={() => this.handlerFocus('dateFocus', 0)}
            onDateChange={date => {
              this.setState({ date });
            }}
          />
          <MyTextInput
            heading="Insurance Policy"
            placeholder="Your Insurance Policy"
            field="insurancePolicy"
            value={insurancePolicy}
            focusInput={insuranceFocus}
            focus="insuranceFocus"
            handlerFocus={this.handlerFocus}
            handlerInput={this.handlerInput}
          />
          <MyTextInput
            heading="Vehicle Number"
            placeholder="Your Vehicle Number"
            field="vehicleNumber"
            value={vehicleNumber}
            focusInput={numberFocus}
            focus="numberFocus"
            handlerFocus={this.handlerFocus}
            handlerInput={this.handlerInput}
          />
          {props.user.vehicleVerificationImage ? (
            <View style={styles.imageDocument}>
              <Image
                style={styles.imageDocumentPic}
                source={{ uri: props.user.vehicleVerificationImage }}
              />
            </View>
          ) : (
            <TouchableOpacity
              style={[styles.imageDocument, avatarFocus ? styles.redFocus : styles.noLight]}
              onPress={this.uploadimage}
            >
              <View style={styles.imageDocumentContainer}>
                <Image style={styles.loadingImageUpload} source={getImage.uploadImage} />
                <Text style={styles.uploadText}>Registration Certificate</Text>
                <Text style={styles.uploadText2}>upload your registration certificate</Text>
              </View>
            </TouchableOpacity>
          )}
          <View style={styles.nextBtnContainer}>
            <TouchableOpacity onPress={this.goBack}>
              <Text style={styles.backbutton}>Back</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => validation(this)}>
              <Text style={styles.nextButton}>Next</Text>
            </TouchableOpacity>
          </View>
        </KeyboardAwareScrollView>
        <Toast
          ref="toast"
          style={{ backgroundColor: colors.toastBgColor }}
          position="bottom"
          positionValue={400}
          fadeInDuration={750}
          fadeOutDuration={1000}
          opacity={0.8}
          textStyle={{ color: colors.toastTextColor, fontSize: 15 }}
        />
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    user: state.currentUser,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    setVehicleDetailDispatcher: (modal, registrationNumber, date, insurance, number) => {
      dispatch(setVehicleDetail(modal, registrationNumber, date, insurance, number));
    },
  };
};

VehicleDetail.propTypes = {
  user: PropTypes.objectOf(PropTypes.any).isRequired,
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VehicleDetail);
