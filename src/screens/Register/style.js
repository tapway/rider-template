import { StyleSheet, Dimensions } from 'react-native';
import colors from '../../utils/colors';
import { heightRatio, widthRatio } from '../../utils/stylesheet';

const { height, width } = Dimensions.get('window');
export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.themeBackgroundColor,
  },
  topContainer: {
    height: heightRatio * 150,
    backgroundColor: colors.themeBackgroundColor,
  },
  headingContainer: {
    paddingLeft: width / 10,
    justifyContent: 'center',
  },
  imageUpload: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 5 * heightRatio,
  },
  imageDocument: {
    marginTop: height / 50,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 2,
    borderColor: colors.imageContainerBorderColor,
    height: height / 4,
    width: width / 1.25,
  },
  imageDocumentContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageDocumentPic: {
    height: height / 4,
    width: width / 1.3,
  },
  loadingImageUpload: {
    marginBottom: height / 50,
  },
  userIconImage: {
    height: width / 5,
    width: width / 5,
    borderRadius: width / 5 / 2,
  },
  uploadText: {
    fontSize: height / 40,
    color: colors.textGrey,
  },
  uploadText2: {
    marginTop: height / 150,
    fontSize: height / 50,
    color: colors.textGrey,
    fontWeight: '200',
  },
  uploadText3: {
    marginTop: height / 150,
    fontSize: 16 * heightRatio,
    color: colors.primaryColor,
    fontWeight: 'bold',
  },
  dropdownStyle: {
    width: width / 1.25,
  },
  dropdown: {
    color: colors.primaryColor,
    borderColor: colors.noFoucsBorderColor,
    fontSize: width / 18,
  },
  dropdownTextStyle: {
    fontSize: width / 18,
  },
  contentContainer: {
    flex: 3,
    paddingLeft: width / 10,
    paddingTop: height / 140,
    paddingRight: width / 10,
  },
  headingText1: {
    fontSize: height / 20,
  },
  headingText2: {
    fontSize: 14 * heightRatio,
    paddingTop: height / 80,
    fontWeight: '200',
  },
  focus: {
    borderColor: colors.primaryColor,
  },
  nofocus: {
    borderColor: colors.noFoucsBorderColor,
  },
  noLight: {},
  redFocus: {
    borderColor: 'red',
  },
  pad: {
    paddingBottom: height / 100,
    justifyContent: 'flex-end',
    marginRight: width / 10,
    marginTop: heightRatio * 10,
    height: heightRatio * 50,
  },
  dropdownBox: {
    borderStyle: 'solid',
    borderBottomWidth: 1,
  },
  textInputBox: {
    borderStyle: 'solid',
    borderBottomWidth: 1,
    padding: width / 30,
    paddingLeft: 0,
    fontSize: 12 * heightRatio,
    color: colors.inputTextColor,
  },
  dateInputBox: {
    marginTop: height / 80,

    borderStyle: 'solid',
    borderBottomWidth: 1,
    width: '100%',
    paddingLeft: 0,
    paddingBottom: width / 60,
    fontSize: 12 * heightRatio,
    color: colors.primaryColor,
  },
  backButton: {
    height: height / 48,
    width: width / 19,
    marginTop: height / 140,
    overflow: 'visible',
  },
  backSignContainer: {
    top: height / 18,
    paddingLeft: width / 15,
  },
  margin: {
    marginTop: height / 40,
  },
  scroll: {
    marginTop: 0,
  },
  operations: {
    flex: 1,
    flexDirection: 'row',
    marginLeft: width / 10,
    marginRight: width / 10,
  },
  operationsText: {
    fontSize: height / 39,
  },
  datePickerCustomStylesDateIcon: {
    position: 'absolute',
    left: 0,
    top: 4,
    marginLeft: 0,
  },
  datePickerCustomStylesDateInput: {
    marginLeft: 40 * widthRatio,
    padding: 0,
    borderWidth: 0,
    alignItems: 'flex-start',
  },
  nextBtnContainer: {
    alignItems: 'center',
    paddingTop: height / 30,
    paddingBottom: height / 20,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  botpad: {
    marginBottom: height / 12,
  },
  nextButton: {
    fontSize: height / 40,
    color: colors.primaryColor,
    fontWeight: 'bold',
  },
  backbutton: {
    fontSize: height / 40,
    paddingRight: width / 10,
    color: colors.secondaryColor,
    fontWeight: 'bold',
  },
  opfocus: {
    fontWeight: 'bold',
    color: colors.textForHighlightedContainer,
  },
  focusBorder: {
    borderStyle: 'solid',
    borderBottomWidth: 3,
    borderColor: colors.primaryColor,
  },
  noopfocus: {
    fontWeight: '200',
  },
  topPad: {
    marginTop: height / 15,
  },
  loginButton: {
    width: width / 2,
    alignItems: 'center',
    padding: width / 30,
    borderRadius: 50,
    backgroundColor: colors.primaryColor,
    justifyContent: 'center',
    borderColor: colors.transparent,
  },
  loginBtnContainer: {
    flex: 2,
    alignItems: 'center',
    marginBottom: height / 50,
    paddingHorizontal: 20 * heightRatio,
  },
  loginText: {
    color: colors.textForPrimaryBgColor,
    fontSize: height / 40,
  },
  backKycBtnContainer: {
    flex: 2,
    alignItems: 'center',
    paddingHorizontal: 20 * heightRatio,
  },
  backKycButton: {
    width: '100%',
    alignItems: 'center',
    padding: 12,
    borderRadius: 50,
    backgroundColor: colors.thirdBtnBgColor,
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: colors.secondaryBorderColor,
  },
  backKycText: {
    color: colors.secondaryColor,
    fontSize: 16 * heightRatio,
  },
});
