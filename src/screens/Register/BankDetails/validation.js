import registerVerify from '../../../apiService/registerVerify';

const validation = incomingThis => {
  const { props, refs } = incomingThis;
  const { accountName, accountNum, IFSCcode, bankName, imageLoader } = incomingThis.state;
  let flag = 0;
  if (accountName === '') {
    flag = 1;
    incomingThis.handlerFocus('nameFocus', 2);
  }
  if (accountNum === '') {
    flag = 1;
    incomingThis.handlerFocus('numberFocus', 2);
  }
  if (IFSCcode === '') {
    flag = 1;
    incomingThis.handlerFocus('codeFocus', 2);
  }
  if (flag === 0) {
    incomingThis.setState({ imageLoader: !imageLoader });
    props.setBankDetailsDispatcher(accountName, accountNum, bankName, IFSCcode);
    const obj = {
      email: props.email,
    };
    registerVerify(props.user.phoneNumber, obj, incomingThis.handleRegister);
    console.log('hereeeeeeeeeeeeee', props);
  } else {
    refs.toast.show('Required Fields Not Filled');
  }
};

export default validation;
