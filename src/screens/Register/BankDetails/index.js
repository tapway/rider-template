import React, { PureComponent } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { connect } from 'react-redux';
import { Dropdown } from 'react-native-material-dropdown';
import Toast from 'react-native-easy-toast';

import PropTypes from 'prop-types';
import styles from '../style';
import colors from '../../../utils/colors';

import { setBankDetails } from '../../../redux/action/register';

import { startNavigation } from '../TopComponent';

import Button from '../../../components/Button';
import MyTextInput from '../../../components/TextInput';
import Loader from '../../../components/Loader';

import { bankDetailsInitialState } from '../initialState';
import validation from './validation';

class BankDetails extends PureComponent {
  constructor() {
    super();
    this.state = bankDetailsInitialState;
  }

  handleRegister = res => {
    const { imageLoader } = this.state;
    this.setState({ imageLoader: !imageLoader });
    if (res.success) {
      const { props } = this;
      props.navigation.navigate('registrationVerification');
    }
  };

  handlerFocus = (input, value) => {
    this.setState({
      [input]: value,
    });
  };

  handlerInput = (input, value) => {
    this.setState({
      [input]: value,
    });
  };

  backToKyc = () => {
    const { props } = this;
    startNavigation(2);
    props.navigation.navigate('kyc');
  };

  render() {
    const { nameFocus, numberFocus, codeFocus } = this.state;
    const { accountName, accountNum, IFSCcode, imageLoader } = this.state;
    const { Banks } = this.state;
    return (
      <View style={styles.container}>
        <Loader loading={imageLoader} />
        <KeyboardAwareScrollView style={[styles.scroll, styles.contentContainer]}>
          <Text style={[styles.headingText2, styles.margin]}>Bank Name</Text>
          <Dropdown
            data={Banks}
            value="Kotak Mahindra"
            // eslint-disable-next-line react/no-unused-state
            onChangeText={value => this.setState({ bankName: value })}
          />
          <MyTextInput
            heading="Account Name"
            placeholder="Your Account Name"
            field="accountName"
            value={accountName}
            focusInput={nameFocus}
            focus="nameFocus"
            handlerFocus={this.handlerFocus}
            handlerInput={this.handlerInput}
          />
          <MyTextInput
            heading="Account Number"
            placeholder="Your Account Number"
            field="accountNum"
            value={accountNum}
            focusInput={numberFocus}
            focus="numberFocus"
            handlerFocus={this.handlerFocus}
            handlerInput={this.handlerInput}
          />
          <MyTextInput
            heading="IFSC Code"
            placeholder="Your IFSC Code"
            field="IFSCcode"
            value={IFSCcode}
            focusInput={codeFocus}
            focus="codeFocus"
            handlerFocus={this.handlerFocus}
            handlerInput={this.handlerInput}
          />
          <View style={[styles.loginBtnContainer, styles.topPad]}>
            <Button submit={() => validation(this)} title="Register" />
          </View>
          <View style={[styles.backKycBtnContainer, styles.botpad]}>
            <TouchableOpacity style={styles.backKycButton} onPress={this.backToKyc}>
              <Text style={styles.backKycText}>Back</Text>
            </TouchableOpacity>
          </View>
        </KeyboardAwareScrollView>
        <Toast
          ref="toast"
          style={{ backgroundColor: colors.toastBgColor }}
          position="bottom"
          positionValue={400}
          fadeInDuration={750}
          fadeOutDuration={1000}
          opacity={0.8}
          textStyle={{ color: colors.toastTextColor, fontSize: 15 }}
        />
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.currentUser,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    setBankDetailsDispatcher: (accname, accnum, bankname, ifsccode) => {
      dispatch(setBankDetails(accname, accnum, bankname, ifsccode));
    },
  };
};

BankDetails.propTypes = {
  navigation: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BankDetails);
