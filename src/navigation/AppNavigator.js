import React from "react";
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import {
  fadeIn,
  zoomIn,
  fromLeft,
  fromRight,
} from "react-navigation-transitions";
import { createDrawerNavigator } from 'react-navigation-drawer';
import Home from '../screens/components/HomePage';
import Ordernotification from '../screens/OrderNotification';
import Login from '../screens/Login';
import OrderHistory from '../screens/OrderHistory';
import Currentstatus from '../screens/CurrentStatus';
import EditProfile from '../screens/EditProfile';
import OrderList from '../screens/OrderList';
import Earning from '../screens/Earning';
import Readyfordelivery from '../screens/ReadyForDelivery';
import OrderComplete from '../screens/OrderComplete';
import RegisterTabNavigator from '../screens/Register';
import ViewProfileTabNav from '../screens/ViewProfile/tabnav';
import Verify from '../screens/Verify/index';
import VerifyRide from '../screens/VerifyRide/index';
import RegistrationVerification from '../screens/RegistrationVerification';
import WelcomeScreen from '../screens/WelcomeScreen';
import Scenes from '../screens/Scenes';
import AuthLoading from "../screens/WelcomeScreen/AuthLoading"
import SignUp from '../screens/SignUp';

//drawermenu screens
import DrawerMenu from '../screens/components/DrawerContent';

import ProfileSetting from '../screens/components/ProfileSetting';
import OrderHistoryMenu from '../screens/components/OrderHistory';
import RiderPayment from '../screens/components/RiderPayment';
import RiderPaymentDetails from '../screens/components/RiderPayment/Details';
import RiderPaymentSuccess from '../screens/components/RiderPayment/Success';
import RiderPaymentSlip from '../screens/components/RiderPayment/PaySlip';

import PackageDetails from '../screens/components/HomePage/PackageDetails';
//import TrackOrder from './components/TrackOrder';
import FAQ from '../screens/components/FAQ';
import CustomerCare from '../screens/components/CustomerCare';
import Wallet from '../screens/components/Wallet';

import ReviewPage from '../screens/components/ReviewPage';
import RefundPage from '../screens/components/RefundPage';
import RouteCalculator from '../screens/components/RouteCalculator';

import TermsOfService from '../screens/components/termsOfService'
import PrivacyPolicy from '../screens/components/privacyPolicy'

const handleCustomTransition = ({ scenes }) => {
  const prevScene = scenes[scenes.length - 2];
  const nextScene = scenes[scenes.length - 1];

  // Custom transitions go there
  if (
    prevScene &&
    prevScene.route.routeName === "welcomeScreen" &&
    nextScene.route.routeName === "login"
  ) {
    return zoomIn();
  }
  //  else if (
  //   prevScene &&
  //   prevScene.route.routeName === "Splash" &&
  //   nextScene.route.routeName === "HomeScreens"
  // ) {
  //   return fromRight(500);
  // }
  return fromRight();
};

const AppDrawerNavigator = createDrawerNavigator(
  {
    home: Home,
    ProfileSetting: ProfileSetting,
    OrderHistoryMenu: OrderHistoryMenu,
    RiderPayment: RiderPayment,
    PackageDetails: PackageDetails,
    FAQ: FAQ,
    CustomerCare: CustomerCare,
    Wallet: Wallet,
    Review: ReviewPage,
    RefundPage: RefundPage,
    RouteCalculator: RouteCalculator
  },
  {
    contentComponent: props => <DrawerMenu {...props} />,
    //drawerBackgroundColor: "#16161a"
    initialRouteName: 'home',
  }
)
const AppNavigator = createStackNavigator(
  {
    home: { screen: AppDrawerNavigator },
    scenes: { screen: Scenes },
    signUp: { screen: SignUp },
    registerTabNavigator: { screen: RegisterTabNavigator },
    orderNotification: { screen: Ordernotification },
    login: { screen: Login },
    orderComplete: { screen: OrderComplete },
    currentStatus: { screen: Currentstatus },
    earning: { screen: Earning },
    readyForDelivery: { screen: Readyfordelivery },
    orderHistory: { screen: OrderHistory },
    editProfile: { screen: EditProfile },
    orderList: { screen: OrderList },
    viewProfileTabNav: { screen: ViewProfileTabNav },
    verify: { screen: Verify },
    verifyRide: { screen: VerifyRide },
    RiderPaymentDetails: RiderPaymentDetails,
    RiderPaymentSuccess: RiderPaymentSuccess,
    RiderPaymentSlip: RiderPaymentSlip,
    registrationVerification: { screen: RegistrationVerification },
    welcomeScreen: { screen: WelcomeScreen },
    AuthLoading: { screen: AuthLoading },
    termsOfService: { screen: TermsOfService },
    privacyPolicy: { screen: PrivacyPolicy }
  },
  {
   // initialRouteName: 'RiderPaymentSlip',
    transitionConfig: nav => handleCustomTransition(nav),
    headerMode: 'none',
  },
);
export default createAppContainer(AppNavigator);
