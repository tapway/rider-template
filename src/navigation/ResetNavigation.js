import { StackActions, NavigationActions } from 'react-navigation';

const resetActionHome = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'scenes' })],
});

const resetActionLogin = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'login' })],
});

export { resetActionHome, resetActionLogin };
