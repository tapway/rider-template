import React, { Component } from 'react';
import { Sentry } from 'react-native-sentry';
import OneSignal from 'react-native-onesignal';
import { StyleProvider } from 'native-base';
import { store } from './src/redux/configureStore';
import setDeviceInfo from './src/redux/action/deviceInfo';
import AppNavigator from './src/navigation/AppNavigator';
import NavigationService from './src/navigation/navigationService';
import setConfig from './src/redux/action/config';
import config from './src/apiService/config';
import getTheme from './theme/native-base-theme/components';
import variables from './theme/native-base-theme/variables/variables';

Sentry.config(
	'https://15a717e49d5a4762a973aec84e687ad4@sentry.io/1460825',
).install();

export default class App extends Component {
	constructor() {
		super();
		this.state = {};
		// OneSignal.init('d126409b-2eeb-44b9-80c8-36b1a39f7f2f', {
		OneSignal.init('207cf73a-33cd-4e0d-bce3-2154d271b41c', {
			kOSSettingsKeyAutoPrompt: true,
		});
		OneSignal.addEventListener('opened', this.onOpened);
		OneSignal.addEventListener('ids', this.onIds);
		OneSignal.configure(); // triggers the ids event
	}

	componentDidMount() {
		config(this.hanleAppConfig);
	}

	componentWillUnmount() {
		OneSignal.removeEventListener('opened', this.onOpened);
		OneSignal.removeEventListener('ids', this.onIds);
	}

	hanleAppConfig = res => {
		if (res.success) {
			store.dispatch(setConfig(res.data));
		}
	};

	onOpened = openResult => {
		if (openResult.notification.payload.additionalData.newOrder) {
			NavigationService.navigate('orderNotification', {});
		}
	};

	onIds = device => {
		store.dispatch(setDeviceInfo(device.userId, device.pushToken));
	};

	render() {
		// eslint-disable-next-line no-console
		console.disableYellowBox = true;
		return (
			<StyleProvider style={getTheme(variables)}>
				<AppNavigator
					ref={navigatorRef => {
						NavigationService.setTopLevelNavigator(navigatorRef);
					}}
				/>
			</StyleProvider>
		);
	}
}
